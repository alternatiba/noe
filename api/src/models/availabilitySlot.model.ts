import {Document, Schema} from "mongoose";

/**************************************************************
 *            TYPES
 **************************************************************/

export type AvailabilitySlotD = Document & {
  start: Date;
  end: Date;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

export const AvailabilitySlotSchema = new Schema<AvailabilitySlotD>({
  start: {type: Date, required: true},
  end: {type: Date, required: true},
});
