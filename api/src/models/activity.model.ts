import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "@config/mongoose";
import {lightSelection} from "@utils/lightSelection";
import {SchemaDescription} from "@utils/routeUtilities";
import {Document, model, Schema, SchemaTypes} from "mongoose";
import {CategoryD} from "./category.model";
import {PlaceD} from "./place.model";
import {ProjectD} from "./project.model";
import {StewardD} from "./steward.model";

/**************************************************************
 *            TYPES
 **************************************************************/

export type ActivityD = Document & {
  name: string;
  category: CategoryD;
  stewards?: Array<StewardD>;
  places?: Array<PlaceD>;
  secondaryCategories?: Array<string>;
  summary?: string;
  description?: string;
  notes?: string;
  tags?: Array<string>;
  slots?: Array<{duration: number}>;
  allowSameTime?: boolean;
  maxNumberOfParticipants?: number;
  volunteeringCoefficient?: number;
  stewardVolunteeringCoefficient?: number;

  customFields?: Record<string, any>;

  project: ProjectD;
  deletedAt?: Date;
  importedId?: string;
};

export type LightActivityD = Pick<ActivityD, keyof typeof lightSelection.activity>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const ActivitySchema = new Schema<ActivityD>(
  {
    name: String,
    stewards: [{type: SchemaTypes.ObjectId, ref: "Steward"}],
    places: [{type: SchemaTypes.ObjectId, ref: "Place"}],
    category: {type: SchemaTypes.ObjectId, ref: "Category", required: true},
    secondaryCategories: [String],
    summary: String,
    description: String,
    notes: String,
    tags: [String],
    slots: [{duration: Number}],
    allowSameTime: {type: Boolean, default: false},
    maxNumberOfParticipants: Number,
    volunteeringCoefficient: Number,
    stewardVolunteeringCoefficient: {type: Number, required: false, default: 1.5},

    customFields: Object,

    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
ActivitySchema.plugin(softDelete);
ActivitySchema.plugin(addDiffHistory);
ActivitySchema.plugin(indexByIdAndProject);
ActivitySchema.plugin(preventDuplicatesInProject, [{name: 1}]);

/**
 * MODEL
 */
export const Activity = model<ActivityD>("Activity", ActivitySchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription<ActivityD> = [
  {key: "name", label: "Nom de l'activité", noGroupEditing: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "stewards", label: "Encadrant⋅es éligibles", isArray: true},
  {key: "places", label: "Espaces éligibles", usePlaces: true, isArray: true},
  {key: "slots", label: "Plages par défaut", isArray: true},
  {key: "category", label: "Catégorie"},
  {key: "secondaryCategories", label: "Catégories secondaires", isArray: true},
  {key: "summary", label: "Résumé"},
  {key: "description", label: "Description détaillée"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "tags", label: "Tags", isArray: true},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de participant⋅es"},
  {key: "volunteeringCoefficient", label: "Coefficient de bénévolat"},
  {key: "stewardVolunteeringCoefficient", label: "Coefficient de bénévolat encadrant⋅es"},
  {key: "allowSameTime", label: "Autoriser les inscriptions en même temps"},
  {key: "customFields", label: "Champs personnalisés", noGroupEditing: true},
];
export const allowedBodyArgs = schemaDescription.map((field) => field.key);
