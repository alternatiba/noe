import {indexByIdAndProject, preventDuplicatesInProject, softDelete} from "@config/mongoose";
import {Document, model, Schema} from "mongoose";
import {SessionD} from "./session.model";

/**************************************************************
 *            TYPES
 **************************************************************/

export type SlotD = Document & {
  duration: number;
  start: Date;
  end: Date;
  session: SessionD;

  deletedAt?: Date;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const SlotSchema = new Schema<SlotD>(
  {
    duration: {type: Number, required: true},

    start: {type: Date, required: true},
    end: {type: Date, required: true},

    deletedAt: Date,
  },
  {timestamps: true, toJSON: {virtuals: true}, toObject: {virtuals: true}}
);

/**
 * POPULATED VIRTUALS
 */
SlotSchema.virtual("session", {
  ref: "Session",
  localField: "_id",
  foreignField: "slots",
  justOne: true,
});

/**
 * PLUGINS
 */
SlotSchema.plugin(softDelete);
SlotSchema.plugin(indexByIdAndProject);
SlotSchema.plugin(preventDuplicatesInProject);

/**
 * MODEL
 */
export const Slot = model<SlotD>("Slot", SlotSchema);
