import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "@config/mongoose";
import {lightSelection} from "@utils/lightSelection";
import {SchemaDescription} from "@utils/routeUtilities";
import {Document, model, Schema, SchemaTypes} from "mongoose";
import {ProjectD} from "./project.model";

/**************************************************************
 *            TYPES
 **************************************************************/

export type CategoryD = Document & {
  name: string;
  color: string;
  summary?: string;

  customFields?: Record<string, any>;

  project: ProjectD;
  deletedAt?: Date;
  importedId?: string;
};

export type LightCategoryD = Pick<CategoryD, keyof typeof lightSelection.category>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const CategorySchema = new Schema<CategoryD>(
  {
    name: {type: String, required: true},
    color: String,
    summary: String,

    customFields: Object,

    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
CategorySchema.plugin(softDelete);
CategorySchema.plugin(addDiffHistory);
CategorySchema.plugin(indexByIdAndProject);
CategorySchema.plugin(preventDuplicatesInProject, [{name: 1}]);

/**
 * MODEL
 */
export const Category = model<CategoryD>("Category", CategorySchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription<CategoryD> = [
  {key: "name", label: "Nom de la catégorie", noGroupEditing: true},
  {key: "color", label: "Couleur"},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Résumé"},
  {key: "customFields", label: "Champs personnalisés", noGroupEditing: true},
];
export const allowedBodyArgs = schemaDescription.map((field) => field.key);
