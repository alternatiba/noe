import {softDelete} from "@config/mongoose";
import {lightSelection} from "@utils/lightSelection";
import bcryptjs from "bcryptjs";
import {Document, model, Schema, SchemaTypes} from "mongoose";
import * as uuid from "uuid";
import {ProjectD} from "./project.model";
import {RegistrationD} from "./registration.model";

/**
 * EXPRESS TYPINGS
 */
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface User extends UserD {}

    export interface Request {
      authenticatedUser: User;
    }
  }
}

/**************************************************************
 *            TYPES
 **************************************************************/

/**
 * DEFINITION
 */
export type UserD = Document & {
  firstName?: string;
  lastName?: string;
  email: string;
  locale: string;

  password?: string;
  passwordResetToken?: string;
  passwordResetExpires?: Date;

  xsrfToken?: string;

  superAdmin?: boolean;

  shownTours?: Array<string>;

  bookmarkedProject?: ProjectD;

  lastAccessedAt?: Date;

  // Virtuals
  registration?: RegistrationD;
  registrations?: Array<RegistrationD>;

  deletedAt?: Date;
};

export type LightUserD = Pick<UserD, keyof typeof lightSelection.user>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const UserSchema = new Schema<UserD>(
  {
    firstName: String,
    lastName: String,
    email: {type: String, required: true},

    password: {type: String, select: false}, // Don't select the password when getting a user
    passwordResetToken: String,
    passwordResetExpires: Date,

    xsrfToken: {
      type: String,
      required: true,
      default: function () {
        return uuid.v4();
      },
    },

    superAdmin: Boolean,

    locale: {type: String, enum: ["fr", "en"], default: "en", required: true},

    shownTours: {type: [String], default: []},

    bookmarkedProject: {type: SchemaTypes.ObjectId, ref: "Project"},

    lastAccessedAt: {type: Date},

    deletedAt: Date,
  },
  {
    timestamps: true,
    toObject: {virtuals: true},
    toJSON: {virtuals: true},
  }
);

/**
 * HOOKS
 */
// On save, hash the password if it has changed
UserSchema.pre(["save", "updateOne"], async function (next) {
  if (!this.isModified("password")) return next();

  try {
    const salt = await bcryptjs.genSalt(10);
    this.password = await bcryptjs.hash(this.password, salt);
    return next();
  } catch (e) {
    return next(e as NativeError);
  }
});

/**
 * POPULATED VIRTUALS
 */
// Returns the project registration when there a unique project registration present and populated
UserSchema.virtual("registration").get(function () {
  return this.registrations?.length === 1 ? this.registrations[0] : undefined;
});
UserSchema.virtual("registrations", {ref: "Registration", localField: "_id", foreignField: "user"});

/**
 * PLUGINS
 */
UserSchema.plugin(softDelete);
UserSchema.index({deletedAt: 1, email: 1}, {unique: true, collation: {locale: "en", strength: 2}});

/**
 * MODEL
 */
export const User = model<UserD>("User", UserSchema);

/**
 * SCHEMA DEFINITION
 */
export const allowedBodyArgs: Array<keyof UserD | "oldPassword"> = [
  "firstName",
  "lastName",
  "email",
  "password",
  "oldPassword",
  "locale",
  "shownTours",
  "bookmarkedProject",
];
