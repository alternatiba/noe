import {Session} from "@models/session.model";

import {User} from "@models/user.model";
import * as chai from "chai";
import * as dotenv from "dotenv";

dotenv.config();

import chaiHttp = require("chai-http");

chai.use(chaiHttp);
const expect = chai.expect;

describe("API Tests", () => {
  let adminId: string;
  let user1Id: string;
  let user2Id: string;
  let resetToken: string;

  const user1 = new User({
    firstName: "User1",
    lastName: "TESTER",
    email: "user1@toto.com",
    password: "user1user1",
    locale: "fr",
    arrivalDate: new Date("2019-07-31T00:00:00.000Z"),
    leavingDate: new Date("2019-08-11T00:00:00.000Z"),
  });

  before(async (done) => {
    await Promise.all([User.deleteMany({}), Session.deleteMany({})]);
    done();
  });

  after(async (done) => {
    await Promise.all([User.deleteMany({}), Session.deleteMany({})]);
    done();
  });

  // describe("Users", () => {
  //   describe("POST /users", () => {
  //     it("should not create a new user if email is not provided", () => {
  //       const user = {
  //         password: "testtest",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/users")
  //         .send(user)
  //         .then((res) => {
  //           expect(res).to.have.status(400);
  //         });
  //     });
  //
  //     it("should not create a new user if password is not provided", () => {
  //       const user = {
  //         email: "test@toto.com",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/users")
  //         .send(user)
  //         .then((res) => {
  //           expect(res).to.have.status(400);
  //         });
  //     });
  //
  //     it("should not create a new user if password is too short", () => {
  //       const user = {
  //         email: "test@toto.com",
  //         password: "test",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/users")
  //         .send(user)
  //         .then((res) => {
  //           expect(res).to.have.status(400);
  //         });
  //     });
  //
  //     it("should not create a new user if email is badly formatted", () => {
  //       const user = {
  //         email: "user1@toto",
  //         password: "testtest",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/users")
  //         .send(user)
  //         .then((res) => {
  //           expect(res).to.have.status(400);
  //         });
  //     });
  //
  //     it("should not create a new user if email already exists", () => {
  //       const user = {
  //         email: user1.email,
  //         password: "testtest",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/users")
  //         .send(user)
  //         .then((res) => {
  //           expect(res).to.have.status(409);
  //         });
  //     });
  //
  //     it("should create a new user", () => {
  //       const user = {
  //         email: "test@teststs.fr",
  //         password: "testtest",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/users")
  //         .send(user)
  //         .then((res) => {
  //           expect(res).to.have.status(201);
  //           expect(res.body).to.contain.keys(["_id", "email"]);
  //           user2Id = res.body._id;
  //         });
  //     });
  //   });
  //
  //   describe("GET /users", () => {
  //     it("should list all users", () => {
  //       const payload = {id: adminId};
  //       const token = jwt.sign(payload, process.env.JWT_SECRET || "", {expiresIn: "15 days"});
  //       return chai
  //         .request(app)
  //         .get("/users")
  //         .set("Authorization", "JWT " + token)
  //         .then((res) => {
  //           expect(res).to.have.status(200);
  //           expect(res.body).to.be.an("array");
  //           // expect(res.body).to.have.length(4);
  //         });
  //     });
  //   });
  // });
  //
  // describe("Auths", () => {
  //   describe("POST /auth/authenticate", () => {
  //     it("should return a valid JWT access token if credentials are ok", () => {
  //       const body = {
  //         email: "user1@toto.com",
  //         password: "user1user1",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/auth/authenticate")
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(200);
  //           expect(res.body).to.have.keys(["jwt_token"]);
  //         });
  //     });
  //
  //     it('should return valid JWT and XSRF token if "cookie" is true', () => {
  //       const body = {
  //         email: "user1@toto.com",
  //         password: "user1user1",
  //         cookie: true,
  //       };
  //       return chai
  //         .request(app)
  //         .post("/auth/authenticate")
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(200);
  //           expect(res.body).to.have.keys(["jwt_token", "xsrf_token"]);
  //           expect(res).to.have.cookie("JWT");
  //         });
  //     });
  //
  //     it("should throw an error if credentials are not ok", () => {
  //       const body = {
  //         email: "user1@toto.com",
  //         password: "Xuser1user1",
  //       };
  //
  //       return chai
  //         .request(app)
  //         .post("/auth/authenticate")
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(401);
  //         });
  //     });
  //   });
  //
  //   describe("POST /auth/password/sendResetEmail", () => {
  //     it('should return after setting "passwordResetToken" on the user', () => {
  //       const body = {
  //         user: {
  //           email: "user1@toto.com",
  //         },
  //         url: "https://machin.com",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/auth/password/sendResetEmail")
  //         .send(body)
  //         .then((res) => {
  //           return User.findById(user1Id);
  //         })
  //         .then((user) => {
  //           expect(user.passwordResetToken).to.be.a("string");
  //           resetToken = user.passwordResetToken;
  //         });
  //     });
  //
  //     it("should throw an error if email doesn't exists", () => {
  //       const body = {
  //         user: {
  //           email: "user12@toto.com",
  //         },
  //         url: "https://machin.com",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/auth/password/sendResetEmail")
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(404);
  //         });
  //     });
  //   });
  //
  //   describe("POST /auth/password/reset?token=resetToken", () => {
  //     it("should return a success message if everything ok", () => {
  //       const body = {
  //         password: "totototo",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/auth/password/reset?token=" + resetToken)
  //         .send(body)
  //         .then((res) => {
  //           // expect(res).to.have.status(200);
  //         });
  //     });
  //
  //     it("should throw an error if token doesn't exists", () => {
  //       const body = {
  //         password: "totototo",
  //       };
  //       return chai
  //         .request(app)
  //         .post("/auth/password/reset?token=" + "tokenThatDoesNotExist")
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(404);
  //         });
  //     });
  //
  //     it("should throw an error if password is not sent", () => {
  //       const body = {};
  //       return chai
  //         .request(app)
  //         .post("/auth/password/reset?token=" + resetToken)
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(404);
  //         });
  //     });
  //   });
  //
  //   describe("GET /auth/refreshAuthTokens", () => {
  //     it("should return a valid JWT if the one sent was correct", () => {
  //       const body = {};
  //       const payload = {id: user1Id};
  //       const token = jwt.sign(payload, config.jwt.secret || "", {expiresIn: "15 days"});
  //       return chai
  //         .request(app)
  //         .get("/auth/refreshAuthTokens")
  //         .set("Authorization", "JWT " + token)
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(200);
  //           // expect(res.body).to.have.keys(['jwt_token']);
  //         });
  //     });
  //
  //     it("should return a valid JWT and XSRF if the jwt sent was correct", () => {
  //       const body = {
  //         cookie: true,
  //       };
  //       const payload = {id: user1Id};
  //       const token = jwt.sign(payload, config.jwt.secret || "", {expiresIn: "15 days"});
  //       return chai
  //         .request(app)
  //         .get("/auth/refreshAuthTokens")
  //         .set("Authorization", "JWT " + token)
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(200);
  //           // expect(res.body).to.have.keys(['jwt_token', 'xsrf_token']);
  //           expect(res).to.have.cookie("JWT");
  //         });
  //     });
  //
  //     it("should not renew JWT if the one sent was incorrect", () => {
  //       const body = {};
  //       const payload = {id: user1Id};
  //       const token = jwt.sign(payload, "toto" || "", {expiresIn: "15 days"});
  //       return chai
  //         .request(app)
  //         .get("/auth/refreshAuthTokens")
  //         .set("Authorization", "JWT " + token)
  //         .send(body)
  //         .then((res) => {
  //           expect(res).to.have.status(401);
  //         });
  //     });
  //   });
  // });
});
