import {logger} from "@services/logger";
import {NextFunction, Request, Response} from "express";
import fs from "fs-extra";
import multer from "multer";

const storageDummy = multer.diskStorage({
  destination: function (req, file, cb) {
    const dir = "./.tmp/uploads/" + req.user.id;
    fs.ensureDir(dir).then(() => {
      cb(null, dir);
    });
  },
  filename: function (req, file, cb) {
    const extArray = file.originalname.split(".");
    const extension = extArray[extArray.length - 1];
    cb(null, Date.now() + "." + extension);
  },
});

const uploadDummy = multer({storage: storageDummy, limits: {fileSize: 2097152}}).single("dummy");

const dummyUpload = (req: Request, res: Response, next: NextFunction): void => {
  uploadDummy(req, res, (err) => {
    if (!err) {
      return next();
    }
    logger.error(err);
    if (err.code == "LIMIT_UNEXPECTED_FILE") {
      // An error occurred when uploading
      res.status(400).json({error: "File should be sent with the filename : 'dummy'"});
    } else {
      res.status(400).json({error: "Error with file upload : " + err.message});
    }
  });
};

export {dummyUpload};
