import {Express} from "express";
import i18next from "i18next";
import Backend from "i18next-fs-backend";
import i18nextMiddleware from "i18next-http-middleware";
import path from "path";

export const initI18n = (app: Express): void => {
  i18next
    .use(i18nextMiddleware.LanguageDetector)
    .use(Backend)
    .init({
      backend: {
        loadPath: path.resolve(__dirname, "../../../shared/locales") + "/{{lng}}/{{ns}}.yml",
      },
      fallbackLng: "en",
      ns: [
        "activities",
        "categories",
        "cockpit",
        "common",
        "emails",
        "places",
        "projects",
        "registrations",
        "sessions",
        "stewards",
        "teams",
        "users",
        "welcome",
      ],
      defaultNS: "common",
      preload: ["en", "fr"],
    });

  // Add the middleware to the Express app
  app.use(i18nextMiddleware.handle(i18next));
};
