import {logger} from "@services/logger";
import Joi from "joi";
import path from "path";

const getConfig = () => {
  // VALIDATE ENV VARIABLES
  const {value: envVars, error} = Joi.object()
    .keys({
      NODE_ENV: Joi.string().valid("production", "development", "debug", "test").required(),
      PORT: Joi.number().default(4000),
      WS_PORT: Joi.number().default(4001),
      OPENSSL_CONF: Joi.string().valid("/dev/null").description("OpensslConf, must be /dev/null"),

      REACT_APP_SENTRY_ENVIRONMENT: Joi.string().valid("SANDBOX", "PRODUCTION"),
      REACT_APP_INSTANCE_NAME: Joi.string()
        .default("NOÉ")
        .description("Give a custom name to your NOÉ instance"),

      REACT_APP_SENTRY_DSN: Joi.string().description(
        "the Sentry DSN url so errors are ent to Sentry properly"
      ),

      JWT_SECRET: Joi.string().required().description("JWT secret key"),
      JWT_TOKEN_EXPIRATION_DAYS: Joi.number()
        .default(13 * 30)
        .description(
          "days after which refresh tokens expire. defaults to 1 year + 1 month (for annual events)."
        ),
      ENCRYPTION_KEY: Joi.string()
        .default("v8y/B?E(H+MbQeThVmYq3t6w9z$C&F)J")
        .length(32)
        .description(
          "If you're storing sensitive data like API keys, provide a good encryption key !"
        ),

      MONGODB_URI: Joi.string().required().description("Mongo DB url"),

      SMTP_HOST: Joi.string().description("server that will send the emails"),
      SMTP_PORT: Joi.number().description("port for email server"),
      SMTP_ADDRESS: Joi.string().description("email address for email server"),
      SMTP_USER: Joi.string().description("username for email server"),
      SMTP_PASSWORD: Joi.string().description("password for email server"),

      REACT_APP_ORGA_FRONT_URL: Joi.string().description("URL of the orga-front frontend"),
      REACT_APP_INSCRIPTION_FRONT_URL: Joi.string().description(
        "URL of the inscription-front frontend"
      ),
      REACT_APP_API_URL: Joi.string().description("URL of the current api server"),
      REACT_APP_PENDING_DONATION_TIME_IN_DAYS: Joi.number().description(
        "Delay after which a project is reset to demo if no donation was received"
      ),
    })
    .unknown()
    .prefs({errors: {label: "key"}})
    .validate(process.env);

  if (error) throw new Error(`Config validation error: ${error.message}`);

  return {
    env: envVars.NODE_ENV,
    port: envVars.PORT,
    wsPort: envVars.WS_PORT,
    appMode: envVars.REACT_APP_SENTRY_ENVIRONMENT,
    instanceName: envVars.REACT_APP_INSTANCE_NAME,
    sentryDsn: envVars.REACT_APP_SENTRY_DSN,
    jwt: {
      secret: envVars.JWT_SECRET,
      expiresIn: envVars.JWT_TOKEN_EXPIRATION_DAYS,
    },
    encryptionKey: envVars.ENCRYPTION_KEY,
    mongoose: {
      uri: envVars.MONGODB_URI + (envVars.NODE_ENV === "test" ? "-test" : ""),
    },
    urls: {
      website: "https://get.noe-app.io",
      donate:
        "https://noe-app.notion.site/Indications-pour-les-dons-libres-et-conscients-au-projet-NO-0ad0938dc1e946f594148922ed3f85d0?pvs=4",
      orgaFront: envVars.REACT_APP_ORGA_FRONT_URL,
      inscriptionFront: envVars.REACT_APP_INSCRIPTION_FRONT_URL,
      api: envVars.REACT_APP_API_URL,
    },
    urlsNames: {
      [envVars.REACT_APP_ORGA_FRONT_URL]: "orga-front",
      [envVars.REACT_APP_INSCRIPTION_FRONT_URL]: "inscription-front",
      [envVars.REACT_APP_API_URL]: "api",
    },
    smtp: {
      host: envVars.SMTP_HOST,
      port: envVars.SMTP_PORT,
      address: envVars.SMTP_ADDRESS || envVars.SMTP_USER, // If no address is provided, use the user as the address
      user: envVars.SMTP_USER || envVars.SMTP_ADDRESS, // If no user is provided, use the address as the user
      pass: envVars.SMTP_PASSWORD,
    },
    pendingDonationTimeInDays: envVars.REACT_APP_PENDING_DONATION_TIME_IN_DAYS,
  };
};

let config: ReturnType<typeof getConfig>;
if (process.env.NODE_ENV === "debug") {
  /**
   * DEBUG CONFIG
   */
  logger.info("==== DEBUG MODE ====");
  logger.info(
    "DEBUG: Fetching environment variables from `docker/dev-env/.env` and `docker/dev-env/.env.api` instead of Docker Compose environment."
  );
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const dotenv = require("dotenv");
  dotenv.config({path: path.join(__dirname, "../../../docker/dev-env/.env")});
  dotenv.config({path: path.join(__dirname, "../../../docker/dev-env/.env.api")});

  config = getConfig();

  // - Change the database path from @db (container name) to @localhost (which is the way to access the container outside Docker)
  config.mongoose.uri = config.mongoose.uri.replace("@db", "@localhost");
} else {
  /**
   * CLASSIC CONFIG
   */
  config = getConfig();
}

export default config;
