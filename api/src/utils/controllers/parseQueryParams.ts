export type GenericQueryParams = {updatedAfter?: string; showDeletedElements?: string};

export const parseQueryParams = (queryParams: GenericQueryParams) => {
  const conditions = {};

  // Updated after
  const updatedAfterUnix = parseInt(queryParams?.updatedAfter);
  if (updatedAfterUnix) Object.assign(conditions, {updatedAt: {$gte: new Date(updatedAfterUnix)}});

  // Show deleted stuff
  const showDeletedElements = queryParams.showDeletedElements === "true";
  if (showDeletedElements) Object.assign(conditions, {searchAlsoInDeletedElements: true});

  return conditions;
};
