import {UserD} from "@models/user.model";
import {Response} from "express";
import {Model} from "mongoose";
import {broadcastUpdate} from "../broadcastUpdate";

export const deleteEntity = async <EntityD, RetValue>(
  id: string,
  Entity: Model<EntityD>,
  updatedByUser: UserD,
  res: Response,
  additionalActions?: (entity?: EntityD) => Promise<any>,
  returnValue?: RetValue,
  hardDelete = false // If true, don't put deletedAt, instead erase the record *for real* in the database
): Promise<Response<RetValue>> => {
  const entity = hardDelete
    ? await Entity.findById(id)
    : await Entity.findByIdAndUpdate(
        id,
        {deletedAt: new Date()},
        {new: true, __user: updatedByUser._id}
      );
  if (!entity) return res.sendStatus(404); // Not found
  hardDelete && (await entity.remove());
  additionalActions && (await additionalActions(entity));
  broadcastUpdate(res, "delete", id);
  return res.status(200).send(returnValue);
};
