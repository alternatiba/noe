import {Response} from "express";
import {Model} from "mongoose";
import {broadcastUpdate} from "../broadcastUpdate";

export const createEntity = async <EntityD>(
  Entity: Model<EntityD>,
  args: Partial<Omit<EntityD, "_id">>,
  res: Response,
  additionalActions?: (entity: EntityD) => Promise<any>
): Promise<Response> => {
  let entity = (await Entity.create(args)) as EntityD;
  if (additionalActions) entity = await additionalActions(entity);
  broadcastUpdate(res, "create", entity);
  return res.status(201).json(entity);
};
