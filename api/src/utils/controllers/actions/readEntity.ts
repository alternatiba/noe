import {isValidObjectId, isValidSlug} from "@config/mongoose";
import {Response} from "express";
import {Query} from "mongoose";

export const readEntity = async <EntityD, EntityDOut = EntityD>(
  id: string,
  projectId: string,
  res: Response,
  queryPromise: (id: string, projectId?: string) => Query<EntityD, EntityD> | Promise<EntityD>,
  additionalActions?: (entity: EntityD) => Promise<EntityDOut>
): Promise<Response | void> => {
  let entity;

  // If there are both a projectId and an id: check that projectId is a slug, at least + check if the id is an objectid
  // If there is only an id, check if it is a slug only (cause it can be an id for a project.
  if (projectId ? isValidObjectId(id) && isValidSlug(projectId) : isValidSlug(id)) {
    entity = await queryPromise(id, projectId);
    if (!entity) return res.sendStatus(404);

    if (additionalActions) {
      const finalReturnedEntity = await additionalActions(entity);
      return res.status(200).json(finalReturnedEntity);
    } else {
      return res.status(200).json(entity);
    }
  } else {
    return res.sendStatus(404);
  }
};
