import {Response} from "express";
import {QueryWithHelpers, SortOrder} from "mongoose";

export const listAllEntities = async <EntityD>(
  query: QueryWithHelpers<EntityD[], EntityD>,
  sortOptions: {[p: string]: SortOrder},
  res: Response
): Promise<Response> => {
  const elements = await query.collation({locale: "fr", strength: 1}).sort(sortOptions).lean();
  return res.json(elements);
};
