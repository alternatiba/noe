import {NextFunction, Request, RequestHandler, Response} from "express";
import {
  body,
  ValidationChain,
  ValidationChainBuilder,
  validationResult,
} from "express-validator/check";

function cancelRequestIfValidationErrors(req: Request, res: Response, next: NextFunction) {
  const errors = validationResult(req);
  if (errors.isEmpty()) next();
  else return res.status(400).json({errors: errors.array()});
}

// Validate the body with some express validator rules
export function validateAndSanitizeRequest(...args: any[]): RequestHandler[] {
  return [
    ...args,
    // Cancel the request if some errors are found
    cancelRequestIfValidationErrors,
  ];
}

//
// HELPERS
//

type ValidationChainHelper = (
  fields: string | string[],
  location?: ValidationChainBuilder
) => ValidationChain;

export const isString: ValidationChainHelper = (fields, location = body) =>
  location(fields)
    .withMessage("Should be a string.")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Should not be an empty string.");

export const isMongoId: ValidationChainHelper = (fields, location = body) =>
  location(fields).isMongoId().withMessage("Should be a Mongo ID.");

export const isHexColor: ValidationChainHelper = (fields, location = body) =>
  location(fields).isHexColor().withMessage("Should be a valid hexadecimal color").trim();

const emailNormalizationParams = {
  gmail_remove_dots: false,
  gmail_remove_subaddress: false,
  outlookdotcom_remove_subaddress: false,
  yahoo_remove_subaddress: false,
  icloud_remove_subaddress: false,
};

export const isEmail: ValidationChainHelper = (fields, location = body) =>
  location(fields)
    .isEmail()
    .withMessage("Should be an email.")
    .trim()
    .normalizeEmail(emailNormalizationParams);
