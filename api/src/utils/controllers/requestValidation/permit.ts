import {withAuthentication} from "@config/passport";
import {UserD} from "@models/user.model";
import {Request, RequestHandler} from "express";

/********************
 * PERMISSIONS
 *******************/

export type UserPermission = {
  challenge: (user: UserD, request?: Request) => boolean;
  errorReason: string;
};

export const ROLES = {
  ADMIN: "admin",
  CONTRIBUTOR: "contrib",
  GUEST: "guest",
};

export const ROLES_LABELS = {
  admin: "Administrateur⋅ice",
  contrib: "Contributeur⋅ice",
  guest: "Invité⋅e",
};

/**
 * Permit function wrapper
 * @param permission the permission object
 * @param excludeScope an optional URL string or regexp that excludes a scope from the check
 */
export const permit = (
  permission: UserPermission,
  excludeScope?: string | RegExp
): Array<RequestHandler> => {
  return [
    // Always authenticate user before
    withAuthentication as unknown as RequestHandler,

    // Then check if they have the correct rights
    async (req, res, next) => {
      // If the user is a super admin, it has all rights. Let it pass.
      if (req.authenticatedUser.superAdmin) return next();
      // If no excludeScope URL is given, don't check. Else, if the origin is different from the scope, let it pass.
      if (excludeScope && req.headers.origin?.match(excludeScope)) return next();

      // Send 404 if no project registration given at all
      if (!req.authenticatedUser.registration) return res.sendStatus(404);

      // If the challenge fails, the user lacks sufficient rights: send a 403 error with the error reason
      if (!permission.challenge(req.authenticatedUser, req)) {
        return res.status(403).send(permission.errorReason);
      }

      // If everything passes, we're all good
      next();
    },
  ];
};

/**
 * User permissions
 */

// Allow only super admins to do this
export const nobodyExceptSuperAdmins: UserPermission = {
  challenge: () => false,
  errorReason: "Cette action n'est pas disponible.",
};

// Allow only admins on the current project
export const projectAdmins: UserPermission = {
  challenge: (user) => user.registration?.role === ROLES.ADMIN,
  errorReason: "Seul⋅e un⋅e administrateur⋅ice peut effectuer cette action.",
};

// Allow only people having at least a contribution role on the current project
export const projectContributors: UserPermission = {
  challenge: (user) => !![ROLES.ADMIN, ROLES.CONTRIBUTOR].includes(user.registration?.role),
  errorReason:
    "Seul⋅e un⋅e administrateur⋅ice ou un⋅e contributeur⋅ice peut effectuer cette action",
};

// Allow only people having at least a guest role on the current project
export const projectGuests: UserPermission = {
  challenge: (user) => !!Object.values(ROLES).includes(user.registration?.role),
  errorReason:
    "Seul⋅e un⋅e administrateur⋅ice, un⋅e contributeur⋅ice ou un⋅e invité⋅e peut effectuer cette action",
};
