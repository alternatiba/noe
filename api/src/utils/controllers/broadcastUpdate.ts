import {Project, WebhookAction, WebhookEndpoint} from "@models/project.model";
import * as Sentry from "@sentry/node";
import {logger} from "@services/logger";
import {broadcastThroughWebsocket} from "@services/websocketServer";
import {Response} from "express";
import config from "../../config/config";

const broadcastThroughWebhook = async <EntityD>(
  projectId: string,
  endpoint: WebhookEndpoint,
  action: WebhookAction,
  payload: EntityD
) => {
  const projectWebhooks =
    (await Project.findByIdOrSlug(projectId, "webhooks").lean())?.webhooks || [];

  for (const [index, webhook] of projectWebhooks.entries()) {
    if (webhook.actions.includes(action) && webhook.endpoints.includes(endpoint)) {
      try {
        await fetch(webhook.url, {
          method: "POST",
          headers: {
            "content-type": "application/json",
            "webhook-api-key": webhook.apiKey,
          },
          body: JSON.stringify({
            action,
            endpoint,
            payload,
          }),
        });
      } catch (e) {
        Sentry.captureException(e);
        logger.error(e);
        const project = await Project.findByIdOrSlug(projectId, "webhooks");
        project.webhooks[index].failedTimes = (project.webhooks[index].failedTimes || 0) + 1;
        project.save({timestamps: false});
      }
    }
  }
};

export const broadcastUpdate = async (
  res: Response,
  action: WebhookAction,
  payload: any
): Promise<void> => {
  try {
    const projectId = res.req.params?.projectId;
    if (projectId) {
      const endpoint = new RegExp(`/${projectId}/([^/]*)`).exec(
        res.req.originalUrl
      )?.[1] as WebhookEndpoint;

      broadcastThroughWebsocket(
        "concurrentEditing",
        projectId,
        JSON.stringify({
          action: "update",
          endpoint,
          wsConn: res.req.headers["x-websocket-connection"],
        })
      );

      // For now, only trigger webhooks if the action is user-made.
      // If the request doesn't come from the "official" frontends, don't send any webhook.
      if ([config.urls.inscriptionFront, config.urls.orgaFront].includes(res.req.headers.origin)) {
        await broadcastThroughWebhook(projectId, endpoint, action, payload);
      }
    }
  } catch (e) {
    // This function is executed asynchronously so it can crash the API and Sentry can't catch it. We have to protect it.
    Sentry.captureException(e);
  }
};
