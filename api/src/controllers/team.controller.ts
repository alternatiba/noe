import {withAuthentication} from "@config/passport";
import {Registration, SessionSubscriptionD} from "@models/registration.model";
import {Session} from "@models/session.model";
import {allowedBodyArgs, LightTeamD, schemaDescription, Team, TeamD} from "@models/team.model";
import {createEntity} from "@utils/controllers/actions/createEntity";
import {deleteEntity} from "@utils/controllers/actions/deleteEntity";
import {listAllEntities} from "@utils/controllers/actions/listAllEntities";
import {readEntity} from "@utils/controllers/actions/readEntity";
import {updateEntity} from "@utils/controllers/actions/updateEntity";
import {GenericQueryParams, parseQueryParams} from "@utils/controllers/parseQueryParams";
import {filterRequestBody} from "@utils/controllers/requestValidation/filterRequestBody";
import {
  permit,
  projectAdmins,
  projectContributors,
} from "@utils/controllers/requestValidation/permit";
import {removeNestedObjectsContent} from "@utils/controllers/requestValidation/removeNestedObjectsContent";
import {
  isMongoId,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {updateManyToManyRelationship} from "@utils/controllers/updateManyToManyRelationship";
import {updateOneToManyRelationship} from "@utils/controllers/updateOneToManyRelationship";
import {lightSelection} from "@utils/lightSelection";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
} from "@utils/routeUtilities";
import {Router} from "express";

const updateSessionsSubscriptionsInRegistrations = async (
  teamId: any,
  registrationsToUpdateIds: string[] = [],
  newTeamRegistrations: Array<string> = [],
  newTeamSessions: Array<any>,
  userId: any
) => {
  // Three cases for sessionsSubscription updates:
  // 1. The user is being added to in the team and should have all the team's sessions subscribed
  // 2. The user is already in the team and the sessions have changed, so we have to add or remove sessions
  // 3. The user is not anymore in the team and should have all subscriptions deleted
  // As a shortcut, we will gather case 2 and 3, and for them, delete all the sessions not
  // containing the team and put back all the subscriptions.

  // Gather registrations to update + all registrations belonging to the team
  const registrationsToUpdate = await Registration.find({
    _id: [...registrationsToUpdateIds, ...newTeamRegistrations],
  });

  for (const registrationToUpdate of registrationsToUpdate) {
    const existingSubscriptionsWithoutCurrentTeam =
      registrationToUpdate.sessionsSubscriptions?.filter(
        (ss) => ss.team?.toString() !== teamId.toString()
      ) || [];

    const registrationBelongsToTeam = newTeamRegistrations.find(
      (registrationId: string) => registrationToUpdate._id.toString() === registrationId
    );
    if (registrationBelongsToTeam) {
      // case 1. + 2.
      const currentTeamSessionsSubscriptions =
        newTeamSessions?.map(
          (session) =>
            ({
              session,
              team: teamId,
              subscribedBy: userId,
            } as SessionSubscriptionD)
        ) || [];
      registrationToUpdate.sessionsSubscriptions = [
        ...existingSubscriptionsWithoutCurrentTeam,
        ...currentTeamSessionsSubscriptions,
      ];
    } else {
      // case 3.
      registrationToUpdate.sessionsSubscriptions = existingSubscriptionsWithoutCurrentTeam;
    }
    await registrationToUpdate.save();
  }
};

export const TeamRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
TeamRouter.get(...MODIFICATIONS_HISTORY("Team"));
TeamRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

/**
 * List all the teams in the project
 * GET /
 */
const LIST_TEAMS = endpoint<ProjectId, undefined, Array<LightTeamD>, GenericQueryParams>(
  "/",
  [withAuthentication],
  async function (req, res) {
    const projectId = req.params.projectId;
    return listAllEntities(
      Team.find({project: projectId, ...parseQueryParams(req.query)}, lightSelection.team).populate(
        {
          path: "activity",
          select: "_id name",
          populate: {path: "category", select: lightSelection.category},
        }
      ),
      {name: "asc"},
      res
    );
  }
);
TeamRouter.get(...LIST_TEAMS);

/**
 * Get a team
 * GET /:id
 */
const GET_TEAM = endpoint<ProjectIdAndId, undefined, TeamD>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Team.findOne({_id: id, project: projectId})
        .populate([{path: "activity", populate: [{path: "category"}]}])
        .lean()
    );
  }
);
TeamRouter.get(...GET_TEAM);

/**
 * Create a team
 * POST /
 */
const CREATE_TEAM = endpoint<
  ProjectId,
  New<TeamD> & {sessions?: Array<string>; registrations?: Array<string>},
  TeamD
>(
  "/",
  [
    permit(projectContributors),
    filterRequestBody(allowedBodyArgs),
    removeNestedObjectsContent("activity", "project", "sessions", "registrations"),
    validateAndSanitizeRequest(isMongoId("project")),
  ],
  async function (req, res) {
    // Save sessions/team links independently
    const {sessions: newTeamSessions, registrations: newTeamRegistrations, ...teamBody} = req.body;

    return await createEntity(Team, teamBody, res, async (team) => {
      // Update sessions that belong to the team
      const sessionsToUpdateIds = await updateOneToManyRelationship(
        team,
        newTeamSessions,
        Session,
        "team"
      );
      // Update participants that are part of the team
      const registrationsToUpdateIds = await updateManyToManyRelationship(
        team,
        newTeamRegistrations,
        Registration,
        "team",
        "teamsSubscriptions",
        {subscribedBy: req.authenticatedUser, updatedAt: Date.now()}
      );
      await updateSessionsSubscriptionsInRegistrations(
        team._id,
        registrationsToUpdateIds,
        newTeamRegistrations,
        newTeamSessions,
        req.user._id
      );
      await team.populate([
        {path: "activity", options: {lean: true}, populate: [{path: "category"}]},
      ]);

      return {...team.toObject(), sessionsToUpdateIds, registrationsToUpdateIds};
    });
  }
);
TeamRouter.post(...CREATE_TEAM);

/**
 * Modify a team
 * PATCH /:id
 */
const MODIFY_TEAM = endpoint<
  ProjectIdAndId,
  Modified<TeamD> & {sessions?: Array<string>; registrations?: Array<string>},
  TeamD
>(
  "/:id",
  [
    permit(projectContributors),
    filterRequestBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("activity", "project", "sessions", "registrations"),
  ],
  async function (req, res) {
    // Save sessions/team links independently
    const {sessions: newTeamSessions, registrations: newTeamRegistrations, ...teamBody} = req.body;

    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Team,
      teamBody,
      req.authenticatedUser,
      res,
      [{path: "activity", populate: [{path: "category"}]}],
      async (team) => {
        // Update sessions that belong to the team
        let sessionsToUpdateIds = await updateOneToManyRelationship(
          team,
          newTeamSessions,
          Session,
          "team"
        );

        // Update participants that are part of the team
        const registrationsToUpdateIds = await updateManyToManyRelationship(
          team,
          newTeamRegistrations,
          Registration,
          "team",
          "teamsSubscriptions",
          {subscribedBy: req.authenticatedUser, updatedAt: Date.now()}
        );

        // If some registrations have changed, we need to update the filling counters of the existing team sessions
        if (registrationsToUpdateIds?.length > 0)
          sessionsToUpdateIds = [...sessionsToUpdateIds, ...newTeamSessions];

        await updateSessionsSubscriptionsInRegistrations(
          team._id,
          registrationsToUpdateIds,
          newTeamRegistrations,
          newTeamSessions,
          req.user._id
        );

        return {...team.toObject(), sessionsToUpdateIds, registrationsToUpdateIds};
      }
    );
  }
);
TeamRouter.patch(...MODIFY_TEAM);

/**
 * Delete a team
 * DELETE /:id
 */
const DELETE_TEAM = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const teamId = req.params.id;

    const sessionsToUpdateIds = await updateOneToManyRelationship(teamId, [], Session, "team");
    const registrationsToUpdateIds = await updateManyToManyRelationship(
      teamId,
      [],
      Registration,
      "team",
      "teamsSubscriptions"
    );
    await updateSessionsSubscriptionsInRegistrations(
      teamId,
      registrationsToUpdateIds,
      [],
      [],
      req.user._id
    );

    return await deleteEntity(teamId, Team, req.authenticatedUser, res, undefined, {
      sessionsToUpdateIds,
      registrationsToUpdateIds,
    });
  }
);
TeamRouter.delete(...DELETE_TEAM);
