import {withAuthentication} from "@config/passport";

import {Activity} from "@models/activity.model";
import {
  allowedBodyArgs,
  Category,
  CategoryD,
  LightCategoryD,
  schemaDescription,
} from "@models/category.model";
import {createEntity} from "@utils/controllers/actions/createEntity";
import {deleteEntity} from "@utils/controllers/actions/deleteEntity";
import {listAllEntities} from "@utils/controllers/actions/listAllEntities";
import {readEntity} from "@utils/controllers/actions/readEntity";
import {updateEntity} from "@utils/controllers/actions/updateEntity";
import {getDependenciesList} from "@utils/controllers/getDependenciesList";
import {GenericQueryParams, parseQueryParams} from "@utils/controllers/parseQueryParams";
import {filterRequestBody} from "@utils/controllers/requestValidation/filterRequestBody";
import {
  permit,
  projectAdmins,
  projectContributors,
} from "@utils/controllers/requestValidation/permit";
import {removeNestedObjectsContent} from "@utils/controllers/requestValidation/removeNestedObjectsContent";
import {
  isHexColor,
  isMongoId,
  isString,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {lightSelection} from "@utils/lightSelection";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
} from "@utils/routeUtilities";
import {Router} from "express";

export const CategoryRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
CategoryRouter.get(...MODIFICATIONS_HISTORY("Category"));
CategoryRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

/**
 * List all the categories in the project
 * GET /
 */
const LIST_CATEGORIES = endpoint<ProjectId, undefined, Array<LightCategoryD>, GenericQueryParams>(
  "/",
  [withAuthentication],
  async function (req, res) {
    return listAllEntities(
      Category.find(
        {project: req.params.projectId, ...parseQueryParams(req.query)},
        lightSelection.category
      ),
      {name: "asc"},
      res
    );
  }
);
CategoryRouter.get(...LIST_CATEGORIES);

/**
 * Get a category
 * GET /:id
 */
const GET_CATEGORY = endpoint<ProjectIdAndId, undefined, CategoryD>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Category.findOne({_id: id, project: projectId}).lean()
    );
  }
);
CategoryRouter.get(...GET_CATEGORY);

/**
 * Create a category
 * POST /
 */
const CREATE_CATEGORY = endpoint<ProjectId, New<CategoryD>, CategoryD>(
  "/",
  [
    permit(projectContributors),
    filterRequestBody(allowedBodyArgs),
    removeNestedObjectsContent("project"),
    validateAndSanitizeRequest(isString("name"), isHexColor("color"), isMongoId("project")),
  ],
  async function (req, res) {
    return await createEntity(Category, req.body, res);
  }
);
CategoryRouter.post(...CREATE_CATEGORY);

/**
 * Modify a category
 * PATCH /:id
 */
const MODIFY_CATEGORY = endpoint<ProjectIdAndId, Modified<CategoryD>, CategoryD>(
  "/:id",
  [
    permit(projectContributors),
    filterRequestBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("project"),
  ],
  async function (req, res) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Category,
      req.body,
      req.authenticatedUser,
      res
    );
  }
);
CategoryRouter.patch(...MODIFY_CATEGORY);

/**
 * Delete a category
 * DELETE /:id
 */
const DELETE_CATEGORY = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const categoryId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {category: categoryId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(req.t("categories:errors.activitiesDependencies", {dependentActivitiesList}));
    }

    return await deleteEntity(categoryId, Category, req.authenticatedUser, res);
  }
);
CategoryRouter.delete(...DELETE_CATEGORY);
