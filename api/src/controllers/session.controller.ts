import {Lean} from "@config/mongoose";
import {withAuthentication} from "@config/passport";
import {
  inconsistenciesForEntities,
  InconsistencyForEntity,
  isStewardForSession,
  readSessionAdditionalActions,
  readSessionQuery,
} from "@helpers/session.helper";
import {getSlotsStartAndEnd} from "@helpers/slot.helper";
import {Project} from "@models/project.model";
import {Registration, RegistrationD, registrationMethods} from "@models/registration.model";
import {
  allowedBodyArgs,
  schemaDescription,
  Session,
  SessionD,
  sessionMethods,
} from "@models/session.model";
import {Slot, SlotD} from "@models/slot.model";
import {computeFunctionForAllElements} from "@utils/computeFunctionForAllElements";
import {deleteEntity} from "@utils/controllers/actions/deleteEntity";
import {readEntity} from "@utils/controllers/actions/readEntity";
import {broadcastUpdate} from "@utils/controllers/broadcastUpdate";
import {GenericQueryParams, parseQueryParams} from "@utils/controllers/parseQueryParams";
import {filterRequestBody} from "@utils/controllers/requestValidation/filterRequestBody";
import {
  permit,
  projectAdmins,
  projectContributors,
} from "@utils/controllers/requestValidation/permit";
import {removeNestedObjectsContent} from "@utils/controllers/requestValidation/removeNestedObjectsContent";
import {
  isMongoId,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {updateManyToManyRelationship} from "@utils/controllers/updateManyToManyRelationship";
import {lightSelection} from "@utils/lightSelection";
import {
  endpoint,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
  TypedRequestHandler,
} from "@utils/routeUtilities";
import {Router} from "express";
import {body} from "express-validator/check";
import config from "../config/config";
import moment = require("moment");

export const SessionRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
SessionRouter.get(...MODIFICATIONS_HISTORY("Session"));
SessionRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

const EXPORT_SESSIONS = endpoint<ProjectId, undefined, Array<SessionD>>(
  "/export",
  [withAuthentication],
  async (req, res) => {
    const projectId = req.params.projectId;

    // Get the sessions from the database
    const sessions: SessionD[] = await Session.find(
      {project: projectId},
      {isScheduleFrozen: 0, __v: 0, id: 0, project: 0} // No need for those things in the sessions list
    )
      .sort({start: 1})
      .populate([
        {path: "stewards", select: "firstName lastName", options: {lean: true}},
        {path: "places", select: "name maxNumberOfParticipants", options: {lean: true}},
        {
          path: "slots",
          options: {lean: true},
          populate: [
            {path: "stewards", select: "firstName lastName"},
            {path: "places", select: "name maxNumberOfParticipants"},
          ],
        },
        {
          path: "activity",
          options: {lean: true},
          populate: [{path: "category", select: lightSelection.category}],
          select: {...lightSelection.activity, notes: 1, tags: 1}, // TODO make a real dedicated endpoint for sessions exports
        },
        {path: "team", select: lightSelection.team, options: {lean: true}},
      ]);

    // Compute all the expensive values needed asynchronously, so we gain (a lot of) time
    await computeFunctionForAllElements(sessions, (session) =>
      sessionMethods.populateParticipants(session, false)
    );

    const sessionsList = sessions.map((session) => {
      const sessionData = session.toObject<SessionD>();
      sessionData.numberParticipants = session.numberParticipants;
      sessionData.participants = session.participants;
      return sessionData;
    });

    return res.status(200).json(sessionsList);
  }
);
SessionRouter.get(...EXPORT_SESSIONS);

/**
 * List all the sessions in the project
 * GET /
 * POST /selectiveLoad
 *
 *   Query params:
 *     - type: "subscribed" | "all"
 *     - fromDate: number (date in Unix format)
 *   Body (for POST method only):
 *     - alreadyLoaded: list of sessions ids separated with a comma
 *
 *   Returns:
 *     - list: list of sessions
 *     - allLoaded: boolean - tells if some sessions are still not loaded for this user
 */
type PostQueryParams = {loadParticipantsData: string};
type GetQueryParams = {
  type: "subscribed" | undefined;
  fromDate: string;
  alreadyLoaded: string;
} & PostQueryParams;
type PostBody = {type: "subscribed" | undefined; fromDate: number; alreadyLoaded: string[]};
type ResponseBody = {list: Array<SessionD>; allLoaded: boolean};

const LIST_SESSIONS: TypedRequestHandler<
  ProjectId,
  PostBody,
  ResponseBody,
  (GetQueryParams | PostQueryParams) & GenericQueryParams
> = async (req, res) => {
  const projectId = req.params.projectId;
  const registration = req.user.registration;

  const queryParams = req.query as GetQueryParams;

  const loadParticipantsData = queryParams.loadParticipantsData === "true";
  const isRequestedFromInscriptionFrontend = req.headers.origin === config.urls.inscriptionFront;

  // Filtering options
  /** **** Support for deprecated requests using the body params **** **/
  const type = queryParams?.type?.toString() || req.body?.type;
  const fromDateUnix: number = parseInt(queryParams?.fromDate?.toString()) || req.body?.fromDate;
  const alreadyLoaded: Array<string> =
    queryParams?.alreadyLoaded?.toString().split(",") || req.body?.alreadyLoaded;

  // Build the base filter
  let filterConditions: any = {project: projectId, _id: {$not: {$in: alreadyLoaded}}};

  // Only fetch from today's date, we'll see later if we need to load the rest
  if (fromDateUnix) {
    filterConditions = {$and: [filterConditions, {end: {$gte: new Date(fromDateUnix)}}]};
  }

  // Only fetch the type of stuff we were asked
  if (type === "subscribed") {
    filterConditions = {
      $and: [
        filterConditions,
        {
          $or: [
            {_id: registration?.sessionsSubscriptions.map((ss) => ss.session)},
            {stewards: registration?.steward},
            {everybodyIsSubscribed: true},
          ],
        },
      ],
    };
  }

  // Get the sessions from the database
  const sessions: SessionD[] = await Session.find(
    {...filterConditions, ...parseQueryParams(req.query)},
    {isScheduleFrozen: 0, __v: 0, createdAt: 0, updatedAt: 0, id: 0, project: 0} // No need for those things in the sessions list
  )
    .sort({start: 1})
    .populate([
      {path: "stewards", select: "firstName lastName customFields", options: {lean: true}},
      {path: "places", select: "name maxNumberOfParticipants customFields", options: {lean: true}},
      {path: "slots", options: {lean: true}},
      {
        path: "activity",
        options: {lean: true},
        populate: [{path: "category", select: lightSelection.category}],
        select: lightSelection.activity,
      },
      {path: "team", select: lightSelection.team, options: {lean: true}},
    ]);

  // Compute all the expensive values needed asynchronously, so we gain (a lot of) time
  await computeFunctionForAllElements(sessions, (session) =>
    loadParticipantsData // If we ask to
      ? sessionMethods.populateParticipants(
          session,
          isStewardForSession(session, registration) || !!registration?.role
        )
      : sessionMethods.populateParticipants(session, false)
  );

  // Only calculate same time sessions for inscription front, and not for participant front
  if (isRequestedFromInscriptionFrontend) {
    const allSessionsSlotsInProject = await sessionMethods.getAllSessionsSlotsInProject(
      projectId.toString()
    );
    await computeFunctionForAllElements(sessions, (session) =>
      sessionMethods.computeSameTimeSessions(session, registration, allSessionsSlotsInProject)
    );
  }

  const sessionsList = sessions.map((session) => {
    const sessionData = session.toObject<SessionD>();
    if (isRequestedFromInscriptionFrontend) {
      Object.assign(sessionData, {
        subscribed:
          type === "subscribed" && !sessionData.everybodyIsSubscribed
            ? true
            : !!registration?.sessionsSubscriptions
                .map((ss) => ss.session.toString())
                .includes(session._id.toString()),
        isSteward: isStewardForSession(session, registration),
        sameTimeSessions: session.sameTimeSessions,
      });
    }
    sessionData.numberParticipants = session.numberParticipants;
    sessionData.participants = session.participants;
    return sessionData;
  });

  const allLoaded =
    alreadyLoaded !== undefined &&
    (await Session.countDocuments({project: projectId})) ===
      (alreadyLoaded?.length || 0) + sessionsList.length;

  return res.status(200).json({list: sessionsList, allLoaded});
};
// There are two endpoints for this one, the first GET "/" is still used by the orga-front (legacy),
// and the new one POST "/selectiveLoad" is used y the inscription-front
SessionRouter.get(
  ...endpoint<ProjectId, undefined, ResponseBody, GetQueryParams>(
    "/",
    [withAuthentication],
    LIST_SESSIONS
  )
);
SessionRouter.post(
  ...endpoint<ProjectId, PostBody, ResponseBody, PostQueryParams>(
    "/selectiveLoad",
    [withAuthentication],
    LIST_SESSIONS
  )
);

/**
 * Get a session
 * GET /:id
 */
const GET_SESSION = endpoint<ProjectIdAndId, undefined, SessionD>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    const registration = req.user.registration;
    const isRequestedFromInscriptionFrontend = req.headers.origin === config.urls.inscriptionFront;

    return await readEntity(
      req.params.id,
      req.params.projectId,
      res,
      readSessionQuery,
      readSessionAdditionalActions(registration, isRequestedFromInscriptionFrontend)
    );
  }
);
SessionRouter.get(...GET_SESSION);

/**
 * Create a session
 * POST /
 */
const CREATE_SESSION = endpoint<
  ProjectId,
  New<SessionD> & {registrations: string[]},
  SessionD & {registrationsToUpdateIds: string[]}
>(
  "/",
  [
    permit(projectContributors),
    filterRequestBody(allowedBodyArgs),
    removeNestedObjectsContent("stewards", "places", "activity", "team", "project"),
    validateAndSanitizeRequest(
      isMongoId("activity"),
      body("slots").isArray().isLength({min: 1}),
      isMongoId("project")
    ),
  ],
  async function (req, res) {
    const {slots: slotsData, registrations: newSessionsSuscriptions, ...sessionBody} = req.body;

    let session = new Session(sessionBody);

    if (slotsData?.length > 0) {
      // Compute start and end of the session
      const {start, end} = getSlotsStartAndEnd(slotsData);
      session.start = start.toDate();
      session.end = end.toDate();

      for (const querySlot of slotsData) {
        // If we are cloning, we don't want to save with the ids, but we want to create new slots
        if (querySlot._id) querySlot._id = undefined;
        const newSlot = await Slot.create(querySlot);
        session.slots.push(newSlot);
      }
    }

    session = await session.save();

    const registrationsToUpdateIds = newSessionsSuscriptions
      ? await updateManyToManyRelationship(
          session,
          newSessionsSuscriptions,
          Registration,
          "session",
          "sessionsSubscriptions",
          {subscribedBy: req.authenticatedUser, updatedAt: Date.now()}
        )
      : [];

    await session.populate([
      {path: "stewards"},
      {path: "places"},
      {path: "slots"},
      {path: "activity", populate: [{path: "category"}]},
      {path: "team", select: lightSelection.team},
    ]);

    const updatedSession = await readSessionAdditionalActions(
      req.authenticatedUser.registration,
      false
    )(session);
    broadcastUpdate(res, "create", updatedSession);
    return res.status(201).send({...updatedSession, registrationsToUpdateIds});
  }
);
SessionRouter.post(...CREATE_SESSION);

/**
 * Modify a session
 * PATCH /:id
 */
const MODIFY_SESSION = endpoint<
  ProjectIdAndId,
  Modified<SessionD> & {registrations: string[]},
  SessionD & {registrationsToUpdateIds: string[]}
>(
  "/:id",
  [
    permit(projectContributors),
    filterRequestBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("stewards", "places", "activity", "team", "project"),
    validateAndSanitizeRequest(
      isMongoId("activity").optional(),
      body("slots").isArray().isLength({min: 1}).optional(),
      isMongoId("project").optional()
    ),
  ],
  async function (req, res) {
    const {registrations: newSessionsSubscriptions, ...sessionBody} = req.body;

    // Modify or create the slots based on the new data. Only if they are given !
    if (sessionBody.slots !== undefined) {
      // Compute start and end of the session
      const {start, end} = getSlotsStartAndEnd(sessionBody.slots);
      sessionBody.start = start.toDate();
      sessionBody.end = end.toDate();

      const session = await Session.findOne({
        _id: req.params.id,
        project: req.params.projectId,
      });
      if (!session) return res.sendStatus(404);

      const newSlots = [];
      for (const querySlot of sessionBody.slots) {
        if (querySlot._id) {
          // If the slot already exists, update it
          newSlots.push(await Slot.findByIdAndUpdate(querySlot._id, querySlot, {new: true}));
        } else {
          // Else, create it
          newSlots.push(await Slot.create(querySlot));
        }
      }

      // Delete unused slots
      const slotsToDelete = session.slots.filter(
        (slotId) => !sessionBody.slots.find((newSlot: SlotD) => newSlot._id === slotId.toString())
      );
      // Here, delete the slots totally because we won't use them anymore at all
      await Slot.deleteMany({_id: slotsToDelete.map((s) => s._id)});

      sessionBody.slots = newSlots.map((slot) => slot._id);
    }

    // Save the session with the new data
    const newSession = await Session.findOneAndUpdate(
      {project: req.params.projectId, _id: req.params.id},
      sessionBody,
      {new: true, __user: req.authenticatedUser._id, __reason: "update()"}
    ).populate([
      {path: "stewards"},
      {path: "places"},
      {path: "slots"},
      {path: "activity", populate: [{path: "category"}]},
      {path: "team", select: lightSelection.team},
    ]);

    const registrationsToUpdateIds = newSessionsSubscriptions
      ? await updateManyToManyRelationship(
          newSession,
          newSessionsSubscriptions,
          Registration,
          "session",
          "sessionsSubscriptions",
          {subscribedBy: req.authenticatedUser, updatedAt: Date.now()}
        )
      : [];

    const updatedSession = await readSessionAdditionalActions(
      req.authenticatedUser.registration,
      false
    )(newSession);
    broadcastUpdate(res, "update", updatedSession);
    return res.status(200).send({...updatedSession, registrationsToUpdateIds});
  }
);
SessionRouter.patch(...MODIFY_SESSION);

/**
 * Delete a session
 * DELETE /:id
 */
const DELETE_SESSION = endpoint<ProjectIdAndId, undefined, {registrationsToUpdateIds: string[]}>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const sessionId = req.params.id;

    const registrationsToUpdateIds = await updateManyToManyRelationship(
      sessionId,
      [],
      Registration,
      "session",
      "sessionsSubscriptions"
    );

    return await deleteEntity(
      sessionId,
      Session,
      req.authenticatedUser,
      res,
      async (session: SessionD) =>
        await Slot.updateMany({_id: {$in: session.slots}}, {deletedAt: new Date()}),
      {registrationsToUpdateIds}
    );
  }
);
SessionRouter.delete(...DELETE_SESSION);

/**
 * Subscribe the user emitting the request to the given session
 * GET /:id/subscribe
 */
const SUBSCRIBE_TO_SESSION = endpoint<
  ProjectIdAndId,
  undefined,
  {updatedSession: SessionD; registration: Lean<RegistrationD>}
>("/:id/subscribe", [withAuthentication], async function (req, res) {
  const sessionId = req.params.id;
  const projectId = req.params.projectId;
  const registration = req.user.registration;
  const session = await readSessionQuery(sessionId, projectId);
  if (!session) return res.sendStatus(404);

  // Check if there is still some pace available in the session
  await sessionMethods.populateParticipants(session, false);
  if (session.numberParticipants >= session.computedMaxNumberOfParticipants) {
    return res.status(405); // Someone already subscribed !
  }

  // Add the session to subscribed sessions
  registration.sessionsSubscriptions.push({
    session: sessionId,
    subscribedBy: req.authenticatedUser._id,
  } as any);
  await registration.save({__reason: "subscribe()", __user: req.authenticatedUser._id});

  await registration.populate("user");

  const updatedSession = await readSessionAdditionalActions(registration)(session);
  return res.status(201).json({
    registration: await registrationMethods.withVoluntaryCounter(registration),
    updatedSession,
  });
});
SessionRouter.get(...SUBSCRIBE_TO_SESSION);

/**
 * Unsubscribe the user emitting the request to the given session
 * GET /:id/unsubscribe
 */
const UNSUBSCRIBE_FROM_SESSION = endpoint<
  ProjectIdAndId,
  undefined,
  {updatedSession: SessionD; registration: Lean<RegistrationD>}
>("/:id/unsubscribe", [withAuthentication], async function (req, res) {
  const sessionId = req.params.id;
  const projectId = req.params.projectId;
  const project = await Project.findByIdOrSlug(projectId);
  const registration = req.user.registration;
  const authenticatedRegistration = req.authenticatedUser.registration;

  if (project.blockSubscriptions && !authenticatedRegistration.role) return res.sendStatus(401);

  const session = await readSessionQuery(sessionId, projectId);
  if (!session) return res.sendStatus(404);

  // Remove the session from subscribed sessions
  registration.sessionsSubscriptions = registration.sessionsSubscriptions.filter(
    (ss) => ss.session.toString() !== sessionId
  );
  await registration.save({__reason: "unsubscribe()", __user: req.authenticatedUser._id});

  await registration.populate("user");

  const updatedSession = await readSessionAdditionalActions(registration)(session);
  return res.status(201).json({
    registration: await registrationMethods.withVoluntaryCounter(registration),
    updatedSession,
  });
});
SessionRouter.get(...UNSUBSCRIBE_FROM_SESSION);

type InconsistencyForSlot = {
  session: Partial<SessionD>;
  slot: number;
  inconsistencies: InconsistencyForEntity[];
};

/**
 * Check inconsistencies for the given session
 * POST /checkInconsistencies
 */
const CHECK_INCONSISTENCIES_FOR_SESSION = endpoint<
  ProjectId,
  SessionD,
  Array<InconsistencyForSlot>
>("/checkInconsistencies", [permit(projectContributors)], async function (req, res) {
  const sessionDraftTested = req.body;
  const projectId = req.params.projectId;

  const project = await Project.findByIdOrSlug(projectId);
  const sessionsOfProject: SessionD[] = await Session.find({project: projectId}, "slots")
    .populate("slots")
    .lean();

  const inconsistencies: any[] = [];
  let slotIndex = 0;

  for (const slotTested of sessionDraftTested.slots) {
    const inconsistenciesForSlot: {
      type: string;
      entity: string;
      entitiesInvolved: any[];
    }[] = [];

    const aProjectAvailabilityExists = project.availabilitySlots.find(
      (slot) =>
        moment(slot.start) <= moment(slotTested.start) && moment(slot.end) >= moment(slotTested.end)
    );
    if (!aProjectAvailabilityExists) {
      inconsistenciesForSlot.push({
        type: "availabilitiesOverlap",
        entity: "project",
        entitiesInvolved: [project],
      });
    }

    inconsistenciesForSlot.push(
      ...inconsistenciesForEntities(
        sessionsOfProject,
        "place",
        "places",
        slotTested,
        sessionDraftTested
      )
    );
    inconsistenciesForSlot.push(
      ...inconsistenciesForEntities(
        sessionsOfProject,
        "steward",
        "stewards",
        slotTested,
        sessionDraftTested
      )
    );

    if (inconsistenciesForSlot.length > 0) {
      inconsistencies.push({
        session: sessionDraftTested,
        slot: slotIndex + 1,
        inconsistencies: inconsistenciesForSlot,
      });
    }
    slotIndex++;
  }

  return res.status(200).json(inconsistencies);
});
SessionRouter.post(...CHECK_INCONSISTENCIES_FOR_SESSION);
