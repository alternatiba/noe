import {isValidObjectId} from "@config/mongoose";
import {withAuthentication} from "@config/passport";
import {Registration} from "@models/registration.model";
import {allowedBodyArgs, LightUserD, User, UserD} from "@models/user.model";
import {createEntity} from "@utils/controllers/actions/createEntity";
import {deleteEntity} from "@utils/controllers/actions/deleteEntity";
import {filterRequestBody} from "@utils/controllers/requestValidation/filterRequestBody";
import {
  isEmail,
  isString,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {lightSelection} from "@utils/lightSelection";
import {endpoint, Modified, New} from "@utils/routeUtilities";
import {compare as bcryptCompare} from "bcryptjs";
import {Request, Response, Router} from "express";
import {body} from "express-validator/check";

export const UserRouter = Router();

/**
 * Get a user's basic details in database
 * GET /:emailOrId (can be either email or _id)
 */
const GET_USER = endpoint<{emailOrId: string}, undefined, LightUserD>(
  "/:emailOrId",
  [withAuthentication],
  async (req, res) => {
    const userEmailOrId = req.params.emailOrId;
    // Match either with ID or with email
    if (req.user._id === userEmailOrId || req.user.email === userEmailOrId) {
      return res.status(200).send(req.user);
    } else {
      const conditions = isValidObjectId(userEmailOrId)
        ? {$or: [{_id: userEmailOrId}, {email: userEmailOrId}]}
        : {email: userEmailOrId};
      const user = await User.findOne(
        // Make sure we only select full, completely defined users (not users from invitations)
        {
          $and: [
            conditions,
            {firstName: {$exists: true}, lastName: {$exists: true}, password: {$exists: true}},
          ],
        },
        lightSelection.user
      ).lean();
      if (!user) return res.sendStatus(404);
      return res.status(200).send(user);
    }
  }
);
UserRouter.get(...GET_USER);

/**
 * Create a new user
 * POST /
 */
const CREATE_USER = endpoint<undefined, New<UserD>, UserD>(
  "/",
  [
    filterRequestBody(allowedBodyArgs),
    validateAndSanitizeRequest(
      isEmail("email"),
      isString(["firstName", "lastName"]),
      body("password").isString().isLength({min: 8}),
      isString("locale").isLength({min: 2, max: 2}).optional()
    ),
  ],
  async function (req, res) {
    const submittedUser = req.body;

    // Check if there is already a user in the DB with this email
    const user = await User.findOne({email: submittedUser.email}, "_id password");

    if (user) {
      // If the user already exists, check that the user in DB doesn't have password (it is an invited user)

      // If the user already has a password, it means we can't update it, because a real user has already created an account with this email
      if (user.password) return res.sendStatus(401);

      // Then, update details
      user.password = submittedUser.password;
      user.firstName = submittedUser.firstName;
      user.lastName = submittedUser.lastName;
      await user.save();

      // And remove all invitation tokens from all other pending invitations for this user.
      // This means that all invitations will be accepted automatically
      await Registration.updateMany({user: user._id}, {$unset: {invitationToken: true}});

      // Code 201 to tell it was updated, not created
      return res.status(201).json(user);
    } else {
      // If there is not existing user, just create one
      return await createEntity(User, req.body, res);
    }
  }
);
UserRouter.post(...CREATE_USER);

/**
 * Modify a user
 * PATCH /:id
 */
const MODIFY_USER = endpoint<{id: string}, Modified<UserD>, UserD>(
  "/:id",
  [
    withAuthentication,
    filterRequestBody(["_id", ...allowedBodyArgs]),
    validateAndSanitizeRequest(
      isEmail("email").optional(),
      isString(["firstName", "lastName"]).optional(),
      body("password").isString().isLength({min: 8}).optional(),
      isString("locale").isLength({min: 2, max: 2}).optional()
    ),
  ],
  async function update(req: Request, res: Response) {
    const authenticatedUser = req.authenticatedUser;
    const {password, oldPassword, ...payload} = req.body;

    // Make sure we modify the authenticated user profile, otherwise do not allow it
    if (authenticatedUser._id.toString() !== req.params.id) return res.sendStatus(401);

    // If there is a password change, make the proper checks
    if (password && oldPassword) {
      const user = await User.findById(req.body._id, {
        password: true,
      });
      const passwordsAreEqual = await bcryptCompare(oldPassword, user.password);
      if (!passwordsAreEqual) return res.status(401).send("Mot de passe incorrect");
      user.password = req.body.password;
      user.save();
    }

    // Update non critical stuff
    const user = await User.findByIdAndUpdate(req.body._id, payload, {new: true})
      .populate({
        path: "registrations",
        select: "project role",
      })
      .lean();

    return res.status(200).json(user);
  }
);
UserRouter.patch(...MODIFY_USER);

/**
 * Delete a user
 * DELETE /:id
 */
const DELETE_USER = endpoint<{id: string}>("/:id", [withAuthentication], async function (req, res) {
  const userId = req.params.id;
  const authenticatedUser = req.authenticatedUser;

  // Make sure we modify the authenticated user profile
  if (authenticatedUser._id.toString() !== userId) return res.sendStatus(401);

  // Delete *for real* the user
  return await deleteEntity(
    userId,
    User,
    req.authenticatedUser,
    res,
    // Delete *for real* all the registrations linked to the user
    async () => await Registration.deleteMany({user: userId}),
    undefined,
    true
  );
});
UserRouter.delete(...DELETE_USER);
