import {Lean} from "@config/mongoose";
import {withAuthentication} from "@config/passport";
import {Project, ProjectD, projectMethods} from "@models/project.model";
import {RegistrationD, registrationMethods} from "@models/registration.model";
import {Ticket} from "@models/ticketing.model";
import {User, UserD} from "@models/user.model";
import {logger} from "@services/logger";
import "../services/websocketServer";
import {broadcastThroughWebsocket} from "@services/websocketServer";
import {handleErrors} from "@utils/routeUtilities";
import {personName} from "@utils/utilities";
import {Request, Response, Router} from "express";
import moment from "moment";
import fetch from "node-fetch";
import config from "../config/config";
import Mailer from "../services/mailer";

type HelloAssoSmallItem = {
  name: string;
  user: {
    firstName: string;
    lastName: string;
  };
  priceCategory: string;
  minAmount: number;
  ticketUrl: string;
  qrCode: string;
  tierDescription: string;
  id: number;
  amount: number;
  type: string;
  initialAmount: number;
  state: string;
};

type HelloAssoItem = HelloAssoSmallItem & {
  order: HelloAssoSmallOrder;
  payer: {
    email: string;
    address: string;
    country: string;
    dateOfBirth: string;
    firstName: string;
    lastName: string;
  };
  payments: Array<{
    cashOutState: string;
    shareAmount: number;
    id: number;
    amount: number;
    date: string;
    paymentMeans: string;
    installmentNumber: number;
    state: string;
    meta: {
      createdAt: string;
      updatedAt: string;
    };
  }>;
  discount: {
    code: string;
    amount: number;
  };
  customFields: Array<{
    name: string;
    type: string;
    answer: string;
  }>;
  options: Array<{
    name: string;
    amount: number;
    priceCategory: string;
    isRequired: boolean;
    customFields: Array<{
      name: string;
      type: string;
      answer: string;
    }>;
  }>;
};

type HelloAssoSmallOrder = {
  id: number;
  date: string;
  formSlug: string;
  formType: string;
  organizationName: string;
  organizationSlug: string;
  formName: string;
  meta: {
    createdAt: string;
    updatedAt: string;
  };
  isAnonymous: boolean;
  isAmountHidden: boolean;
};

type HelloAssoOrder = HelloAssoSmallOrder & {
  payer: {
    email: string;
    firstName: string;
    lastName: string;
  };
  items: Array<HelloAssoSmallItem>;
};

const ticketingUtilities = {
  /**
   * Hello Asso ticketing utility functions
   */
  helloAsso: {
    fetchItem: async (project: ProjectD, ticketId: string) => {
      await projectMethods.refreshHelloAssoAuthToken(project);
      // Add withDetails to get options and custom fields
      const res = await fetch(`https://api.helloasso.com/v5/items/${ticketId}?withDetails=true`, {
        headers: {Authorization: `Bearer ${project.helloAsso?.token}`},
      });
      try {
        return {status: res.status, itemData: await res?.json()};
      } catch (e: any) {
        return {status: res.status, error: e?.message};
      }
    },
    createTicketsFromItemData: (item: HelloAssoItem) => {
      return {
        id: item.id.toString(), // The ticket id
        orderId: item.order.id.toString(), // The global order id
        name: item.name,
        amount: item.amount / 100, // Divide by 100 to get amount in euro and not in cents
        user: item.user,
        options: item.options?.map((opt) => ({name: opt.name, amount: opt.amount / 100})),
      };
    },
    parseOrderInfo: (orderData: HelloAssoOrder) => ({
      firstName: orderData.payer.firstName,
      lastName: orderData.payer.lastName,
      email: orderData.payer.email,
      orderId: orderData.id.toString(),
      ticketIds: orderData.items?.map((item) => item.id.toString()),
      eventSlug: orderData.formSlug,
    }),
  },

  /**
   * Custom ticketing utility functions
   */
  customTicketing: {
    fetchItem: async (project: ProjectD, ticketId: string) => {
      const res = await fetch(project.customTicketing, {
        headers: {Accept: "application/json", "Content-Type": "application/json"},
        method: "POST",
        body: JSON.stringify({ticketId}),
      });
      try {
        return {status: res.status, itemData: await res?.json()};
      } catch (e: any) {
        return {status: res.status, error: e?.message};
      }
    },
    createTicketsFromItemData: (item: any) => ({
      id: item?.id,
      name: item?.name,
      amount: item?.amount,
    }),
  },
};

export const TicketingRouter = Router({mergeParams: true});

/**
 * Check if  ticket is valid
 * POST /check/:ticketId
 */
TicketingRouter.post(
  "/check/:ticketId",
  withAuthentication,
  handleErrors(async function checkTicket(req: Request, res: Response) {
    const projectId: string = req.params.projectId;
    const ticketId: string = req.params.ticketId;
    const project = await Project.findByIdOrSlug(projectId).populate<{
      registrations: Array<RegistrationD>;
    }>("registrations");

    if (!project?.ticketingMode) return res.sendStatus(404);

    const ticketingMode = project.ticketingMode.valueOf() as string;

    // Check i the ticket has already been used
    const registrationWithSameTicket = project.registrations.find((registration) =>
      registration[`${ticketingMode}Tickets`]
        ?.map((ticketInfo: Ticket) => {
          if (!ticketInfo || !ticketInfo?.id)
            logger.warn("NO ID FOR TICKET IN REGISTRATION:", registration);
          return ticketInfo?.id;
        })
        .includes(ticketId)
    );
    if (registrationWithSameTicket) return res.status(405).send("Ticket already used"); // Ticket already used

    // Then if not already used, check that the ticket is valid by asking the ticket checker service
    const {status, itemData, error} = await ticketingUtilities[ticketingMode].fetchItem(
      project,
      ticketId
    );
    // Analyze response
    if (status != 200) {
      console.error(`${ticketingMode} returned an error during ticket check`, error);
      return res.sendStatus(404); // Ticket number not found
    }

    const ticket = ticketingUtilities[ticketingMode].createTicketsFromItemData(itemData);
    logger.debug(`Ticket validated: ${ticket?.id}`);
    return res.json(ticket);
  })
);

/**
 * Transfer a ticket from one registration to another
 * POST /transfer
 */
TicketingRouter.post(
  "/transfer",
  withAuthentication,
  handleErrors(async function checkTicket(req: Request, res: Response) {
    const projectId = req.params.projectId;
    const {fromEmail, toEmail, ticketId} = req.body;

    // Check if the user has the right to execute the transfer.
    // If the user is not an admin, and has not the same email as the fromEmail, then they don't have the right
    if (req.authenticatedUser.registration?.role != "admin" && req.user.email !== fromEmail) {
      return res.status(400);
    }

    const project = await Project.findByIdOrSlug(projectId).populate<{
      registrations: Array<RegistrationD>;
    }>({
      path: "registrations",
      populate: [{path: "user", select: "firstName lastName email", options: {lean: true}}],
    });

    // Can't transfer to the same registration
    if (fromEmail === toEmail) return res.status(400).send("User can't give ticket to themselves");

    if (!project?.ticketingMode) return res.status(404).send("Ticketing not activated on project");

    const ticketingMode = project.ticketingMode.valueOf() as string;
    const ticketsKey = `${ticketingMode}Tickets`;

    const getTransferredTicket = (registration: RegistrationD) =>
      registration[ticketsKey]?.find((ticketInfo: Ticket) => ticketInfo.id === ticketId);

    // Get the from registration, and make sur it has the ticket
    const fromRegistration = project.registrations.find(
      (registration) =>
        registration.user.email === fromEmail && !!getTransferredTicket(registration)
    );
    if (!fromRegistration) return res.sendStatus(404);

    const toRegistration = project.registrations.find(
      (registration) => registration.user.email === toEmail
    );
    if (!toRegistration) return res.sendStatus(404);

    // Get the ticket info
    const transferredTicketInfo: Ticket = {
      ...getTransferredTicket(fromRegistration),
      transferInfos: {fromEmail, date: moment().toDate()},
    };

    // remove it from fromRegistration
    fromRegistration[ticketsKey] = fromRegistration[ticketsKey].filter(
      (ticketInfo: Ticket) => ticketInfo.id !== transferredTicketInfo.id
    );

    // Add it to toRegistration
    toRegistration[ticketsKey] = [...(toRegistration[ticketsKey] || []), transferredTicketInfo];

    if (fromRegistration[ticketsKey]?.length === 0) {
      await registrationMethods.unregister(fromRegistration);
    } else {
      await fromRegistration.save();
    }
    // Save everything
    await toRegistration.save();

    // TODO improve broadcastUpdate to make this work here
    // broadcastUpdate()

    await Mailer.sendMail("ticketing.transfer.recipient", toEmail, {
      recipientFirstName: toRegistration.user.firstName,
      ticketSenderName: personName(fromRegistration.user),
      projectName: project.name,
      link: `${process.env.REACT_APP_INSCRIPTION_FRONT_URL}/${
        project.slug || project._id
      }/registration`,
      ticketId,
      lng: toRegistration.user.locale,
    });

    return res.send(200);
  })
);

/**
 * Provides a test endpoint to make tests on the registration page without needing a complete ticketing setup
 * This returns always success except if the ticketId is equal to "fail"
 * POST /test
 */
TicketingRouter.post(
  "/test",
  handleErrors(function test(req: Request, res: Response) {
    if (req.body.ticketId !== "fail") {
      return res.status(200).send({name: "Test", amount: 100, id: req.body.ticketId});
    } else {
      return res.sendStatus(404);
    }
  })
);

/**
 * Once a payment is done, Hello Asso notifies NOE with this webhook. NOE makes some verifications, and then
 * updates the user's web client if a websocket connection is found. It also sends an invitation email with a custom link to register.
 * The Hello Asso ticketing should be activated in order for this to work.
 * POST /onHelloAssoOrder
 */
TicketingRouter.post("/sendInvitationEmailAfterHelloAssoOrder", handleErrors(onHelloAssoOrder)); // This is for legacy
TicketingRouter.post("/onHelloAssoOrder", handleErrors(onHelloAssoOrder));
async function onHelloAssoOrder(req: Request, res: Response) {
  logger.debug("Started onHelloAssoOrder()");

  const HelloAsso = ticketingUtilities.helloAsso;

  const projectId: string = req.params.projectId;
  const project = await Project.findByIdOrSlug(
    projectId,
    "ticketingMode helloAsso name slug locale"
  );
  const ticketingMode = project?.ticketingMode?.valueOf() as string;

  ///// CHECKS /////

  // Hello Asso ticketing is activated
  if (!ticketingMode || ticketingMode !== "helloAsso") {
    logger.debug("'helloAsso' ticketing not activated");
    return res.sendStatus(404);
  }

  // The api hook is sending an "Order"
  if (req.body.eventType !== "Order") {
    return res.status(200).send("Nothing to do, it's not an order");
  }

  // Parse the order info and keep only the things we need
  const orderInfo = HelloAsso.parseOrderInfo(req.body.data as HelloAssoOrder);

  // The formSlug is matching one the correct Hello Asso ticketing that we have selected in our NOÉ project
  if (project.helloAsso.selectedEvent !== orderInfo.eventSlug) {
    return res.status(200).send("Nothing to do, the ticketing is different");
  }

  ///// TICKETS VALIDATION AND AGGREGATION /////

  const tickets = [];
  for (const ticketId of orderInfo.ticketIds) {
    // The ticketId is valid according to Hello Asso (make only one check for the first ticket, that's enough)
    const {status, itemData, error} = await HelloAsso.fetchItem(project, ticketId);
    if (status !== 200) {
      logger.debug("Invalid ticketId or event:", error);
      return res.sendStatus(200);
    }

    // The ticket formSlug matches a correct event according to Hello Asso
    if (itemData.order.formSlug !== orderInfo.eventSlug) {
      logger.debug("Bad formSlug between api order and ticketId");
      return res.sendStatus(404);
    }

    // Then add the ticket to the tickets list
    tickets.push(HelloAsso.createTicketsFromItemData(itemData));
  }

  ///// SEND DATA TO USER /////

  const recipientUser = await User.findOne({email: orderInfo.email}, "locale").lean();

  const connectionsSent = sendTicketsThroughWebsocket(
    recipientUser?._id?.toString(),
    orderInfo.ticketIds
  );
  await sendInvitationEmail(recipientUser, connectionsSent.length > 0, orderInfo, req, project);
  logger.debug(
    `New Hello Asso booking for user ${recipientUser?._id}, sent via Websocket ? ${
      connectionsSent.length > 0
    }`
  );

  return res.sendStatus(200);
}

/**
 * Helper function that sends an email based on the person first name, last name, email, and ticketId
 * with a custom URL so that the person can register easily after they have ordered a ticket.
 */
async function sendInvitationEmail(
  recipientUser: Lean<UserD>,
  paidInNOE: boolean,
  orderInfo: {
    email: string;
    firstName: string;
    lastName: string;
    orderId: string;
    ticketIds: string[];
  },
  req: Request,
  project: ProjectD
) {
  const {email, firstName, lastName, orderId, ticketIds} = orderInfo;

  const rootUrl = `${config.urls.inscriptionFront}/${project.slug || project.id}`;

  let finalUrl, emailKey;
  if (paidInNOE) {
    // When the user has a NOE page open with the ticketing payment inside, it also means that the user already has an account, so there's only one case
    emailKey = "invitation.send_known_participant_invitation_paid_in_noe";
    finalUrl = `${rootUrl}/registration`;
  } else {
    // Else if the participant didn't pay directly in NOE there are two options: if the user exists in NOE already, or not.

    // In both cases, we need to send some info via queryparams to pre-register the person
    const queryParams = `?firstName=${firstName}&lastName=${lastName}&email=${email}&ticketId=${ticketIds.join(
      ","
    )}`;

    if (recipientUser) {
      emailKey = "invitation.send_known_participant_invitation";
      finalUrl = `${rootUrl}/registration` + queryParams;
    } else {
      emailKey = "invitation.send_new_participant_invitation";
      finalUrl = `${rootUrl}/signup` + queryParams;
    }
  }

  // Send the invitation email
  await Mailer.sendMail(emailKey, email, {
    lng: recipientUser?.locale || project.locale,
    link: finalUrl,
    firstName,
    orderId,
    ticketIds,
    eventName: project.name,
  });
}

/**
 * Sends the tickets back to the client's frontend via websocket connection if there is an existing one.
 */
function sendTicketsThroughWebsocket(userId: string, ticketIds: string[]) {
  return broadcastThroughWebsocket("ticketing", userId, JSON.stringify(ticketIds));
}
