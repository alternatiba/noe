import {withAuthentication} from "@config/passport";
import {
  getAllEntitiesLinkedToProject,
  processProjectImport,
  ProjectImportPayload,
  rollbackDeletedElements,
} from "@helpers/project.helper";
import {Activity} from "@models/activity.model";
import {Category} from "@models/category.model";
import {Place} from "@models/place.model";
import {
  adminOnlyFields,
  allowedBodyArgs,
  Project,
  ProjectD,
  projectMethods,
  superAdminOnlyHiddenFields,
  superAdminOnlyVisibleFields,
} from "@models/project.model";
import {Registration, RegistrationD, registrationMethods} from "@models/registration.model";
import {Session, SessionD} from "@models/session.model";
import {Slot} from "@models/slot.model";
import {Steward} from "@models/steward.model";
import {Team} from "@models/team.model";
import {logger} from "@services/logger";
import {computeFunctionForAllElements} from "@utils/computeFunctionForAllElements";
import {listAllEntities} from "@utils/controllers/actions/listAllEntities";
import {readEntity} from "@utils/controllers/actions/readEntity";
import {filterRequestBody} from "@utils/controllers/requestValidation/filterRequestBody";
import {
  nobodyExceptSuperAdmins,
  permit,
  projectAdmins,
  projectContributors,
  projectGuests,
  ROLES,
} from "@utils/controllers/requestValidation/permit";
import {
  isEmail,
  isString,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {lightSelection} from "@utils/lightSelection";
import {
  endpoint,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  TypedRequestHandler,
} from "@utils/routeUtilities";
import {Request, Response, Router} from "express";
import {omit, pick} from "lodash";
import type {Document} from "mongoose";
import config from "../config/config";

// Create a string that can be used to select super admin fields, like "+superAdminNotes +status" etc.
const withSuperAdminOnlyHiddenFieldsSelection = superAdminOnlyHiddenFields
  .reduce((acc, field) => {
    acc += `+${field} `;
    return acc;
  }, "")
  .trim();

export const ProjectRouter = Router();

/**
 * Generic endpoints
 */
ProjectRouter.get(...MODIFICATIONS_HISTORY("Project"));

/**
 * List all the projects for which the user is registered
 * (either as orga or as participant, depending on the frontend that is requesting projects)
 * GET /
 */
const LIST_PROJECTS = endpoint<undefined, undefined, Array<ProjectD>>(
  "/",
  [withAuthentication],
  async function (req, res) {
    const isRequestedFromInscriptionFrontend = req.headers.origin === config.urls.inscriptionFront;
    return isRequestedFromInscriptionFrontend
      ? await listAllProjectsForParticipant(req, res)
      : await listAllProjectsForOrga(req, res);
  }
);
ProjectRouter.get(...LIST_PROJECTS);

async function listAllProjectsForOrga(req: Request, res: Response) {
  const registeredProjectsIds = await Registration.distinct("project", {
    user: req.user,
    role: {$exists: true},
  });

  const projects = (
    await Project.find(
      // Fetch the projects administrated by the user. If super admin, show all the projects of the platform
      req.user.superAdmin ? {} : {_id: registeredProjectsIds},
      {
        ...lightSelection.project,
        ...Object.fromEntries(superAdminOnlyVisibleFields.map((f) => [f, 1])),
        // If super admin, select the superAdmin data as well as well
        ...(req.user.superAdmin
          ? Object.fromEntries(superAdminOnlyHiddenFields.map((f) => [f, 1]))
          : {}),
      }
    )
      // Populate registrations and sessions so we can make stats on them
      .populate<{registrations: RegistrationD[]}>({
        path: "registrations",
        select: "role user",
        populate: {path: "user", select: "email"},
      })
      .populate<{sessions: SessionD[]}>({path: "sessions", select: "_id"})
      // Standard query params
      .collation({locale: "fr", strength: 1})
      .sort({updatedAt: "desc"})
      .lean()
  ).map((project) => {
    // Then, count sessions, total registrations, and orga registrations
    const {sessions, registrations, ...projectData} = project;
    const orgaRegistrations = registrations.filter((r) => r.role);
    const orgaEmails = orgaRegistrations.map((r) => r.user.email);
    const participants = registrations.length - orgaRegistrations.length;
    return {
      ...projectData,
      orgaEmails,
      stats: {
        sessions: sessions.length,
        participants,
        orgas: orgaRegistrations.length,
      },
    };
  });

  return res.send(projects);
}

async function listAllProjectsForParticipant(req: Request, res: Response) {
  const registeredProjectsIds = await Registration.distinct("project", {
    user: req.user,
  });
  return listAllEntities(
    Project.find({_id: registeredProjectsIds}, lightSelection.project),
    {start: "asc"},
    res
  );
}

/**
 * List all the publicly available projects on NOÉ
 * GET /public
 */
const LIST_PUBLIC_PROJECTS = endpoint<undefined, undefined, Array<ProjectD>>(
  "/public",
  [],
  async function (req, res) {
    return listAllEntities(
      Project.find({isPublic: true}, lightSelection.project),
      {start: "asc"},
      res
    );
  }
);
ProjectRouter.get(...LIST_PUBLIC_PROJECTS);

/**
 * Get a full project
 * GET /:projectId
 */
const GET_PROJECT = endpoint<ProjectId, undefined, ProjectD>(
  "/:projectId",
  [permit(projectGuests, config.urls.inscriptionFront)], // Allow all persons having a role in orga front, and any person requesting from the inscription side
  async function (req, res) {
    const registration = req.user.registration;
    const role = registration?.role; // Maybe there is no registration at all !
    const isRequestedFromOrgaFrontend = req.headers.origin === config.urls.orgaFront;

    await readEntity(
      req.params.projectId,
      undefined,
      res,
      (id) =>
        Project.findByIdOrSlug(
          id,
          req.user.superAdmin ? withSuperAdminOnlyHiddenFieldsSelection : undefined
        ),
      async (project: ProjectD) => {
        if (
          isRequestedFromOrgaFrontend &&
          role === ROLES.ADMIN &&
          project.ticketingMode === "helloAsso"
        ) {
          await projectMethods.refreshHelloAssoAuthToken(project);
          await projectMethods.populateHelloAssoEventCandidates(project);
        }
        projectMethods.hideSensitiveFields(project, registration);
        return project;
      }
    );
  }
);
ProjectRouter.get(...GET_PROJECT);

/**
 * Get a project when not connected
 * GET /:projectId/public
 */
const GET_PUBLIC_PROJECT = endpoint<ProjectId, undefined, ProjectD>(
  "/:projectId/public",
  [],
  async function (req, res) {
    await readEntity(req.params.projectId, undefined, res, (id) =>
      Project.findByIdOrSlug(id, {
        ...lightSelection.project,
        customCss: true,
        socialImageUrl: true,
      }).lean()
    );
  }
);
ProjectRouter.get(...GET_PUBLIC_PROJECT);

/**
 * Create a project
 * POST /
 */
const CREATE_PROJECT = endpoint<undefined, New<ProjectD>, ProjectD>(
  "/",
  // Allow project creation only for super  admins, or for everybody
  [
    withAuthentication,
    validateAndSanitizeRequest(
      isString("name"),
      isEmail("contactEmail"),
      isString("locale").optional()
    ),
  ],
  async function (req, res) {
    const project = new Project({
      name: req.body.name,
      contactEmail: req.body.contactEmail,
      availabilitySlots: req.body.availabilitySlots,
      useTeams: req.body.useTeams,
      usePlaces: req.body.usePlaces,
      locale: req.user.locale, // Assign the user's locale as a default locale for the project
      status: req.user.superAdmin ? req.body.status : "demo",
    });
    await project.save();

    // Create the project registration linked to this project, and make the current user admin of the project
    const userRegistration = new Registration({
      role: "admin",
      project: project,
      user: req.user,
    });
    await userRegistration.save();

    // Prepare the project for response
    projectMethods.hideSensitiveFields(project, userRegistration);
    res.status(201).json(project);
  }
);
ProjectRouter.post(...CREATE_PROJECT);

/**
 * Modify a project
 * PATCH /:projectId
 */
const MODIFY_PROJECT = endpoint<
  ProjectId,
  Modified<ProjectD & {breakfastTime: string; lunchTime: string; dinnerTime: string}>,
  ProjectD
>(
  "/:projectId",
  [filterRequestBody(["_id", ...allowedBodyArgs]), permit(projectContributors)],
  async function (req, res) {
    const projectId = req.params.projectId;
    // First, know who the person is (there can be no registration at all)
    const userRegistration = req.user.registration;
    const userIsAdmin = userRegistration?.role === ROLES.ADMIN;
    const userIsSuperAdmin = req.user.superAdmin;

    const project = await Project.findById(
      projectId,
      req.user.superAdmin ? withSuperAdminOnlyHiddenFieldsSelection : undefined
    );
    if (!project) return res.sendStatus(404);

    // WARNING: this is before everything else because we need the "old" values of meal times to know
    // if we have to compute afterwards
    const shouldUpdateAllRegistrationsDaysOfPresence =
      (req.body.availabilitySlots &&
        JSON.stringify(project.availabilitySlots.map((s) => s.toObject())) !==
          JSON.stringify(req.body.availabilitySlots)) ||
      (req.body?.breakfastTime && project.breakfastTime.toISOString() !== req.body.breakfastTime) ||
      (req.body?.lunchTime && project.lunchTime.toISOString() !== req.body.lunchTime) ||
      (req.body?.dinnerTime && project.dinnerTime.toISOString() !== req.body.dinnerTime);

    const nonSensitiveProjectData: Partial<ProjectD> = omit(req.body, [
      ...adminOnlyFields,
      ...superAdminOnlyHiddenFields,
      ...superAdminOnlyVisibleFields,
    ]);

    // First, set the non-sensitive data
    project.set(nonSensitiveProjectData);

    // Then, update admin-only fields
    if (userIsAdmin) {
      // Set admin fields
      const adminData = pick(req.body, adminOnlyFields);
      project.set(adminData);

      // Ticketing - Custom API
      const shouldUpdateCustomTicketing = project.customTicketing !== req.body.customTicketing;
      if (shouldUpdateCustomTicketing) project.customTicketing = req.body.customTicketing;

      // Ticketing - HelloAsso
      const shouldUpdateHelloAssoTicketing =
        project.helloAsso?.clientId !== req.body.helloAsso?.clientId ||
        project.helloAsso?.clientSecret !== req.body.helloAsso?.clientSecret ||
        project.helloAsso?.selectedEvent !== req.body.helloAsso?.selectedEvent;
      if (shouldUpdateHelloAssoTicketing) {
        if (
          req.body.helloAsso?.clientId?.length > 0 &&
          req.body.helloAsso?.clientSecret?.length > 0
        ) {
          project.helloAsso = req.body.helloAsso;
          await projectMethods.getHelloAssoAuthTokens(project);
          await projectMethods.populateHelloAssoEventCandidates(project);
        } else {
          project.helloAsso = {
            clientId:
              req.body.helloAsso?.clientId?.length > 0 ? req.body.helloAsso?.clientId : undefined,
            clientSecret:
              req.body.helloAsso?.clientId?.length > 0
                ? req.body.helloAsso?.clientSecret
                : undefined,
          };
        }
      }

      // Ticketing - TiBillet
      const shouldUpdateTiBilletTicketing =
        project.tiBillet?.serverUrl !== req.body.tiBillet?.serverUrl ||
        project.tiBillet?.eventSlug !== req.body.tiBillet?.eventSlug ||
        project.tiBillet?.apiKey !== req.body.tiBillet?.apiKey;
      if (shouldUpdateTiBilletTicketing) {
        project.tiBillet = req.body.tiBillet;
      }
    }

    if (userIsSuperAdmin) {
      // Set super admin fields
      const superAdminData = pick(req.body, [
        ...superAdminOnlyHiddenFields,
        ...superAdminOnlyVisibleFields,
      ]);
      project.set(superAdminData);
    }

    await project.save({__reason: "update()", __user: req.authenticatedUser._id});
    projectMethods.hideSensitiveFields(project, userRegistration);

    res.status(200).json(project);

    // Compute all days of presence of all registrations if the project dates or meal times change
    if (shouldUpdateAllRegistrationsDaysOfPresence) {
      // Update asynchronously all dates of presence if it is needed
      await computeFunctionForAllElements(
        await Registration.find({project: projectId}, "availabilitySlots project"),
        async (el) => {
          await registrationMethods.computeDaysOfPresence(el);
          await el.save({
            __reason: "update() days of presence",
            __user: req.authenticatedUser._id,
          });
        }
      );
      logger.info("Successfully updated dates of presence of all registrations.");
    }
  }
);
ProjectRouter.patch(...MODIFY_PROJECT);

/**
 * Import a project configuration file to modify the current project data
 * POST /:projectId/import
 */
const IMPORT_PROJECT = endpoint<
  ProjectId,
  ProjectImportPayload,
  ProjectD & {notFoundInImportIds?: Array<string>},
  {additiveImport: string; withRegistrations: string}
>(
  "/:projectId/import",
  [permit(projectAdmins), validateAndSanitizeRequest(isEmail("registrations.*.user.email"))],
  async function (req, res) {
    const projectId: string = req.params.projectId;
    const additiveImportForElements = req.query.additiveImport === "true";
    const withRegistrations = req.query.withRegistrations === "true";
    const existingProject = await Project.findByIdOrSlug(projectId);
    if (!existingProject) return res.sendStatus(404);

    const importedProject = req.body;
    const {importedProjectData, notFoundInImportIds} = await processProjectImport(
      importedProject,
      existingProject,
      additiveImportForElements,
      withRegistrations
    );

    // Assign the imported project config data by replacing it when it exists in the importedProjectData
    Object.assign(existingProject, importedProjectData);
    await existingProject.save({__reason: "import()", __user: req.authenticatedUser._id});

    return res.status(200).json({...existingProject.toObject(), notFoundInImportIds});
  }
);
ProjectRouter.post(...IMPORT_PROJECT);

/**
 * Export all the data of a project in a JSON format
 * GET /:projectId/export
 * GET /:projectId/allData (/!\ deprecated)
 */
const EXPORT_PROJECT: TypedRequestHandler<
  ProjectId,
  undefined,
  unknown,
  {withRegistrations: string; anonymized: string; keepStewardNames: string}
> = async (req, res) => {
  const projectId: string = req.params.projectId;
  const withRegistrations = req.query.withRegistrations === "true";
  const anonymized = req.query.anonymized === "true";
  const keepStewardNames = req.query.keepStewardNames === "true";
  const project = await Project.findByIdOrSlug(projectId);
  if (!project) return res.sendStatus(404);
  projectMethods.hideSensitiveFields(project, req.user.registration);

  res.status(200).json({
    ...(await getAllEntitiesLinkedToProject(projectId, {
      withRegistrations,
      anonymized,
      keepStewardNames,
    })),
    project,
  });
};
ProjectRouter.get(...endpoint("/:projectId/export", [permit(projectContributors)], EXPORT_PROJECT));
// Deprecated route
ProjectRouter.get(
  ...endpoint("/:projectId/allData", [permit(projectContributors)], EXPORT_PROJECT)
);

/**
 * Rollback deletion of project related objects for a given period in minutes. ("Oops" feature in Advanced panel)
 * POST /:projectId/rollback/:rollbackPeriod
 */
const ROLLBACK_PROJECT = endpoint<ProjectId & {rollbackPeriod: string}, undefined, undefined>(
  "/:projectId/rollback/:rollbackPeriod",
  [permit(projectAdmins)],
  async function (req, res) {
    const projectId = req.params.projectId;
    const rollbackPeriod: number = parseInt(req.params.rollbackPeriod);

    const rollbackStartDate = new Date();
    rollbackStartDate.setMinutes(rollbackStartDate.getMinutes() - rollbackPeriod); // Remove the period given as argument
    await Promise.all([
      rollbackDeletedElements(Place, projectId, rollbackStartDate),
      rollbackDeletedElements(Category, projectId, rollbackStartDate),
      rollbackDeletedElements(Steward, projectId, rollbackStartDate),
      rollbackDeletedElements(Session, projectId, rollbackStartDate),
      rollbackDeletedElements(Slot, projectId, rollbackStartDate),
      rollbackDeletedElements(Activity, projectId, rollbackStartDate),
      rollbackDeletedElements(Team, projectId, rollbackStartDate),
    ]);

    return res.sendStatus(200);
  }
);
ProjectRouter.get(...ROLLBACK_PROJECT);

const DELETE_PROJECT = endpoint<ProjectId, undefined, undefined>(
  "/:projectId",
  [permit(projectAdmins)],
  async function (req, res) {
    const projectId: string = req.params.projectId;
    const project = await Project.findByIdOrSlug(projectId);

    const entitiesDictionary = await getAllEntitiesLinkedToProject(projectId, {
      withRegistrations: true,
    });

    const now = new Date();

    // First, delete all the session slots because they don't have a project id inside them and
    // can't be linked back to the deleted project directly
    const slotsToDelete = (
      (entitiesDictionary.sessions as SessionD[]).map(
        (session) => session.slots?.map((slot) => slot._id) || []
      ) || []
    ).flat();
    await Slot.updateMany({_id: {$in: slotsToDelete}}, {deletedAt: now});

    // Then, aggregate and flatten all the objects of the project...
    const entities: (Document & {deletedAt?: Date})[] = [project];
    Object.entries(entitiesDictionary).forEach(([, value]) => {
      value.forEach((entity) => {
        entities.push(entity);
      });
    });

    // ... and delete each entity, including project
    await Promise.all(
      entities.map(async (entity) => {
        entity.deletedAt = now;
        await entity.save();
      })
    );

    res.sendStatus(200);
  }
);
ProjectRouter.delete(...DELETE_PROJECT);
