import {withAuthentication} from "@config/passport";

import {Activity} from "@models/activity.model";
import {Session} from "@models/session.model";
import {Slot} from "@models/slot.model";
import {
  allowedBodyArgs,
  LightStewardD,
  schemaDescription,
  Steward,
  StewardD,
} from "@models/steward.model";
import {createEntity} from "@utils/controllers/actions/createEntity";
import {deleteEntity} from "@utils/controllers/actions/deleteEntity";
import {listAllEntities} from "@utils/controllers/actions/listAllEntities";
import {readEntity} from "@utils/controllers/actions/readEntity";
import {updateEntity} from "@utils/controllers/actions/updateEntity";
import {getDependenciesList} from "@utils/controllers/getDependenciesList";
import {GenericQueryParams, parseQueryParams} from "@utils/controllers/parseQueryParams";
import {filterRequestBody} from "@utils/controllers/requestValidation/filterRequestBody";
import {
  permit,
  projectAdmins,
  projectContributors,
} from "@utils/controllers/requestValidation/permit";
import {removeNestedObjectsContent} from "@utils/controllers/requestValidation/removeNestedObjectsContent";
import {
  isMongoId,
  isString,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {lightSelection} from "@utils/lightSelection";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
} from "@utils/routeUtilities";
import {Router} from "express";

export const StewardRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
StewardRouter.get(...MODIFICATIONS_HISTORY("Steward"));
StewardRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

/**
 * List all the stewards in the project
 * GET /
 */
const LIST_STEWARDS = endpoint<ProjectId, undefined, Array<LightStewardD>, GenericQueryParams>(
  "/",
  [withAuthentication],
  async function (req, res) {
    return listAllEntities(
      Steward.find(
        {project: req.params.projectId, ...parseQueryParams(req.query)},
        lightSelection.steward
      ),
      {firstName: "asc"},
      res
    );
  }
);
StewardRouter.get(...LIST_STEWARDS);

/**
 * Get a steward
 * GET /:id
 */
const GET_STEWARD = endpoint<ProjectIdAndId, undefined, StewardD>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Steward.findOne({_id: id, project: projectId}).lean()
    );
  }
);
StewardRouter.get(...GET_STEWARD);

/**
 * Create a steward
 * POST /
 */
const CREATE_STEWARD = endpoint<ProjectId, New<StewardD>, StewardD>(
  "/",
  [
    permit(projectContributors),
    filterRequestBody(allowedBodyArgs),
    removeNestedObjectsContent("project"),
    validateAndSanitizeRequest(isString("firstName"), isMongoId("project")),
  ],
  async function (req, res) {
    return await createEntity(Steward, req.body, res);
  }
);
StewardRouter.post(...CREATE_STEWARD);

/**
 * Modify a steward
 * PATCH /:id
 */
const MODIFY_STEWARD = endpoint<ProjectIdAndId, Modified<StewardD>, StewardD>(
  "/:id",
  [
    permit(projectContributors),
    filterRequestBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("project"),
  ],
  async function (req, res) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Steward,
      req.body,
      req.authenticatedUser,
      res
    );
  }
);
StewardRouter.patch(...MODIFY_STEWARD);

/**
 * Delete a steward
 * DELETE /:id
 */
const DELETE_STEWARD = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const stewardId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {stewards: stewardId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(
          `Cet encadrant est encore référencé par des activités (${dependentActivitiesList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    // TODO make it work with session slots too!
    const dependentSessionsList = await getDependenciesList(Session, {stewards: stewardId});
    if (dependentSessionsList) {
      return res
        .status(405)
        .send(
          `Cet encadrant est encore référencé par des sessions (${dependentSessionsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    return await deleteEntity(stewardId, Steward, req.authenticatedUser, res);
  }
);
StewardRouter.delete(...DELETE_STEWARD);
