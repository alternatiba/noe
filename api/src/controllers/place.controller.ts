import {withAuthentication} from "@config/passport";

import {Activity} from "@models/activity.model";
import {allowedBodyArgs, LightPlaceD, Place, PlaceD, schemaDescription} from "@models/place.model";
import {Session} from "@models/session.model";
import {Slot} from "@models/slot.model";
import {createEntity} from "@utils/controllers/actions/createEntity";
import {deleteEntity} from "@utils/controllers/actions/deleteEntity";
import {listAllEntities} from "@utils/controllers/actions/listAllEntities";
import {readEntity} from "@utils/controllers/actions/readEntity";
import {updateEntity} from "@utils/controllers/actions/updateEntity";
import {getDependenciesList} from "@utils/controllers/getDependenciesList";
import {GenericQueryParams, parseQueryParams} from "@utils/controllers/parseQueryParams";
import {filterRequestBody} from "@utils/controllers/requestValidation/filterRequestBody";
import {
  permit,
  projectAdmins,
  projectContributors,
} from "@utils/controllers/requestValidation/permit";
import {removeNestedObjectsContent} from "@utils/controllers/requestValidation/removeNestedObjectsContent";
import {
  isMongoId,
  isString,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {lightSelection} from "@utils/lightSelection";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
} from "@utils/routeUtilities";
import {Router} from "express";

export const PlaceRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
PlaceRouter.get(...MODIFICATIONS_HISTORY("Place"));
PlaceRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

/**
 * List all the places in the project
 * GET /
 */
const LIST_PLACES = endpoint<ProjectId, undefined, Array<LightPlaceD>, GenericQueryParams>(
  "/",
  [withAuthentication],
  async function (req, res) {
    return listAllEntities(
      Place.find(
        {project: req.params.projectId, ...parseQueryParams(req.query)},
        lightSelection.place
      ),
      {name: "asc"},
      res
    );
  }
);
PlaceRouter.get(...LIST_PLACES);

/**
 * Get a place
 * GET /:id
 */
const GET_PLACE = endpoint<ProjectIdAndId, undefined, PlaceD>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Place.findOne({_id: id, project: projectId}).lean()
    );
  }
);
PlaceRouter.get(...GET_PLACE);

/**
 * Create a place
 * POST /
 */
const CREATE_PLACE = endpoint<ProjectId, New<PlaceD>, PlaceD>(
  "/",
  [
    permit(projectContributors),
    filterRequestBody(allowedBodyArgs),
    removeNestedObjectsContent("project"),
    validateAndSanitizeRequest(isString("name"), isMongoId("project")),
  ],
  async function (req, res) {
    return await createEntity(Place, req.body, res);
  }
);
PlaceRouter.post(...CREATE_PLACE);

/**
 * Modify a place
 * PATCH /:id
 */
const MODIFY_PLACE = endpoint<ProjectIdAndId, Modified<PlaceD>, PlaceD>(
  "/:id",
  [
    permit(projectContributors),
    filterRequestBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("project"),
  ],
  async function (req, res) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Place,
      req.body,
      req.authenticatedUser,
      res
    );
  }
);
PlaceRouter.patch(...MODIFY_PLACE);

/**
 * Delete a place
 * DELETE /:id
 */
const DELETE_PLACE = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const placeId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {places: placeId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(
          `Cet espace est encore référencé par des activités (${dependentActivitiesList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    const dependentSessionsList = await getDependenciesList(Session, {places: placeId});
    if (dependentSessionsList) {
      return res
        .status(405)
        .send(
          `Cet espace est encore référencé par des sessions (${dependentSessionsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    return await deleteEntity(placeId, Place, req.authenticatedUser, res);
  }
);
PlaceRouter.delete(...DELETE_PLACE);
