import * as Sentry from "@sentry/node";
import http from "http";
import sockjs, {Connection} from "sockjs";
import config from "../config/config";
import {logger} from "./logger";

// All clients websocket messages have at least a type and a user id. Both are associated to a Connection object.
// We can store multiple connections with same type and id (the connection stays unique). We can then store
// metadata associated to a connection.
type ClientInitialMessage = {type: string; id: string};
type Client = ClientInitialMessage & {connection: Connection};

const wsServer = sockjs.createServer();
const clients: Array<Client> = [];

// Logger utils
const clientToString = (c?: Client) => JSON.stringify({...c, connection: c?.connection.id});
const showClients = (text: string) =>
  logger.debug(text, ...clients.map((c) => JSON.parse(clientToString(c))));

wsServer.on("connection", (conn) => {
  // On data received
  conn.on("data", (messageAsString) => {
    const message: Record<string, any> = JSON.parse(messageAsString);

    // If it's a connection message, save it.
    // Else, it can be another data message that we don't want to save.
    if (message.type && message.id) {
      const connectionObject: Client = {
        ...(message as ClientInitialMessage),
        connection: conn,
      };
      logger.info("[websocket] Adding " + clientToString(connectionObject));
      clients.push(connectionObject);
    } else {
      // TODO To be coded later
      // onDataReceived(conn, message);
    }
  });

  // on connection close
  conn.on("close", () => {
    try {
      const connectionToDeleteIndex = clients.findIndex(({connection}) => connection === conn);
      if (connectionToDeleteIndex === -1) {
        logger.error("[websocket] Could not find connection to delete:" + conn.id);
      }

      logger.info("[websocket] Deleting " + clientToString(clients[connectionToDeleteIndex]));
      clients.splice(connectionToDeleteIndex, 1);
    } catch (e) {
      logger.error(e);
      showClients("[websocket] Deleted FAILED");
      Sentry.captureException(e);
    }
  });
});

// /**
//  * What to do on reception of a data message ?
//  * @param conn the current connection from which a message was sent
//  * @param message the message
//  */
// const onDataReceived = (conn: Connection, message: Record<string, any>) => {
//   const connectionObject = clients.find((client) => client.connection.id === conn.id);
//
//   if (connectionObject.type === "concurrentEditing") {
//     // Broadcast information to all clients on the project (same id), except for the client that sent the update
//     const allImpactedConnsExceptSelf = getWsConnections("concurrentEditing", message.id).filter(
//       (c) => c.id !== conn.id
//     );
//     const allImpactedConnsWithSelf = getWsConnections("concurrentEditing", message.id);
//     allImpactedConnsExceptSelf.forEach((conn) => conn.write(JSON.stringify(message)));
//   }
// };

/**
 * UTILS
 */

/**
 * Gets all the connections for a particular type + id
 * @param type the websocket message type
 * @param id the id (can be a user or project id for example)
 */
const getWsConnections = (type: string, id: string): Array<Connection> =>
  clients.filter((c) => c.type === type && c.id === id).map((c) => c.connection);

/**
 * Broadcasts a message to all the messages matching a type + id
 * @param type the websocket message type
 * @param id the id (can be a user or project id for example)
 * @param message the message to send
 */
export const broadcastThroughWebsocket = (
  type: string,
  id: string,
  message: string
): Array<Connection> => {
  // Get all the active connections for user
  const connectionsForId = id ? getWsConnections(type, id) : [];
  // Send to all active connections
  connectionsForId.forEach((conn) => conn.write(message));
  //return the connections just in case
  return connectionsForId;
};

/**
 * SERVER
 */
const server = http.createServer();
wsServer.installHandlers(server, {
  prefix: "/ws",
  log(severity: string, message: string) {
    logger[severity]?.("[websocket] " + message);
  },
});
server.listen(config.wsPort, "0.0.0.0");
