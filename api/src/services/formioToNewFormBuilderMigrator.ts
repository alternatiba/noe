import {FieldComponent} from "@models/project.model";

/**
 * This file is about migrating the legacy FormIO formComponents (was removed in Feb. 2024) to the new form builder data structure.
 * It is used when importing some project import files with legacy FormIO formComponents.
 */

const typeMigration = {
  textfield: "text",
  textarea: "longText",
  selectboxes: "checkboxGroup",
  radio: "radioGroup",
  htmlelement: "content",
  well: "panel",
};

const migrateConditional = ({
  show,
  when,
  eq,
}: {show?: boolean | null; when?: string | null; eq?: string | null} = {}) => {
  if ([show, when, eq].includes(null) || [show, when, eq].includes(undefined)) {
    return undefined;
  } else {
    return {
      show: show ? "show" : "doNotShow",
      when,
      eq,
    };
  }
};

type OptionsArray = Array<{label: string; value: string}>;
const migrateOptions = (values: OptionsArray, dataValues: OptionsArray) => {
  const options = (values || dataValues)?.map(({label, value}) => ({label, value}));
  return options?.length > 0 ? options : undefined;
};

// We can be sure that this are FormIO based formComponents when there are props like "customConditional" inside them.
export const isFormIoFormComponents = (formComponents: Array<any>) =>
  formComponents.find((formComp) => formComp.customConditional !== undefined);

export const migrateFormIOComponentsToNewFormComponents = (
  formComponents: Array<any>,
  formMapping: Array<any>
): Array<FieldComponent> => {
  return formComponents
    .map(
      ({
        input,
        label,
        title,
        placeholder,
        description,
        tooltip,
        disabled,
        data,
        values,
        validate,
        key,
        type: formioType,
        html,
        content,
        conditional,
        components,
      }) =>
        ({
          input,
          label: formioType === "panel" ? title : label,
          description: description || tooltip,
          disabled,
          required: validate?.required,
          key,
          placeholder,
          content: html || content,
          components:
            components?.length > 0
              ? migrateFormIOComponentsToNewFormComponents(components, formMapping)
              : undefined,

          // Legacy form mapping
          displayName: formMapping?.find(({formField}) => formField === key)?.columnName,
          inPdfExport: formMapping?.find(({formField}) => formField === key)?.inPdfExport,

          type: typeMigration[formioType] || formioType,

          conditional: migrateConditional(conditional),
          options: migrateOptions(values, data?.values),
        } satisfies FormComponent as FormComponent)
    )
    .map((formComp) => {
      // Remove undefined values from formComp
      return Object.fromEntries(
        Object.entries(formComp).filter((e) => {
          // For boolean values, we only keep the ones that are true
          if (["disabled", "required"].includes(e[0]) && e[1] === false) return false;

          // For the rest, we keep the ones that are not null or undefined
          return e[1] !== undefined && e[1] !== null && e[1] !== "";
        })
      );
    }) as Array<FormComponent>;
};
