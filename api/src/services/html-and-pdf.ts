import fs from "fs-extra";
import handlebars from "handlebars";
import i18next from "i18next";
import path from "path";
import puppeteer from "puppeteer";

/**
 * Handlebars helpers
 */
handlebars.registerHelper("i18n", function (key) {
  return i18next.t(key);
});

/**
 * Handlebars HTML compilation generator
 * This function saves the generated templates and returns them when we need them.
 * @param fileName the name of the template to use
 */
const compiledTemplates = {};
export async function compileHtmlTemplate(
  fileName: string,
  context: Record<any, any>
): Promise<any> {
  if (!compiledTemplates[fileName]) {
    const filePath = path.join(__dirname, "..", "templates", `${fileName}.hbs`);
    const htmlString = await fs.readFile(filePath, "utf-8");
    compiledTemplates[fileName] = handlebars.compile(htmlString);
  }
  return compiledTemplates[fileName](context);
}

export async function printPdf(html: string): Promise<Buffer> {
  const browser = await puppeteer.launch({
    headless: true,
    args: ["--no-sandbox", "--disable-setuid-sandbox", "--disable-dev-shm-usage"],
  });
  const page = await browser.newPage();
  await page.setContent(html);
  const pdfBlob = await page.pdf({timeout: 90000});

  await browser.close();
  return pdfBlob;
}
