import {config as migrateMongoConfig, database, MigrationStatus, status} from "migrate-mongo";
import config from "../config/config";

// Get the config from the local config file, but change the database url with the appropriate one
import migrateMongoConfigOptions from "./../../migrate-mongo-config";
import {logger} from "./logger";

migrateMongoConfigOptions.mongodb.url = config.mongoose.uri;

migrateMongoConfig.set(migrateMongoConfigOptions as Partial<migrateMongoConfig.Config>);

export const checkDatabaseMigrationStatus = async (): Promise<void> => {
  const {db, client} = await database.connect();
  try {
    const migrationStatus = await status(db);
    const pendingMigrations = migrationStatus.filter(
      (migration: MigrationStatus) => migration.appliedAt === "PENDING"
    );
    if (pendingMigrations.length > 0) {
      if (config.env === "production") {
        throw new Error(
          "Some database migrations are still pending. Please run them by running `npx migrate-mongo up`."
        );
      } else {
        logger.error(
          "WARNING: Some database migrations are still pending. Please run them by running `npx migrate-mongo up`."
        );
      }
    }
  } finally {
    await client.close();
  }
};
