import {fakerFR} from "@faker-js/faker";
import {Activity, ActivityD} from "@models/activity.model";
import {Category, CategoryD} from "@models/category.model";
import {Place, PlaceD} from "@models/place.model";
import {ProjectD} from "@models/project.model";
import {Registration, RegistrationD} from "@models/registration.model";
import {Session, SessionD} from "@models/session.model";
import {Slot} from "@models/slot.model";
import {Steward, StewardD} from "@models/steward.model";
import {Team, TeamD} from "@models/team.model";
import {User, UserD} from "@models/user.model";
import {
  isFormIoFormComponents,
  migrateFormIOComponentsToNewFormComponents,
} from "@services/formioToNewFormBuilderMigrator";
import {logger} from "@services/logger";
import {updateManyToManyRelationship} from "@utils/controllers/updateManyToManyRelationship";
import {personName} from "@utils/utilities";
import {UniqueEnforcer} from "enforce-unique";
import {Document, Model, ObjectId} from "mongoose";

const emailUniqueEnforcer = new UniqueEnforcer();

const generatePhone = () =>
  fakerFR.helpers.fromRegExp("+33 [67] [0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2}");

const generateUserInfo = () => {
  const firstName = fakerFR.person.firstName();
  const lastName = fakerFR.person.lastName();
  const password = fakerFR.string.alphanumeric(30);
  const email = emailUniqueEnforcer.enforce(() =>
    fakerFR.internet.email({firstName, lastName}).toLowerCase()
  );

  return {firstName, lastName, email, password};
};

const anonymizeRegistrations = (registrations?: Array<RegistrationD & {user: UserD}>) =>
  registrations?.map((registration) => {
    return {
      ...registration.toObject(),
      user: generateUserInfo(),
    };
  });

const anonymizeStewards = (
  stewards?: Array<StewardD>,
  registrations?: Array<RegistrationD & {user: UserD}>,
  keepStewardNames = false
) =>
  stewards?.map((steward) => {
    const linkedRegistration = registrations?.find(
      (registration) => registration.steward === steward._id
    );

    const phoneNumber = steward.phoneNumber ? generatePhone() : undefined;
    const stewardInfo = linkedRegistration?.user || generateUserInfo();

    return {
      ...steward.toObject(),
      ...(!keepStewardNames && {
        firstName: stewardInfo.firstName,
        lastName: stewardInfo.lastName,
      }),
      phoneNumber,
    };
  });

//// Export helper functions
/**
 * Allows to export all data linked to a project
 * @param projectId the project ID
 * @param withRegistrations if you want to export the project's registrations as well, or not
 * @param anonymized if you want to export with anonymized registrations and stewards
 * @param keepStewardNames if anonymized=true but you want to keep the stewards names the same
 */
export async function getAllEntitiesLinkedToProject(
  projectId: string,
  {withRegistrations = false, anonymized = false, keepStewardNames = false} = {}
): Promise<Record<string, Document[]>> {
  // find all entities linked to this project and store them in promises

  const projection = "-createdAt -updatedAt -id -project -__v";

  const query = {project: projectId};
  const [activities, categories, places, sessions, stewards, teams] = await Promise.all([
    Activity.find(query, projection),
    Category.find(query, projection),
    Place.find(query, projection),
    Session.find(query, projection).populate({path: "slots", select: projection}),
    Steward.find(query, projection),
    Team.find(query, projection),
  ]);
  const projectData: any = {activities, categories, places, sessions, stewards, teams};

  if (withRegistrations) {
    projectData.registrations = projectData.registrations = await Registration.find(
      query,
      projection
    ).populate({path: "user", select: projection});
  }

  if (anonymized) {
    projectData.registrations = anonymizeRegistrations(projectData.registrations);
    projectData.stewards = anonymizeStewards(
      projectData.stewards,
      projectData.registrations,
      keepStewardNames
    );
  }

  return projectData;
}

//// Import helper functions and types

type WithImportedId<T> = {
  importedId?: string;
} & T;

export type ProjectImportPayload = {
  activities: Array<
    WithImportedId<ActivityD> & {
      category?: ObjectId;
      places?: Array<ObjectId>;
      stewards?: Array<ObjectId>;
    }
  >;
  categories: Array<WithImportedId<CategoryD>>;
  places: Array<WithImportedId<PlaceD>>;
  project: Partial<ProjectD & {formMapping: Array<any>}>; // There can be some legacy formMappings in the import
  registrations: Array<WithImportedId<RegistrationD & {user: WithImportedId<UserD>}>>;
  sessions: Array<
    WithImportedId<SessionD> & {
      activity?: ObjectId;
      places?: Array<ObjectId>;
      stewards?: Array<ObjectId>;
      team?: ObjectId;
    }
  >;
  stewards: Array<StewardD>;
  teams: Array<WithImportedId<TeamD>>;
};

/**
 * Creates new elements of a particular type in batch
 * @param importedElements the imported elements we want to save in the DB
 * @param createElementAction the element creation function that returns an element to save in the DB
 */
export async function createNewElements(
  importedElements: Array<any>,
  createElementAction: (importedElement: any) => any
) {
  const newElements: Array<any> = [];
  for (const importedElement of importedElements) {
    const newElement = createElementAction(importedElement);
    await newElement.save();
    newElements.push(newElement);
  }
  return newElements;
}

/**
 * Deletes all existing elements of a particular type in batch for a given project
 * @param ElementModel the Model of the element
 * @param projectId the project ID
 * @param beforeDeleteAction if we need to do a particular operation with the element before deleting it
 */
export async function deleteExistingElements(
  ElementModel: any,
  projectId: any,
  beforeDeleteAction?: (oldElements: any) => any
) {
  const oldElements = await ElementModel.find({
    $and: [{project: projectId}, {deletedAt: {$exists: false}}],
  });
  beforeDeleteAction && beforeDeleteAction(oldElements);
  for (const oldElement of oldElements) {
    oldElement.deletedAt = new Date();
    await oldElement.save({validateBeforeSave: false});
  }
}

/**
 * Prepares the imported data with some operations with ids
 * @param importedElements the aray of imported elements
 * @param projectId the project ID
 */
export function prepareImportedData<T extends {_id?: string}>(
  importedElements: Array<T>,
  projectId?: ObjectId
) {
  return (
    importedElements?.map((el) => ({
      ...el,
      importedId: el._id,
      _id: undefined,
      project: projectId,
    })) || []
  );
}

export async function processProjectImport(
  importedProject: Partial<ProjectImportPayload>,
  existingProject: ProjectD,
  additiveImportForElements: boolean,
  withRegistrations: boolean
) {
  const notFoundInImportIds: string[] = [];

  // Return the id, or undefined if the importedID is null
  function getByImportedId<T extends {importedId?: any; _id?: any}>(
    list: Array<T>,
    importedId: any
  ): string {
    try {
      return importedId ? list.find((el) => el.importedId === importedId)._id : undefined;
    } catch {
      if (!notFoundInImportIds.includes(importedId)) notFoundInImportIds.push(importedId);
    }
  }

  function getByImportedIdArray<T extends {importedId?: any; _id?: any}>(
    list: Array<T>,
    importedIdArray: any[]
  ): string[] {
    return importedIdArray?.map((el: string) => getByImportedId(list, el)).filter((el) => !!el);
  }

  // Extract data from the imported project
  let {
    activities: importedActivities,
    stewards: importedStewards,
    sessions: importedSessions,
    places: importedPlaces,
    categories: importedCategories,
    teams: importedTeams,
    registrations: importedRegistrations,
  } = importedProject;

  // Give each imported element an importedId, and link to the existing project + remove the _id so it can be recreated by Mongoose

  importedActivities = prepareImportedData(importedActivities, existingProject._id);
  importedStewards = prepareImportedData(importedStewards, existingProject._id);
  importedSessions = prepareImportedData(importedSessions, existingProject._id);
  importedPlaces = prepareImportedData(importedPlaces, existingProject._id);
  importedCategories = prepareImportedData(importedCategories, existingProject._id);
  importedTeams = prepareImportedData(importedTeams, existingProject._id);

  // If it's an additive import, don't delete the elements
  if (!additiveImportForElements) {
    await Promise.all([
      deleteExistingElements(Place, existingProject._id),
      deleteExistingElements(Category, existingProject._id),
      deleteExistingElements(Steward, existingProject._id, (oldStewards) =>
        oldStewards?.forEach((el: StewardD) =>
          Registration.updateMany({steward: el._id}, {$unset: {steward: true}})
        )
      ),
      deleteExistingElements(Activity, existingProject._id),
      deleteExistingElements(Slot, existingProject._id),
      // For teams and sessions, we need to unsubscribe people before deleting them
      deleteExistingElements(Session, existingProject._id, (oldSessions) =>
        oldSessions?.forEach((el: SessionD) =>
          updateManyToManyRelationship(el, [], Registration, "session", "sessionsSubscriptions")
        )
      ),
      deleteExistingElements(Team, existingProject._id, (oldTeams) =>
        oldTeams?.forEach((el: TeamD) =>
          updateManyToManyRelationship(el, [], Registration, "team", "teamsSubscriptions")
        )
      ),
    ]);
  }

  const newPlaces: Array<PlaceD> = await createNewElements(
    importedPlaces,
    (importedEl) => new Place({...importedEl})
  );
  const newCategories: Array<CategoryD> = await createNewElements(
    importedCategories,
    (importedEl) => new Category({...importedEl})
  );
  const newStewards: Array<StewardD> = await createNewElements(
    importedStewards,
    (importedEl) => new Steward({...importedEl})
  );
  const newActivities: Array<ActivityD> = await createNewElements(
    importedActivities,
    (importedActivity) => {
      return new Activity({
        ...importedActivity,
        category: getByImportedId(newCategories, importedActivity.category),
        stewards: getByImportedIdArray(newStewards, importedActivity.stewards),
        places: getByImportedIdArray(newPlaces, importedActivity.places),
      } as ActivityD);
    }
  );

  const newTeams: Array<TeamD> = await createNewElements(importedTeams, (importedTeam) => {
    return new Team({
      ...importedTeam,
      activity: getByImportedId(newActivities, importedTeam.activity),
    });
  });

  const newSessions: SessionD[] = [];
  for (const session of importedSessions) {
    const sessionSlots = prepareImportedData(session.slots, existingProject._id);
    const newSlots = await createNewElements(sessionSlots, (slot) => new Slot(slot));

    const newSession = new Session({
      ...session,
      slots: newSlots.map((s) => s._id),
      activity: getByImportedId(newActivities, session.activity),
      stewards: getByImportedIdArray(newStewards, session.stewards),
      places: getByImportedIdArray(newPlaces, session.places),
      // TODO allow a session to be used by multiple teams ?
      team: getByImportedId(newTeams, session.team),
    });
    await newSession.save();
    newSessions.push(newSession);
  }

  if (withRegistrations) {
    // Set imported Ids for registration objects
    importedRegistrations = prepareImportedData(importedRegistrations, existingProject._id);

    for (const importedRegistration of importedRegistrations) {
      // TODO: right now, the info about the subscribedBy is not properly reimported if the user doesn't exist yet /!\

      // Replace the source ID
      importedRegistration.user.importedId = importedRegistration.user._id;
      delete importedRegistration.user._id;

      // First check if the user exists and create if it doesn't
      const existingUser = await User.findOne({email: importedRegistration.user.email});
      const user = existingUser || (await User.create(importedRegistration.user));

      // Now that we created the user, replace it with the id in the registration
      importedRegistration.user = user._id;

      // If this registration exists already and has already a registration, don't import it.
      // Else, import it in hidden mode
      const existingRegistration = await Registration.findOne({
        user: importedRegistration.user,
        project: existingProject._id,
      });
      if (!existingRegistration) {
        const teamsSubscriptions = importedRegistration.teamsSubscriptions?.map(
          (teamSubscription) => ({
            ...teamSubscription,
            team: getByImportedId(newTeams, teamSubscription.team),
          })
        );
        const sessionsSubscriptions = importedRegistration.sessionsSubscriptions
          ?.map(
            (sessionSubscription) =>
              getByImportedId(newSessions, sessionSubscription.session) && {
                ...sessionSubscription,
                session: getByImportedId(newSessions, sessionSubscription.session),
                team: getByImportedId(newTeams, sessionSubscription.team),
              }
          )
          .filter((el) => !!el);

        await Registration.create({
          ...importedRegistration,
          hidden: true,
          sessionsSubscriptions,
          teamsSubscriptions,
        });

        if (!existingUser) {
          logger.debug(
            `Created new user ${personName(user)} without password + related hidden registration`
          );
        } else {
          logger.debug(`Created related hidden registration for existing user ${personName(user)}`);
        }
      } else {
        logger.debug(
          `Existing registration found for existing user ${personName(
            user
          )}. Not modifying anything.`
        );
      }
    }
  }

  // Get all the data except the project name + be safe if no project object provided
  const {name, formMapping, slug, slug_history, ...importedProjectData} =
    importedProject.project || {};

  // Migrate any legacy FormIO formComponents to the new system
  const isFormIO =
    importedProjectData.formComponents?.length > 0 &&
    isFormIoFormComponents(importedProjectData.formComponents);
  if (isFormIO)
    importedProjectData.formComponents = migrateFormIOComponentsToNewFormComponents(
      importedProjectData.formComponents,
      formMapping
    );

  return {importedProjectData, notFoundInImportIds};
}

//// Rollback feature

export async function rollbackDeletedElements(
  model: Model<any>,
  existingProjectId: string,
  startDate: Date
): Promise<void> {
  const deletedElementsToRollback = await model.find({
    searchAlsoInDeletedElements: true,
    $and: [{project: existingProjectId}, {deletedAt: {$gte: startDate}}],
  });
  for (const deletedElement of deletedElementsToRollback) {
    logger.debug(`Rolled back element ${deletedElement._id}`);
    deletedElement.deletedAt = undefined;
    try {
      await deletedElement.save({validateBeforeSave: false});
    } catch (e) {
      console.error(e);
    }
  }
}
