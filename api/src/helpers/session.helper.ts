import {RegistrationD} from "@models/registration.model";
import {Session, SessionD, sessionMethods} from "@models/session.model";
import {SlotD} from "@models/slot.model";
import {StewardD} from "@models/steward.model";
import {TeamD} from "@models/team.model";
import * as Sentry from "@sentry/node";
import {logger} from "@services/logger";
import {lightSelection} from "@utils/lightSelection";
import moment from "moment";
import {getSlotsStartAndEnd} from "./slot.helper";
import {ObjectId} from "mongoose";
import {PlaceD} from "@models/place.model";

export const readSessionQuery = (id: string, projectId: string) =>
  Session.findOne({_id: id, project: projectId}).populate([
    {path: "stewards"},
    {path: "places"},
    {path: "activity", populate: ["stewards", "places", "category"]},
    {path: "slots"},
    {path: "team", select: lightSelection.team},
  ]);

export const readSessionAdditionalActions =
  (
    registration: RegistrationD & {steward?: StewardD | null},
    isRequestedFromInscriptionFrontend = true
  ) =>
  async (session: SessionD): Promise<SessionD> => {
    // User is allowed to see participants if...
    const canSendAllParticipantsInfo = !!(
      registration && // a registration exists...
      (registration.role || // ... and it is either an orga with a role
        (registration.steward && // ... or a steward on the current session
          session.stewards.find(
            (steward: StewardD) => steward._id.toString() === registration.steward.toString()
          )))
    );

    // Populate the participants, and send the whole data only if we're allowed to
    await sessionMethods.populateParticipants(session, canSendAllParticipantsInfo);

    const sessionData = session.toObject();
    if (isRequestedFromInscriptionFrontend) {
      await sessionMethods.computeSameTimeSessions(session, registration);
      Object.assign(sessionData, {
        subscribed: registration
          ? registration.sessionsSubscriptions
              .map((ss) => ss.session.toString())
              .includes(session._id.toString())
          : false,
        isSteward: isStewardForSession(session, registration),
        sameTimeSessions: session.sameTimeSessions,
      });
    }

    if (canSendAllParticipantsInfo) sessionData.participants = session.participants;
    sessionData.numberParticipants = session.numberParticipants;
    return sessionData as SessionD;
  };

export const isStewardForSession = (
  session: SessionD,
  currentRegistration: RegistrationD & {steward?: null | StewardD}
): boolean => {
  if (!currentRegistration?.steward) return false;
  const registrationStewardId = currentRegistration.steward._id.toString();
  return session.stewards.some((steward) => steward._id.toString() === registrationStewardId);
};

export type InconsistencyForEntity = {
  type: "alreadyUsedEntity" | "availabilitiesOverlap";
  entity: string; //entity name
  entitiesInvolved: string[]; //ids
};

export const inconsistenciesForEntities = (
  sessionsOfProject: SessionD[],
  entityName: string,
  arrayOfEntitiesName: "stewards" | "places",
  slotTested: SlotD,
  sessionDraftTested: Partial<SessionD>
) => {
  const inconsistenciesFound = [];

  // If in sync, check the session entities, else check the slot entities
  const entitiesTested = sessionDraftTested?.[arrayOfEntitiesName];

  if (entitiesTested?.length > 0) {
    const entitiesWithAvailabilitiesOverlap: typeof entitiesTested = [];
    entitiesTested.forEach((entity) => {
      // If the entity has availabilities, check them. Else, do not check.
      if (entity.availabilitySlots?.length > 0) {
        // If the entity has an availability during the slot's time, that's great
        const anAvailabilityExists = entity.availabilitySlots?.find(
          (slot) =>
            moment(slot.start) <= moment(slotTested.start) &&
            moment(slot.end) >= moment(slotTested.end)
        );
        // If the entity is unavailable, that is an inconsistency
        if (!anAvailabilityExists) entitiesWithAvailabilitiesOverlap.push(entity as any);
      }
    });
    if (entitiesWithAvailabilitiesOverlap.length > 0) {
      inconsistenciesFound.push({
        type: "availabilitiesOverlap",
        entity: entityName,
        entitiesInvolved: entitiesWithAvailabilitiesOverlap,
      });
    }

    // Check if entity is still available
    const alreadyUsedEntities: typeof entitiesTested = [];
    sessionsOfProject.forEach((session: SessionD) => {
      session.slots.forEach((existingSlot: SlotD) => {
        if (existingSlot._id != slotTested._id) {
          const existingEntitiesIds = sessionDraftTested[arrayOfEntitiesName]?.map(
            (entity) => entity._id
          );
          if (
            moment(slotTested.start) < moment(existingSlot.end) &&
            moment(slotTested.end) > moment(existingSlot.start)
          ) {
            const overlappingEntities = entitiesTested.filter((entityTested) =>
              existingEntitiesIds.includes(entityTested._id)
            );
            alreadyUsedEntities.push(...(overlappingEntities as any[]));
          }
        }
      });
    });
    if (alreadyUsedEntities.length > 0) {
      inconsistenciesFound.push({
        type: "alreadyUsedEntity",
        entity: entityName,
        entitiesInvolved: alreadyUsedEntities,
      });
    }
  }
  return inconsistenciesFound;
};

// A copy of this function is also available in both frontends (with small modifs)
export const slotNumberString = (slot: SlotD) => {
  const numberOfSlotsInSession = slot.session.slots.length;
  return numberOfSlotsInSession > 1
    ? "(" +
        (slot.session.slots.findIndex((s) => s.toString() === slot._id.toString()) + 1) +
        "/" +
        numberOfSlotsInSession +
        ") "
    : "";
};

// A copy of this function is also available in both frontends (with small modifs)
export const getSessionName = (session: SessionD, teams: Array<TeamD>): string => {
  const sessionName = session.name?.length > 0 ? session.name + " - " : "";
  const teamName = teams?.find((t) => t._id.toString() === session.team?.toString())?.name;
  const sessionNameSuffix = teamName ? ` (${teamName})` : "";
  return sessionName + session.activity?.name + sessionNameSuffix;
};

export const validateSessionStartAndEnd = (session: SessionD) => {
  const errorMessages = [];
  if (session.slots?.length === 0) errorMessages.push("There are no session slots");
  if (!session.start || !session.end) errorMessages.push("Session start or end do not exist");
  const {start: calculatedStart, end: calculatedEnd} = getSlotsStartAndEnd(session?.slots);

  // Correct dates if needed
  if (!moment(session.start).isSame(calculatedStart)) {
    errorMessages.push("start does not match");
    session.start = calculatedStart.toDate();
  }
  if (!moment(session.end).isSame(calculatedEnd)) {
    errorMessages.push("end does not match");
    session.end = calculatedEnd.toDate();
  }

  if (errorMessages.length > 0) {
    logger.error("Session start and end synchronization error : " + errorMessages.join(", "));
    Sentry.captureException(
      "Session start and end synchronization error : " + errorMessages.join(", ")
    );
  }
};
