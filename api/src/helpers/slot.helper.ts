import {AvailabilitySlotD} from "@models/availabilitySlot.model";
import moment from "moment";

export const getSlotsStart = (slots: Array<AvailabilitySlotD>) => {
  return slots?.length > 0 ? moment.min(slots.map((slot) => moment(slot.start))) : undefined;
};

export const getSlotsEnd = (slots: Array<AvailabilitySlotD>) => {
  return slots?.length > 0 ? moment.max(slots.map((slot) => moment(slot.end))) : undefined;
};

/**
 * Returns the earliest start and latest end of all slots given
 */
export const getSlotsStartAndEnd = (slots: Array<AvailabilitySlotD>) => {
  return {start: getSlotsStart(slots), end: getSlotsEnd(slots)};
};
