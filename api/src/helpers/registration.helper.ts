import {AvailabilitySlotD} from "@models/availabilitySlot.model";
import {SlotD} from "@models/slot.model";

export const flattenFieldComponents = (components: Array<any>) => {
  if (!components) return [];

  const tree: Array<any> = components.map((comp) => {
    if (comp.input) {
      // If the component is a simple one, return it
      return comp;
    } else {
      let branchs: Array<any> = [];
      // Put together all child components present in the "components" and "columns" arguments, and then apply recursively the flattenFieldComponents function on it
      if (comp.components) branchs = branchs.concat(comp.components);
      if (comp.columns) branchs = branchs.concat(comp.columns);
      return flattenFieldComponents(branchs);
    }
  });
  // Flatten the return value
  return tree.flat();
};

// Same function as in the frontends, with a few differences
export const cleanAnswer = (answer: any, separator = ",\n", fieldComp: any = {}) => {
  const {type, options} = fieldComp;

  const findLabelForOption = (optValue: any) =>
    options?.find((opt: any) => opt.value === optValue)?.label || optValue;

  if (type === "checkboxGroup" || type === "multiSelect") {
    return answer?.map(findLabelForOption).join(separator);
  } else if (type === "checkbox") {
    return answer ? "Yes" : "No";
  } else if (type === "select" || type === "radioGroup") {
    return findLabelForOption(answer);
  } else {
    return answer;
  }
};

// This function is duplicated in /xxxxxxx-front/src/helpers/sessionsUtilities.js
export const isParticipantAvailable = (
  sessionSlots: SlotD[],
  availabilitySlots: AvailabilitySlotD[]
) => {
  if (!sessionSlots || !availabilitySlots) return false;

  for (const sessionSlot of sessionSlots) {
    const slotStart = sessionSlot.start;
    const slotEnd = sessionSlot.end;
    const compatibleUserAvailability = availabilitySlots.find((availabilitySlot) => {
      const availabilitySlotStart = availabilitySlot.start;
      const availabilitySlotEnd = availabilitySlot.end;

      // Return true if the slot is withing the user availability
      return availabilitySlotStart <= slotStart && availabilitySlotEnd >= slotEnd;
    });

    // If one of the slots is not compatible, then return false
    if (!compatibleUserAvailability) return false;
  }

  return true;
};
