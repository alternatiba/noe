module.exports = {
  async up(db, client) {
    await db
      .collection("registrations")
      .updateMany({specific: {$exists: true}}, {$rename: {specific: "formAnswers"}});
  },

  async down(db, client) {
    await db
      .collection("registrations")
      .updateMany({formAnswers: {$exists: true}}, {$rename: {formAnswers: "specific"}});
  },
};
