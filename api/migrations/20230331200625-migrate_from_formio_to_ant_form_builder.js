// Imported from NOE code
const flattenFieldComponents = (components) => {
  if (!components) return [];

  const tree = components.map((comp) => {
    if (comp.input) {
      // If the component is a simple one, return it
      return comp;
    } else {
      let branchs = [];
      // Put together all child components present in the "components" and "columns" arguments, and then apply recursively the flattenFieldComponents function on it
      if (comp.components) branchs = branchs.concat(comp.components);
      if (comp.columns) branchs = branchs.concat(comp.columns);
      return flattenFieldComponents(branchs);
    }
  });
  // Flatten the return value
  return tree.flat();
};

async function changeFormAnswers(db, getNewAnswer) {
  // *********** Fetch all registrations and projects with some form data
  const registrations = await db
    .collection("registrations")
    .find({specific: {$exists: true}})
    .project({specific: true, project: true})
    .toArray();
  const projects = (
    await db.collection("projects").find().project({formComponents: true}).toArray()
  ).map((project) => ({
    ...project,
    formComponents: flattenFieldComponents(project.formComponents).map(({type, key, values}) => ({
      type,
      key,
      values,
    })),
  }));

  let matched = 0;
  let modified = 0;
  // *********** For each registration change the form responses so they are properly read by the new form
  for (const registration of registrations) {
    // Get the project's formComponents
    const formComponents = projects.find((p) => p._id.equals(registration.project)).formComponents;

    const newAnswers = {};

    for (const formComponent of formComponents) {
      const oldValue = registration.specific[formComponent.key];
      if (oldValue && oldValue !== [] && oldValue !== {} && oldValue !== "") {
        newAnswers[formComponent.key] = getNewAnswer(formComponent, oldValue);
      }
    }
    const {matchedCount, modifiedCount} = await db
      .collection("registrations")
      .updateOne({_id: registration._id}, {$set: {specific: newAnswers}});
    modified += modifiedCount;
    matched += matchedCount;
  }

  console.info(`SUCCESS: ${matched} were matched and ${modified} were modified.`);
}

module.exports = {
  async up(db, client) {
    function formioToAntdFormBuilder({type, key, values: formOptions}, oldValue) {
      // Transform or copy the formAnswers according to the project's formComponents
      if (type === "selectboxes") {
        if (!Array.isArray(oldValue)) {
          const newValue = Object.entries(oldValue)
            .filter(([_, val]) => !!val) // Remove the false values
            .map(([key, _]) => key); // Keep the positive keys
          console.log("SELECTBOXES", oldValue, "->", newValue);
          return newValue.length > 0 ? newValue : undefined;
        } else {
          console.error(
            "WARNING: SELECTBOXES value not conform. Returning old value:",
            key,
            oldValue
          );
        }
      }
      return oldValue;
    }

    await changeFormAnswers(db, formioToAntdFormBuilder);
  },

  async down(db, client) {
    function antdFormBuilderToFormio({type, key, values: formOptions}, oldValue) {
      // Transform or copy the formAnswers according to the project's formComponents
      if (type === "selectboxes") {
        if (Array.isArray(oldValue)) {
          const newValue = Object.fromEntries(
            formOptions?.map((o) => [o.value, oldValue.includes(o.value)]) || []
          );
          console.log("SELECTBOXES", oldValue, "->", newValue);
          return newValue;
        } else {
          console.error(
            "WARNING: SELECTBOXES value not conform. Returning old value:",
            key,
            oldValue
          );
        }
      }
      return oldValue;
    }

    await changeFormAnswers(db, antdFormBuilderToFormio);
  },
};
