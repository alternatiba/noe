module.exports = {
  async up(db, client) {
    const projects = await db
      .collection("projects")
      .find({cockpitPreferences: {$exists: true}})
      .toArray();

    for (const project of projects) {
      if (
        project.cockpitPreferences?.charts?.useful &&
        !project.cockpitPreferences?.charts?.useful.includes("numberOfParticipantsByHourStats")
      ) {
        project.cockpitPreferences.charts.useful.splice(
          Math.min(project.cockpitPreferences.charts.useful?.length, 1),
          0,
          "numberOfParticipantsByHourStats"
        );
        await db
          .collection("projects")
          .updateOne({_id: project._id}, {$set: {cockpitPreferences: project.cockpitPreferences}});
      }
    }
  },

  async down(db, client) {
    const projects = await db
      .collection("projects")
      .find({cockpitPreferences: {$exists: true}})
      .toArray();

    for (const project of projects) {
      if (project.cockpitPreferences?.charts?.useful.includes("numberOfParticipantsByHourStats")) {
        const cockpitPreferences = {
          ...project.cockpitPreferences,
          charts: {
            ...project.cockpitPreferences.charts,
            useful: project.cockpitPreferences?.charts?.useful.filter(
              (chartKey) => chartKey !== "numberOfParticipantsByHourStats"
            ),
          },
        };
        await db.collection("projects").updateOne({_id: project._id}, {$set: {cockpitPreferences}});
      }
    }
  },
};
