const {pick} = require("lodash");
const mapAllComponents = (components, mapFn) =>
  components?.map((comp) => {
    comp = mapFn(comp);
    if (comp.components?.length > 0) {
      comp.components = mapAllComponents(comp.components, mapFn);
    }
    return comp;
  });

module.exports = {
  async up(db, client) {
    const upFunction = (comp) => {
      const {
        inListView,
        inPdfExport,
        inOrgaFilters,
        inParticipantFilters,
        inSessionsCards,
        inParticipantSessionView,
        ...restComp
      } = comp;
      const displayOptions = [];

      inListView && displayOptions.push("inListView");
      inPdfExport && displayOptions.push("inPdfExport");
      inOrgaFilters && displayOptions.push("inOrgaFilters");
      inParticipantFilters && displayOptions.push("inParticipantFilters");
      inSessionsCards && displayOptions.push("inSessionsCards");
      inParticipantSessionView && displayOptions.push("inParticipantSessionView");
      return {displayOptions, ...restComp};
    };

    const projects = await db
      .collection("projects")
      .find({$or: [{formComponents: {$exists: true}}, {customFieldsComponents: {$exists: true}}]})
      .toArray();
    console.log(`${projects.length} projects`);

    for (const project of projects) {
      const newFormComps = mapAllComponents(project.formComponents, upFunction);
      const newCustomFieldsComps = mapAllComponents(project.customFieldsComponents, upFunction);

      await db.collection("projects").updateOne(
        {_id: project._id},
        {
          $set: {formComponents: newFormComps, customFieldsComponents: newCustomFieldsComps},
        }
      );
    }
  },

  async down(db, client) {
    const downFunction = (comp) => {
      const {displayOptions, ...restComp} = comp;

      if (displayOptions.includes("inListView")) restComp["inListView"] = true;
      if (displayOptions.includes("inPdfExport")) restComp["inPdfExport"] = true;
      if (displayOptions.includes("inOrgaFilters")) restComp["inOrgaFilters"] = true;
      if (displayOptions.includes("inParticipantFilters")) restComp["inParticipantFilters"] = true;
      if (displayOptions.includes("inSessionsCards")) restComp["inSessionsCards"] = true;
      if (displayOptions.includes("inParticipantSessionView"))
        restComp["inParticipantSessionView"] = true;

      return restComp;
    };

    const projects = await db
      .collection("projects")
      .find({$or: [{formComponents: {$exists: true}}, {customFieldsComponents: {$exists: true}}]})
      .toArray();
    console.log(`${projects.length} projects`);

    for (const project of projects) {
      const newFormComps = mapAllComponents(project.formComponents, downFunction);
      const newCustomFieldsComps = mapAllComponents(project.customFieldsComponents, downFunction);

      await db.collection("projects").updateOne(
        {_id: project._id},
        {
          $set: {formComponents: newFormComps, customFieldsComponents: newCustomFieldsComps},
        }
      );
    }
  },
};
