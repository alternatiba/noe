module.exports = {
  async up(db, client) {
    const projectsToClean = await db
      .collection("projects")
      .find({
        $or: [{formComponentsBackup: {$exists: true}}, {formMappingBackup: {$exists: true}}],
      })
      .toArray();
    console.log(
      `Removing formComponentsBackup and formMappingBackup from ${projectsToClean.length} projects`
    );

    for (const project of projectsToClean) {
      await db.collection("projects").updateOne(
        {_id: project._id},
        {
          $unset: {
            formComponentsBackup: true,
            formMappingBackup: true,
          },
        }
      );
    }
  },

  async down(db, client) {},
};
