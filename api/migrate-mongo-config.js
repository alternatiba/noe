const path = require("path");
require("dotenv").config({path: path.join(__dirname, "../docker/prod-env/.env.api")});

const localDbUri = "mongodb://noe:noe@localhost:27017/main";

const dbUriIsGiven = process.env.MONGODB_URI && process.env.MONGODB_URI.length > 0;

let dbUri;

if (process.env.LOCAL === "true") {
  console.log("Using local mongo uri");
  dbUri = localDbUri;
} else {
  // If launched from cmd line (no NODE_ENV) and db uri has been found, we must have RUN_WITH_PROD_URI set to true,
  // otherwise we fail to protect the production from bad manipulation.
  if (dbUriIsGiven && !process.env.NODE_ENV && process.env.RUN_WITH_PROD_URI !== "true") {
    console.error(
      "A production Mongo URI was found, but you haven't set the variable RUN_WITH_PROD_URI to true. Failing.",
      "\nTo allow this, run `RUN_WITH_PROD_URI=true npx migrate-mongo <your_command>",
      "\nTo run only on the local database, run `LOCAL=true npx migrate-mongo <your_command>\n"
    );
    throw Error();
  }

  dbUri = dbUriIsGiven ? process.env.MONGODB_URI : localDbUri;
}

// If launched from command line only, give some insights. Else, when called from the api app, remain silent.
if (!process.env.NODE_ENV) {
  const isLocalDb = dbUri.includes("@localhost") || dbUri.includes("@db");
  console.log(
    isLocalDb ? "=== Connecting to local Mongo DB ===" : "=== Connecting to remote Mongo DB ===",
    `\n > ${dbUri.replace(/\/\/.*:.*@/, "// ... : ... @")}\n`
  );
}

module.exports = {
  mongodb: {
    // The uri to your MongoDB database
    url: dbUri,
  },

  // Migrations directory. Can be a relative or absolute path. Only edit this when really necessary.
  migrationsDir: "migrations",

  // The mongodb collection where the applied changes are stored. Only edit this when really necessary.
  changelogCollectionName: "migrations",

  // Don't change this, unless you know what you're doing
  moduleSystem: "commonjs",
};
