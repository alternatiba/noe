![NOÉ](shared/images/readme-banner.jpg)

# Contribuer au projet NOÉ

## Table des matières

[[_TOC_]]

## Structure de l'application

### Division en micro-services

L'application est divisée en cinq micro-services :

- deux frontends qui vivent dans `orga-front/` (frontend orga) et `inscription-front/` (frontend participant⋅e)
- un backend API qui vit dans `api/`
- un site internet contenant la documentation, qui vit dans `website/`

![Structure de l'application](https://docs.google.com/drawings/d/e/2PACX-1vQExKtbzUq3-qQ0I9ttBDyS4UmYkFu55WhCMGmT2OQtvLZxE4SStQ9Cc8F0sHSfdIDdXQTAKfof9PkA/pub?w=1102&h=744)

> Un second backend dédié aux calculs d'IA qui vivait dans dans `ia-back/` mais n'existe plus car il n'était pas utilisé. L'archive est dispo sur le dépôt Gitlab [noe-app/ia](https://gitlab.com/noe-app/ia).

### Stack technique

#### Frontends orga et participant⋅e - `orga-front/` et `inscription-front/`

- **Base :** Javascript
- **Frameworks :** ReactJS / Redux
- **Librairie graphique :** [Ant.Design](https://ant.design/)

#### Serveur backend API - `api/`

- **Base :** NodeJS / Typescript (compilé en Javascript avec `esbuild`)
- **Framework :** Express
- **Base de données :** Mongoose (base MongoDB)

#### Site internet - `website/`

Elle est basée sur [Docusaurus](https://docusaurus.io/).

### Conteneurisation

L'application est entièrement conteneurisée dans des containers Docker. Nous utilisons Docker Compose pour gérer le
lancement des containers.

La configuration de l'application est décrite dans les fichiers `docker-compose.yml` à la racine du projet.

> **Comment installer ça ?**
>
> Pour Linux, allez voir [ici pour Docker](https://linuxize.com/post/how-to-install-and-use-docker-on-debian-10/)
> et [là pour Docker Compose](https://linuxize.com/post/how-to-install-and-use-docker-compose-on-debian-10/) (les liens sont obsolètes, mieux vaut faire tes recherches)

## Commandes de base

Pour voir le détail des commandes, il suffit d'[aller consulter le fichier `Makefile`](Makefile) disponible à la racine
du projet, les commandes y sont très bien expliquées.

### Mettre en route l'application (en mode développement)

C'est super simple ! Suivez le guide :

1.  **Lancez l'initialisation de l'application :**

    ```bash
    # À la racine du projet : noe/
    make init
    ```

    > Cette commande va :
    >
    > - Installer certains outils de développement (pour pouvoir utiliser notamment les commandes liées à `eslint` et `prettier`, notamment)
    > - Préparer votre environnement Docker, en lançant la construction des containers
    > - Installer les dépendances NPM des différents containers

    > **La première mise en route de l'application peut prendre un peu de temps** (entre 5 et 10 minutes, en
    > fonction de votre connexion internet). Surveillez les logs pour vérifier qu'il n'y a pas d'erreurs.

2.  **Lancez les migrations de la base de données :**

    ```bash
    # Allez dans le dossier api/ et lancez la commande de migration :
    cd api
    npx migrate-mongo up
    ```

    > [Plus d'infos sur les migrations ici](#lancer-les-migrations)

3.  **Lancez l'application !**

    ```bash
    # Retournez à la racine noe/ :
    cd ..
    make full-start
    ```

    > Cette commande va lancer tous les services de l'application en une fois. **À l'avenir, vous aurez juste besoin de celle-ci pour démarrer NOÉ.**
    >
    > Vous pouvez aussi lancer les services individuellement avec `make start-<service>` et `make restart-<service>`

    > Par défaut, le service `website` n'est pas lancé automatiquement avec `make full-start`. Vous
    > pouvez utiliser `make full-start-optional` pour tout lancer d'un coup.

Il ne vous reste plus qu'à attendre quelques minutes, puis vous pourrez vous rendre sur :

- [`localhost:5000`](http://localhost:5000/) pour accéder à l'interface orga
- [`localhost:5020`](http://localhost:5020/) pour accéder à l'interface participant⋅e

Pour information, l'API est exposée sur [`localhost:4000`](http://localhost:4000/).

> L'application supporte le **hot-restart**, donc une fois que les containers sont lancés, plus besoin de les relancer :
> la compilation se relance dès qu'un changement est détecté dans les fichiers.

### Stopper l'application

`make full-stop`

### Afficher les logs

- Logs de l'API : `make log`
- Logs des frontends : `make log-orga-front` / `make log-inscription-front` ou `make log-front`
- Logs de tous les containers : `make log-all`

### Installer/désinstaller des packages (modification de `package.json`)

> **Attention :** Pour les frontends, le fichier `package.json` est unique ! Il se trouve dans le dossier `shared/`.

Voici la démarche :

1. Modifiez votre `package.json` en faisant votre ajout, modification ou suppression de package.
2. Lancez la commande `make install-deps-api` pour l'API, `make install-deps-website` pour le site internet, ou `make install-deps-front` pour les frontends (la commande est unifiée pour les deux frontends).

> La commande `make install-deps-front` se chargera de copier le fichier `package.json` de `shared/` vers les deux dossiers `orga-front/` et `inscription-front/`, ainsi que d'installer les dépendances dans chacun d'eux.

### Reformater le code avec [Prettier](https://prettier.io/)

- avec Make : `make prettier` (cela agit sur le projet entier)
- avec npm dans un des sous-dossiers : `npm run prettier`

**Attention :** si vous oubliez de faire `make prettier` avant de pousser des changements, Gitlab vous indiquera que les
tests sur le formatage de code ne sont pas passés et la pipeline de votre branche échouera (impossible alors d'accepter
un merge request dont la pipeline échoue).

> Pour faire une vérification simple _sans modifier les fichiers_, tapez `make prettier-check`.

> **Intégrez prettier à votre IDE**
>
> Prettier est compatible avec la plupart des
> IDEs : [le vôtre est forcément dans ce guide](https://prettier.io/docs/en/editors.html).
> Votre IDE missionnera alors Prettier pour reformater vos fichiers.

### Vérifier et résoudre les problèmes avec [ESLint](https://eslint.org/)

- avec Make : `make lint-show` (cela agit sur le projet entier)
- avec npm dans un des sous-dossiers : `npm run lint-show`

> Pour résoudre les problèmes qui peuvent être résolus automatiquement, tapez `make lint`.

> **Intégrez ESLint à votre IDE**
>
> ESLint est compatible avec la plupart des
> IDEs : [le vôtre est forcément dans ce guide](https://eslint.org/docs/user-guide/integrations).

### Gérer les migrations de la base de données

La base de données de NOÉ est versionnée, c'est-à-dire que nous utilisons un mécanisme de migrations successives pour
faire évoluer la structure de la base au fil du temps (pour que tout ne casse pas entre les versions de l'application
quand on fait les mises à jour !). Une [page Wikipédia](https://en.wikipedia.org/wiki/Schema_migration)
existe pour expliquer un peu les concepts.

> **Attention :** Toutes les opérations avec `migrate-mongo` se font depuis le dossier `api/`.
>
> Plus d'infos sur `migrate-mongo` : [Github](https://github.com/seppevs/migrate-mongo) /
> commande `npx migrate-mongo -h`

#### Voir l'état des migrations

```bash
npx migrate-mongo status
```

#### Créer une nouvelle migration

C'est une opération que l'on fait **exclusivement quand on développe !**

```bash
npx migrate-mongo create <nom_de_la_migration>
```

#### Lancer les migrations

Une fois que vous êtes sûr·es de votre coup, vous pouvez lancer la migration :

```bash
npx migrate-mongo up
```

#### Revenir en arrière

Si vous avez développé la fonction `down()` de la migration, vous pouvez aussi revenir en arrière. C'est utile si vous
voulez revenir à une version plus ancienne, et que vous voulez revenir dans le temps avec la base de données.

```bash
npx migrate-mongo down
```

## Déployer l'application

> Si vous voulez automatiser votre déploiement et les mises à jour avec la CI Gitlab, contactez les membres du
> projet. On vous expliquera la marche à suivre.

### Sur un serveur distant avec Docker Compose

1. **Clonez le projet :**

   ```bash
   # Dans votre dossier personnel sur le serveur : ~/
   git clone git@gitlab.com:noe-app/noe.git # ou https : https://gitlab.com/noe-app/noe.git
   ```

2. **Créez vos fichiers d'environnement pour votre serveur de production :**

   ```bash
   # À la racine du projet : noe/
   cp -r docker/.example.prod-env/ docker/prod-env
   ```

   > Cette commande copie dans le dossier `docker/prod-env/` tous les fichiers d'environnement dont l'application a
   > besoin pour se déployer en production, à partir du
   > dossier d'exemple `docker/.example.prod-env/` qui est déjà fourni.

   > **À vous de jouer !** Remplissez maintenant les différents fichiers dans le dossier `docker/prod-env/` avec votre
   > propre configuration.

3. **Déployez !**

   Là, vous avez fait le plus gros du travail, maintenant c'est la machine qui va travailler pour vous !

   Vous n'avez qu'à suivre la démarche de lancement en développement, en lançant `make full-start-prod` à la place
   de `make full-start`. Ce qui nous donne donc :

   ```bash
   ### Etape 1 : Construction
   # À la racine du projet : noe/
   make init-prod

   ### Etape 2 : Migration
   # Allez dans le dossier api/ et lancez la commande de migration :
   cd api
   npx migrate-mongo up # Vous aurez peut-être besoin d'ajuster la commande, lisez les indications.

   ### Etape 3 : Lancement
   # Retournez à la racine noe/ et lancez tout :
   cd ..
   make full-start-prod # si votre serveur est peu puissant, lancez chaque service espacés de quelques minutes avec les commandes `make start-prod-<service>`
   ```

> Une version alternative consiste à stocker son propre dossier de configuration sur un dépot Git privé, par exemple
> nommé `prod-deployment`, et à le cloner juste à côté du dépôt Git de NOÉ :
>
> ```bash
>  /~
>  |
>  |- /prod-deployment # Votre dossier de déploiement
>  |   |- .env
>  |   |- .env.api
>  |   |- .env.inscription-front
>  |   |- .env.orga-front
>  |   |- .env.website
>  |
>  |- /noe # Le dépôt NOÉ
>      |- /api
>      |- /inscription-front
>      |- /orga-front
>      |- ...
> ```
>
> De cette manière, si vous faites des changements dans votre configuration, vous aurez simplement à faire un `git pull`
> dans votre dossier `prod-deployment/`, et ensuite créer une copie des fichiers d'env de `prod-deployment/` vers le dossier
> `docker/prod-env/` de NOÉ. C'est aussi la structure de fichiers qui nous permet d'automatiser les déploiements en
> production depuis la CI Gitlab (voir [`.gitlab-ci.yml`](.gitlab-ci.yml) et [`scripts/upgrade-prod.sh`](scripts/upgrade-prod.sh)).

### Sur un serveur distant avec Kubernetes

> TODO !

### Sur un réseau local

Si vous le souhaitez, vous pouvez lancer l'application sur votre réseau local pour pouvoir accéder à NOÉ sur tous les
appareils connectés à votre réseau. Cela fonctionne sur une box internet classique par exemple.

L'application est alors compilée comme en mode "production", et sera rendu disponible sur les mêmes ports que
d'habitude.

> **Attention :** le déploiement sur un réseau local n'est pas encore supporté en HTTPS. Pour l'instant, c'est
> seulement du HTTP (non sécurisé).
>
> La fonctionnalité n'a pas été testée pour de vrai et pourrait être buggée.

**Marche à suivre :**

1. Récupérez
   votre [adresse IP locale](https://blog.shevarezo.fr/post/2019/01/08/comment-obtenir-adresse-ip-linux-ligne-de-commande)
   (qui est différente de votre adresse IP publique). Elle commence par `192.xx.xx.xx.xx`.

2. Modifiez le fichier `docker/prod-env/.env` en dé-commantant les lignes dédiées au lancement sur un réseau local. Ajoutez votre
   adresse IP sur la variable `LOCAL_NETWORK_IP_ADDRESS`:

3. Lancez la commande `make start-local-network`.

## Guide de contribution

### Dossiers partagés `shared/`

Pour simplifier le développement, nous utilisons un dossier [`shared/`](shared/) qui contient les éléments communs :

- [`shared/front/`](shared/front/) : Code partagé entre les frontends (orga-front, inscription-front et website)
- [`shared/images/`](shared/images/) : Images communes
- [`shared/locales/`](shared/locales/) : Fichiers de traduction
- [`shared/styles/`](shared/styles/) : Styles CSS/LESS partagés

Les frontends orga-front et inscription-front partagent aussi leur dossier [`src/shared/`](orga-front/src/shared/). Pour les garder synchronisés :

1. Lancez `make continuous-sync-shared-frontend-folder` avant de développer
2. Toute modification sera automatiquement synchronisée entre les deux frontends
3. Configurez votre IDE pour exclure/ignorer le dossier `orga-front/src/shared/` des résultats de recherche. Ainsi, lors des recherches de fichiers, seuls ceux de `inscription-front/src/shared/` seront affichés, évitant les doublons.
4. Une fois le code poussé, la CI Gitlab s'assure que les dossiers restent identiques (`make check-folders-are-synced`) et échoue sinon.

> **Commandes make :**
>
> - `make sync-folders` : Synchronise les dossiers partagés automatiquement
> - `make continuous-sync-shared-frontend-folder` : Lance la synchronisation continue des dossiers partagés (lance make sync-folders si pas synchro)
> - `make check-folders-are-synced` : Vérifie que les dossiers partagés sont synchronisés

Nous essayons aussi de garder les dossiers [`****-front/src/utils/`](orga-front/src/utils/) similaires. Si des fichiers deviennent identiques, on les déplace dans [`****-front/src/shared/utils/`](orga-front/src/shared/utils/) (ils deviennent alors partagés).

Tout la logique de synchronisation repose sur l'utilisation des [alias TypeScript](#configuration-des-alias-typescript) dans les fichiers `tsconfig.json`, et sur leur résolution à la compilation dans le fichier [`craco.config.js`](shared/front/craco.config.js).

> **Comment veiller à la synchronisation des fichiers ?**
>
> - Comparer des fichiers sur [VSCode](https://stackoverflow.com/a/30142271/13976355)
> - Comparer des fichiers
>   sur [Webstorm - Jetbrains](https://www.jetbrains.com/help/webstorm/comparing-files-and-folders.html)
> - Comparer des fichiers sur [Atom](https://stackoverflow.com/a/49169858/13976355)
> - Ajoute ton IDE s'il n'est pas listé !

### Configuration des alias TypeScript

Les fichiers `tsconfig.json` dans [`api/`](api/tsconfig.json), [`inscription-front/`](inscription-front/tsconfig.json), [`orga-front/`](orga-front/tsconfig.json), et [`shared/`](shared/tsconfig.json) configurent des alias pour simplifier les imports. Cette configuration permet d'utiliser des chemins plus courts et lisibles (par exemple, `@shared/components/CardElement` au lieu de `../../../shared/components/CardElement`).

Le `tsconfig.json` dans `shared/` est particulier : il n'a pas d'utilité pour la compilation. Il aide simplement l'IDE à faire le lien entre les fichiers du dossier `shared/` et le reste de l'application.

#### Synchronisation de fichiers supplémentaires

Pour garantir une cohérence accrue entre les deux frontends, des fichiers supplémentaires sont synchronisés :

- [`craco.config.js`](shared/craco.config.js) :
  - Monté dans les conteneurs frontends pour une configuration unifiée.
    ```
    ./shared/craco.config.js:/home/node/app/craco.config.js
    ```
- [`package.json`](shared/front/package.json) et [`package-lock.json`](shared/front/package-lock.json) :
  - Montés dans les conteneurs :
    ```
    ./shared/front/package.json:/home/node/app/package.json
    ./shared/front/package-lock.json:/home/node/app/package-lock.json
    ```
  - Ils sont ensuite copiés dans `orga-front/` et `inscription-front/` pour la compatibilité avec l'IDE. Ce qui veut dire que `orga-front/package.json` et `inscription-front/package.json` sont ignorés et écrasés : seul `shared/front/package.json` fait foi pour les deux frontends.
  - Toute la gestion des dépendances des frontends est centralisée via `make install-deps-front` dans le [Makefile](Makefile).

#### Configuration pour l'API

Pour l'API, la configuration est plus simple. Dans le container, le dossier `shared/` est monté au même endroit que dans l'arborescence de fichiers réelle.

### Conventions suivies dans le code

#### Mongoose: `id` ou `_id` ?

Avec Mongoose, l'ID des objets est toujours repéré par l'attribut `_id`. Mais pour nous rendre la vie plus simple,
Mongoose copie aussi cet ID dans un attribut `id`. Le problème, c'est qu'il n'est pas toujours présent partout, car on
manipule souvent les objets dans le frontend, et parfois `id` n'existe pas.

Du coup : **n'utilisez _jamais_ `id`** sur des objets Mongoose, **utilisez _toujours_ `_id`**. Dans le backend, parfois
les IDs sont des strings (quand ils viennent de la requête du front, par exemple), mais parfois ce seront des
ObjectIds (le
type de MongoDB pour ses ids). Pour faire des comparaisons, il faudra toujours les transformer en strings, en
utilisant `toString()` :

```js
myObjectId.toString() === myStringId;
```

#### Variables d'environnement

Les deux frontends sont construits en React avec un boilerplate qui s'appelle CreateReactApp, qui propose plein d'outils
d'aide au développement. Notamment, il permet d'intégrer facilement des variables d'environnement dans les applis
React (ce qui n'est pas aisé à faire, vu que le code s'exécute chez le client). Pour cela, il est nécessaire que les
variables d'environnement commencent par `REACT_APP_`.

Par convention, on préfèrera donc mettre `REACT_APP_` devant toutes les variables d'environnement utilisées pour
homogénéiser les noms de variables.

```bash
MA_VARIABLE=truc # ne sera pas prise en compte par les frontends
REACT_APP_MA_VARIABLE=truc # sera prise en compte par les frontends
```

#### Les différents scripts `docker compose`

Vous avez dû voir qu'on avait beaucoup fichiers `docker-compose.*****.yml`. Ils ont tous des buts différents :

- `docker-compose.yml` : C'est la configuration de base qui définit les services principaux (orga-front, inscription-front, api, website). Il spécifie les volumes, les ports et les configurations de base pour chaque service.

- `docker-compose.local.yml` : Étend la configuration de base pour un environnement local. Il ajoute un service de base de données MongoDB local et configure l'API pour en dépendre.

- `docker-compose.local.dev.yml` : Adapte la configuration locale pour le développement. Il modifie les commandes de démarrage des services pour utiliser les modes de développement avec rechargement à chaud (hot-reload).

- `docker-compose.prod.yml` : Ajoute la configuration nécessaire pour la production.
  - Ajout d'un service Traefik pour le routage et la gestion des certificats SSL.
  - Configuration des services pour fonctionner avec Traefik via des labels.
  - Ajout de variables d'environnement spécifiques à la production.
  - Configuration de volumes supplémentaires (comme .git et letsencrypt).

Le Makefile facilite l'utilisation de ces configurations Docker Compose. Allez voir le fichier [Makefile](Makefile) pour voir les commandes disponibles.

#### Routing

Pour le routing, nous utilisons le module [`react-router`](https://reactrouter.com). Voici quelques conseils autour
de son utilisation :

Nous utilisons la navigation du routeur grâce au hook `useNavigate`. et aux composants `<Link />`. Nous préférons
utiliser des liens relatifs plutôt que des liens absolus, partout où c'est possible (en s'efforçant de **toujours les
commencer par `./my/path`** pour éviter toute ambiguïté et nous évitons donc les syntaxes `../my/path` ou `my/path`).

**Nous excluons les liens en durs (`href` et liens absolus)** partout où c'est possible : ils provoquent souvent un
rechargement total de la page et causent des désagréments pour l'utilisateur⋅ice (glitches et perte des données
non-enregistrées).

#### Les classes CSS

Nous avons créé des classes CSS customisées pour nous aider dans la construction des pages des frontends.

##### Classes `containerH` et `containerV`

Elles permettent d'afficher en mode `flex` les objets dans l'application, soit en horizontal, soit en vertical. C'est à
privilégier dès qu'on est face à ce besoin.

On peut les associer à `buttons-container` pour créer un bloc dont les éléments sont espacés comme il faut de manière
régulière et se réadaptent bien à la taille de l'écran.

_Exemple :_

```jsx
<div style={{border: "2px solid red"}}>
  <div className="containerH buttons-container">
    <Button>Bouton Bouton</Button><Button>Bouton Bouton</Button>
    <Button>Bouton Bouton</Button><Button>Bouton Bouton</Button>
    <Button>Bouton Bouton</Button><Button>Bouton Bouton</Button>
  </div>
</div>

<div style={{border: "2px solid blue"}}>
  <div className="containerV buttons-container">
    <Button>Bouton Bouton</Button>
    <Button>Bouton Bouton</Button>
    <Button>Bouton Bouton</Button>
  </div>
</div>
```

![](shared/images/buttons-container.png)

##### Classe `container-grid`

Pour créer des vues avec des divisions en deux ou trois colonnes, nous utilisons `container-grid` associé aux classes :

- `two-per-row` et `three-per-row` pour créer des colonnes de même largeur,
- `two-thirds-one-third` pour avoir une division deux tiers / un tiers.

```jsx
<div className="container-grid two-thirds-one-third">
  <div>Contenu qui prend 2 tiers de l'écran</div>
  <div>Contenu qui prend 1 tiers de l'écran</div>
</div>
```

##### Classes `page-content`/`modal-page-content` et `page-container`

Pour pouvoir créer des bordures propres sur les pages, tout en se réservant la possibilité à tout moment de pouvoir
mettre des objets en pleine largeur (par exemple une barre de recherche, etc.), on utilise une imbrication de classes
CSS comme suit.

En imbriquant systématiquement un `page-container` dans un élément `page-content`, on crée une page avec des bordures
bien propres, génériques et responsives. Lorsque le contenu est affiché dans un modal (pas dans une page classique), on
préfère utiliser `modal-page-content`, car les modals de la librairie AntDesign ont des marges différentes.

Pour mettre un élément en pleine page, il suffit ensuite de rajouter la classe `full-width-content` à cet élément. Si on
veut mettre cet élément en pleine page, mais garder les marges à l'intérieur de cet élément, on ajoute `with-margins` à
côté de `full-width-content`.

_Exemple :_

```jsx
<div style={{border: "2px solid grey"}}>
  <div className="page-content">
    <div className="page-container">
      <div style={{border: "2px solid red"}}>Bloc simple avec marges classiques</div>

      <div className="full-width-content" style={{border: "2px solid orange"}}>
        Bloc en pleine largeur, contenu sans marges <code>className="full-width-content"</code>
      </div>

      <div className="full-width-content with-margins" style={{border: "2px solid green"}}>
        Bloc en pleine largeur, contenu avec marges{" "}
        <code>className="full-width-content with-margins"</code>
      </div>
    </div>
  </div>
</div>
```

![](shared/images/page-container.png)

##### Classes `success-button` et `warning-button`

Ant Design ne permet que de faire des boutons bleus et des boutons rouges (en utilisant l'argument `danger`). Pour varier
un peu les couleurs, on peut ajouter des classes custom dessus. Il faut penser à bien mettre l'argument `type="primary"`
pour que ça fonctionne.

```jsx
<Button type="primary" className="success-button">
  Success Button
</Button>

<Button type="primary" className="warning-button">
  Warning Button
</Button>
```

> **Attention :**
>
> Cette technique ne fonctionne pas sur les boutons qui sont encapsulés dans un `PopConfirm` et
> `disabled` à la fois. Dans ce cas-là, il faut renlever la classe si le bouton est `disabled`.
>
> ```jsx
> <Button className={isDisabled ? undefined : registrationButtonClassName} disabled={isDisabled}>
>   Disabled button in Popconfirm
> </Button>
> ```

### Conventions suivies pour l'UI / UX (Interface utilisateur⋅ice / Expérience utilisateur⋅ice)

#### Le texte dans l'application

Lorsque nous développons, nous prenons garde à écrire de la manière la plus 'professionnelle' possible. Nous mettons des
majuscules en début de mots quand il faut, nous mettons des accents et vérifions systématiquement l'ortographe du texte
écrit (cela évite de repasser ensuite derrière pour corriger des fautes facilement évitables).

#### Convention d'écriture inclusive

Nous rédigeons nos textes de manière la plus inclusive possible. Dans certains cas, nous avons recours au point médian.
Voici la convention d'utilisation du point médian.

Ne pas faire :

- participant.e
- participant.es
- participant.e.s

Faire :

- participant⋅e
- participant⋅es

##### Messages à destination de l'utilisateur⋅ice

Nous appliquons les bonnes pratiques lorsqu'il s'agit d'informer l'utilisateur ou de l'encourager à exécuter une action.

Nous suivons [les principes suivants](https://uxplanet.org/how-to-write-good-error-messages-858e4551cd4), et en
particulier ceux énoncés ici :

- Be Clear And Not Ambiguous
- Be Short And Meaningful
- Don’t Use Technical Jargons
- Be Humble — Don’t Blame User
- Avoid Negative Words
- Give Direction to User
- Be Specific And Relevant
- Provide Appropriate Actions
- Use Proper Placement

### Utilisation de Git et de Gitlab

#### Les issues

Les issues sont créées par n'importe qui souhaitant soumettre une fonctionnalité ou référencer un bug. Il est fortement
encouragé de reprendre les modèles d'issue adaptés
en [sélectionnant le modèle adéquat dans le dropdown "Description"](https://docs.gitlab.com/ee/user/project/description_templates.html#use-the-templates)

Depuis les issues, il est possible
de [créer une nouvelle branche](https://docs.gitlab.com/ee/user/project/issues/issue_data_and_actions.html#create-merge-request)
qui sera directement nommée de la bonne manière et qui sera liée à l'issue.

#### Les merge requests (MRs)

![image initiale](https://wac-cdn.atlassian.com/dam/jcr:01b0b04e-64f3-4659-af21-c4d86bc7cb0b/01.svg?cdnVersion=1606)

Une fois notre branche de développement prête (disons qu'elle s'appelle `Feature` comme dans l'image), il y a deux
façons de fusionner les changements avec la branche `master` principale :

- Par `git merge`
- Par `git rebase`

> Les schémas de cette partie sont tirées
> de [cet excellent article](https://www.atlassian.com/fr/git/tutorials/merging-vs-rebasing) sur la différence
> entre `git merge` et `git rebase`.

##### Fusion par `git merge`

C'est la méthode la plus répandue. Il y a trois étapes :

- Merger `master` dans notre propre branche - c'est-à-dire, apporter les changements de `master` qui ont été faits
  depuis qu'on a commencé à développer, afin de se remettre à jour, puis s'il y a des conflits, résoudre ces conflits
- Retester l'application, car c'est souvent lors des `merge` que l'on crée de nouveaux bugs...
- Puis une fois que la MR a été relue par les pairs, la branche peut être mergée dans `master` (via l'interface Gitlab
  souvent).

![image merge](https://wac-cdn.atlassian.com/dam/jcr:e229fef6-2c2f-4a4f-b270-e1e1baa94055/02.svg?cdnVersion=1606)

**Avantage :** la simplicité.

**Inconvénient :** on obtient un arbre de développement assez chaotique et il est difficile de s'y retrouver lorsqu'on
cherche l'origine d'un bug, ou quand on doit revenir dans l'arbre des commits.

##### Fusion par `git rebase`

C'est une méthode plus avancée. Au lieu de fusionner des changements (faire que `master` et notre branche re-fusionnent
après s'être séparées), on va carrément remettre les changements apportés par notre branche au-dessus de tous les
nouveaux changements qu'on n'a pas pris en compte.

- Rebaser votre branche sur `master` - c'est-à-dire déplacer l'origine de la branche sur le dernier commit de master et
  réécrire les commits depuis cet endroit.
- S'il y a des conflits, les résoudre - c'est un peu plus tordu qu'un `merge` classique, car il peut y avoir des
  conflits sur plusieurs commits lorsque les commits sont mal délimités les uns des autres.
- Retester l'application, encore et toujours...
- Puis une fois que la MR a été relue par les pairs, la branche peut être mergée dans `master` (via l'interface Gitlab
  souvent).

![image rebase](https://wac-cdn.atlassian.com/dam/jcr:5b153a22-38be-40d0-aec8-5f2fffc771e5/03.svg?cdnVersion=1606)

**Avantage :** un arbre des commits super clean, un historique de développement clair comme de l'eau de roche.

**Inconvénient :** `rebase` est un peu plus technique et nécessite une bonne maîtrise de Git (mais une fois qu'on a
compris la chose, ça va aussi vite qu'un `merge`).

De nombreux IDEs intègrent le processus de `rebase` nativement dans leur outil de versionning. Regardez peut-être si le
vôtre le fait !

Pour aller plus loin sur `rebase`, c'est [ici](https://www.atlassian.com/fr/git/tutorials/merging-vs-rebasing).

### Ressources utiles

#### Extensions dans le navigateur (super utiles, vraiment !)

- React Dev Tools :
  - [Firefox](https://addons.mozilla.org/fr/firefox/addon/react-devtools/)
  - [Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
- Redux Dev Tools :
  - [Firefox](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools/)
  - [Chrome](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=fr)

#### Tutoriels

- [Initiation à React](https://fr.reactjs.org/tutorial/tutorial.html)
- [Initiation à Redux](https://redux.js.org/tutorials/index)

#### À rajouter dans ce Guide

- le fichier configuration.js pour paramétrer l'apparence graphique de NOE
- comment débugger directement dans son IDE
