addAFreeContent: Ajouter du contenu libre
addAPanel: Ajouter un groupe de questions
addAQuestion: Ajouter une question
defaultPlaceholders:
  email: john@doe.com
  multiSelect: Sélectionnez une ou plusieurs options…
  phone: +33 6 12 34 56 78
  select: Sélectionnez une option…
  text: Votre réponse…
  url: https://exemple.com
displayConditionsHelp:
  advancedConditions:
    text: |
      <p>
        Si la question dont dépend votre condition est un champ complexe (cases à cocher, choix
        multiple, select, multi-select), vous allez sûrement vouloir utiliser des conditions
        avancées.
      </p>
      <p>
        Pour ces champs, utilisez les valeurs exactes des clés associées aux réponses voulues. Elles
        se trouvent dans le champ "Clé en BDD" des options de la question concernée.
        <br />
        <exampleText>
          ex: <i>jeSuisMajeur_s4d</i> pour sélectionner la réponse <i>Je suis majeur</i>.
        </exampleText>
      </p>
      <p>
        Vous pouvez ensuite assembler plusieurs réponses ensembles, avec nos opérateurs <i>OU</i> et <i>ET</i> :
      </p>
      <p>
        <strong>Séparateur <i>OU</i> :</strong> Vous pouvez utiliser l'opérateur <i>;</i> pour activer la
        condition si une ou plusieurs valeurs sont sélectionnées.
        <br />
        <exampleText>
          ex: <i>jeSuisMajeur_s4d;jeSuisAccompagne_3ka</i> pour activer la condition si <i>Je suis majeur</i>
          <strong>ou</strong> <i>Je suis accompagné</i> est sélectionné.
        </exampleText>
      </p>
      <p>
        <strong>Séparateur <i>ET</i> :</strong> Vous pouvez utiliser le opérateur <i>AND</i> pour activer la
        condition si toutes les valeurs demandées sont sélectionnées. <br />
        <exampleText>
          ex: <i>dispoLe28_8sd AND dispoLe29_la6</i> pour activer la condition si <i>Dispo le 28</i>
          <strong>et</strong> <i>Dispo le 29</i> sont tous les deux sélectionnés.
        </exampleText>
      </p>
      <p>
        <strong>Utilisez <i>OU</i> et <i>ET</i> en même temps :</strong> Vous pouvez utiliser les deux
        opérateurs en même temps.
        <br />
        <exampleText>
          ex: <i>dispoLe28_8sd;dispoLe29_la6 AND dispoLe31_nk1</i> pour activer la condition si <i>Dispo le
          28</i> <strong>ou</strong> <i>Dispo le 29</i> est sélectionné <strong>et</strong> si <i>Dispo le 31</i>
          est sélectionné.
        </exampleText>
      </p>
      <p>Les opérateurs sont disponibles pour les composants suivants </p>
      <ul>
        <li>Opérateur <i>OU</i> : dispo sur Select, Multi-select, Choix multiples et Checkboxes</li>
        <li>Opérateur <i>ET</i> : dispo sur Checkboxes et Multi-select</li>
      </ul>
    title: Conditions avancées
  notice: Apprenez à écrire le texte de conditions avec nos conseils et exemples.
  simpleConditions:
    text: |
      <p>
        Si la question dont dépend votre condition est un champ simple (texte, nombre, checkbox),
        vous pouvez renseigner directement la valeur exacte à obtenir pour la condition réussisse.
      </p>
      <p>Voici nos conseils pour chaque type de champ :</p>
      <ul>
        <li>
          Texte court, Texte long, Email, URL, Téléphone : la valeur exacte à obtenir <exampleText>
            (ex: <i>je suis une licorne !</i>, <i>+33 6 12 12 12 12</i>, <i>https://sesame-ouvre-toi.com</i>)
          </exampleText>
        </li>
        <li>
          Checkbox : <i>true</i> pour coché, <i>false</i> pour non-coché <exampleText>(ex: <i>true</i>, <i>false</i>)</exampleText>
        </li>
        <li>
          Number : utilisez le point pour rentrer un chiffre décimal <exampleText>(ex: <i>4</i> ou <i>5.6</i>)</exampleText>
        </li>
      </ul>
    title: Conditions simples
move:
  down: Descendre
  up: Monter
newFieldContentComp: Nouveau contenu libre
newFieldInputComp: Nouvelle question
newFieldPanelComp: Nouveau groupe
schema:
  conditional:
    conditions: conditions
    doNotShow: Ne pas montrer
    helpForDisplayConditions: Aide sur les champs conditionnels
    isEqualTo: est égale à
    question: question
    show: Montrer
    showPlaceholder: Montrer / Ne pas montrer
    whenQuestion: quand la question
  content:
    label: Contenu
    placeholder: tapez ici le contenu libre que vous souhaitez
  description:
    label: Description
    placeholder: description
  disabled:
    label: Désactivé
  displayName:
    label: Nom d'affichage
    placeholder: nom d'affichage
  displayOptions:
    label: Montrer dans…
    options:
      inListView: La vue liste orga
      inOrgaFilters: Les filtres orga
      inParticipantFilters: Les filtres participant·es
      inParticipantSessionView: La vue pleine page des sessions participant·es
      inPdfExport: Les exports PDF
      inSessionsCards: La miniature des sessions participant·es
  hidden:
    label: Caché pour les participant·es
  key:
    label: Clé en base de données
    placeholder: clé
  label:
    label: Question
    placeholder: question
  minMax:
    labelNumber: Valeurs mini/maxi
    labelSelectedCount: Nombre mini/maxi de réponses
    minDisabledIfNotRequired: Vous devez rendre cette question obligatoire pour spécifier un minimum.
    placeholderMax: max
    placeholderMin: min
  options:
    addOption: Ajouter une option
    atLeastOneOptionNeeded: Au moins une option est requise.
    label: Options
    optionKey: "Clé en BDD :"
    optionLabel: nom de l'option
  required:
    label: Obligatoire
  title:
    label: Titre
    placeholder: titre
  type:
    label: Type de question
    placeholder: type
    typesAreIncompatible: Seuls les types similaires sont permis car la question a déjà été créée.
tabs:
  advanced: Avancé
  displayConditions: Conditions d'affichage
  general: Général
types:
  checkbox: Checkbox / validation
  checkboxGroup: Checkboxes (plusieurs choix possibles)
  content: Contenu libre
  datetime: Date et heure
  day: Jour
  email: Email
  longText: Paragraphe
  multiSelect: Multi-select (plusieurs choix possibles)
  number: Nombre
  panel: Groupe de questions
  phoneNumber: Téléphone
  radioGroup: Choix multiples (un choix possible)
  select: Select (un choix possible)
  text: Texte court
  url: URL
