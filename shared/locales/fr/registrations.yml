aProblemToRegister: Un problème pour s'inscrire ?
actions:
  changeIdentity: Changer d'identité
  changeIdentityDescription: Cliquez sur un⋅e utilisateur⋅ice pour prendre son identité en vous connectant sur son compte.
  stopChangeIdentity: Stopper le changement d'identité
availabilities:
  accessAdvancedEditor: accéder à l'éditeur avancé
  andILeaveOn: et je repars le
  backToSimpleEditor: retour à l'éditeur simplifié
  datePicker:
    aroundXh: vers {{hour}}h
    datePlaceholder: Date…
    hourPlaceholder: Heure…
    shortcuts: Raccourcis
    timestampTemplate: "{{label}} (~ {{hour}}h)"
  datesAreComplicated: "Mes dates de présence sont un peu compliquées :"
  datesAreSimple: "Mes dates de présence sont simples :"
  errors:
    addAStartDate: Ajoutez votre date d'arrivée.
    addAnEndDate: Ajoutez votre date de départ.
  iArriveOn: J'arrive le
  infoMessage: >-
    L'événement a lieu du <strong>{{start}}</strong> au <strong>{{end}}</strong> inclus. <br/>Si vous ne savez pas
    encore quand vous venez, faites au mieux : vous pourrez corriger vos dates plus tard.
  linkedToStewardAlert:
    dates:
      description: |
        <p>Les organisateur⋅ices ont défini vos dates de présence, visibles ci-dessous.</p>
        <p>
          <strong>Si vous constatez des différences</strong> entre vos dates et celles
          rentrées par les organisateur⋅ices, merci de rentrer en contact avec elleux pour
          <strong>mettre vos dates à jour</strong>.
        </p>
      title: Vos dates de présence en tant qu'$t(stewards:labelDown)
    description: >-
      Si cette information est fausse ou si vos dates de présence sont différentes de celles affichées, contactez
      l'équipe organisatrice.
    message: Vous avez été désigné⋅e comme $t(stewards:labelDown) de l'événement, sous le nom de {{name}}
  timestamps:
    afterDinner: après le dîner
    afterLunch: après le repas du midi
    earlyInTheMorning: tôt le matin
    inTheLateAfternoon: en fin d'après-midi
    inTheMorning: dans la matinée
  title: Quand serez-vous présent⋅e à l'événement ?
backToForm: Retour au formulaire
form:
  mandatoryFieldsNotice: Les champs précédés d'une astérisque <redColor>*</redColor> sont obligatoires.
  title: Formulaire d'inscription
groupedSmsButtons:
  noPhoneNumbersFound: Pas de numéros de téléphone trouvés.
  tooltip: Envoyer un SMS groupé à tous·tes les participant·es
  useThisFeatureOnAPhone: Pour envoyer des SMS, utilisez votre téléphone :)
label: Participant⋅e
labelMyRegistration: Mon inscription
labelRegister: S'inscrire
label_edit: Modifier un⋅e participant⋅e
label_other: Participant⋅es
list:
  advancedMode: Mode avancé
  dataDisplayOptionsTitle: Affichage des données
  displayUnbookedUsers: Afficher les personnes désinscrit⋅es et non-inscrites
  groupedPdfPlanningExportButton:
    customFileName: Sélection de plannings participant·es
    tooltip: Exporter les plannings des participant·es sélectionné·es en une seule fois.
main:
  erase: Effacer
  eventNotOpen:
    comeBackLater: Les inscriptions sont fermées pour l'instant. Revenez un peu plus tard.
    full: Oh mince ! L'événement est complet.
    notYet: Oh mince ! L'événement n'est pas encore ouvert.
  helpToast: Besoin d'aide ? Cliquez ici pour savoir comment vous inscrire.
  registerToTheEvent: Valider mon inscription
  saveModifications: Enregistrer les modifications
  unregisterButton:
    linkedToStewardCantUnregister: Vous êtes associé⋅e à un⋅e $t(stewards:labelDown), vous ne pouvez pas vous désinscrire par vous-même.
    thisWillUnsubscribeParticipantsFromSessions: Cela désinscrira également les personnes de <br /> leurs sessions et équipes si elles en ont.
    title: Me désinscrire
    yesRestartFromBeginning: Oui, je veux recommencer de zéro.
    yesUnregister: Oui, je me désinscris.
    youWillBeUnregisteredFromActivities: Vous serez désinscrit⋅e de toutes vos activités.
    youWillLooseYouProgress: Tout ce que vous avez renseigné sur cette page sera effacé.
  validateAndProceed: Valider et continuer
  welcomeMessages:
    newUser:
      description: |
        Vous êtes sur la page d'inscription de l'événement {{projectName}}. 
        Pour vous inscrire, il suffit de remplir les informations nécessaires sur cette page,
        puis de cliquer sur "Valider mon inscription".
      message: Bienvenue, {{userName}} !
    registrationComplete:
      description: |
        <p>
          Vous pouvez modifier vos informations en cliquant sur "Enregistrer les modifications".
        </p>
        Accédez à votre planning en cliquant sur <linkToPlanning><strong>"Mon Planning"</strong></linkToPlanning>.
      message: Vous êtes inscrit⋅e.
    registrationSaved:
      description: Vos données d'inscription ont été sauvegardées. Rentrez les derniers détails demandés pour vous inscrire.
      message: Vous y êtes presque.
messages:
  bravoYouAreRegistered:
    comeBackWhenSubscriptionsAreOpen: >-
      Revenez plus tard quand les inscriptions aux activités seront ouvertes, pour vous inscrire au programme de
      l'événement. À très vite 👋
    subTitle: Inscrivez-vous maintenant aux sessions de l'événement !
    title: Bravo, vous êtes inscrit⋅e ! 🎉
  defaultError: >-
    Oups, vous avez trouvé un bug ! Il y a eu un petit problème pendant l'enregistrement de vos données. Merci d'en
    informer l'équipe organisatrice.
  registrationIncomplete: Votre inscription n'est pas encore complète.
  registrationIncompleteCanAlreadySaveProgress: Votre inscription n'est pas encore complète, mais vous pouvez déjà sauver vos progrès si vous le souhaitez :)
  registrationIncompleteCantSaveModifications: Votre inscription n'est pas complète, vous ne pouvez pas enregistrer les modifications.
  registrationIncompleteShouldSetDatesOfPresence: Votre inscription n'est pas complète. Commencez par renseigner vos dates de présence :)
  savedProgress: Renseignez toutes les informations requises pour valider votre inscription. Votre progrès ont été sauvegardés.
  unregistrationSuccessful: Désinscription réussie !
notifIncomplete:
  completeMyRegistration: Compléter mon inscription
  form: <strong>Formulaire incomplet :</strong> Renseignez les champs obligatoires pour valider votre inscription.
  ticketing: <strong>Billet nécessaire :</strong> Renseignez un numéro de billet pour valider votre inscription.
  title: Votre inscription n'est pas encore complète.
numberOfDaysOfPresence:
  label: Jours
participantsAndStats: Participant⋅es & stats
registrationHelpButton:
  consultHelpCenter:
    description: Consulter le centre d'aide de NOÉ
    subtitle: Fonctionnement de l'outil, données personnelles, etc.
    title: J'ai une question liée à l'utilisation de l'outil
  contactEventTeam:
    description: Contacter l'équipe organisatrice de l'événement
    emailSubject: J'ai un problème lié à mon inscription sur NOÉ
    subtitle: Formulaire, billetterie, inscription aux sessions, etc.
    title: J'ai un souci lié à mon inscription
  contactNOETeam:
    description: Contacter l'équipe NOÉ
    emailSubject: J'ai un bug sur NOÉ
    subtitle: Bug, page d'erreur, comportement inattendu, etc.
    title: La plateforme ne fonctionne pas
registrationValidation:
  endCannotBeBeforeStart: Votre date de départ ne peut pas être avant votre date d'arrivée.
  selectDatesToRegister: Sélectionnez une date d'arrivée et une date de départ pour vous inscrire.
  someFormFieldsAreMandatory: 'Certains champs obligatoires du formulaire restent à remplir: "{{list}}"'
  ticketingIsMandatory: >-
    Vous devez réserver un billet pour vous inscrire. Si vous avez déjà le vôtre, vous pourrez le renseigner
    manuellement.
roles:
  orgaOfTheEvent: organisateur⋅ice de l'événement
  stewardOnThisSession: $t(stewards:labelDown) de cette session
schema:
  arrivalDateTime:
    label: Date d'arrivée
  departureDateTime:
    label: Date de départ
  everythingIsOk:
    label: Inscrit·e
  hasCheckedIn:
    label: Arrivé·e
  hidden:
    label: Caché⋅e
  role:
    addRole: Ajouter de nouveaux droits
    label: Droits
    options:
      admin:
        description: >-
          Droits de consultation, de création, d'édition et de suppression. Accès à la configuration avancée et à la
          gestion des droits.
        label: Administrateur⋅ice
      contrib:
        description: Droits de consultation, de création et d'édition.
        label: Contributeur⋅ice
      guest:
        description: Droits de consultation seulement.
        label: Invité⋅e
    placeholder: droits
    roleScale: Échelle des droits
  steward:
    label: $t(stewards:label) lié·e
  tags:
    label: Tags
  team:
    label: Équipe
  tickets:
    label: Billets
  voluntaryCounter:
    label: Bénévolat
ticketing:
  add: Ajouter
  addAnotherTicket: "Renseigner un autre billet :"
  addManually: "Vous avez déjà votre billet ? Ajoutez le ici :"
  addYourTicketHere: "Renseignez votre numéro de billet ici :"
  alreadyHaveTicket: Vous avez déjà votre billet ?
  helloAssoAlert: >-
    Ci-dessous se trouve une <strong>page de paiement Hello Asso</strong> 👇 C'est ici que vous pouvez acheter votre
    billet. <br/> <strong>Renseignez la même adresse email</strong> que celle avec laquelle vous avez créé votre compte
    NOÉ (<i>{{email}}</i>). De cette manière, NOÉ saura que c'est bien vous qui effectuez l'achat 😌
  labelBookATicket: Achetez votre billet
  labelIHaveATicket: J'ai un numéro de billet
  labelMyTickets: Mes billets
  messages:
    alreadyUsedTicket: Le billet a déjà été utilisé.
    ticketAdded: Billet ajouté !
    ticketNotFound: Le numéro de billet n'a pas été trouvé.
    unregisterBeforeRemovingTicket: Pour retirer tous vos billets, désinscrivez-vous de l'événement d'abord.
  schema:
    amount: Montant
    article: Article
    id: Numéro de billet
    options: Options
    userName: Nom
  thanksToTiBilletAlert: |
    NOÉ est fier de travailler avec <linkToTiBillet>TiBillet</linkToTiBillet>, une billetterie 
    open-source et coopérative. Ce sont eux qui s'occupent des tickets : achetez le vôtre juste en dessous 👇
  ticketInputPlaceholder: numéro de billet
  transfer:
    button:
      label: Transférer
      tooltip: Transférez votre billet à un·e autre participant·e en un clic !
    infoAlert: >-
      <p>
        Vous pouvez transférer votre billet à un·e autre participant·e en renseignant l'email de son compte NOÉ.
        Iel recevra alors un email pour l'informer du transfert.
      </p>  Le transfert de ticket ne prend pas en compte le transfert d'argent. Si votre billet était payant, assurez
      vous de récupérer l'argent auprès du destinataire par vos propres moyens.
    messages:
      notFound: Oh oh… ça n'a pas marché. Est-ce que le destinataire a un compte sur NOÉ ?
      success: Transfert réussi ! Le destinataire a reçu une confirmation par email à l'adresse {{toEmail}}.
    modalTitle: Transférer mon billet
    toEmail:
      errorCantBeSameEmailAsUser: L'email du destinataire doit être différente de votre propre compte.
      label: Email du destinataire
    transfer: Transférer
    transferAndUnregister: Transférer et se désinscrire
    transferredFromEmailOnDate: Transféré depuis l'email {{fromEmail}} le {{date}}
    warningAlert: >-
      Les arnaques à la vente de billet sont fréquentes : achetez et vendez votre billet seulement entre  personnes de
      confiance.
    warningOnlyOneTicketAlert:
      description: >-
        Si vous transférez votre ticket, vous serez <strong>automatiquement désinscrit·e</strong> de l'événement et vos
        inscriptions aux activités seront supprimées.
      message: Vous ne possédez qu'un seul billet !
  youHaveYourTicket: C'est bon, vous avez votre billet.
volunteeringGauge:
  fullRegistrationRecap: >-
    Tu comptabilises {{numberOfDaysOfPresence}} jours de présence, et ton total de bénévolat s'élève à {{total}}. Par
    jour, cela fait donc {{totalPerDay}}.
  fullRegistrationRecapAbsolute: Ton total de bénévolat s'élève à {{total}}.
  hint:
    itsPerfect: "Ni trop, ni pas assez: ta jauge de bénévolat est parfaite."
    lessNeeded: Tu fais beaucoup de bénévolat... Pense à prendre du temps pour toi !
    moreNeeded: Inscris toi à plus d'activités de bénévolat pour remplir ta jauge.
  projectRecap: Le nombre d'heures par jour conseillé se trouve entre {{min}} et {{max}}.
