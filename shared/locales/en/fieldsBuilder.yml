addAFreeContent: Add free content
addAPanel: Add a question group
addAQuestion: Add a question
defaultPlaceholders:
  email: john@doe.com
  multiSelect: Select one or more options…
  phone: +44 020 3040 5060
  select: Select an option…
  text: Your answer…
  url: https://exemple.com
displayConditionsHelp:
  advancedConditions:
    text: |
      <p>
        If the question on which your condition depends is a complex field (checkbox, multiple choice
        select, multi-select), you'll probably want to use advanced
        conditions.
      </p>
      <p>
        For these fields, use the exact values of the keys associated with the desired answers. They
        can be found in the "Key in DB" field of the relevant question options.
      <br />
        <exampleText>
          ex: <i>jeSuisMajeur_s4d</i> to select the answer <i>Je suis majeur</i>.
      </exampleText>
      </p>
      <p>
        You can then assemble several answers together, using our operators <i>OR</i> and <i>AND</i>:
      </p>
      <p>
      <strong>Separator <i>OR</i>:</strong> You can use the operator <i>;</i> to activate the
        condition if one or more values are selected.
      <br />
        <exampleText>
          ex: <i>jeSuisMajeur_s4d;jeSuisAccompagne_3ka</i> to activate the condition if <i>Je suis majeur</i>
          <strong>or</strong> <i>Je suis accompagné</i> is selected.
      </exampleText>
      </p>
      <p>
        <strong>Separator <i>AND</i>:</strong> You can use the operator <i>AND</i> to activate the
        condition if all the requested values are selected. <br />
        <exampleText>
          ex: <i>dispoLe28_8sd AND dispoLe29_la6</i> to activate the condition if <i>Dispo le 28</i>
          <strong>and</strong> <i>Dispo le 29</i> are both selected.
      </exampleText>
      </p>
      <p>
        <strong>Use <i>OR</i> and <i>AND</i> at the same time:</strong> You can use both operators at the same time.
        operators at the same time.
      <br />
        <exampleText>
          ex: <i>dispoLe28_8sd;dispoLe29_la6 AND dispoLe31_nk1</i> to activate the condition if <i>Dispo le
          28</i> <strong>or</strong> <i>Dispo le 29</i> is selected <strong>and</strong> if <i>Dispo le 31</i>
          is selected.
      </exampleText>
      </p>
      <p>Operators are available for the following components </p>
      <ul>
      <li>Operator <i>OR</i>: available on Select, Multi-select, Multiple choice and Checkboxes</li>
        <li>Operator <i>AND</i>: available on Checkboxes and Multi-select</li>
      </ul>
    title: Advanced conditions
  notice: Learn how to write the text for conditionals with our tips and examples.
  simpleConditions:
    text: |
      <p>
        If the question on which your condition depends is a simple field (text, number, checkbox),
        you can directly enter the exact value you need to obtain for the condition to succeed.
      </p>
      <p>Here are our tips for each type of field:</p>
      <ul>
      <li>
          Short text, Long text, Email, URL, Phone: the exact value to be obtained <exampleText>
            (e.g.: <i>I'm a unicorn!</i>, <i>+33 6 12 12 12 12</i>, <i>https://sesame-ouvre-toi.com</i>)
        </exampleText>
        </li>
      <li>
          Checkbox : <i>true</i> for checked, <i>false</i> for unchecked <exampleText>(ex: <i>true</i>, <i>false</i>)</exampleText>
        </li>
      <li>
          Number : use the dot to enter a decimal number <exampleText>(ex: <i>4</i> or <i>5.6</i>)</exampleText>
        </li>
      </ul>
    title: Simple conditions
move:
  down: Move down
  up: Move up
newFieldContentComp: New free content
newFieldInputComp: New question
newFieldPanelComp: New group
schema:
  conditional:
    conditions: conditions
    doNotShow: Do not show
    helpForDisplayConditions: Help with conditional fields
    isEqualTo: is equal to
    question: question
    show: Show
    showPlaceholder: Show / Do not show
    whenQuestion: when the question
  content:
    label: Content
    placeholder: type the free content you want here
  description:
    label: Description
    placeholder: description
  disabled:
    label: Disabled
  displayName:
    label: Display name
    placeholder: display name
  displayOptions:
    label: Show in…
    options:
      inListView: The orga list view
      inOrgaFilters: The orga filters
      inParticipantFilters: The participants filters
      inParticipantSessionView: The full-page view of participants sessions
      inPdfExport: PDF exports
      inSessionsCards: The thumbnail of participating sessions
  hidden:
    label: Hidden for participants
  inListView:
    label: Show in list view
  inOrgaFilters:
    label: Show in orga filters
  inPdfExport:
    label: Add to PDF exports
  key:
    label: Database key
    placeholder: key
  label:
    label: Question
    placeholder: question
  minMax:
    labelNumber: Min/max values
    labelSelectedCount: Min/max number of answers
    minDisabledIfNotRequired: You must make this question mandatory to specify a minimum.
    placeholderMax: max
    placeholderMin: min
  move:
    moveDown: Move down
    moveUp: Move up
  options:
    addOption: Add an option
    atLeastOneOptionNeeded: At least one option is required.
    label: Options
    optionKey: "DB key :"
    optionLabel: option name
  required:
    label: Required
  title:
    label: Title
    placeholder: title
  type:
    label: Question type
    placeholder: type
    typesAreIncompatible: Only similar types are allowed because the question has already been created.
tabs:
  advanced: Advanced
  displayConditions: Display conditions
  general: General
types:
  checkbox: Checkbox / validation
  checkboxGroup: Checkboxes (several possible choices)
  content: Free content
  datetime: Date and time
  day: Day
  email: Email
  longText: Paragraph
  multiSelect: Multi-select (several choices possible)
  number: Number
  panel: Group of questions
  phoneNumber: Phone
  radioGroup: Multiple choice (one choice possible)
  select: Select (one possible choice)
  text: Short text
  url: URL
