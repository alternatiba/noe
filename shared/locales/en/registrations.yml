aProblemToRegister: Need help to register?
actions:
  changeIdentity: Identity swap
  changeIdentityDescription: Click on a user to take their identity by logging into their account.
  stopChangeIdentity: Stop identity swap
availabilities:
  accessAdvancedEditor: access the advanced editor
  andILeaveOn: and I leave on
  backToSimpleEditor: back to the simple editor
  datePicker:
    aroundXh: around {{hour}}:00
    datePlaceholder: Date…
    hourPlaceholder: Hour…
    shortcuts: Shortcuts
    timestampTemplate: "{{label}} (~ {{hour}}:00)"
  datesAreComplicated: "My attendance dates are a bit complicated:"
  datesAreSimple: "My attendance dates are simple:"
  errors:
    addAStartDate: Add your arrival date.
    addAnEndDate: Add your departure date.
  iArriveOn: I arrive on
  infoMessage: >-
    The event takes place from <strong>{{start}}</strong> to <strong>{{end}}</strong> inclusive. <br/>If you are still
    unsure when you will be present, do your best: you will be able to modify your dates later.
  linkedToStewardAlert:
    dates:
      description: |
        <p>The organizers have set your attendance dates, visible below.</p>
        <p>
          <strong>If you notice any differences</strong> between your dates and the ones
          entered by the organizers, please contact them to
          <strong>update your dates</strong>.
        </p>
      title: Your dates of presence as a supervisor
    description: >-
      If this information is incorrect or if your attendance dates are different from those posted, contact the
      organizing team.
    message: You have been designated as a supervisor of the event, under the name {{name}}
  timestamps:
    afterDinner: after dinner
    afterLunch: after lunch
    earlyInTheMorning: early in the morning
    inTheLateAfternoon: in the late afternoon
    inTheMorning: in the morning
  title: When will you be present at the event?
backToForm: Back to form
form:
  mandatoryFieldsNotice: Fields preceded by an asterisk <redColor>*</redColor> are mandatory.
  title: Registration form
groupedSmsButtons:
  noPhoneNumbersFound: No phone numbers found.
  tooltip: Send a grouped SMS message to all participants
  useThisFeatureOnAPhone: To send SMS messages, use your phone :)
label: Participant
labelMyRegistration: My registration
labelRegister: Register
label_edit: Edit a participant
label_other: Participants
list:
  advancedMode: Advanced mode
  dataDisplayOptionsTitle: Data display
  displayUnbookedUsers: Show unregistered and non-registered participants
  groupedPdfPlanningExportButton:
    customFileName: Selection of participant schedules
    tooltip: Export the schedules of selected participants in one go.
main:
  erase: Clear
  eventNotOpen:
    comeBackLater: Registrations are closed for now. Come back a little later.
    full: Oh damn! The event is sold out.
    notYet: Oh damn! The event is not yet open.
  helpToast: Need help? Click here to find out how to register.
  registerToTheEvent: Validate my registration
  saveModifications: Save changes
  saveProgressButton:
    title: Save my progress
    tooltip: Save your registration to come back to it later.
  unregisterButton:
    linkedToStewardCantUnregister: You are associated with a supervisor, you cannot unregister by yourself.
    thisWillUnsubscribeParticipantsFromSessions: This will also unregister people from <br /> their sessions and teams if they have any.
    title: Unregister
    yesRestartFromBeginning: Yes, I want to start over.
    yesUnregister: Yes, unregister.
    youWillBeUnregisteredFromActivities: You will be unregistered from all your activities.
    youWillLooseYouProgress: Everything you have entered on this page will be deleted.
  validateAndProceed: Validate and proceed
  welcomeMessages:
    newUser:
      description: |
        You are on the registration page of the {{projectName}} event. 
        To register, simply fill in the necessary information on this page,
        then click on "Validate my registration".
      message: Welcome, {{userName}} !
    registrationComplete:
      description: |
        <p>
          You can change your information at any time by clicking on "Save changes".
        </p>
        Access your schedule by clicking on <linkToPlanning><strong>"My Schedule"</strong></linkToPlanning>.
      message: You are registered.
    registrationSaved:
      description: Your registration details have been saved. Please enter the last required details to register.
      message: You're almost there.
messages:
  bravoYouAreRegistered:
    comeBackWhenSubscriptionsAreOpen: Come back later when registrations to activities are open, so you can register to the planning. See you soon 👋
    subTitle: Register now for activities!
    title: Congratulations, you are registered! 🎉
  defaultError: >-
    Oops, you found a bug! There was a small problem during the registration of your data. Please inform the organizing
    team.
  registrationIncomplete: Your registration is not yet complete.
  registrationIncompleteCanAlreadySaveProgress: Your registration is not complete yet, but you can already save your progress if you wish :)
  registrationIncompleteCantSaveModifications: Your registration is not complete, you cannot save your changes.
  registrationIncompleteShouldSetDatesOfPresence: Your registration is not complete. Start by filling in your attendance dates :)
  savedProgress: Fill in all the required information to validate your registration. Your progress has been saved.
  unregistrationSuccessful: Unregistered successfully!
notifIncomplete:
  completeMyRegistration: Complete my registration
  form: <strong>Incomplete form :</strong> Fill in the required fields to validate your registration.
  ticketing: <strong>Ticket required:</strong> Enter a ticket number to validate your registration.
  title: Your registration is not yet complete.
numberOfDaysOfPresence:
  label: Days
participantsAndStats: Participants & stats
registrationHelpButton:
  consultHelpCenter:
    description: Consult the NOÉ Help Center
    subtitle: Tool operation, personal data, etc.
    title: I have a question related to the use of the tool
  contactEventTeam:
    description: Contact the event team
    emailSubject: I have a problem with my NOÉ registration
    subtitle: Form, ticketing, session registration, etc.
    title: I have a problem with my registration
  contactNOETeam:
    description: Contact the NOÉ team
    emailSubject: I have a bug on NOÉ
    subtitle: Bug, error page, unexpected behavior, etc.
    title: The platform does not work
registrationValidation:
  endCannotBeBeforeStart: Your departure date cannot be earlier than your arrival date.
  selectDatesToRegister: Select an arrival date and a departure date to register.
  someFormFieldsAreMandatory: 'Some mandatory fields of the form remain to be filled: "{{list}}"'
  ticketingIsMandatory: You must book a ticket to register. If you already have yours, you can fill it in manually.
roles:
  orgaOfTheEvent: organizer of the event
  stewardOnThisSession: supervisor of this session
schema:
  arrivalDateTime:
    label: Arrival date
  departureDateTime:
    label: Departure date
  everythingIsOk:
    label: Registered
  hasCheckedIn:
    label: Arrived
  hidden:
    label: Hidden
  role:
    addRole: Add new rights
    label: Rights
    options:
      admin:
        description: Viewing, creation, edition and deletion rights. Access to advanced configuration and rights management.
        label: Administrator
      contrib:
        description: Consultation, creation and publishing rights.
        label: Contributor
      guest:
        description: Viewing rights only.
        label: Guest
    placeholder: rights
    roleScale: Scale of rights
  steward:
    label: Linked supervisor
  tags:
    label: Tags
  team:
    label: Team
  tickets:
    label: Tickets
  voluntaryCounter:
    label: Volunteering
ticketing:
  add: Add
  addAnotherTicket: "Submit an additional ticket:"
  addManually: "Already have your ticket? Add it here:"
  addYourTicketHere: "Enter your ticket number here:"
  alreadyHaveTicket: Already have your ticket?
  helloAssoAlert: >-
    Below, you'll find an <strong>Hello Asso payment page</strong> 👇 This is where you can buy your ticket. <br/>
    <strong>Give the same email address</strong> as the one with which you created your NOÉ account (<i>{{email}}</i>).
    This way, NOÉ will know it's really you who makes the purchase 😌
  labelBookATicket: Buy your ticket
  labelIHaveATicket: I have a ticket number
  labelMyTickets: My tickets
  messages:
    alreadyUsedTicket: The ticket has already been used.
    ticketAdded: Ticket added!
    ticketNotFound: The ticket number was not found.
    unregisterBeforeRemovingTicket: To remove all your tickets, unregister from the event first.
  schema:
    amount: Amount
    article: Article
    id: Ticket number
    options: Options
    userName: Name
  thanksToTiBilletAlert: |
    NOÉ is proud to work with <linkToTiBillet>TiBillet</linkToTiBillet>, an open-source 
    and cooperative ticketing service. They are the ones who manage the tickets stuff: buy yours just below 👇
  ticketInputPlaceholder: ticket number
  transfer:
    button:
      label: Transfer
      tooltip: Transfer your ticket to another participant in just one click!
    infoAlert: >-
      <p>
        You can transfer your ticket to another participant by entering the email address of their NOÉ account.
        They will then receive an email informing them of the transfer.
      </p> Ticket transfer does not include money transfer. If your ticket was paid for, make sure you collect the money
      from the recipient by your own means.
    messages:
      notFound: Oh oh… it didn't work. Does the recipient have a NOÉ account?
      success: Transfer successful! The recipient has received confirmation by email to {{toEmail}}.
    modalTitle: Transfer my ticket
    toEmail:
      errorCantBeSameEmailAsUser: The recipient's email must be different from your own account.
      label: Recipient's e-mail address
    transfer: Transfer
    transferAndUnregister: Transfer and unregister
    transferredFromEmailOnDate: Forwarded from email {{fromEmail}} on {{date}}
    warningAlert: "Ticket scams are common: buy and sell your ticket only between people you trust."
    warningOnlyOneTicketAlert:
      description: >-
        If you transfer your ticket, you will be <strong>automatically unregistered</strong> from the Event and your
        registrations for activities will be deleted.
      message: You only have one ticket!
  youHaveYourTicket: All good, you have your ticket.
volunteeringGauge:
  fullRegistrationRecap: >-
    You have {{numberOfDaysOfPresence}} days of attendance, and your total volunteer time is {{total}}. Per day, that's
    {{totalPerDay}}.
  fullRegistrationRecapAbsolute: Your total volunteer time is {{total}}.
  hint:
    itsPerfect: "Not too much, not too little: your volunteer gauge is perfect."
    lessNeeded: You do a lot of volunteer work... Remember to take some time for yourself!
    moreNeeded: Register for more volunteer work to fill your gauge.
  projectRecap: The recommended number of hours per day is between {{min}} and {{max}}.
