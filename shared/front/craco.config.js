const CracoLessPlugin = require("craco-less");
const SentryWebpackPlugin = require("@sentry/webpack-plugin");
const {resolve} = require("node:path");

const deploySentryRelease =
  process.env.NODE_ENV === "production" &&
  process.env.SENTRY_PROJECT_NAME &&
  process.env.SENTRY_AUTH_TOKEN;

const getGitHash = () =>
  require("child_process")
    .execSync("git config --global --add safe.directory /app && git rev-parse HEAD")
    .toString()
    .trim();

/**
 * @type {import("@craco/types").CracoConfig}
 */
module.exports = {
  devServer: {
    client: {
      overlay: {
        // Do not show errors related to Resize observer loop
        runtimeErrors: (error) => {
          // Do not show errors related to Resize observer loop
          if (error.message.includes("ResizeObserver loop")) {
            return false;
          }
          return true;
        },
      },
    },
  },
  webpack: {
    configure: (webpackConfig, {env, paths}) => {
      // Remove the "Relative imports outside of src/ are not supported" limit from create-react-app
      const scopePluginIndex = webpackConfig.resolve.plugins.findIndex(
        ({constructor}) => constructor && constructor.name === "ModuleScopePlugin"
      );
      webpackConfig.resolve.plugins.splice(scopePluginIndex, 1);

      // Don't watch node_modules
      // https://webpack.js.org/configuration/watch/#watchoptionsignored
      webpackConfig.watchOptions = {ignored: /node_modules/};

      // Add aliases for shared assets which is inside the shared folder (outside the src/ folder)
      // Aliases are defined in the tsconfig.json
      webpackConfig.resolve.alias = {
        ...webpackConfig.resolve.alias,
        // Shared folder
        // All the shared/ folder is actually copied inside the Docker container at src/shared in frontend folders.
        "@config": resolve(__dirname, "src/app/config"),
        // Images, styles and locales stay out of it
        "@images": resolve(__dirname, "../shared/images"),
        "@styles": resolve(__dirname, "../shared/styles"),
        "@locales": resolve(__dirname, "../shared/locales"),

        // Frontend folders
        "@shared": resolve(__dirname, "src/shared"),
        "@app": resolve(__dirname, "src/app"),
        "@features": resolve(__dirname, "src/features/"),
        "@routes": resolve(__dirname, "src/routes/"),
        "@utils": resolve(__dirname, "src/utils/"),
      };

      // YML Locales files loader
      webpackConfig.module.rules.push({
        test: /locales/,
        loader: "@turtle328/i18next-loader",
        options: {basenameAsNamespace: true},
      });

      // Ignore warnings about source maps (don't display them)
      webpackConfig.ignoreWarnings = [/Failed to parse source map/];

      // Activate source maps for Sentry
      if (deploySentryRelease) {
        webpackConfig.devtool = "source-map";

        webpackConfig.plugins.push(
          new SentryWebpackPlugin({
            org: "noe-app",
            project: process.env.SENTRY_PROJECT_NAME,

            // Specify the directory containing build artifacts
            include: "./build",

            // Auth tokens can be obtained from https://sentry.io/settings/account/api/auth-tokens/
            // and needs the `project:releases` and `org:read` scopes
            authToken: process.env.SENTRY_AUTH_TOKEN,

            // Optionally uncomment the line below to override automatic release name detection
            release: getGitHash(),
          })
        );
      }

      return webpackConfig;
    },
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
