const fs = require("fs");
const path = require("path");

/**
 * Lightens the export by removing unnecessary data and creates multiple files
 * @param {string} filename - The name of the file to process
 */
function lightenExport(filename) {
  // Read and parse the JSON file
  const data = JSON.parse(fs.readFileSync(filename, "utf8"));

  const elementsToKeep = ["project", "activities", "categories", "places", "stewards", "teams"];

  // Create a new object with only the elements we want to keep (excluding sessions)
  const lightenedExport = Object.fromEntries(
    Object.entries(data).filter(([key]) => elementsToKeep.includes(key))
  );

  // Clean each object in the lightened export
  for (const [key, value] of Object.entries(lightenedExport)) {
    if (Array.isArray(value)) {
      value.forEach((item) => cleanObject(item, key));
    } else if (typeof value === "object") {
      cleanObject(value, key);
    }
  }

  // Create a directory for the lightened exports
  const exportDir = path.join(path.dirname(filename), "lightened_export");
  if (!fs.existsSync(exportDir)) {
    fs.mkdirSync(exportDir);
  }

  // Save the main lightened export (without sessions)
  const mainExportFilename = path.join(exportDir, "main_export.json");
  fs.writeFileSync(mainExportFilename, JSON.stringify(lightenedExport, null, 2));
  console.log(`Main lightened export saved to ${mainExportFilename}`);

  // Process and save sessions by day
  if (data.sessions) {
    const sessionsByDay = groupSessionsByDay(data.sessions);
    for (const [day, sessions] of Object.entries(sessionsByDay)) {
      const cleanedSessions = sessions.map((session) => {
        const cleanedSession = cleanObject({...session}, "sessions");

        const activity = data.activities.find((a) => a._id === session.activity);
        const category = data.categories.find((c) => c._id === activity.category);

        cleanedSession.activity = activity.name;
        cleanedSession.category = category.name;

        cleanedSession.numberOfParticipants =
          session.maxNumberOfParticipants || activity.maxNumberOfParticipants;

        cleanedSession.places = session.places.map(
          (place) => data.places.find((p) => p._id === place).name
        );
        cleanedSession.stewards = session.stewards.map((steward) => {
          const stewardData = data.stewards.find((s) => s._id === steward);
          return `${stewardData.firstName} ${stewardData.lastName}`;
        });

        cleanSessionSlots(cleanedSession);
        return cleanedSession;
      });

      const sessionsFilename = path.join(exportDir, `sessions_${day}.json`);
      fs.writeFileSync(sessionsFilename, JSON.stringify(cleanedSessions, null, 2));
      console.log(`Sessions for ${day} saved to ${sessionsFilename}`);
    }
  }
}

/**
 * Cleans an object by removing unnecessary fields
 * @param {Object} obj - The object to clean
 * @param {string} objectType - The type of the object
 * @returns {Object} - The cleaned object
 */
function cleanObject(obj, objectType) {
  const fieldsToKeep = {
    project: ["_id", "name", "start", "end", "availabilitySlots"],
    activities: ["_id", "name", "category"],
    categories: ["_id", "name"],
    places: ["_id", "name"],
    sessions: ["_id", "name", "activity", "stewards", "places", "slots", "maxNumberOfParticipants"],
    stewards: ["_id", "firstName", "lastName"],
    teams: ["_id", "name", "stewards"],
    registrations: [
      "_id",
      "user",
      "steward",
      "availabilitySlots",
      "sessionsSubscriptions",
      "teamsSubscriptions",
    ],
  };

  const fields = fieldsToKeep[objectType] || [];
  for (const key of Object.keys(obj)) {
    if (!fields.includes(key)) {
      delete obj[key];
    }
  }
  return obj;
}

/**
 * Cleans the slots of a session
 * @param {Object} session - The session object to clean
 */
function cleanSessionSlots(session) {
  if (!session.slots) return;

  session.slots.forEach((slot) => {
    const stewardsSynchro = slot.stewardsSessionSynchro || false;
    const placesSynchro = slot.placesSessionSynchro || false;

    if (stewardsSynchro && "stewards" in slot) {
      delete slot.stewards;
    }
    if (placesSynchro && "places" in slot) {
      delete slot.places;
    }

    // Remove the synchro properties as they're no longer needed
    delete slot.stewardsSessionSynchro;
    delete slot.placesSessionSynchro;
  });
}

/**
 * Groups sessions by day
 * @param {Array} sessions - The array of sessions to group
 * @returns {Object} - An object with days as keys and arrays of sessions as values
 */
function groupSessionsByDay(sessions) {
  const sessionsByDay = {};
  sessions.forEach((session) => {
    if (session.slots && session.slots.length > 0) {
      const day = new Date(session.slots[0].start).toISOString().split("T")[0];
      if (!sessionsByDay[day]) {
        sessionsByDay[day] = [];
      }
      sessionsByDay[day].push(session);
    }
  });
  return sessionsByDay;
}

// Check if a filename was provided as a command line argument
if (process.argv.length !== 3) {
  console.log("Usage: node light-export.js <export_filename>");
  process.exit(1);
}

const exportFilename = process.argv[2];
lightenExport(exportFilename);
