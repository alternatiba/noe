const MongoDBScriptRunner = require("../MongoDBScriptRunner");
const {ObjectId} = require("mongodb");
const dayjs = require("dayjs");

const HOURS_SHIFT = -1;

const DRY_RUN = true;

const PROJECT_ID = "xxxxxxxxxxxxxxxxxxxxx";

const run = async (db, Collections) => {
  /******************************************************
   * SHIFT SLOTS FOR PROJECT
   ******************************************************/

  // Get all sessions for the specified project
  const projectId = ObjectId(PROJECT_ID);
  const projectSessions = await Collections.sessions.find({project: projectId}).toArray();
  console.log(`Found ${projectSessions.length} sessions for the project`);

  const shiftOperation = HOURS_SHIFT >= 0 ? "add" : "subtract";

  /////////////////////////////////////////
  // SHIFTING SESSIONS START & END DATES //
  /////////////////////////////////////////

  // Shift session start and end dates
  for (const session of projectSessions) {
    if (session.start && session.end) {
      const shiftedStart = dayjs(session.start)
        [shiftOperation](Math.abs(HOURS_SHIFT), "hour")
        .toDate();
      const shiftedEnd = dayjs(session.end)[shiftOperation](Math.abs(HOURS_SHIFT), "hour").toDate();

      if (DRY_RUN) {
        console.log(
          `[DRY RUN] Would update session ${session._id}:\n`,
          `     (old) - start: ${session.start},   end: ${session.end}\n`,
          `     (new) - start: ${shiftedStart},   end: ${shiftedEnd}`
        );
      } else {
        await Collections.sessions.updateOne(
          {_id: session._id},
          {$set: {start: shiftedStart, end: shiftedEnd}}
        );
      }
    } else {
      console.log(`Session ${session._id} does not have start and/or end dates defined.`);
    }
  }
  console.log(
    `Shifted ${projectSessions.length} sessions ${Math.abs(HOURS_SHIFT)} hour(s) ${
      HOURS_SHIFT >= 0 ? "forward" : "backward"
    }`
  );

  //////////////////////////////////////
  // SHIFTING SLOTS START & END DATES //
  //////////////////////////////////////

  // Get all slot IDs from the project's sessions
  const projectSlotIds = projectSessions.flatMap((session) => session.slots || []);

  // Get all slots that reference any of the project's sessions
  const projectSlots = await Collections.slots.find({_id: {$in: projectSlotIds}}).toArray();
  console.log(`Found ${projectSlots.length} slots for the project's sessions`);

  // Shift each slot's start and end dates one hour forward
  for (const slot of projectSlots) {
    const shiftedStart = dayjs(slot.start)[shiftOperation](Math.abs(HOURS_SHIFT), "hour").toDate();
    const shiftedEnd = dayjs(slot.end)[shiftOperation](Math.abs(HOURS_SHIFT), "hour").toDate();

    // Update the slot in the database
    if (DRY_RUN) {
      console.log(
        `[DRY RUN] Would update slot ${slot._id}:\n`,
        `     (old) - start: ${slot.start},   end: ${slot.end}\n`,
        `     (new) - start: ${shiftedStart},   end: ${shiftedEnd}`
      );
    } else {
      await Collections.slots.updateOne(
        {_id: slot._id},
        {$set: {start: shiftedStart, end: shiftedEnd}}
      );
    }
  }

  console.log(
    `Shifted ${projectSlots.length} slots ${Math.abs(HOURS_SHIFT)} hour(s) ${
      HOURS_SHIFT >= 0 ? "forward" : "backward"
    }`
  );
};
MongoDBScriptRunner.runScript(run); //, PROD_MONGO_URI, PROD_DB_NAME); //, DEMO_MONGO_URI, DEMO_DB_NAME);
