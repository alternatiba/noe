const bisyncFiles = require("bisync-files");
const {checkFoldersIdentical} = require("./checkFoldersAreSynced");
const {syncFolders} = require("./syncFolders");
const path = require("path");

/**
 * This script is used to continuously sync the shared/ folder between the
 * orga-front and inscription-front frontends.
 *
 * It wont start unless the shared/ folder is identical between the two frontends.
 *
 * It uses bisync-files to watch for changes in the shared/ folder and sync them between the two frontends.
 */

const ORGA_FRONT_SHARED_FOLDER = "orga-front/src/shared";
const INSCRIPTION_FRONT_SHARED_FOLDER = "inscription-front/src/shared";

// Vérifier si les dossiers sont identiques avant de commencer la synchronisation
const startSync = async () => {
  const {success: foldersAreIdentical, nonIdenticalFiles} = await checkFoldersIdentical(
    ORGA_FRONT_SHARED_FOLDER,
    INSCRIPTION_FRONT_SHARED_FOLDER
  );

  if (foldersAreIdentical) {
    // Watch frontends shared folder
    console.log("Starting sync...");
    bisyncFiles.watch(
      path.resolve(ORGA_FRONT_SHARED_FOLDER),
      path.resolve(INSCRIPTION_FRONT_SHARED_FOLDER)
    );
  } else {
    await syncFolders(ORGA_FRONT_SHARED_FOLDER, INSCRIPTION_FRONT_SHARED_FOLDER);
  }
};

startSync();
