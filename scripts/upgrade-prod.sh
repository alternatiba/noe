SERVER_IP=$1
ENVS_REPO_FOLDER=$2
ENV_NAME=$3
BRANCH_NAME=$4
DELAY_AFTER_INSCRIPTION_FRONT=$5
DELAY_AFTER_API=$6
DELAY_AFTER_ORGA_FRONT=$7
DEPLOY_WEBSITE=$8
DELAY_AFTER_WEBSITE=$9

SCRIPTS_DIRECTORY="$(dirname -- "$0")"

echo
echo
echo "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"
echo "■■■■■■■■■■■■■■■■■■ CONFIGURATION ■■■■■■■■■■■■■■■■■■■"
echo "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"

echo
echo "SERVER_IP: $SERVER_IP"
echo "ENVS_REPO_FOLDER: $ENVS_REPO_FOLDER"
echo "ENV_NAME: $ENV_NAME"
echo "BRANCH_NAME: $BRANCH_NAME"
echo "DELAY_AFTER_INSCRIPTION_FRONT: $DELAY_AFTER_INSCRIPTION_FRONT"
echo "DELAY_AFTER_API: $DELAY_AFTER_API"
echo "DELAY_AFTER_ORGA_FRONT: $DELAY_AFTER_ORGA_FRONT"
echo "DEPLOY_WEBSITE: $DEPLOY_WEBSITE"
echo "DELAY_AFTER_WEBSITE: $DELAY_AFTER_WEBSITE"

echo
echo
echo "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"
echo "■■■■■■■■■■■■■■■ PREPARING DEPLOYMENT ■■■■■■■■■■■■■■■"
echo "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"

echo
echo
echo "■■■■■■■■■■ Updating production configuration from folder ../$ENVS_REPO_FOLDER/$ENV_NAME."

echo "■■■■■ Pulling last changes..."
cd "$SCRIPTS_DIRECTORY/../../$ENVS_REPO_FOLDER" || exit
git pull
cd ../noe || exit

echo "■■■■■ Copying the environment folder into /noe/docker/prod-env..."
rm -r "docker/prod-env"
cp -r "../$ENVS_REPO_FOLDER/$ENV_NAME" "docker/prod-env"
source "docker/prod-env/.env"

if [ "$DEPLOY_WEBSITE" = "true" ] && [ -n "$WEBSITE_URI" ]
then
  echo "■■■■■ Website deployment is activated at address $WEBSITE_URI."
  SERVICES='api inscription-front orga-front website'
else
  SERVICES='api inscription-front orga-front'
fi


echo
echo
echo "■■■■■■■■■■ Updating the NOÉ repository files."

echo "■■■■■ Fetching last changes for the branch \"$BRANCH_NAME\"..."
git fetch origin "$BRANCH_NAME"

echo "■■■■■ Checking if there are any changes to every service in the project..."
CHANGED_FILES_GLOBAL=$(git diff-tree -r --name-only --no-commit-id "origin/$BRANCH_NAME" "$BRANCH_NAME")
if [ -n "$CHANGED_FILES_GLOBAL" ]
then
  echo $CHANGED_FILES_GLOBAL
else
  echo "No changes."
fi

echo "■■■■■ Checking if there are incoming changes to any package.json file in the project..."
CHANGED_PACKAGE_JSON_FILES=$(git diff-tree -r --name-only --no-commit-id "origin/$BRANCH_NAME" "$BRANCH_NAME" | grep "package.json")
if [ -n "$CHANGED_PACKAGE_JSON_FILES" ]
then
  echo $CHANGED_PACKAGE_JSON_FILES
else
  echo "No changes."
fi

echo "■■■■■ Resetting all files on the remote origin of branch \"$BRANCH_NAME\". Checking out branch..."
git reset --hard "origin/$BRANCH_NAME"
git checkout "$BRANCH_NAME"


echo
echo
echo "■■■■■■■■■■ Updating dependencies when needed..."
for service in $SERVICES
do
  if [ "$(echo "$CHANGED_PACKAGE_JSON_FILES" | grep "$service" | wc -c)" -gt 1 ]
  then
    echo "■■■■■ package.json has changed in container $service. Reinstalling node_modules."
    make install-deps-prod-$service
  fi
done

if [ "$(echo "$CHANGED_PACKAGE_JSON_FILES" | grep "shared" | wc -c)" -gt 1 ]
then
  echo "■■■■■ package.json has changed in shared/ folder. Reinstalling node_modules for both orga-front and inscription-front."
  make install-deps-prod-front
fi

echo
echo
echo "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"
echo "■■■■■■■■■■■■■■■■ READY FOR LAUNCH ! ■■■■■■■■■■■■■■■■"
echo "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"

if [ "$(echo "$CHANGED_FILES_GLOBAL" | grep -E "inscription-front|shared" | wc -c)" -gt 1 ]
then
  echo
  echo
  echo "■■■■■■■■■■ Source files in inscription-front have changed. Restarting container and waiting $DELAY_AFTER_INSCRIPTION_FRONT..."
  make restart-prod-inscription-front && sleep "$DELAY_AFTER_INSCRIPTION_FRONT"
fi

if [ "$(echo "$CHANGED_FILES_GLOBAL" | grep -E "api|shared/locales|shared/images" | wc -c)" -gt 1 ]
then
  echo
  echo
  echo "■■■■■■■■■■ Source files in api have changed. Restarting container and waiting $DELAY_AFTER_API..."
  make restart-prod-api && sleep "$DELAY_AFTER_API"
fi

if [ "$(echo "$CHANGED_FILES_GLOBAL" | grep -E "orga-front|shared" | wc -c)" -gt 1 ]
then
  echo
  echo
  echo "■■■■■■■■■■ Source files in orga-front have changed. Restarting orga-front container and waiting $DELAY_AFTER_ORGA_FRONT..."
  make restart-prod-orga-front && sleep "$DELAY_AFTER_ORGA_FRONT"
fi

if [ "$(echo "$CHANGED_FILES_GLOBAL" | grep -E "website|shared/images|shared/styles" | wc -c)" -gt 1 ] && [ "$DEPLOY_WEBSITE" = "true" ]
then
  echo
  echo
  echo "■■■■■■■■■■ Source files in website have changed. Restarting website container..."
  make restart-prod-website && sleep "$DELAY_AFTER_WEBSITE"
fi

echo
echo
echo "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"
echo "■■■■■■■■■■■■■■■■■ RESTART COMPLETE ■■■■■■■■■■■■■■■■■"
echo "■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■"