const fs = require("fs");
const path = require("path");
const crypto = require("crypto");
const {log} = require("console");

/**
 * This script checks if two folders are identical.
 * It takes the paths of two folders, from the project root.
 * It returns an object with a success flag and, if not successful, a list of non-identical files.
 *
 * The script can be run in verbose mode to display detailed logs during the comparison process.
 */

/**
 * Calculates the MD5 hash of a file
 * @param {string} filePath - The path to the file
 * @returns {Promise<string>} A promise that resolves to the MD5 hash of the file
 */
const calculateFileHash = (filePath) => {
  return new Promise((resolve, reject) => {
    const hash = crypto.createHash("md5");
    const stream = fs.createReadStream(filePath);
    stream.on("data", (data) => hash.update(data));
    stream.on("end", () => resolve(hash.digest("hex")));
    stream.on("error", reject);
  });
};

/**
 * Checks if two folders are identical
 * @param {string} folder1 - Path to the first folder
 * @param {string} folder2 - Path to the second folder
 * @param {boolean} verbose - If "debug", enables verbose logging. If "result", shows only the result. If not given, doesn't show anything
 * @returns {Promise<{success: boolean, nonIdenticalFiles?: Array<{newest: string, oldest?: string, missing?: string}>}>} An object indicating if the folders are identical and, if not, the list of non-identical files
 */
const checkFoldersIdentical = async (folder1, folder2, verbose) => {
  /**
   * Recursively compares files in two directories
   * @param {string} dir1 - Path to the first directory
   * @param {string} dir2 - Path to the second directory
   * @param {number} [index=0] - Indentation level for logging
   * @returns {Promise<{success: boolean, nonIdenticalFiles: Array<{newest: string, oldest?: string, missing?: string}>}>} Comparison result
   */
  const compareFiles = async (dir1, dir2, index = 0) => {
    const nonIdenticalFiles = [];
    let files1 = fs.readdirSync(dir1);
    const files2 = fs.readdirSync(dir2);

    // Create a logging function that respects the verbose flag
    const log = (...message) => {
      if (verbose === "debug") {
        console.log("   ".repeat(index) + "| " + message[0], ...message.slice(1));
      }
    };

    if (verbose === "debug") console.log();
    log("Comparing files in", path.relative(".", dir1), "and", path.relative(".", dir2));

    // Check if the directories have the same number of files
    if (files1.length !== files2.length) {
      const filesInDir1NotInDir2 = files1.filter((file) => !files2.includes(file));
      const filesInDir2NotInDir1 = files2.filter((file) => !files1.includes(file));

      log("> Some files are missing in one of the directories.");
      log("   Not in", path.relative(".", dir2), ":", filesInDir1NotInDir2);
      log("   Not in", path.relative(".", dir1), ":", filesInDir2NotInDir1);

      // Add missing files to nonIdenticalFiles array
      nonIdenticalFiles.push(
        ...filesInDir1NotInDir2.map((file) => ({
          newest: path.relative(".", path.join(dir1, file)),
          missing: path.relative(".", path.join(dir2, path.relative(dir1, path.join(dir1, file)))),
        })),
        ...filesInDir2NotInDir1.map((file) => ({
          newest: path.relative(".", path.join(dir2, file)),
          missing: path.relative(".", path.join(dir1, path.relative(dir2, path.join(dir2, file)))),
        }))
      );

      log("  Pushed to non-identical files:", nonIdenticalFiles);

      // Remove missing files from files1 to avoid further comparison
      files1 = files1.filter((file) => !filesInDir1NotInDir2.includes(file));
    }

    // Compare remaining files
    for (const file of files1) {
      const filePath1 = path.relative(".", path.join(dir1, file));
      const filePath2 = path.relative(".", path.join(dir2, file));
      log("> Comparing", file);

      let stat1, stat2;
      try {
        stat1 = fs.statSync(filePath1);
        stat2 = fs.statSync(filePath2);

        // Check if both are directories or both are files
        if (stat1.isDirectory() !== stat2.isDirectory()) {
          log("  One of the files is a directory and the other is a file. Stopping comparison.");
          return {success: false, nonIdenticalFiles};
        }
      } catch (error) {
        log("  Can't access one of the directories.", error);
        return {success: false, nonIdenticalFiles};
      }

      if (stat1.isDirectory()) {
        log("  Both are directories. Recursively comparing subdirectories.");
        // If directory : recursively compare subdirectories
        const result = await compareFiles(filePath1, filePath2, index + 1);
        if (!result.success) {
          log("  Non-identical files in subdirectories:", result.nonIdenticalFiles);
          nonIdenticalFiles.push(...result.nonIdenticalFiles);
          log("  Pushed to non-identical files:", nonIdenticalFiles);
        }
        if (verbose === "debug") console.log();
      } else {
        // If file : compare file hashes
        const hash1 = await calculateFileHash(filePath1);
        const hash2 = await calculateFileHash(filePath2);
        if (hash1 !== hash2) {
          log("  File hashes are different.");
          const newestFile = stat1.mtime > stat2.mtime ? filePath1 : filePath2;
          const oldestFile = stat1.mtime > stat2.mtime ? filePath2 : filePath1;
          log("   Newest file:", newestFile);
          log("   Oldest file:", oldestFile);
          nonIdenticalFiles.push({
            newest: newestFile,
            oldest: oldestFile,
          });
          log("  Pushed to non-identical files:", nonIdenticalFiles);
        }
      }
    }

    return {success: nonIdenticalFiles.length === 0, nonIdenticalFiles};
  };

  try {
    const folder1AbsolutePath = path.resolve(folder1);
    const folder2AbsolutePath = path.resolve(folder2);
    const result = await compareFiles(folder1AbsolutePath, folder2AbsolutePath);

    console.log(result.success ? "Folders are identical." : "Folders are not identical.");

    if (result.nonIdenticalFiles?.length > 0 && (verbose === "result" || verbose === "debug")) {
      console.log("Non-identical files:");
      console.log("--------------------------------------------------------------------");
      result.nonIdenticalFiles.forEach((file) => {
        console.log(`${file.newest.split("/").pop()} :`);
        if (file.newest) console.log(`  New: ${file.newest}`);
        if (file.oldest) console.log(`  Old : ${file.oldest}`);
        if (file.missing) console.log(`  Missing: ${file.missing}`);
        console.log("--------------------------------------------------------------------");
      });
    }

    return result;
  } catch (error) {
    console.error("Error while checking folders:", error);
    return {success: false};
  }
};

// Execute the function only if launched from the command line
if (require.main === module) {
  // Get command line arguments
  const [, , folder1, folder2, verboseArg] = process.argv;
  const verbose = verboseArg === "--verbose" || verboseArg === "-v";

  if (!folder1 || !folder2) {
    console.error("Error: Please provide two folder paths as arguments.");
    process.exit(1);
  }

  checkFoldersIdentical(folder1, folder2, verbose ? "debug" : "result").then((result) => {
    if (result.success) {
      process.exit(0);
    } else {
      process.exit(1);
    }
  });
}

// Export the function for use in other modules
module.exports = {checkFoldersIdentical};
