const {checkFoldersIdentical} = require("./checkFoldersAreSynced");
const path = require("path");
const fs = require("fs");
const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

const syncFoldersBase = async (nonIdenticalFiles) => {
  for (const file of nonIdenticalFiles) {
    if (file.missing) {
      // Create missing file
      await fs.promises.mkdir(path.dirname(file.missing), {recursive: true});
      await fs.promises.copyFile(file.newest, file.missing);
      console.log(`File created : ${file.missing}`);
    } else {
      // Overwrite oldest with newest
      await fs.promises.copyFile(file.newest, file.oldest);
      console.log(`File updated : ${file.oldest}`);
    }
  }

  console.log("Synchronization done.");
};

const syncFolders = async (folder1, folder2, auto = false) => {
  const {success, nonIdenticalFiles} = await checkFoldersIdentical(folder1, folder2, "result");

  if (success) {
    console.log("No need to synchronize.");
    process.exit(0);
  }

  if (auto) {
    await syncFoldersBase(nonIdenticalFiles);
  } else {
    readline.question(
      "Do you want to synchronize the files automatically ? (yes/no) ",
      async (answer) => {
        if (answer.toLowerCase() === "yes") {
          await syncFoldersBase(nonIdenticalFiles);
        } else {
          console.log("Synchronization cancelled. Stopping.");
        }
        readline.close();
        process.exit(0);
      }
    );
  }
};

// Execute the function only if launched from the command line
if (require.main === module) {
  // Get command line arguments
  const [, , folder1, folder2] = process.argv;

  if (!folder1 || !folder2) {
    console.error("Error: Please provide two folder paths as arguments.");
    process.exit(1);
  }

  syncFolders(folder1, folder2);
}

// Export the function for use in other modules
module.exports = {syncFolders};
