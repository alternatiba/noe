import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {categoriesActions, categoriesSelectors} from "@features/categories";
import {ListPage} from "@shared/pages/ListPage";
import {ColorDot} from "@shared/components/ColorDot";
import {useTranslation} from "react-i18next";
import {TagOutlined} from "@ant-design/icons";
import {useLoadList} from "@shared/hooks/useLoadList";
import {sorter} from "@shared/utils/sorters";
import {editableCellColumn} from "@shared/components/EditableCell";
import {useColumnsBlacklistingSelect} from "@shared/hooks/useColumnsBlacklistingSelect";
import {useCustomFieldsColumns} from "@shared/utils/columns/useCustomFieldsColumns";

function CategoryList() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const categories = useSelector(categoriesSelectors.selectList);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "categories",
  });
  const columns = [
    {
      title: t("categories:schema.color.label"),
      dataIndex: "color",
      width: "80pt",
      render: (text, record) => <ColorDot color={record.color} />,
    },
    {
      ...editableCellColumn({
        title: t("common:schema.name.label"),
        dataIndex: "name",
        type: "text",
        placeholder: t("common:schema.name.placeholder"),
        required: true,
        elementsActions: categoriesActions,
      }),
      sorter: (a, b) => sorter.text(a.name, b.name),
      searchable: true,
    },
    {
      ...editableCellColumn({
        title: t("common:schema.summary.label"),
        dataIndex: "summary",
        type: "longText",
        placeholder: t("common:schema.summary.placeholder"),
        elementsActions: categoriesActions,
      }),
      searchable: true,
    },
    ...useCustomFieldsColumns({
      endpoint: "categories",
      elementsActions: categoriesActions,
    }),
  ];

  useLoadList(() => {
    dispatch(categoriesActions.loadList());
  });

  return (
    <ListPage
      icon={<TagOutlined />}
      i18nNs="categories"
      searchInFields={["name"]}
      elementsActions={categoriesActions}
      elementsSelectors={categoriesSelectors}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      columns={filterBlacklistedColumns(columns)}
      dataSource={categories}
      groupEditable
      groupImportable
    />
  );
}

export default CategoryList;
