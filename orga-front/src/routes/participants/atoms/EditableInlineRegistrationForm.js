import React, {useState} from "react";
import {App, Button, Form} from "antd";
import {PendingSuspense} from "@shared/components/Pending";
import {safeValidateFields} from "@shared/inputs/FormElement";
import {registrationsActions} from "@features/registrations";
import {useDispatch, useSelector} from "react-redux";
import {projectsSelectors} from "@features/projects";
import {useTranslation} from "react-i18next";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";

const RegistrationForm = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/RegistrationForm")
);

export const EditableInlineRegistrationForm = React.memo(
  ({record}) => {
    const {message} = App.useApp();
    const {t} = useTranslation();
    const dispatch = useDispatch();
    const project = useSelector(projectsSelectors.selectEditing);

    const [isModified, setIsModified] = useState(false);
    const [form] = Form.useForm();

    return (
      <PendingSuspense>
        <RegistrationForm
          formComponents={project.formComponents}
          initialValues={record.formAnswers}
          form={form}
          onChange={() => setIsModified(true)}
        />
        <Button
          type={"primary"}
          disabled={!isModified}
          onClick={() =>
            safeValidateFields(
              form,
              () => {
                dispatch(
                  registrationsActions.persist({
                    _id: record._id,
                    formAnswers: form.getFieldsValue(true),
                  })
                );
                setIsModified(false);
              },
              () => message.error(t("common:editPage.formInvalid"))
            )
          }>
          {t("common:editPage.buttonTitle.edit")}
        </Button>
      </PendingSuspense>
    );
  },
  (pp, np) => JSON.stringify(pp.initialValues || {}) === JSON.stringify(np.initialValues || {})
);
