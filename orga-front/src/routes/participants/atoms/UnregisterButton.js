import {Button, Popconfirm} from "antd";
import {DeleteOutlined} from "@ant-design/icons";
import React from "react";
import {registrationsActions} from "@features/registrations";
import {useDispatch} from "react-redux";

export const UnregisterButton = ({
  entityIds,
  title,
  onClick,
}: {
  entityIds?: Array<string>,
  title?: React.Node,
}) => {
  const dispatch = useDispatch();

  const unregisterFromProject = () => {
    entityIds?.length > 0
      ? entityIds.forEach((id) => dispatch(registrationsActions.unregister(id)))
      : dispatch(registrationsActions.unregister());
    onClick?.();
  };

  const disabled = entityIds && entityIds.length === 0;

  return (
    <Popconfirm
      title={title}
      okButtonProps={{danger: true}}
      disabled={disabled}
      onConfirm={unregisterFromProject}>
      <Button danger type="link" icon={<DeleteOutlined />} disabled={disabled}>
        Désinscrire
      </Button>
    </Popconfirm>
  );
};
