import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {EditPage} from "@shared/pages/EditPage";
import {GetPdfPlanningButton} from "@shared/components/buttons/GetPdfPlanningButton";
import {currentProjectSelectors} from "@features/currentProject";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {Alert, Button, Form, Popconfirm} from "antd";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {teamsActions, teamsSelectors} from "@features/teams";
import {TableElement} from "@shared/components/TableElement";
import {stewardsActions, stewardsSelectors} from "@features/stewards";
import {PendingSuspense} from "@shared/components/Pending";
import {CardElement} from "@shared/components/CardElement";
import {personName, pick} from "@shared/utils/utilities";
import {getSessionSubscription} from "@utils/registrationsUtilities";
import {Link, useLocation, useNavigate} from "react-router-dom";
import {DeleteOutlined, ScheduleOutlined, TeamOutlined, UserOutlined} from "@ant-design/icons";
import {TextInput} from "@shared/inputs/TextInput";
import {SwitchInput} from "@shared/inputs/SwitchInput";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {UnregisterButton} from "./atoms/UnregisterButton";
import {ChangeIdentityButton} from "./atoms/ChangeIdentityButton";
import {useLoadEditing} from "@shared/hooks/useLoadEditing";
import {WaitingInvitationTag} from "@shared/components/WaitingInvitationTag";
import {useRegistrationTicketsColumns} from "@utils/columns/useRegistrationTicketsColumns";
import {TextAreaInput} from "@shared/inputs/TextAreaInput";
import {SelectInput} from "@shared/inputs/SelectInput";
import {WithEntityLinks} from "@shared/components/WithEntityLinks";
import {TagsSelectInput} from "@shared/components/TagsSelectInput";
import {RoleTag} from "@shared/components/RoleTag";
import {SessionNoShowCheckbox} from "@shared/components/SessionNoShowCheckbox";
import {useSessionsColumns} from "@utils/columns/useSessionsColumns";
import {useTeamsColumns} from "@utils/columns/useTeamsColumns";
import {generateSubscriptionInfo} from "@utils/columns/generateSubscriptionInfo";
import {withFallBackOnUrlId} from "@shared/utils/withFallbackOnUrlId";
import {useTranslation} from "react-i18next";
import {CustomFieldsInputs} from "@shared/inputs/CustomFieldsInputs";
import {FormAvailabilitySlots} from "@shared/inputs/FormAvailabilitySlots";

const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../stewards/StewardEdit")
);
const RegistrationForm = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/RegistrationForm")
);

function RegistrationEdit({id}) {
  const {t} = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const registration = useSelector(registrationsSelectors.selectEditing);
  const registrations = useSelector(registrationsSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const teams = useSelector(teamsSelectors.selectList);
  const stewards = useSelector(stewardsSelectors.selectList);
  const stewardId = registration.steward?._id || registration.steward;
  const endpoint = "registrations";
  const setIsModified = EditPage.useSetIsModified({
    resetOnUnmount: true,
    endpoint,
    id: registration?._id,
  });
  const [registrationForm] = Form.useForm();

  const registrationTickets = registration[`${currentProject.ticketingMode}Tickets`];

  const teamsRegistrations = teams.filter((team) =>
    registration.teamsSubscriptions?.find((ts) => ts.team._id === team._id)
  );
  const sessionsRegistrations = sessions.filter((session) =>
    getSessionSubscription(registration, session)
  );
  const stewardRegistrations = stewardId
    ? sessions
        .filter((session) => session.stewards.find((steward) => steward._id === stewardId))
        .map((session) => ({...session, isSteward: true}))
    : [];

  const ticketsColumns = useRegistrationTicketsColumns();

  const groupEditing = location?.state?.groupEditing;

  useLoadEditing(registrationsActions, id, () => {
    dispatch(sessionsActions.loadList());
    dispatch(teamsActions.loadList());
    dispatch(stewardsActions.loadList());
    dispatch(registrationsActions.loadList());
  });

  const sessionsColumnsBase = useSessionsColumns("./../..", currentProject.usePlaces, false, true);

  const onValidation = async (formData, fieldsKeysToUpdate) => {
    const payload = {
      ...registration,
      ...formData,
      formAnswers: registrationForm.getFieldsValue(true),
    };

    if (fieldsKeysToUpdate) {
      // Pick also start and end because they are together with the rest
      const onlyFieldsToUpdate = pick(payload, fieldsKeysToUpdate);

      for (const entityId of groupEditing.elements) {
        const entityPayload = {...onlyFieldsToUpdate, _id: entityId};

        // Modify only form answers that have been touched
        if (onlyFieldsToUpdate.formAnswers) {
          const groupEditedRegistration = registrations.find(
            (registration) => registration._id === entityId
          );
          entityPayload.formAnswers = {
            ...groupEditedRegistration.formAnswers,
            ...entityPayload.formAnswers,
          };
        }

        await dispatch(registrationsActions.persist(entityPayload));
      }
    } else {
      return dispatch(registrationsActions.persist(payload));
    }
  };

  const deleteFromProject = () => {
    dispatch(registrationsActions.remove(registration._id));
    navigate(-1); // TODO check
  };

  const noShowColumn = {
    title: "No show",
    dataIndex: "hasNotShownUp",
    render: (text, record) => (
      <SessionNoShowCheckbox
        session={record}
        registration={registration}
        registrations={registrations}
        disabled
      />
    ),
  };

  const sessionSubscriptionInfoColumn = {
    title: "Inscription",
    width: "300px",
    render: (text, record) =>
      generateSubscriptionInfo(
        registration,
        getSessionSubscription(registration, record),
        registrations,
        true
      ),
  };

  const teamsColumns = useTeamsColumns("./../..");
  const teamSubscriptionInfoColumn = {
    title: "Inscription",
    width: "300px",
    render: (text, record) => {
      const teamSubscription = registration.teamsSubscriptions.find(
        (ss) => ss.team._id === record._id
      );
      return generateSubscriptionInfo(registration, teamSubscription, registrations);
    },
  };

  const roleNotice = registration.role && (
    <>
      {personName(registration.user)} est{" "}
      <span style={{marginLeft: 3, marginRight: -4}}>
        <RoleTag roleValue={registration.role} />
      </span>{" "}
      de l'événement.
    </>
  );

  return (
    <EditPage
      i18nNs={endpoint}
      icon={<TeamOutlined />}
      onValidation={onValidation}
      elementsActions={registrationsActions}
      record={registration}
      initialValues={{...registration, steward: stewardId}}
      customButtons={
        <>
          {!registration.booked && registration.hidden && (
            <Popconfirm
              title={<>Cela supprimera totalement la personne et son inscription</>}
              okText="Je supprime la personne"
              okButtonProps={{danger: true}}
              cancelText="Annuler"
              onConfirm={deleteFromProject}>
              <Button danger type="link" icon={<DeleteOutlined />}>
                Supprimer
              </Button>
            </Popconfirm>
          )}
          {registration.booked && (
            <>
              <UnregisterButton
                title={
                  <>
                    Cela désinscrira également la personne de <br />
                    ses sessions et équipes si elle en a.
                  </>
                }
              />
              <ChangeIdentityButton registration={registration} />
              <GetPdfPlanningButton
                customFileName={() => personName(registration.user)}
                elementsActions={registrationsActions}
                tooltip="Si le⋅a participant·e est lié⋅e à un⋅e encadrant⋅e, les inscriptions de ce⋅tte encadrant⋅e seront aussi intégrées dans l'export."
              />
            </>
          )}
        </>
      }
      groupEditing={groupEditing}>
      {registration.hidden && (
        <Alert
          style={{marginBottom: 26}}
          message="Participant⋅e caché⋅e"
          description="Cela signifie que l'inscription pour cet⋅te utilisateur⋅ice sera invisible. Iel ne verra pas l'événement dans sa liste d'événements et ne pourra pas accéder à ses informations avec son compte."
        />
      )}
      {registration.booked === false && (
        <Alert
          style={{marginBottom: 26}}
          type="warning"
          message="Participant⋅e désinscrit⋅e"
          description="La personne a été désinscrite de l'événement."
        />
      )}
      {registration.invitationToken && (
        <Alert
          showIcon
          icon={WaitingInvitationTag}
          style={{marginBottom: 26}}
          message={
            <>
              Cette personne a été invitée sur NOÉ, mais ne s'est pas encore créé de compte. Vous
              pouvez gérer les invitations{" "}
              <Link to="./../../config#members">
                dans l'onglet "Membres" de la page Configuration
              </Link>
              .
            </>
          }
        />
      )}
      {roleNotice && <Alert showIcon style={{marginBottom: 26}} message={roleNotice} />}

      <div className="container-grid two-thirds-one-third">
        <CardElement greyedOut>
          <div className="container-grid two-per-row">
            <TextInput
              label="Nom"
              value={personName(registration.user)}
              readOnly
              title="Ce champ n'est pas modifiable"
            />
            <TextInput
              label="Email"
              value={registration.user?.email}
              readOnly
              title="Ce champ n'est pas modifiable"
            />
          </div>
        </CardElement>

        <CardElement>
          <WithEntityLinks
            endpoint="stewards"
            name="steward"
            createButtonText={t("activities:schema.slots.editModal.stewards.buttonTitle")}
            ElementEdit={StewardEdit}>
            <SelectInput
              label={t("registrations:schema.steward.label")}
              icon={<UserOutlined />}
              placeholder={`sélectionnez un⋅e ${t("stewards:labelDown")}`}
              options={[
                {value: null, label: `- Pas d'${t("stewards:labelDown")} -`},
                ...stewards.map((d) => ({value: d._id, label: personName(d)})),
              ]}
              showSearch
            />
          </WithEntityLinks>
        </CardElement>
      </div>

      {!groupEditing && (
        <div className={currentProject.ticketingMode && "container-grid two-thirds-one-third"}>
          {currentProject.ticketingMode && (
            <TableElement.WithTitle
              title="Billets"
              showHeader
              fullWidth
              columns={ticketsColumns}
              dataSource={registrationTickets}
            />
          )}

          <FormAvailabilitySlots label="Dates de présence" name={"availabilitySlots"} disabled />
        </div>
      )}

      <CustomFieldsInputs form={registrationForm} endpoint={endpoint} />

      <CardElement>
        <div className="container-grid three-per-row">
          <SwitchInput label="Arrivé⋅e" name="hasCheckedIn" />

          <TextAreaInput
            label="Notes privées pour les orgas"
            name="notes"
            placeholder="notes privées"
            tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
          />

          <TagsSelectInput label="Tags" name="tags" elementsSelectors={registrationsSelectors} />
        </div>
      </CardElement>

      {currentProject.useTeams && !groupEditing && (
        <TableElement.WithTitle
          title="Inscriptions aux équipes"
          icon={<TeamOutlined />}
          showHeader
          fullWidth
          navigableRootPath="./../../teams"
          columns={[...teamsColumns, teamSubscriptionInfoColumn]}
          dataSource={teamsRegistrations}
          pagination
        />
      )}

      {!groupEditing && (
        <TableElement.WithTitle
          title="Inscriptions aux sessions"
          icon={<ScheduleOutlined />}
          subtitle={
            stewardRegistrations.length > 0 &&
            `En bleu clair figurent les sessions encadrées par ${personName(registration.user)}`
          }
          showHeader
          fullWidth
          navigableRootPath="./../../sessions"
          rowClassName={(record) => record.isSteward && "ant-table-row-selected"}
          columns={[...sessionsColumnsBase, noShowColumn, sessionSubscriptionInfoColumn]}
          dataSource={[...stewardRegistrations, ...sessionsRegistrations]}
          pagination
        />
      )}

      <CardElement title="Formulaire">
        <PendingSuspense>
          <RegistrationForm
            form={registrationForm}
            formComponents={currentProject.formComponents}
            initialValues={registration.formAnswers}
            onChange={() => setIsModified(true)}
          />
        </PendingSuspense>
      </CardElement>
    </EditPage>
  );
}

export default withFallBackOnUrlId(RegistrationEdit);
