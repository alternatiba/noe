import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {placesActions, placesSelectors} from "@features/places";

import {ListPage} from "@shared/pages/ListPage";
import {EnvironmentOutlined} from "@ant-design/icons";
import {useColumnsBlacklistingSelect} from "@shared/hooks/useColumnsBlacklistingSelect";
import {useLoadList} from "@shared/hooks/useLoadList";
import {sorter} from "@shared/utils/sorters";
import {editableCellColumn} from "@shared/components/EditableCell";
import {useTranslation} from "react-i18next";
import {useCustomFieldsColumns} from "@shared/utils/columns/useCustomFieldsColumns";

function PlaceList() {
  const dispatch = useDispatch();
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "places",
  });
  const places = useSelector(placesSelectors.selectList);
  const {t} = useTranslation();

  const columns = [
    {
      ...editableCellColumn({
        title: t("common:schema.name.label"),
        dataIndex: "name",
        type: "text",
        placeholder: t("common:schema.name.placeholder"),
        required: true,
        elementsActions: placesActions,
      }),
      sorter: (a, b) => sorter.text(a.name, b.name),
      searchable: true,
    },
    {
      ...editableCellColumn({
        title: t("places:schema.maxNumberOfParticipants.labelShort"),
        dataIndex: "maxNumberOfParticipants",
        type: "number",
        placeholder: "max",
        minMaxNumber: {min: 0},
        elementsActions: placesActions,
      }),
      sorter: (a, b) => sorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
      width: 200,
      searchable: true,
    },
    {
      ...editableCellColumn({
        title: t("common:schema.summary.label"),
        dataIndex: "summary",
        type: "longText",
        placeholder: t("common:schema.summary.placeholder"),
        elementsActions: placesActions,
      }),
      searchable: true,
    },
    ...useCustomFieldsColumns({
      endpoint: "places",
      elementsActions: placesActions,
    }),
  ];

  useLoadList(() => {
    dispatch(placesActions.loadList());
  });

  return (
    <ListPage
      icon={<EnvironmentOutlined />}
      searchInFields={["name"]}
      i18nNs="places"
      elementsActions={placesActions}
      elementsSelectors={placesSelectors}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      columns={filterBlacklistedColumns(columns)}
      dataSource={places}
      groupEditable
      groupImportable
    />
  );
}

export default PlaceList;
