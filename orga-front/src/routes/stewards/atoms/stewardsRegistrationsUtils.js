import {personName} from "@shared/utils/utilities";

export const getRegistrationsLinkedToSteward = (steward, registrations) =>
  registrations?.filter((r) => r.steward?._id === steward._id);

export const getStewardAndLinkedUsersNames = (steward, registrations) => {
  // Add the names of the steward and participants to the PDF name
  const linkedUsers = getRegistrationsLinkedToSteward(steward, registrations)
    ?.map((r) => personName(r.user))
    .join("-");
  const stewardName = personName(steward);
  return linkedUsers && stewardName !== linkedUsers
    ? `${stewardName}---${linkedUsers}`
    : stewardName;
};
