import React, {Suspense} from "react";
import {useDispatch, useSelector} from "react-redux";
import {stewardsActions, stewardsSelectors} from "@features/stewards";
import {EditPage, ElementEditProps} from "@shared/pages/EditPage";
import {GetPdfPlanningButton} from "@shared/components/buttons/GetPdfPlanningButton";
import {Button, Form, List} from "antd";
import {Link, useLocation} from "react-router-dom";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {CardElement} from "@shared/components/CardElement";
import {personName} from "@shared/utils/utilities";
import {useRegistrationEditModal} from "../config/MembersConfigTab";
import {MailOutlined, TeamOutlined, UserOutlined} from "@ant-design/icons";
import {
  getRegistrationsLinkedToSteward,
  getStewardAndLinkedUsersNames,
} from "./atoms/stewardsRegistrationsUtils";
import {TextInput} from "@shared/inputs/TextInput";
import {useLoadEditing} from "@shared/hooks/useLoadEditing";
import {WaitingInvitationTag} from "@shared/components/WaitingInvitationTag";
import {RichTextInput} from "@shared/inputs/RichTextInput";
import {TextAreaInput} from "@shared/inputs/TextAreaInput";
import {DisplayInput} from "@shared/inputs/DisplayInput";
import {isValidObjectId} from "@shared/utils/stringUtilities";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {withFallBackOnUrlId} from "@shared/utils/withFallbackOnUrlId";
import {useTranslation} from "react-i18next";
import {CustomFieldsInputs} from "@shared/inputs/CustomFieldsInputs";
import {FormAvailabilitySlots} from "@shared/inputs/FormAvailabilitySlots";

const PhoneInput = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/inputs/PhoneInput").then((module) => ({
    default: module["PhoneInput"],
  }))
);

function StewardEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const {t} = useTranslation();
  const location = useLocation();
  const dispatch = useDispatch();
  const steward = useSelector(stewardsSelectors.selectEditing);
  const registrations = useSelector(registrationsSelectors.selectList);

  const endpoint = "stewards";
  const [form] = Form.useForm();

  const registrationsLinkedToSteward = getRegistrationsLinkedToSteward(steward, registrations);
  const [onNewRegistration, _, RegistrationEditModal] = useRegistrationEditModal(
    registrations.filter((r) => r.invitationToken && !r.steward),
    {
      steward: steward._id,
      firstName: steward?.firstName,
      lastName: steward?.lastName,
    }
  );

  const persistLinkedParticipantRegistration = (selectedRegistration) => {
    {
      dispatch(
        registrationsActions.persist({
          _id:
            registrations.find((r) => r.user._id === selectedRegistration.user._id)?._id || "new", // If no id found, it means it's a new one
          user: selectedRegistration.user._id,
          steward: steward?._id,
        })
      );
    }
  };

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    stewardsActions,
    id,
    () => dispatch(registrationsActions.loadList()),
    clonedElement
  );

  return (
    <>
      <EditPage
        i18nNs={endpoint}
        form={form}
        icon={<UserOutlined />}
        clonable
        clonedElement={clonedElement}
        asModal={asModal}
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        onCreate={onCreate}
        deletable
        elementsActions={stewardsActions}
        customButtons={
          <GetPdfPlanningButton
            customFileName={() => getStewardAndLinkedUsersNames(steward, registrations)}
            elementsActions={stewardsActions}
            tooltip="Si l'encadrant⋅e est lié⋅e à un⋅e participant⋅e, les inscriptions de ce⋅tte participant⋅e seront aussi intégrées dans l'export."
          />
        }
        record={steward}
        initialValues={steward}
        groupEditing={groupEditing}>
        <div className={`container-grid ${isValidObjectId(id) ? " two-thirds-one-third" : ""}`}>
          <div className="container-grid">
            <CardElement>
              <div className="container-grid two-per-row">
                <TextInput name="firstName" label="Prénom" placeholder="prénom" required />

                <TextInput name="lastName" label="Nom" placeholder="nom" />

                <Suspense fallback={null}>
                  <PhoneInput name="phoneNumber" label="Téléphone" placeholder="téléphone" />
                </Suspense>
              </div>
            </CardElement>

            <CustomFieldsInputs form={form} endpoint={endpoint} />
          </div>
          {isValidObjectId(id) && (
            <CardElement greyedOut>
              <DisplayInput icon={<TeamOutlined />} label="Participant⋅e associé⋅e">
                <div style={{paddingLeft: 10}}>
                  {registrationsLinkedToSteward.length > 0 ? (
                    <List
                      rowKey="_id"
                      dataSource={registrationsLinkedToSteward}
                      renderItem={(r) => (
                        <div>
                          <Link to={`./../../participants/${r._id}`}>
                            {r.invitationToken && <WaitingInvitationTag />}
                            {personName(r.user).length > 0 ? personName(r.user) : r.user.email}
                          </Link>
                        </div>
                      )}
                    />
                  ) : (
                    <Button icon={<MailOutlined />} onClick={onNewRegistration}>
                      Inviter un⋅e {t("stewards:labelDown")} par email
                    </Button>
                  )}
                </div>
              </DisplayInput>
            </CardElement>
          )}
        </div>
        <CardElement>
          <div className="container-grid">
            <TextAreaInput name="summary" label="Résumé" placeholder="Résumé" />

            <RichTextInput
              name="notes"
              label="Notes privées pour les orgas"
              placeholder="notes privées"
              tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
            />
          </div>
        </CardElement>

        <FormAvailabilitySlots
          name={"availabilitySlots"}
          label={"Disponibilités"}
          disableDatesIfOutOfProject // We grey out all the dates out of the project scope, but we allow to select outside of them
          allowImportProjectDates
        />
      </EditPage>

      <RegistrationEditModal
        persistRegistration={persistLinkedParticipantRegistration}
        addNewRegistrationTitle={`Associer l'${t("stewards:labelDown")}`}
      />
    </>
  );
}

export default withFallBackOnUrlId(StewardEdit);
