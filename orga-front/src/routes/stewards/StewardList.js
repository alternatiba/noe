import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {stewardsActions, stewardsSelectors} from "@features/stewards";
import {ListPage} from "@shared/pages/ListPage";
import {listOfClickableElements} from "@shared/utils/listOfClickableElements";
import {GetPdfPlanningButton} from "@shared/components/buttons/GetPdfPlanningButton";
import {
  getRegistrationsLinkedToSteward,
  getStewardAndLinkedUsersNames,
} from "./atoms/stewardsRegistrationsUtils";
import {Link} from "react-router-dom";
import {personName} from "@shared/utils/utilities";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {UserOutlined} from "@ant-design/icons";
import {useColumnsBlacklistingSelect} from "@shared/hooks/useColumnsBlacklistingSelect";
import {useLoadList} from "@shared/hooks/useLoadList";
import {WaitingInvitationTag} from "@shared/components/WaitingInvitationTag";
import {useParticipantsFormAnswersColumns} from "@utils/columns/useParticipantsFormAnswersColumns";
import {sorter} from "@shared/utils/sorters";
import {editableCellColumn} from "@shared/components/EditableCell";
import {useCustomFieldsColumns} from "@shared/utils/columns/useCustomFieldsColumns";
import {useTranslation} from "react-i18next";

function StewardList() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const stewards = useSelector(stewardsSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "stewards",
  });

  // Not counting sessions that are desynchronized
  const getNumberOfSessionsForSteward = (steward) =>
    sessions.filter((session) => session.stewards.find((s) => s._id === steward._id)).length;

  const participantsFormAnswersColumns = useParticipantsFormAnswersColumns({
    nameSuffix: t("registrations:label").toLowerCase(),
    getAllFormAnswers: (record) =>
      getRegistrationsLinkedToSteward(record, registrations).map((r) => r.formAnswers),
  });

  const columns = [
    {
      width: 48,
      render: (text, record) => (
        <GetPdfPlanningButton
          customFileName={() => getStewardAndLinkedUsersNames(record, registrations)}
          elementsActions={stewardsActions}
          id={record._id}
          noText
          noTooltip
        />
      ),
    },
    {
      ...editableCellColumn({
        title: t("users:schema.firstName.label"),
        dataIndex: "firstName",
        type: "text",
        placeholder: "prénom",
        required: true,
        elementsActions: stewardsActions,
      }),
      sorter: (a, b) => sorter.text(a.firstName, b.firstName),
      searchable: true,
    },
    {
      ...editableCellColumn({
        title: t("users:schema.lastName.label"),
        dataIndex: "lastName",
        type: "text",
        placeholder: "nom",
        elementsActions: stewardsActions,
      }),
      sorter: (a, b) => sorter.text(a.lastName, b.lastName),
      searchable: true,
    },
    {
      ...editableCellColumn({
        title: t("stewards:schema.phoneNumber.labelShort"),
        dataIndex: "phoneNumber",
        type: "phoneNumber",
        elementsActions: stewardsActions,
      }),
      searchable: true,
    },
    {
      title: t("stewards:schema.linkedParticipant.label"),
      dataIndex: "registrationsLinkedToSteward",
      sorter: (a, b) =>
        sorter.text(
          listOfClickableElements(getRegistrationsLinkedToSteward(a, registrations), (r) =>
            personName(r.user)
          ),
          listOfClickableElements(getRegistrationsLinkedToSteward(b, registrations), (r) =>
            personName(r.user)
          )
        ),
      searchable: true,
      searchText: (record) =>
        getRegistrationsLinkedToSteward(record, registrations)
          .map((r) => personName(r.user))
          .join(" "),
      render: (text, record) =>
        listOfClickableElements(
          getRegistrationsLinkedToSteward(record, registrations),
          (el, index) => (
            <Link to={`./../participants/${el._id}`} key={index}>
              {el.invitationToken && <WaitingInvitationTag />}
              {personName(el.user)}
            </Link>
          )
        ),
    },
    {
      title: t("stewards:list.linkedParticipantEmail"),
      dataIndex: "registrationsLinkedToStewardEmail",
      sorter: (a, b) =>
        sorter.text(
          listOfClickableElements(
            getRegistrationsLinkedToSteward(a, registrations),
            (r) => r.user.email
          ),
          listOfClickableElements(
            getRegistrationsLinkedToSteward(b, registrations),
            (r) => r.user.email
          )
        ),
      searchable: true,
      searchText: (record) =>
        getRegistrationsLinkedToSteward(record, registrations)
          .map((r) => r.user.email)
          .join(" "),
      render: (text, record) =>
        listOfClickableElements(
          getRegistrationsLinkedToSteward(record, registrations),
          (el, index) => el.user.email
        ),
    },
    ...participantsFormAnswersColumns,
    ...useCustomFieldsColumns({
      endpoint: "stewards",
      elementsActions: stewardsActions,
    }),
    {
      title: t("sessions:nbSessions"),
      render: (text, record) => getNumberOfSessionsForSteward(record) || "",
      sorter: (a, b) =>
        sorter.number(getNumberOfSessionsForSteward(a), getNumberOfSessionsForSteward(b)),
      width: 125,
    },
  ];

  useLoadList(() => {
    dispatch(stewardsActions.loadList());
    dispatch(registrationsActions.loadList());
    dispatch(sessionsActions.loadList());
  });

  return (
    <ListPage
      i18nNs="stewards"
      icon={<UserOutlined />}
      elementsActions={stewardsActions}
      elementsSelectors={stewardsSelectors}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      searchInFields={["firstName", "lastName"]}
      customButtons={
        <GetPdfPlanningButton
          customFileName={() =>
            t("stewards:list.groupedPdfPlanningExportButton.customFileNameAllStewards")
          }
          elementsActions={stewardsActions}
          id="all"
          tooltip={t("stewards:list.groupedPdfPlanningExportButton.tooltipAllStewards")}
        />
      }
      multipleActionsButtons={({selectedRowKeys, setSelectedRowKeys}) => (
        <GetPdfPlanningButton
          customFileName={() => t("stewards:list.groupedPdfPlanningExportButton.customFileName")}
          elementsActions={stewardsActions}
          id={selectedRowKeys}
          noText
          onClick={() => setSelectedRowKeys([])}
          tooltip={t("stewards:list.groupedPdfPlanningExportButton.tooltip")}
        />
      )}
      columns={filterBlacklistedColumns(columns)}
      dataSource={stewards}
      groupEditable
      groupImportable
    />
  );
}

export default StewardList;
