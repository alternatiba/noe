import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {teamsActions, teamsSelectors} from "@features/teams";
import {Form} from "antd";
import {EditPage, ElementEditProps} from "@shared/pages/EditPage";
import {activitiesActions, activitiesSelectors} from "@features/activities";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {CardElement} from "@shared/components/CardElement";
import {currentProjectSelectors} from "@features/currentProject";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {useColumnsBlacklistingSelect} from "@shared/hooks/useColumnsBlacklistingSelect";
import {BookOutlined, ScheduleOutlined, TeamOutlined} from "@ant-design/icons";
import {TextInput} from "@shared/inputs/TextInput";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {SwitchInputInline} from "@shared/inputs/SwitchInput";
import {useLoadEditing} from "@shared/hooks/useLoadEditing";
import {RichTextInput} from "@shared/inputs/RichTextInput";
import {TextAreaInput} from "@shared/inputs/TextAreaInput";
import {WithEntityLinks} from "@shared/components/WithEntityLinks";
import {generateRegistrationDispoColumn} from "@utils/columns/generateRegistrationDispoColumn";
import {useRegistrationsColumns} from "@utils/columns/useRegistrationsColumns";
import {useSessionsColumns} from "@utils/columns/useSessionsColumns";
import {generateSubscriptionInfo} from "@utils/columns/generateSubscriptionInfo";
import {searchInRegistrationFields} from "@shared/utils/searchInFields/searchInRegistrationFields";
import {SelectInput} from "@shared/inputs/SelectInput";
import {useLocation} from "react-router-dom";
import {withFallBackOnUrlId} from "@shared/utils/withFallbackOnUrlId";
import {ElementsTableWithModal} from "@shared/components/ElementsTableWithModal";
import {CustomFieldsInputs} from "@shared/inputs/CustomFieldsInputs";

const SessionEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../sessions/SessionEdit")
);
const ActivityEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../activities/ActivityEdit")
);

function TeamEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const location = useLocation();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const team = useSelector(teamsSelectors.selectEditing);
  const teamActivityVal = Form.useWatch("activity", form);
  const teamSessionsVal = Form.useWatch("sessions", form);
  const activities = useSelector(activitiesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const [filterBlacklistedSessionsColumns] = useColumnsBlacklistingSelect({endpoint: "sessions"});
  const [filterBlacklistedParticipantsColumns] = useColumnsBlacklistingSelect({
    endpoint: "participants",
  });

  const endpoint = "teams";
  const setIsModified = EditPage.useSetIsModified({resetOnUnmount: true, endpoint, id});

  const [filterSessionsWithLinkedActivity, setFilterSessionsWithLinkedActivity] = useState(true);
  const [filterRegistrationsWithAlreadyATeam, setFilterRegistrationsWithAlreadyATeam] =
    useState(true);

  const sessionsColumnsBase = filterBlacklistedSessionsColumns(
    useSessionsColumns("./../..", currentProject.usePlaces, false, true)
  );
  const sessionsColumnsSimple = filterBlacklistedParticipantsColumns(
    useSessionsColumns("./../..", currentProject.usePlaces, false, true, true)
  );

  const groupEditing = location?.state?.groupEditing;

  useLoadEditing(teamsActions, id, () => {
    dispatch(activitiesActions.loadList());
    dispatch(sessionsActions.loadList());
    dispatch(registrationsActions.loadList());
  });

  const teamSessions = sessions.filter((s) => s.team?._id === team._id);
  useEffect(() => {
    if (team._id) form.setFieldValue("sessions", teamSessions);
  }, [JSON.stringify(teamSessions), team._id]);

  const teamRegistrations = registrations.filter((r) =>
    r.teamsSubscriptions?.find((ts) => ts.team._id === team._id)
  );
  useEffect(() => {
    if (team._id) form.setFieldValue("registrations", teamRegistrations);
  }, [JSON.stringify(teamRegistrations), team._id]);

  const teamPostTransform = (values) => {
    if (typeof values.activity === "string") {
      values.activity = activities.find((activity) => activity._id === values.activity);
    }
    return values;
  };

  const teamRegistrationsColumns = [
    ...filterBlacklistedParticipantsColumns(useRegistrationsColumns("./../..", currentProject)),
    generateRegistrationDispoColumn("./../.."),
  ];
  const teamSubscriptionInfoColumn = {
    title: "Inscription",
    render: (text, record) => {
      const teamSubscription = record.teamsSubscriptions.find((ss) => ss.team._id === team._id);
      return generateSubscriptionInfo(record, teamSubscription, registrations);
    },
  };

  return (
    <EditPage
      icon={<TeamOutlined />}
      i18nNs={endpoint}
      form={form}
      deletable
      asModal={asModal}
      modalOpen={modalOpen}
      setModalOpen={setModalOpen}
      onCreate={onCreate}
      elementsActions={teamsActions}
      record={team}
      initialValues={{...team, activity: team.activity?._id}}
      postTransform={teamPostTransform}
      groupEditing={groupEditing}>
      <CardElement>
        <div className="container-grid two-per-row">
          <TextInput label="Nom de l'équipe" name="name" placeholder="nom" required />

          <WithEntityLinks
            endpoint="activities"
            name={"activity"}
            createButtonText="Créer une nouvelle activité"
            ElementEdit={ActivityEdit}>
            <SelectInput
              placeholder="activité"
              label="Activité liée"
              icon={<BookOutlined />}
              options={[
                {value: null, label: "- Pas d'activité liée -"},
                ...activities.map((d) => ({value: d._id, label: d.name})),
              ]}
              showSearch
            />
          </WithEntityLinks>
        </div>
      </CardElement>

      <CardElement>
        <div className="container-grid two-thirds-one-third">
          <RichTextInput
            label="Description détaillée"
            tooltip={
              'Elle sera affichée seulement dans la vue "pleine page" de l\'activité. ' +
              "C'est ici que vous pouvez rédiger une description plus longue de ce qui attend les participant⋅es"
            }
            name="description"
            placeholder="description de l'activité"
          />

          <TextAreaInput
            label="Résumé"
            name="summary"
            tooltip="Il sera affiché dans la liste des sessions et devra être très court."
            placeholder="résumé court (idéalement une quinzaine de mots)"
            rules={[
              {
                max: 250,
                message: (
                  <>
                    Votre résumé est trop long. ne sera pas affiché de manière optimale dans
                    l'interface utilisateur⋅ice. Considérez le résumé comme un sous-titre court qui
                    décrit en quelques mots ce qu'est l'activité. Si vous voulez donner des détails
                    sur l'activité, utilisez la <strong>Description détaillée</strong>.
                  </>
                ),
              },
            ]}
          />

          <RichTextInput
            label="Notes privées pour les orgas"
            name="notes"
            placeholder="notes privées"
            tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
          />
        </div>
      </CardElement>

      <CustomFieldsInputs form={form} endpoint={endpoint} />

      <ElementsTableWithModal
        name={"sessions"}
        onChange={() => setIsModified(true)}
        allElements={sessions}
        filteredAddableElements={
          teamActivityVal && filterSessionsWithLinkedActivity
            ? sessions.filter((s) => s.activity._id === teamActivityVal)
            : sessions
        }
        title="Sessions de l'équipe"
        icon={<ScheduleOutlined />}
        subtitle="En rouge clair figurent les sessions qui n'appartiennent pas à l'activité liée choisie."
        showHeader
        buttonTitle="Ajouter une session"
        rowClassName={(record) =>
          record.activity._id === teamActivityVal ? "" : "ant-table-row-danger"
        }
        navigableRootPath="./../../sessions"
        columns={sessionsColumnsBase}
        ElementEdit={SessionEdit}
        elementSelectionModalProps={{
          large: true,
          subtitle: teamActivityVal && (
            <SwitchInputInline
              style={{marginBottom: 12}}
              label={"Sessions de l'activité liée seulement"}
              checked={filterSessionsWithLinkedActivity}
              onChange={(value) => setFilterSessionsWithLinkedActivity(value)}
            />
          ),
          title: "Ajouter des sessions à l'équipe",
          createNewElementButtonTitle: "Créer une session",
          searchInFields: searchInRegistrationFields,
          columns: sessionsColumnsSimple,
        }}
      />

      <ElementsTableWithModal
        name={"registrations"}
        onChange={() => setIsModified(true)}
        allElements={registrations}
        filteredAddableElements={
          filterRegistrationsWithAlreadyATeam
            ? registrations.filter(
                (r) => !r.teamsSubscriptions || r.teamsSubscriptions?.length === 0
              )
            : registrations
        }
        title="Membres de l'équipe"
        icon={<TeamOutlined />}
        subtitle="En rouge clair figurent les participant⋅es qui ne sont pas inscrit⋅es à toutes les sessions prévues pour l'équipe."
        showHeader
        buttonTitle="Ajouter un⋅e membre"
        rowClassName={(record) =>
          record.sessionsSubscriptions.filter((ss) => ss.team === team._id).length ===
          teamSessionsVal?.length
            ? ""
            : "ant-table-row-danger"
        }
        navigableRootPath="./../../participants"
        columns={[...teamRegistrationsColumns, teamSubscriptionInfoColumn]}
        elementSelectionModalProps={{
          large: true,
          subtitle: (
            <SwitchInputInline
              style={{marginBottom: 12}}
              label={"Participant⋅es sans équipe seulement"}
              checked={filterRegistrationsWithAlreadyATeam}
              onChange={(value) => setFilterRegistrationsWithAlreadyATeam(value)}
            />
          ),
          title: "Ajouter des membres à l'équipe",
          searchInFields: searchInRegistrationFields,
          columns: teamRegistrationsColumns,
        }}
      />
    </EditPage>
  );
}

export default withFallBackOnUrlId(TeamEdit);
