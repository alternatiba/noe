import React, {ReactNode} from "react";
import {NamePath} from "rc-field-form/es/interface";
import {useFieldCompsFormInstance} from "@shared/components/FieldsBuilder/atoms/useFieldCompsFormInstance";
import {Form} from "antd";

export type DisplayOption = {icon: ReactNode, value: string, label: string};

type DisplayOptionsIconListProps = {
  componentRootName: NamePath,
  displayOptions: Array<DisplayOption>,
};

export const DisplayOptionsIconsList = ({
  componentRootName,
  displayOptions,
}: DisplayOptionsIconListProps) => {
  const form = useFieldCompsFormInstance();
  const displayNameVal = Form.useWatch([...componentRootName, "displayName"], form);
  const displayOptionsVal = Form.useWatch([...componentRootName, "displayOptions"], form);

  return (
    <>
      {displayNameVal &&
        displayOptions
          .filter(({value}) => displayOptionsVal?.includes(value))
          .map(({label, icon}) => {
            return <div title={label}>{icon}</div>;
          })}
    </>
  );
};
