import {PendingSuspense} from "@shared/components/Pending";
import React, {useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "@features/projects";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {EditPage} from "@shared/pages/EditPage";

const WelcomePageEditor = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/WelcomePageEditor/WelcomePageEditor")
);

export const WelcomePageConfigTab = () => {
  const dispatch = useDispatch();
  const onChangeTriggersCount = useRef(0);
  const setIsModified = EditPage.useSetIsModified();

  const projectWelcomePageContent = useSelector((s) => projectsSelectors.selectEditing(s).content);

  const onChangeWelcomePage = (value) => {
    // The first call to the onChange function on initialization is false positive.... Yeah that sucks. So we just discard it.
    onChangeTriggersCount.current++;
    if (onChangeTriggersCount.current <= 1) return;

    setIsModified(true);
    dispatch(projectsActions.changeEditing({content: value}));
  };

  return (
    <PendingSuspense>
      <WelcomePageEditor
        value={projectWelcomePageContent}
        onChange={onChangeWelcomePage}
        roundedBorders
      />
      <div style={{minHeight: 400}}></div>
    </PendingSuspense>
  );
};
