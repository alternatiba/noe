import {useSelector} from "react-redux";
import {FieldsBuilder} from "@shared/components/FieldsBuilder/FieldsBuilder";
import {FieldsBuilderLivePreview} from "@shared/components/FieldsBuilder/FieldsBuilderLivePreview";
import {SwitchInputInline} from "@shared/inputs/SwitchInput";
import {projectsSelectors} from "@features/projects";
import {EditPage} from "@shared/pages/EditPage";
import {FormElement} from "@shared/inputs/FormElement";
import {formCompCustomPropsConfig} from "@shared/components/FieldsBuilder/fieldCompCustomProps/fieldCompCustomPropsConfig";
import {ClosableAlert} from "@shared/components/ClosableAlert";
import {useTranslation} from "react-i18next";
import {Stack} from "@shared/layout/Stack";
import {EyeOutlined, FilePdfOutlined} from "@ant-design/icons";
import {CheckboxGroupInput} from "@shared/inputs/CheckboxGroupInput";
import {DisplayOption, DisplayOptionsIconsList} from "./DisplayOptionsIconsList";

export const RegistrationFormBuilderTab = ({formComponentsForm}) => {
  const {t} = useTranslation();
  const formComponents = useSelector(
    (state) => projectsSelectors.selectEditing(state)?.formComponents
  );
  const setIsModified = EditPage.useSetIsModified();

  const displayOptions: Array<DisplayOption & {onlyFilterableFields?: boolean}> = [
    {
      icon: <EyeOutlined />,
      value: "inListView",
      label: t("fieldsBuilder:schema.displayOptions.options.inListView"),
    },
    {
      icon: <FilePdfOutlined />,
      value: "inPdfExport",
      label: t("fieldsBuilder:schema.displayOptions.options.inPdfExport"),
    },
  ];

  const FormCompTitleExtras = ({componentRootName}) => (
    <Stack row gap={2} mx={2} alignStart>
      <DisplayOptionsIconsList
        componentRootName={componentRootName}
        displayOptions={displayOptions}
      />
    </Stack>
  );

  return (
    <div className="container-grid two-per-row" style={{rowGap: 40, marginBottom: 40}}>
      <FormElement
        requiredMark
        form={formComponentsForm}
        initialValues={{formComponents}}
        onValuesChange={() => setIsModified(true)}>
        <FieldsBuilder
          name={"formComponents"}
          config={{
            customPropsConfig: formCompCustomPropsConfig,
            customDisplayConfig: ({name, rootName}) => {
              const displayOptionsForCheckboxGroup = displayOptions.map((o) => ({
                ...o,
                // Add the icon at the beginning of the option
                label: (
                  <>
                    {o.icon} {o.label}
                  </>
                ),
              }));

              return (
                <CheckboxGroupInput
                  label={t("fieldsBuilder:schema.displayOptions.label")}
                  options={displayOptionsForCheckboxGroup}
                  name={[name, "displayOptions"]}
                />
              );
            },
            customGeneralConfig: ({name, rootName}) => (
              <SwitchInputInline
                label={t("fieldsBuilder:schema.disabled.label")}
                name={[name, "disabled"]}
              />
            ),
            FieldCompTitleExtras: FormCompTitleExtras,
          }}
        />
      </FormElement>
      <div>
        <ClosableAlert
          localStorageKey={"fieldsBuilderPreview"}
          message={t("projects:schema.formComponents.livePreviewNotice")}
          style={{marginBottom: 26}}
        />

        <FieldsBuilderLivePreview
          name={"formComponents"}
          fieldsComponentsForm={formComponentsForm}
        />
      </div>
    </div>
  );
};
