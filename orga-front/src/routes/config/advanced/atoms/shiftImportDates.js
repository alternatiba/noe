import dayjs from "@shared/services/dayjs";

export const shiftImportDates = (dataToImport, dateShift) => {
  const shiftedData = {...dataToImport};
  const shiftDate = (dateString) =>
    dateString
      ? dayjs(dateString).add(dateShift, "day").second(0).millisecond(0).toISOString()
      : dateString;

  const shiftAvailabilities = (availabilitySlots) =>
    availabilitySlots
      ? availabilitySlots?.map((slot) => ({
          ...slot,
          start: shiftDate(slot.start),
          end: shiftDate(slot.end),
        }))
      : availabilitySlots;

  const applyArrayModification = (obj, key, transformFn) => {
    if (obj[key]?.length > 0) obj[key] = obj[key].map((el) => ({...el, ...transformFn(el)}));
  };

  // Shift project dates
  if (shiftedData.project) {
    shiftedData.project = {
      ...shiftedData.project,
      availabilitySlots: shiftAvailabilities(shiftedData.project.availabilitySlots),
    };
  }

  // Shift sessions dates
  applyArrayModification(shiftedData, "sessions", (session) => ({
    start: shiftDate(session.start),
    end: shiftDate(session.end),
    slots: shiftAvailabilities(session.slots),
  }));

  // Shift stewards dates
  applyArrayModification(shiftedData, "stewards", (steward) => ({
    availabilitySlots: shiftAvailabilities(steward.availabilitySlots),
  }));

  // Shift places dates
  applyArrayModification(shiftedData, "places", (place) => ({
    availabilitySlots: shiftAvailabilities(place.availabilitySlots),
  }));

  // Shift registrations dates
  applyArrayModification(shiftedData, "registrations", (registration) => ({
    availabilitySlots: shiftAvailabilities(registration.availabilitySlots),
    daysOfPresence: shiftAvailabilities(registration.daysOfPresence),
  }));

  return shiftedData;
};
