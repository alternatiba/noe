import {t} from "i18next";
import React from "react";

const getFieldsOptions = (obj, getLabel) =>
  Object.keys(obj || {}).map((key) => ({
    key,
    label: getLabel(key),
  }));
export const extractImportExportFields = (dataToImportExport, operationType) => {
  // Extract and format fields from the data at global and project level
  const projectFields = getFieldsOptions(dataToImportExport?.project, (key) =>
    t(`projects:schema.${key}.label`, key)
  ).filter((field) => !["start", "end"].includes(field.key)); // Don't include start and end in th choice selection, as they are calculated automatically by the backend

  const globalFields = getFieldsOptions(dataToImportExport, (key) =>
    key === "project" ? (
      <>
        {t("projects:schema.projectConfiguration.label")}
        <span style={{color: "gray"}}>
          {" - "}
          {operationType === "import"
            ? t("projects:advanced.importExport.youCanSelectLaterWhatYouWantToImport")
            : t("projects:advanced.importExport.youCanSelectLaterWhatYouWantToExport")}
        </span>
      </>
    ) : (
      <>
        {t(`${key}:label_other`, key)}{" "}
        <span style={{color: "gray"}}>
          {" - "}
          {t("projects:advanced.importExport.xElements", {count: dataToImportExport[key]?.length})}
        </span>
      </>
    )
  );

  return {projectFields, globalFields};
};
