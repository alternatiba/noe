import {useRef, useState} from "react";
import {Alert, App, Button, InputNumber, Modal, Space, Upload} from "antd";
import {CalendarOutlined, ExportOutlined, ImportOutlined} from "@ant-design/icons";
import {projectsActions, projectsSelectors} from "@features/projects";
import {useDispatch, useSelector} from "react-redux";
import {FormElement} from "@shared/inputs/FormElement";
import {BetaTag} from "@shared/components/BetaTag";
import {CardElement} from "@shared/components/CardElement";
import {Trans, useTranslation} from "react-i18next";
import {SwitchInput} from "@shared/inputs/SwitchInput";
import {ElementSelectionModal} from "@shared/components/ElementSelectionModal";
import {DisplayInput} from "@shared/inputs/DisplayInput";
import {normalize} from "@shared/utils/stringUtilities";
import {extractImportExportFields} from "./atoms/extractImportExportFields";
import {getSelectionWithDependencies} from "./atoms/getSelectionWithDependencies";
import {pickSelectedImportExportData} from "./atoms/pickSelectedImportExportData";
import {shiftImportDates} from "./atoms/shiftImportDates";
import {useNavigate} from "react-router-dom";
import {sorter} from "@shared/utils/sorters";
import {getSlotsStart} from "@utils/slotsUtilities";
import {dateFormatter} from "@shared/utils/formatters";
import {dayjsBase} from "@shared/services/dayjs";

export function ImportExportTab() {
  const {message} = App.useApp();
  const navigate = useNavigate();
  const {t} = useTranslation();
  const project = useSelector(projectsSelectors.selectEditing);
  const dispatch = useDispatch();
  const downloadButton = useRef();

  // Can ben either "import" or "export"
  const [operationType, setOperationType] = useState();

  // Import data & options
  const [dataToImportExport, setDataToImportExport] = useState();
  const [isAdditiveImport, setIsAdditiveImport] = useState(true);

  // Date shifting
  const [dateShift, setDateShift] = useState(0);

  // Export options
  const [exportWithRegistrations, setExportWithRegistrations] = useState(false);
  const [anonymizedExport, setAnonymizedExport] = useState(false);
  const [keepStewardNames, setKeepStewardNames] = useState(false);

  // Open modals: can be "globalConfig", "projectConfig" or "dateShifting", or undefined if none are open
  const [modalOpen, setModalOpen] = useState(undefined);

  // Fields to import
  const {globalFields, projectFields} = extractImportExportFields(
    dataToImportExport,
    operationType
  );
  const [globalFieldsSelection, setGlobalFieldsSelection] = useState([]);
  const [projectFieldsSelection, setProjectFieldsSelection] = useState([]);

  const safelySetGlobalFieldsSelection = (fields) => {
    const valuesKeys = fields.map((v) => v.key);
    const valueKeysWithDependencies = getSelectionWithDependencies(valuesKeys, dataToImportExport);
    const newFields = globalFields.filter((f) => valueKeysWithDependencies.includes(f.key));
    setGlobalFieldsSelection(newFields);
  };

  const importModalColumns = [
    {
      title:
        operationType === "import"
          ? t("projects:advanced.importExport.dataToImport")
          : t("projects:advanced.importExport.dataToExport"),
      dataIndex: "label",
      defaultSortOrder: "ascend",
      sorter: (a, b) => sorter.text(a.label, b.label),
    },
  ];

  /**
   * UTILS
   */

  const initializeImportExport = (opType, dataToImportExport) => {
    // Initialize the type of operation
    setOperationType(opType);

    // Store the import/export data
    setDataToImportExport(dataToImportExport);

    // Open the first modal
    setModalOpen("globalConfig");
  };

  const closeModals = () => setModalOpen(undefined);

  /**
   * IMPORT / EXPORT PREPARATION
   */

  const prepareExport = async () => {
    try {
      const data = await dispatch(
        projectsActions.export(exportWithRegistrations, anonymizedExport, keepStewardNames)
      );
      initializeImportExport("export", data);
    } catch (e) {
      message.error(t("projects:advanced.importExport.errors.cannotExportProject"));
    }
  };

  // Parse the imported file, and set the data to import in the state
  const prepareImport = (file) => {
    let reader = new FileReader();
    reader.onload = function (e) {
      try {
        let data = JSON.parse(e.target.result);
        initializeImportExport("import", data);
      } catch (e) {
        message.error(t("projects:advanced.importExport.errors.incorrectImportFile"));
      }
    };
    reader.readAsText(file);
  };

  /**
   * IMPORT / EXPORT VALIDATION
   */

  const validateExport = () => {
    // Build the export file name

    const exportName = normalize(
      t("projects:advanced.importExport.exportFileName", {
        projectName: project.name,
        date: dayjsBase().format("L LT"),
      }),
      true
    ).replace(/[^\w ]/g, "-");

    // Pick the data to export
    const pickedExportData = pickSelectedImportExportData(
      dataToImportExport,
      globalFieldsSelection,
      projectFieldsSelection
    );

    // Create the export file
    const file = new File([JSON.stringify(pickedExportData, null, 2)], exportName, {
      type: "application/json",
    });

    // Download the file
    let exportUrl = URL.createObjectURL(file);
    downloadButton.current.setAttribute("href", exportUrl);
    downloadButton.current.setAttribute("download", exportName);
    downloadButton.current.click();

    // Close modals
    closeModals();
  };

  // Finally, process and send the import data to backend
  const validateImport = async () => {
    // Pick the data to import
    const pickedImportData = pickSelectedImportExportData(
      dataToImportExport,
      globalFieldsSelection,
      projectFieldsSelection
    );

    // Shift the data's dates according to the date shift
    const shiftedImportData = shiftImportDates(pickedImportData, dateShift);

    // Import the data into database
    dispatch(
      projectsActions.import(shiftedImportData, isAdditiveImport, !!shiftedImportData.registrations)
    ).then(() =>
      message
        .loading(t("projects:advanced.importExport.importSuccessful"))
        .then(() => window.location.reload())
    );

    // Close modals
    closeModals();
  };

  const validateOperation = operationType === "import" ? validateImport : validateExport;

  /**
   * ELEMENTS SELECTION MODAL CONFIGURATION
   */

  const globalConfigRowSelection = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      safelySetGlobalFieldsSelection(selectedRowObject);
    },
  };
  const projectConfigRowSelection = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setProjectFieldsSelection(selectedRowObject);
    },
  };

  const globalConfigModalParams = {
    title: t(`projects:advanced.importExport.globalModal.${operationType}.title`),
    subtitle: (
      <>
        {t(`projects:advanced.importExport.globalModal.${operationType}.subtitle`)}{" "}
        <Alert
          style={{marginTop: 10}}
          showIcon
          message={
            <Trans
              i18nKey={"advanced.importExport.globalModal.someElementsAreInterdependent"}
              ns={"projects"}
            />
          }
        />
        {globalFieldsSelection.find((field) => field.key === "registrations") && (
          <Alert
            style={{marginTop: 10}}
            type={"warning"}
            showIcon
            message={
              (operationType === "import"
                ? t("projects:advanced.importExport.globalModal.import.warning")
                : t("projects:advanced.importExport.globalModal.export.warning")) +
              " " +
              t("common:areYouSure")
            }
          />
        )}
      </>
    ),
    okText:
      !globalFieldsSelection.find((field) => field.key === "project") &&
      operationType === "export" &&
      t("projects:advanced.importExport.launchExport"),
    onOk: () => {
      if (globalFieldsSelection.find((field) => field.key === "project")) {
        // If the project config is selected, then go to the project config
        setModalOpen("projectConfig");
      } else if (operationType === "import") {
        // If the project config is NOT selected, and we are importing stuff, then go to the date shifting modal
        setModalOpen("dateShifting");
      } else {
        // Else, validate
        validateOperation();
      }
    },
    rowSelection: globalConfigRowSelection,
    selectedRowKeys: globalFieldsSelection,
    setSelectedRowKeys: safelySetGlobalFieldsSelection,
    columns: importModalColumns,
    dataSource: globalFields,
  };

  const projectConfigModalParams = {
    title: t(`projects:advanced.importExport.projectModal.${operationType}.title`),
    subtitle: t(`projects:advanced.importExport.projectModal.${operationType}.subtitle`),
    onOk: () => {
      if (operationType === "import") {
        setModalOpen("dateShifting");
      } else {
        validateOperation();
      }
    },
    okText: operationType === "export" && t("projects:advanced.importExport.launchExport"),
    rowSelection: projectConfigRowSelection,
    selectedRowKeys: projectFieldsSelection,
    setSelectedRowKeys: setProjectFieldsSelection,
    columns: importModalColumns,
    dataSource: projectFields,
  };

  const importedProjectStart =
    getSlotsStart(dataToImportExport?.project?.availabilitySlots) ||
    dataToImportExport?.project?.start;

  return (
    <>
      <a ref={downloadButton} style={{display: "none"}} />

      <div className="container-grid two-per-row">
        <div className="container-grid">
          <CardElement
            icon={<ExportOutlined />}
            title={t("projects:advanced.importExport.exportSection.title")}>
            <Trans ns="projects" i18nKey="advanced.importExport.exportSection.body" />
            <CardElement
              size={"small"}
              style={{marginBottom: 26}}
              title={t("projects:advanced.importExport.exportWithRegistrationsSection.title")}>
              <Trans
                ns="projects"
                i18nKey="advanced.importExport.exportWithRegistrationsSection.body"
              />
              <FormElement>
                <SwitchInput
                  formItemProps={{style: {marginBottom: 0}}}
                  onChange={() => setExportWithRegistrations(!exportWithRegistrations)}
                  checked={exportWithRegistrations}
                  label={t(
                    "projects:advanced.importExport.exportWithRegistrationsSection.switchLabel"
                  )}
                />
              </FormElement>
            </CardElement>

            <CardElement
              size={"small"}
              style={{marginBottom: 26}}
              title={t("projects:advanced.importExport.anonymizeSection.title")}>
              <Trans ns="projects" i18nKey="advanced.importExport.anonymizeSection.body" />
              <FormElement>
                <SwitchInput
                  onChange={() => setAnonymizedExport(!anonymizedExport)}
                  checked={anonymizedExport}
                  label={t("projects:advanced.importExport.anonymizeSection.title")}
                />
                {anonymizedExport && (
                  <SwitchInput
                    onChange={() => setKeepStewardNames(!keepStewardNames)}
                    checked={keepStewardNames}
                    tooltip={t(
                      "projects:advanced.importExport.anonymizeSection.keepStewardNames.tooltipLabel"
                    )}
                    label={t(
                      "projects:advanced.importExport.anonymizeSection.keepStewardNames.switchLabel"
                    )}
                  />
                )}
              </FormElement>
              {anonymizedExport && (
                <Alert
                  type={"warning"}
                  showIcon
                  message={t(
                    "projects:advanced.importExport.anonymizeSection.warningRegistrationsAndPaymentsAreNotAnonymized"
                  )}
                />
              )}
            </CardElement>

            <Button onClick={prepareExport} icon={<ExportOutlined />} type={"primary"}>
              {exportWithRegistrations
                ? t("projects:advanced.importExport.launchExportWithRegistrations")
                : t("projects:advanced.importExport.launchExport")}
            </Button>
          </CardElement>

          <CardElement
            icon={<ExportOutlined />}
            title={t("projects:advanced.importExport.otherExportsSection.title")}>
            <Trans
              ns="projects"
              i18nKey="advanced.importExport.otherExportsSection.body"
              components={{exportIcon: <ExportOutlined />}}
            />
            <div className={"containerH"} style={{gap: "4pt", flexWrap: "wrap"}}>
              <Button
                type="link"
                onClick={() => navigate("./../participants")}
                icon={<ExportOutlined />}>
                {t("registrations:participantsAndStats")}
              </Button>
              <Button
                type="link"
                onClick={() => navigate("./../sessions")}
                icon={<ExportOutlined />}>
                {t("sessions:label_other")}
              </Button>
            </div>
          </CardElement>
        </div>

        <div className="container-grid">
          <div>
            <CardElement
              icon={<ImportOutlined style={{color: !isAdditiveImport && "red"}} />}
              title={
                <span style={{color: !isAdditiveImport && "red"}}>
                  {t("projects:advanced.importExport.importFromFileSection.title")}
                </span>
              }
              style={{borderColor: !isAdditiveImport && "red"}}
              headStyle={{borderColor: !isAdditiveImport && "red"}}>
              <Trans ns="projects" i18nKey="advanced.importExport.importFromFileSection.body" />
              <CardElement
                size={"small"}
                style={{marginBottom: 26}}
                title={t("projects:advanced.importExport.importAndReplaceAllSection.title")}>
                <Trans
                  ns="projects"
                  i18nKey="advanced.importExport.importAndReplaceAllSection.body"
                />
                <FormElement>
                  <SwitchInput
                    style={{background: !isAdditiveImport && "red"}}
                    formItemProps={{style: {marginBottom: 0}}}
                    onChange={(value) => setIsAdditiveImport(!value)}
                    checked={!isAdditiveImport}
                    label={t(
                      "projects:advanced.importExport.importAndReplaceAllSection.switchLabel"
                    )}
                  />
                </FormElement>
              </CardElement>

              <p></p>
              <Upload
                beforeUpload={(file) => {
                  prepareImport(file);
                  return false;
                }}
                accept="application/json"
                showUploadList={false}>
                <Button
                  icon={<ImportOutlined />}
                  danger={!isAdditiveImport}
                  type={!isAdditiveImport ? "primary" : "default"}>
                  {isAdditiveImport
                    ? t("projects:advanced.importExport.launchAdditiveImport")
                    : t("projects:advanced.importExport.launchImportAndReplaceAll")}
                </Button>
              </Upload>
            </CardElement>

            <CardElement
              icon={<ImportOutlined />}
              title={t("projects:advanced.importExport.otherImportsSection.title")}>
              <Trans
                ns="projects"
                i18nKey="advanced.importExport.otherImportsSection.body"
                components={{importIcon: <ImportOutlined />}}
              />
              <div className={"containerH"} style={{gap: "4pt", flexWrap: "wrap"}}>
                <Button
                  type="link"
                  onClick={() => navigate("./../participants")}
                  icon={<ImportOutlined />}>
                  {t("registrations:label_other")}
                </Button>
                <Button
                  type="link"
                  onClick={() => navigate("./../categories")}
                  icon={<ImportOutlined />}>
                  {t("categories:label_other")}
                </Button>
                {project.usePlaces && (
                  <Button
                    type="link"
                    onClick={() => navigate("./../places")}
                    icon={<ImportOutlined />}>
                    {t("places:label_other")}
                  </Button>
                )}
                <Button
                  type="link"
                  onClick={() => navigate("./../stewards")}
                  icon={<ImportOutlined />}>
                  {t("stewards:label_other")}
                </Button>
                {project.useTeams && (
                  <Button
                    type="link"
                    onClick={() => navigate("./../teams")}
                    icon={<ImportOutlined />}>
                    {t("teams:label_other")}
                  </Button>
                )}
                <Button
                  type="link"
                  onClick={() => navigate("./../activities")}
                  icon={<ImportOutlined />}>
                  {t("activities:label_other")}
                </Button>
                <Button
                  type="link"
                  onClick={() => navigate("./../sessions")}
                  icon={<ImportOutlined />}>
                  {t("sessions:label_other")}
                </Button>
              </div>
            </CardElement>
          </div>
        </div>

        <ElementSelectionModal
          open={modalOpen === "globalConfig"}
          {...globalConfigModalParams}
          rowKey="key"
          size="small"
          onCancel={closeModals}
        />
        <ElementSelectionModal
          open={modalOpen === "projectConfig"}
          {...projectConfigModalParams}
          rowKey="key"
          size="small"
          onCancel={closeModals}
        />
        <Modal
          open={modalOpen === "dateShifting"}
          title={
            <>
              {t(`projects:advanced.importExport.dateShiftingModal.title`)}{" "}
              <BetaTag title={"new"} />
            </>
          }
          centered
          onOk={validateOperation}
          okText={
            operationType === "import"
              ? t("projects:advanced.importExport.launchImport")
              : t("projects:advanced.importExport.launchExport")
          }
          okButtonProps={{danger: true, type: "default"}}
          onCancel={closeModals}>
          <FormElement onValidate={validateOperation}>
            <div className="container-grid">
              <div>{t(`projects:advanced.importExport.dateShiftingModal.subtitle`)}</div>

              <DisplayInput
                label={t("projects:advanced.importExport.dateShiftingModal.dateShift.label")}
                name="dateShift"
                formItemProps={{
                  help:
                    !dateShift &&
                    t("projects:advanced.importExport.dateShiftingModal.leaveEmptyToDoNothing"),
                }}>
                <Space.Compact>
                  <InputNumber
                    keyboard
                    value={dateShift === 0 ? undefined : dateShift}
                    onChange={setDateShift}
                    placeholder={0}
                    min={0}
                    rules={[{type: "number"}]}
                  />
                  <Button icon={<CalendarOutlined />} onClick={() => setDateShift(dateShift + 365)}>
                    + 1 {t("common:time.year")}
                  </Button>
                </Space.Compact>
              </DisplayInput>

              {importedProjectStart && (
                <div>
                  <p>
                    <Trans
                      ns={"projects"}
                      i18nKey={
                        "advanced.importExport.dateShiftingModal.importedProjectInitiallyStartedAt"
                      }
                      values={{
                        date: dateFormatter.longDate(importedProjectStart, {
                          short: false,
                          withYear: true,
                        }),
                      }}
                    />
                  </p>
                  {!dateShift ? (
                    t(
                      "projects:advanced.importExport.dateShiftingModal.withoutDateShiftProjectWillBeImportedAsIs"
                    )
                  ) : (
                    <Trans
                      ns={"projects"}
                      i18nKey={
                        "advanced.importExport.dateShiftingModal.withDateShiftProjectwillStartAt"
                      }
                      values={{
                        dateShift,
                        date: dateFormatter.longDate(importedProjectStart, {
                          short: false,
                          withYear: true,
                        }),
                      }}
                    />
                  )}
                </div>
              )}
            </div>
          </FormElement>
        </Modal>
      </div>
    </>
  );
}
