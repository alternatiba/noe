import React from "react";
import {Alert} from "antd";
import {projectsActions, projectsSelectors} from "@features/projects";
import {useDispatch, useSelector} from "react-redux";
import {CardElement} from "@shared/components/CardElement";
import {SwitchInput} from "@shared/inputs/SwitchInput";

export const AdditionalFeatures = ({project, toggle, subtitle, noTeams}) => (
  <CardElement subtitle={subtitle} title="Fonctionnalités supplémentaires">
    <div className="container-grid two-per-row">
      <SwitchInput
        tooltip={
          <>
            Quand vous créez des activités et des sessions, dites où elles ont lieu, et profitez de
            l'intelligence de NOÉ et son système de jauges de personnes.
          </>
        }
        onChange={() => toggle("usePlaces")}
        checked={project.usePlaces}
        label="Utiliser les espaces"
      />
      {!noTeams && (
        <SwitchInput
          tooltip="Besoin de ranger vos participant⋅es par groupes de personnes, ou de gérer leurs inscriptions aux différentes sessions par lots ? Les équipes semblent faites pour vous."
          onChange={() => toggle("useTeams")}
          checked={project.useTeams}
          label="Utiliser les équipes"
        />
      )}
    </div>
  </CardElement>
);

export function FeaturesFormContent({setIsModified}) {
  const project = useSelector(projectsSelectors.selectEditing);
  const dispatch = useDispatch();

  const toggle = (key) => {
    setIsModified(true);
    dispatch(projectsActions.changeEditing({[key]: !project[key]}));
  };

  return (
    <AdditionalFeatures
      subtitle={
        <Alert
          style={{marginBottom: 26}}
          message="Vous devez recharger la page lors de la modification de ces fonctionnalités."
          description="L'application a besoin de charger de nouvelles données une fois ces changements effectuées. Rechargez la page pour éviter les bugs."
          showIcon
          type="warning"
        />
      }
      project={project}
      toggle={toggle}
    />
  );
}
