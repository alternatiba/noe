import React, {useMemo} from "react";
import {useDispatch, useSelector} from "react-redux";
import {App, Button, Space, Steps, Form} from "antd";
import {FormElement} from "@shared/inputs/FormElement";
import dayjs from "@shared/services/dayjs";
import {OpeningState, projectsActions, projectsSelectors} from "@features/projects";
import {CardElement} from "@shared/components/CardElement";
import {TimeInput} from "@shared/inputs/TimeInput";
import {TextInput, TextInputEmail} from "@shared/inputs/TextInput";
import {SwitchInput} from "@shared/inputs/SwitchInput";
import {ColorInput} from "@shared/inputs/ColorInput";
import {Trans, useTranslation} from "react-i18next";
import {DisplayInput} from "@shared/inputs/DisplayInput";
import {EditPage} from "@shared/pages/EditPage";
import {FormAvailabilitySlots} from "@shared/inputs/FormAvailabilitySlots";
import {LanguageSelectInput} from "@shared/inputs/LanguageSelectInput";
import {HideableContent} from "@shared/components/HideableContent";

export function GeneralConfigTab({generalConfigForm}) {
  const {message} = App.useApp();
  const setIsModified = EditPage.useSetIsModified();
  const dispatch = useDispatch();
  const {t} = useTranslation();
  const project = useSelector(projectsSelectors.selectEditing);
  const projectIsNotOpened = project.openingState === "notOpened";
  const projectIsInDemoMode = project.status === "demo";
  const onValuesChange = (changedFields, allFields) => {
    dispatch(projectsActions.changeEditing(allFields));
    setIsModified(true);
  };

  const onOpeningStateChange = (newStateIndex) => {
    const newState = Object.keys(OpeningState).find((key) => OpeningState[key] === newStateIndex);

    const projectData = newStateIndex >= 2 ? {openingState: newState} : {openingState: newState};

    if (newStateIndex >= 3 && !project.isPublic && !projectIsInDemoMode) {
      message.info(
        <span>
          {t("projects:wantToMakeYourProjectPublicMessage.content")}{" "}
          <Button
            type={"primary"}
            style={{marginLeft: 8}}
            target="_blank"
            onClick={() => {
              dispatch(projectsActions.changeEditing({isPublic: true}));
              generalConfigForm.setFieldValue("isPublic", true); // For now, form maanagement is shit and doesn't update by itself
              message.destroy();
            }}
            rel="noreferrer">
            {t("projects:wantToMakeYourProjectPublicMessage.buttonTitle")}
          </Button>
        </span>
      );
    }
    dispatch(projectsActions.changeEditing(projectData));
    setIsModified(true);
  };

  const sessionNameTemplateTags = {
    "{{sessionName}}": t("sessions:schema.name.label"),
    "{{activityName}}": t("activities:schema.name.label"),
  };

  // We have to memoize this because with the current system, selectors were rerendered each time we click, which was annoying.
  const memoizedAccentuationColorsSelectors = useMemo(
    () => (
      <DisplayInput label={t("projects:schema.theme.colors.accentuation")}>
        <Space>
          <ColorInput name={["theme", "accent1"]} noStyle />
          <ColorInput name={["theme", "accent2"]} noStyle />
        </Space>
      </DisplayInput>
    ),
    []
  );

  const SocialImagePreview = () => {
    const socialImageUrlVal = Form.useWatch("socialImageUrl", generalConfigForm);
    return socialImageUrlVal ? (
      <HideableContent
        buttonOpenTitle={t("projects:schema.socialImageUrl.seePreview")}
        buttonCloseTitle={t("projects:schema.socialImageUrl.hidePreview")}>
        <CardElement borderless style={{overflow: "hidden"}}>
          <img
            src={socialImageUrlVal}
            alt="Social media image preview"
            style={{maxWidth: "100%"}}
          />
        </CardElement>
      </HideableContent>
    ) : null;
  };

  return (
    <FormElement
      form={generalConfigForm}
      initialValues={{
        ...project,
        breakfastTime: dayjs(project.breakfastTime),
        lunchTime: dayjs(project.lunchTime),
        dinnerTime: dayjs(project.dinnerTime),
      }}
      onValuesChange={onValuesChange}>
      <div className="container-grid two-per-row" style={{alignItems: "start"}}>
        <div className="container-grid">
          <CardElement>
            <TextInput i18nNs={"projects"} name="name" required />

            <TextInputEmail i18nNs={"projects"} name="contactEmail" required />
          </CardElement>

          <CardElement title={t("projects:generalConfig.eventManagement")}>
            <div className="container-grid">
              <DisplayInput i18nNs={"projects"} name={"openingState"}>
                <Steps
                  current={OpeningState[project.openingState]}
                  direction={"vertical"}
                  onChange={onOpeningStateChange}
                  items={[
                    {
                      title: t("projects:schema.openingState.options.closed.title"),
                      key: "0",
                      description: t("projects:schema.openingState.options.closed.description"),
                    },
                    {
                      title: t("projects:schema.openingState.options.preRegisterOnly.title"),
                      key: "1",
                      description: t(
                        "projects:schema.openingState.options.preRegisterOnly.description"
                      ),
                    },
                    {
                      title: t(
                        "projects:schema.openingState.options.registerForStewardsOnly.title"
                      ),
                      key: "2",
                      description: t(
                        "projects:schema.openingState.options.registerForStewardsOnly.description"
                      ),
                    },
                    {
                      title: t("projects:schema.openingState.options.registerForAll.title"),
                      key: "3",
                      description: t(
                        "projects:schema.openingState.options.registerForAll.description"
                      ),
                    },
                  ]}
                />
              </DisplayInput>

              <SwitchInput i18nNs={"projects"} name="full" disabled={projectIsNotOpened} />

              <SwitchInput
                i18nNs={"projects"}
                name="isPublic"
                disabled={projectIsInDemoMode}
                title={
                  projectIsInDemoMode
                    ? t("projects:schema.isPublic.disabledIfInDemoMode")
                    : undefined
                }
              />

              <SwitchInput i18nNs={"projects"} name="secretSchedule" />
            </div>
          </CardElement>
        </div>

        <div className="container-grid">
          <div className="userTourProjectAvailabilities containerV">
            <FormAvailabilitySlots
              label={t("projects:schema.availabilitySlots.label")}
              name={"availabilitySlots"}
            />
          </div>

          <CardElement title={t("projects:generalConfig.mealsTimes")}>
            <div className="container-grid">
              <p style={{color: "gray"}}>
                <Trans i18nKey="generalConfig.mealsTimesDescription" ns="projects" />
              </p>
              <div className="container-grid three-per-row">
                <TimeInput i18nNs={"projects"} name="breakfastTime" required />
                <TimeInput i18nNs={"projects"} name="lunchTime" required />
                <TimeInput i18nNs={"projects"} name="dinnerTime" required />
              </div>
            </div>
          </CardElement>

          <CardElement title={t("projects:generalConfig.themeAndAppearance")}>
            <div className="container-grid">
              <div className="container-grid three-per-row">
                <ColorInput
                  label={t("projects:schema.theme.colors.primary")}
                  name={["theme", "primary"]}
                />
                <ColorInput label={t("projects:schema.theme.colors.bg")} name={["theme", "bg"]} />
                {memoizedAccentuationColorsSelectors}
              </div>

              <TextInput
                i18nNs="projects"
                name="sessionNameTemplate"
                placeholder={t("projects:schema.sessionNameTemplate.defaultValue")}
                tooltip={
                  <>
                    {t("projects:schema.sessionNameTemplate.tooltip")}
                    <ul>
                      {Object.entries(sessionNameTemplateTags).map(([key, val]) => (
                        <li key={key}>
                          <strong>{key} :</strong> {val}
                        </li>
                      ))}
                    </ul>
                  </>
                }
                allowClear
              />

              <SwitchInput i18nNs={"projects"} name="hideVolunteeringJaugeForParticipants" />

              <LanguageSelectInput i18nNs="projects" name="locale" />

              <div>
                <TextInput
                  i18nNs={"projects"}
                  name={"socialImageUrl"}
                  rules={[{type: "url"}]}
                  formItemProps={{style: {marginBottom: 0}}}
                />
                <SocialImagePreview />
              </div>

              {/* <TextAreaInput
                i18nNs={"projects"}
                name={"customCss"}
                autoSize={{minRows: 3, maxRows: 6}}
              /> */}
            </div>
          </CardElement>
        </div>
      </div>
    </FormElement>
  );
}
