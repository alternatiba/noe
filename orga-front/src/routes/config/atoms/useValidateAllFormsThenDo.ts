import {safeValidateFields} from "@shared/inputs/FormElement";
import {App, FormInstance} from "antd";

// Utility function to validate multiple forms, then execute a callback if success
export const useValidateAllFormsThenDo = () => {
  const {message} = App.useApp();

  return (
    formsWithErrorMessages: Array<{form: FormInstance; errorMessage: string}>,
    callbackOnSuccess: () => void
  ) => {
    const validateNextForm = (index: number) => {
      if (index >= formsWithErrorMessages.length) {
        // All forms validated, execute callback
        callbackOnSuccess();
        return;
      }

      const {form, errorMessage} = formsWithErrorMessages[index];
      safeValidateFields(
        form,
        () => validateNextForm(index + 1),
        (error) => message.error(errorMessage, 8)
      );
    };

    validateNextForm(0);
  };
};
