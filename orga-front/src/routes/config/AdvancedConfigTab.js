import React from "react";
import {Alert, Button, FormInstance, Popconfirm, Tabs} from "antd";
import {DeleteOutlined} from "@ant-design/icons";
import {projectsActions, projectsSelectors} from "@features/projects";
import {useDispatch, useSelector} from "react-redux";
import {FormElement} from "@shared/inputs/FormElement";
import {ImportExportTab} from "./advanced/ImportExportTab";
import {FeaturesFormContent} from "./advanced/FeaturesFormContent";
import ModificationsHistoryTimeline from "@shared/components/ModificationsHistoryTimeline";
import {getLocalStorage} from "@shared/utils/localStorageUtilities";
import {useNavigate} from "react-router-dom";
import {EditPage} from "@shared/pages/EditPage";
import {RollbackTab} from "./advanced/RollbackTab";
import {WebhooksTab} from "./advanced/WebhooksTab";
import {useTranslation} from "react-i18next";

const TestCrashAppButton = () => {
  console.log("test", getLocalStorage("testMode", false));
  return getLocalStorage("testMode", false) ? (
    <Button onClick={(unknown) => (unknown.variable.that.crashes = unknown)}>
      Crash the app (test)
    </Button>
  ) : null;
};

export default function AdvancedConfigTab({webhooksForm}: {webhooksForm: FormInstance}) {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const setIsModified = EditPage.useSetIsModified();
  const project = useSelector(projectsSelectors.selectEditing);
  const dispatch = useDispatch();

  const deleteProject = () => {
    dispatch(projectsActions.remove()).then(() => navigate("/projects"));
  };

  return (
    <Tabs
      items={[
        {label: "Import & Export", key: "import-export", children: <ImportExportTab />},
        {
          label: "Fonctionnalités",
          key: "features",
          children: (
            <FormElement>
              <FeaturesFormContent setIsModified={setIsModified} />
            </FormElement>
          ),
        },
        {
          label: "Webhooks",
          key: "webhooks",
          children: <WebhooksTab webhooksForm={webhooksForm} />,
        },
        {
          label: "Oops!",
          key: "rollback",
          children: <RollbackTab />,
        },
        {
          label: "Historique de modification",
          key: "history",
          children: (
            <ModificationsHistoryTimeline
              elementsActions={projectsActions}
              record={project}
              path="./.."
            />
          ),
        },
        {
          label: "Suppression du projet",
          key: "delete",
          children: (
            <>
              <Alert
                style={{marginBottom: 26}}
                type={"warning"}
                showIcon
                message={
                  <>
                    Attention, <strong>la suppression du projet est irréversible</strong> et
                    engendrera également la suppression des catégories, espaces,{" "}
                    {t("stewards:labelDown_other")}, activités et sessions liés au projet.
                  </>
                }
              />
              <Popconfirm
                title={
                  "Ëtes-vous sûr de vouloir supprimer l'entièreté du projet " + project.name + " ?"
                }
                onConfirm={deleteProject}
                okText="Oui, je veux définitivement supprimer le projet"
                cancelText="Non, je veux revenir en arrière">
                <Button type="primary" danger icon={<DeleteOutlined />}>
                  Supprimer le projet
                </Button>
              </Popconfirm>
              <TestCrashAppButton />
            </>
          ),
        },
      ]}></Tabs>
  );
}
