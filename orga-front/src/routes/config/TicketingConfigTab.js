import React, {useMemo} from "react";
import {Alert} from "antd";
import {projectsActions, projectsSelectors} from "@features/projects";
import {useDispatch, useSelector} from "react-redux";
import {FormElement} from "@shared/inputs/FormElement";
import {addKeyToItemsOfList} from "@shared/utils/tableUtilities";
import {TableElement} from "@shared/components/TableElement";
import dayjs from "@shared/services/dayjs";
import Paragraph from "antd/es/typography/Paragraph";
import {CardElement} from "@shared/components/CardElement";
import {URLS} from "@app/configuration";
import {TextInput, TextInputPassword} from "@shared/inputs/TextInput";
import {useUserTour} from "@shared/utils/userTourUtilities";
import helloAssoUserTour from "@utils/userTours/helloAssoUserTour";
import {Trans, useTranslation} from "react-i18next";
import {SelectInput} from "@shared/inputs/SelectInput";
import {sorter} from "@shared/utils/sorters";
import {EditPage} from "@shared/pages/EditPage";

const useHelloAssoTicketingColumns = () => {
  const {t} = useTranslation();
  return [
    {
      title: t("projects:ticketing.helloAssoTicketingColumns.title"),
      dataIndex: "title",
      sorter: (a, b) => sorter.text(a.title, b.title),
      defaultSortOrder: "descend",
      width: 500,
    },
    {
      title: t("projects:ticketing.helloAssoTicketingColumns.startDate"),
      dataIndex: "startDate",
      sorter: (a, b) => sorter.date(a.startDate, b.startDate),
      render: (text, record) => (text !== undefined ? dayjs(text).format("LL") : text),
      width: 170,
    },
    {
      title: t("projects:ticketing.helloAssoTicketingColumns.description"),
      dataIndex: "description",
      width: 1000,
    },
  ];
};

export function TicketingConfigTab({ticketingConfigForm}) {
  const {t} = useTranslation();
  const setIsModified = EditPage.useSetIsModified();
  const isModified = EditPage.useIsModified();
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const ticketingIsSetUp = {
    helloAsso:
      project.helloAsso?.selectedEvent &&
      project.helloAsso?.clientId &&
      project.helloAsso?.clientSecret,
    tiBillet: project.tiBillet?.serverUrl && project.tiBillet?.apiKey,
    customTicketing: project.customTicketing?.length > 0,
  };
  const ticketingIsOperational = project.ticketingMode && ticketingIsSetUp[project.ticketingMode];

  const helloAssoTicketingColumns = useHelloAssoTicketingColumns();

  useUserTour("helloAsso", helloAssoUserTour(), {
    shouldShowNow: () =>
      project.ticketingMode === "helloAsso" && window.location.hash === "#ticketing",
    deps: [project.ticketingMode, window.location.hash],
  });

  const setHelloAssoEvent = (selectedRowObject) => {
    dispatch(
      projectsActions.changeEditing({
        helloAsso: {...project.helloAsso, selectedEvent: selectedRowObject[0].key},
      })
    );
    setIsModified(true);
  };

  const onTicketingValuesConfigChange = (_, allValues) => {
    dispatch(projectsActions.changeEditing(allValues));
    setIsModified(true);
  };

  const rowSelectionHelloAssoEvent = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setHelloAssoEvent(selectedRowObject);
    },
    type: "radio",
  };

  const TicketingCardElement = useMemo(
    () =>
      ({ticketingMode, children, ...props}) =>
        project.ticketingMode === ticketingMode ? (
          <CardElement {...props} className="fade-in" style={{marginBottom: 0, marginTop: 16}}>
            <FormElement
              form={ticketingConfigForm}
              initialValues={project}
              onValuesChange={onTicketingValuesConfigChange}>
              {children}
            </FormElement>
          </CardElement>
        ) : null,
    [project?.ticketingMode]
  );

  return (
    <>
      {ticketingIsOperational && !isModified && (
        <Alert
          style={{marginBottom: 26}}
          message={t("projects:ticketing.ticketingIsActivated")}
          showIcon
          type="success"
        />
      )}

      <CardElement>
        <FormElement initialValues={project} onValuesChange={onTicketingValuesConfigChange}>
          <SelectInput
            className={"userTourTicketingOptions"}
            i18nNs={"projects"}
            name="ticketingMode"
            options={[
              {value: null, label: <em>- {t("projects:ticketing.noTicketingOption")} -</em>},
              {value: "tiBillet", label: t("projects:schema.tiBillet.label")},
              {value: "helloAsso", label: t("projects:schema.helloAsso.label")},
              {value: "customTicketing", label: t("projects:schema.customTicketing.label")},
            ]}
          />
        </FormElement>
      </CardElement>
      <TicketingCardElement title={t("projects:schema.tiBillet.label")} ticketingMode="tiBillet">
        <div className="container-grid three-per-row">
          <TextInput
            label={t("projects:ticketing.tiBilletTicketing.url.label")}
            required
            name={["tiBillet", "serverUrl"]}
            placeholder={t("projects:ticketing.tiBilletTicketing.url.placeholder")}
            rules={[{type: "url"}]}
          />
          <TextInput
            label={t("projects:ticketing.tiBilletTicketing.slug.label")}
            name={["tiBillet", "eventSlug"]}
            required
            placeholder={t("projects:ticketing.tiBilletTicketing.slug.placeholder")}
          />
          <TextInputPassword
            label={t("projects:ticketing.tiBilletTicketing.apiKey.label")}
            name={["tiBillet", "apiKey"]}
            required
            placeholder={t("projects:ticketing.tiBilletTicketing.apiKey.placeholder")}
          />
        </div>
      </TicketingCardElement>

      <TicketingCardElement
        title={t("projects:schema.customTicketing.label")}
        ticketingMode="customTicketing">
        <Alert
          style={{marginBottom: 26}}
          message={t("projects:ticketing.customTicketing.message")}
          //description={t("projects:")}
          description={
            <>
              <Trans
                ns={"projects"}
                i18nKey={"ticketing.customTicketing.description"}
                values={{
                  destinationUrl: project.customTicketing || " -",
                }}
              />
              <a href={`${URLS.API}/projects/${project._id}/ticketing/test`}>
                {URLS.API}/projects/{project._id}/ticketing/test
              </a>
            </>
          }
          type="info"
        />
        <TextInput
          label={t("projects:ticketing.customTicketing.url.label")}
          name="customTicketing"
          required
          placeholder={t("projects:ticketing.customTicketing.url.placeholder")}
          rules={[{type: "url"}]}
        />
      </TicketingCardElement>

      <TicketingCardElement title={t("projects:schema.helloAsso.label")} ticketingMode="helloAsso">
        {!project.helloAsso?.selectedEvent &&
          project.helloAsso?.organizationSlug &&
          !isModified && (
            <Alert
              type="warning"
              showIcon
              style={{marginBottom: 26}}
              className="bounce-in"
              message={t("projects:ticketing.helloAssoTicketing.configuration.message")}
              description={t("projects:ticketing.helloAssoTicketing.configuration.description")}
            />
          )}
        {project.helloAsso?.organizationSlug && (
          <Alert
            showIcon
            style={{marginBottom: 26}}
            className="bounce-in"
            message={t("projects:ticketing.helloAssoTicketing.integration.message")}
            description={
              <>
                <Trans
                  ns={"projects"}
                  i18nKey={"ticketing.helloAssoTicketing.integration.description"}
                />
                <Paragraph
                  copyable
                  style={{
                    color: "var(--noe-primary)",
                    backgroundColor: "white",
                    width: "fit-content",
                    borderRadius: 8,
                    padding: 8,
                    marginBottom: 0,
                  }}>
                  {`${URLS.API}/projects/${project._id}/ticketing/onHelloAssoOrder`}
                </Paragraph>
              </>
            }
          />
        )}
        <div className="container-grid two-per-row">
          <TextInputPassword
            label={t("projects:ticketing.helloAssoTicketing.clientId.label")}
            name={["helloAsso", "clientId"]}
            required
            placeholder={t("projects:ticketing.helloAssoTicketing.clientId.placeholder")}
          />
          <TextInputPassword
            label={t("projects:ticketing.helloAssoTicketing.clientSecret.label")}
            name={["helloAsso", "clientSecret"]}
            required
            placeholder={t("projects:ticketing.helloAssoTicketing.clientSecret.placeholder")}
          />
        </div>
        {project.helloAsso?.organizationSlug && (
          <>
            <div style={{marginBottom: 26}}>
              <strong>{t("projects:ticketing.helloAssoTicketing.foundOrganization")}</strong>{" "}
              {project.helloAsso?.organizationSlug}
            </div>
            <TableElement.Simple
              showHeader
              scroll={{x: (helloAssoTicketingColumns.length - 1) * 160 + 70}}
              rowSelection={rowSelectionHelloAssoEvent}
              selectedRowKeys={[{key: project.helloAsso?.selectedEvent}]}
              columns={helloAssoTicketingColumns}
              rowKey="formSlug"
              dataSource={addKeyToItemsOfList(
                project.helloAsso?.selectedEventCandidates,
                "formSlug"
              )}
            />
          </>
        )}
      </TicketingCardElement>
    </>
  );
}
