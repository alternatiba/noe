import {CardElement} from "@shared/components/CardElement";
import {FormElement} from "@shared/inputs/FormElement";
import React from "react";
import dayjs from "@shared/services/dayjs";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "@features/projects";
import {Trans, useTranslation} from "react-i18next";
import {SwitchInput} from "@shared/inputs/SwitchInput";
import {TextAreaInput} from "@shared/inputs/TextAreaInput";
import {SliderInput} from "@shared/inputs/SliderInput";
import {Link} from "react-router-dom";
import {clickOnTab} from "@shared/utils/userTourUtilities";
import {EditPage} from "@shared/pages/EditPage";
import {HideableContent} from "@shared/components/HideableContent";
import {flattenFieldComponents} from "@shared/utils/flattenFieldComponents";

import {dateFormatter, timeFormatter} from "@shared/utils/formatters";

export const SubscriptionsConfigTab = () => {
  const {t} = useTranslation();
  const setIsModified = EditPage.useSetIsModified();
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);

  const smsMessageTags = {
    "{{sessionName}}": t("sessions:schema.name.label"),
    "{{startDate}} / {{endDate}}": dateFormatter.longDate(dayjs()),
    "{{startDateTime}} / {{endDateTime}}": dateFormatter.longDateTime(dayjs()),
    "{{startTime}} / {{endTime}}": timeFormatter.time(dayjs()),
  };

  const changeEditing = (changedFields, values) => {
    dispatch(projectsActions.changeEditing(values));
    setIsModified(true);
  };

  return (
    <FormElement onValuesChange={changeEditing} initialValues={project}>
      <div className={"container-grid two-per-row"}>
        <div>
          <CardElement title={t("projects:registrations.subscriptionsManagement")}>
            <div className="container-grid">
              <SwitchInput i18nNs={"projects"} name="notAllowOverlap" />

              <SwitchInput i18nNs={"projects"} name="blockSubscriptions" />

              <SwitchInput
                i18nNs={"projects"}
                disabled={project.blockSubscriptions}
                name="blockVolunteeringUnsubscribeIfBeginsSoon"
              />

              <SliderInput
                i18nNs={"projects"}
                name="minMaxVolunteering"
                range
                inputProps={{
                  tooltip: {formatter: (val) => timeFormatter.duration(val, {short: true})},
                }}
                step={15}
                max={720}
                marks={{
                  0: "0" + t("common:time.h"),
                  60: "1" + t("common:time.h"),
                  120: "2" + t("common:time.h"),
                  180: "3" + t("common:time.h"),
                  240: "4" + t("common:time.h"),
                  300: "5" + t("common:time.h"),
                  360: "6" + t("common:time.h"),
                  420: "7" + t("common:time.h"),
                  480: "8" + t("common:time.h"),
                  540: "9" + t("common:time.h"),
                  600: "10" + t("common:time.h"),
                  660: "11" + t("common:time.h"),
                  720: "12" + t("common:time.h"),
                }}
              />

              <HideableContent
                buttonOpenTitle={t(
                  "projects:registrations.moreInfoAboutVolunteeringAndDaysOfPresence.title"
                )}>
                <Trans
                  ns="projects"
                  i18nKey="registrations.moreInfoAboutVolunteeringAndDaysOfPresence.description"
                />
              </HideableContent>
            </div>
          </CardElement>
        </div>

        <div>
          <CardElement
            title={t("projects:registrations.bulkSmsMessages")}
            subtitle={t("projects:schema.smsMessageTemplate.description")}
            className={"userTourSmsMessageTemplate"}>
            {flattenFieldComponents(project.formComponents).find(
              (formComp) => formComp.type === "phoneNumber"
            ) ? (
              <TextAreaInput
                label={t("projects:schema.smsMessageTemplate.label")}
                formItemProps={{style: {marginTop: 26, marginBottom: 0}}}
                placeholder={t("projects:schema.smsMessageTemplate.defaultValue")}
                tooltip={
                  <>
                    {t("projects:schema.smsMessageTemplate.tooltip")}
                    <ul>
                      {Object.entries(smsMessageTags).map(([key, val]) => (
                        <li key={key}>
                          <strong>{key} :</strong> {val}
                        </li>
                      ))}
                    </ul>
                  </>
                }
                name={"smsMessageTemplate"}
              />
            ) : (
              <>
                <p style={{color: "grey", marginTop: 16}}>
                  {t("projects:schema.smsMessageTemplate.addPhoneNumberFormComp")}
                </p>
                <Link to={"#form"} onClick={() => clickOnTab("form")}>
                  {t("projects:schema.smsMessageTemplate.goToForm")}
                </Link>
              </>
            )}
          </CardElement>
        </div>
      </div>
    </FormElement>
  );
};
