import {Alert} from "antd";
import {Trans, useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {CardElement} from "@shared/components/CardElement";
import {projectsSelectors} from "@features/projects";
import {Link} from "react-router-dom";
import {SelectInput} from "@shared/inputs/SelectInput";
import {DEFAULT_COCKPIT_PREFERENCES} from "../CockpitConfigDrawer";
import {DisplayInput} from "@shared/inputs/DisplayInput";

const ALLOWED_FORM_FIELD_TYPES = [
  "radioGroup",
  "select",
  "checkboxGroup",
  "multiSelect",
  "checkbox",
];

const ChartSelectionInputs = () => {
  const {t} = useTranslation();

  const flatFormComponents = useSelector(projectsSelectors.selectFlatFormComponents);

  // Get only questions with a type that can be displayed in a chart view
  const formAnswersOptions = flatFormComponents
    .filter((component) => ALLOWED_FORM_FIELD_TYPES.includes(component.type))
    .map((answer) => ({label: answer.label, value: answer.key}));

  const GraphMultiSelect = ({label, type, options}) => {
    return (
      <SelectInput
        label={label}
        mode="multiple"
        name={["charts", type]}
        placeholder={t("cockpit:dashboard.chartSelection.placeholder")}
        options={options}
      />
    );
  };

  return (
    <CardElement title={t("cockpit:dashboard.participantsDataTabLabel")}>
      <GraphMultiSelect
        label={t("cockpit:dashboard.chartSelection.usefulGraphs")}
        type="useful"
        options={DEFAULT_COCKPIT_PREFERENCES.charts.useful.map((chartName) => ({
          label: t(`cockpit:${chartName}.label`),
          value: chartName,
        }))}
      />

      {formAnswersOptions?.length > 0 ? (
        <GraphMultiSelect
          label={t("cockpit:dashboard.chartSelection.answersGraphs")}
          type="answers"
          options={formAnswersOptions}
        />
      ) : (
        <DisplayInput label={t("cockpit:dashboard.chartSelection.answersGraphs")}>
          <Alert
            message={
              <Trans
                ns="cockpit"
                i18nKey="dashboard.chartSelection.addQuestionsToFormToVisualizeAnswers"
                components={{linkToFormConfig: <Link to="../config#form" />}}
              />
            }
          />
        </DisplayInput>
      )}
    </CardElement>
  );
};

export default ChartSelectionInputs;
