import {Alert} from "antd";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Pending} from "@shared/components/Pending";
import {useLoadList} from "@shared/hooks/useLoadList";
import {projectsSelectors} from "@features/projects";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {useRegistrationsWithDaysOfPresence} from "../atoms/statsUtils";
import PresenceAndMealsChart from "./charts/PresenceAndMealsChart";
import TagsPerDayChart from "./charts/TagsPerDayChart";
import RegistrationsChart from "./charts/RegistrationsChart";
import FormAnswerChart from "./charts/FormAnswerChart/FormAnswerChart";
import NumberOfParticipantsByHourChart from "./charts/NumberOfParticipantsByHourChart";
import {useTranslation} from "react-i18next";

const usefulChartsMapping = {
  presenceAndMealsStats: PresenceAndMealsChart,
  numberOfParticipantsByHourStats: NumberOfParticipantsByHourChart,
  tagsStats: TagsPerDayChart,
  registrationStats: RegistrationsChart,
};

export default function ParticipantDashboard() {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // Very narrow selectors to avoid re-render on charts prefes changes
  const usefulCharts = useSelector(
    (state) => projectsSelectors.selectEditing(state).cockpitPreferences?.charts?.useful
  );
  const answersCharts = useSelector(
    (state) => projectsSelectors.selectEditing(state).cockpitPreferences?.charts?.answers
  );

  const registrationsLoaded = useSelector(registrationsSelectors.selectIsLoaded);

  const registrationsBooked = useRegistrationsWithDaysOfPresence();

  useLoadList(() => {
    dispatch(registrationsActions.loadList());
  });

  // If loading, return the spinner
  if (!registrationsLoaded) return <Pending />;

  // If no participants at all, show  alert at the top
  if (registrationsBooked.length === 0)
    return (
      <Alert
        message={t("cockpit:dashboard.participantsData.noRegistrationYet.message")}
        description={t("cockpit:dashboard.participantsData.noRegistrationYet.description")}
      />
    );

  return (
    <>
      <Alert
        description={t("cockpit:dashboard.participantsData.displayOngoingAndCompleteRegistrations")}
        style={{marginBottom: 26}}
      />

      {usefulCharts?.map((chartName) => {
        const UsefulChartComp = usefulChartsMapping[chartName];
        return UsefulChartComp ? <UsefulChartComp key={chartName} /> : null;
      })}

      {answersCharts?.map((answer) => (
        <FormAnswerChart key={answer.key} field={answer.key} displayMode={answer.displayMode} />
      ))}
    </>
  );
}
