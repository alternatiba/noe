import React from "react";
import {Pending, PendingSuspense} from "@shared/components/Pending";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {addKeyToItemsOfList} from "@shared/utils/tableUtilities";
import RawDataCollapsibleTable from "./utils/RawDataCollapsibleTable";
import {CardElement} from "@shared/components/CardElement";
import {useTranslation} from "react-i18next";
import {sorter} from "@shared/utils/sorters";

import {useRegistrationsStatsByDaysOfPresence} from "./utils/useRegistrationsStatsByDaysOfPresence";
import {Stack} from "@shared/layout/Stack";
import {useDayStatColumn} from "../../atoms/useDayStatColumn";

const DualAxesChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/charts/DualAxesChart")
);

export default function PresenceAndMealsChart() {
  const {t} = useTranslation();
  const columnsDOPMealStats = [
    useDayStatColumn(),
    {
      title: t("cockpit:presenceAndMealsStats.participantsOnSite"),
      dataIndex: "participantsOnSite",
      sorter: (a, b) => sorter.number(a.participantsOnSite, b.participantsOnSite),
    },
    {
      title: t("cockpit:presenceAndMealsStats.participantsEatingBreakfast"),
      dataIndex: "breakfast",
      sorter: (a, b) => sorter.number(a.breakfast, b.breakfast),
    },
    {
      title: t("cockpit:presenceAndMealsStats.participantsEatingLunch"),
      dataIndex: "lunch",
      sorter: (a, b) => sorter.number(a.lunch, b.lunch),
    },
    {
      title: t("cockpit:presenceAndMealsStats.participantsEatingDinner"),
      dataIndex: "dinner",
      sorter: (a, b) => sorter.number(a.dinner, b.dinner),
    },
  ];

  const mealsAndFreqStats = useRegistrationsStatsByDaysOfPresence();

  if (!mealsAndFreqStats) return <Pending />;

  // Create the stats objects for the Ant design plot
  const plottedFreqStats = mealsAndFreqStats.map((stat) => ({
    dateShort: stat.dateShort,
    participantsOnSite: stat.participantsOnSite,
    name: t("cockpit:presenceAndMealsStats.participantsOnSite"),
  }));

  // Create the stats objects for the Ant design plot
  const plottedMealsStats = mealsAndFreqStats.reduce(
    (acc, stat) => [
      ...acc,
      {
        dateShort: stat.dateShort,
        count: stat.breakfast,
        name: t("cockpit:presenceAndMealsStats.breakfastsToPlan"),
      },
      {
        dateShort: stat.dateShort,
        count: stat.lunch,
        name: t("cockpit:presenceAndMealsStats.lunchesToPlan"),
      },
      {
        dateShort: stat.dateShort,
        count: stat.dinner,
        name: t("cockpit:presenceAndMealsStats.dinnersToPlan"),
      },
    ],
    []
  );

  return (
    <>
      <CardElement title={t("cockpit:presenceAndMealsStats.label")}>
        <Stack gap={2}>
          <PendingSuspense>
            <DualAxesChart
              config={{
                data: [plottedFreqStats, plottedMealsStats],
                height: 300,
                xField: "dateShort",
                yField: ["participantsOnSite", "count"],
                geometryOptions: [
                  {geometry: "column", seriesField: "name"},
                  {geometry: "line", smooth: true, seriesField: "name"},
                ],
                // Use sync to synchronize both axis and hide the yAxis of the second chart
                meta: {
                  maxNumberOfParticipants: {sync: "count"},
                  count: {sync: true},
                },
                yAxis: {count: false},
              }}
            />
          </PendingSuspense>
          <RawDataCollapsibleTable
            exportTitle={t("cockpit:presenceAndMealsStats.label")}
            columns={columnsDOPMealStats}
            dataSource={addKeyToItemsOfList(mealsAndFreqStats)}
          />
        </Stack>
      </CardElement>
    </>
  );
}
