import dayjs from "@shared/services/dayjs";
import {useRegistrationsWithDaysOfPresence} from "../../../atoms/statsUtils";
import {getDateShort, initializeGraphDates} from "./initializeGraphDates";

/**
 * Custom React hook that calculates and provides registration statistics.
 *
 * @returns {Array<Object>} An array of objects representing registration statistics.
 * @example
 * const registrationsStats = useRegistrationsStats();
 */
export const useRegistrationsStats = () => {
  const registrationsBooked = useRegistrationsWithDaysOfPresence();

  let registrationsStats = [];
  // Initialize and format the dates
  const statsDates = initializeGraphDates(
    registrationsBooked,
    (r) => dayjs(r.createdAt),
    (r) => dayjs(r.createdAt)
  );

  statsDates.forEach((dateRender) => {
    registrationsStats.push({
      ...dateRender,
      newRegistrations: 0,
      completedRegistrations: 0,
    });
  });

  registrationsBooked.forEach((registration) => {
    // Increment +1 the date at which the current registration was created.
    const statForCreationDate = registrationsStats.find(
      (st) => st.dateShort === getDateShort(registration.createdAt)
    );

    if (statForCreationDate) {
      statForCreationDate.newRegistrations += 1;

      // If the registration is valid, also add it to the completed registrations
      if (registration.everythingIsOk) statForCreationDate.completedRegistrations += 1;
    }
  });

  // Make the sums of new and completed Registrations
  for (let i = 0; i < registrationsStats.length - 1; i++) {
    registrationsStats[i].newRegistrationsTotal =
      (registrationsStats[i - 1]?.newRegistrationsTotal || 0) +
      registrationsStats[i].newRegistrations;
    registrationsStats[i].completedRegistrationsTotal =
      (registrationsStats[i - 1]?.completedRegistrationsTotal || 0) +
      registrationsStats[i].completedRegistrations;
  }

  return registrationsStats;
};
