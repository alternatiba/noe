import {useSelector} from "react-redux";
import {registrationsSelectors} from "@features/registrations";
import {useRegistrationsWithDaysOfPresence} from "../../../atoms/statsUtils";
import {projectsSelectors} from "@features/projects";

export const useFormAnswerStatsAbsolute = (fieldKey) => {
  const registrationsLoaded = useSelector(registrationsSelectors.selectIsLoaded);
  const registrationsBooked = useRegistrationsWithDaysOfPresence();
  const flatFormComponents = useSelector(projectsSelectors.selectFlatFormComponents);
  const formComp = flatFormComponents.find((comp) => comp.key === fieldKey);

  if (!registrationsLoaded || registrationsBooked.length === 0) return;

  const answersForField = registrationsBooked
    .map((registration) => {
      const formAnswer = registration.formAnswers?.[fieldKey];
      if (formComp.type === "checkbox") {
        // If registration okay, and undefined, thena checkbox that has not been answered is considered as false
        return formAnswer === undefined
          ? registration.everythingIsOk
            ? "false"
            : "undefined"
          : formAnswer;
      } else {
        return formAnswer;
      }
    })
    .filter((field) => !!field)
    .flat();

  const countedOccurrencesOfEachOption = answersForField.reduce((acc, box) => {
    acc[box] = (acc[box] || 0) + 1;
    return acc;
  }, {});

  return Object.entries(countedOccurrencesOfEachOption).map(([key, count]) => ({
    key,
    count,
  }));
};
