export const meals = [
  {name: "matin", field: "breakfast"},
  {name: "midi", field: "lunch"},
  {name: "soir", field: "dinner"},
];
