import {useSelector} from "react-redux";
import {registrationsSelectors} from "@features/registrations";
import {projectsSelectors} from "@features/projects";
import {useMemo} from "react";
import {calculateStatsForEachDay} from "./calculateMealsAndFreqStat";
import {useDaysOfPresenceBaseStats} from "./useDaysOfPresenceBaseStats";

export const useFormAnswerStatsByDaysOfPresence = (fieldKey) => {
  const registrationsBooked = useSelector(registrationsSelectors.selectListWithMetadata).filter(
    (registration) => registration.availabilitySlots?.length > 0
  );
  const daysOfPresenceBaseStats = useDaysOfPresenceBaseStats();
  const flatFormComponents = useSelector(projectsSelectors.selectFlatFormComponents);

  return useMemo(() => {
    const formComp = flatFormComponents.find((comp) => comp.key === fieldKey);
    const formAnswerStats = [];

    // Create base containers for tags and specified key.
    const baseContainer =
      formComp.type === "checkbox"
        ? {true: 0, false: 0, undefined: 0}
        : formComp.options.reduce((acc, option) => ({...acc, [option.value]: 0}), {});

    // If a key is specified and no base containers are created, return undefined

    daysOfPresenceBaseStats.forEach(
      // Initialize stats entry for each date render.
      (dateRender) => {
        formAnswerStats.push({
          ...dateRender,
          participantsOnSite: 0,
          [fieldKey]: {...baseContainer}, // Destructure to clone the object
        });
      }
    );

    // Calculate meals and frequency statistics.
    registrationsBooked.forEach((registration, indexReg) => {
      const formAnswer = registration.formAnswers?.[fieldKey];

      calculateStatsForEachDay(registration, formAnswerStats, (statForDay) => {
        statForDay.participantsOnSite += 1;

        // Update specific key counts (if provided)
        if (formComp.type === "checkbox") {
          const value =
            formAnswer === undefined
              ? registration.everythingIsOk
                ? "false"
                : "undefined"
              : formAnswer;
          statForDay[fieldKey][value] += 1;
        } else if (formAnswer) {
          if (Array.isArray(formAnswer)) {
            formAnswer.forEach((optionKey) => {
              statForDay[fieldKey][optionKey] += 1;
            });
          } else {
            statForDay[fieldKey][formAnswer] += 1;
          }
        }
      });
    });
    return formAnswerStats;
  }, [daysOfPresenceBaseStats, registrationsBooked, fieldKey]);
};
