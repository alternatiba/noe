import dayjs, {dayjsBase} from "@shared/services/dayjs";
import {useSelector} from "react-redux";
import {projectsSelectors} from "@features/projects";
import {registrationsSelectors} from "@features/registrations";
import {initializeGraphDates} from "./initializeGraphDates";
import {useMemo} from "react";

export const useDaysOfPresenceBaseStats = () => {
  const registrationsLoaded = useSelector(registrationsSelectors.selectIsLoaded);
  const project = useSelector(projectsSelectors.selectEditing);
  const registrationsBooked = useSelector(registrationsSelectors.selectListWithMetadata).filter(
    (registration) => registration.availabilitySlots?.length > 0
  );

  return useMemo(() => {
    if (!registrationsLoaded || registrationsBooked.length === 0) return;

    // Initialize graph dates and calculate stats.
    return initializeGraphDates(
      registrationsBooked,
      // Get the minimum start date from days of presence.
      (registration) =>
        registration.daysOfPresence.reduce((accumulator, day) => {
          const start = dayjs(day.start);
          return accumulator ? dayjsBase.min(start, accumulator) : start;
        }, undefined),
      // Get the maximum end date from days of presence.
      (registration) =>
        registration.daysOfPresence.reduce((accumulator, day) => {
          const end = dayjs(day.end);
          return accumulator ? dayjsBase.max(end, accumulator) : end;
        }, undefined),
      {
        start: dayjs(project.start).startOf("day").subtract(30, "day"), // Put a boundary 30 days before the project start
        end: dayjs(project.end).startOf("day").add(30, "day"), // Put a boundary 30 days after the project end
      }
    );
  }, [registrationsLoaded, registrationsBooked, project]);
};
