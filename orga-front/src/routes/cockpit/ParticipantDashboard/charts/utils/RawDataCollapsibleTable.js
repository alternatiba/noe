import {Collapse} from "antd";
import {TableElement} from "@shared/components/TableElement";
import CsvExportButton from "@shared/components/buttons/CsvExportButton";
import {useSelector} from "react-redux";
import {projectsSelectors} from "@features/projects";
import {useTranslation} from "react-i18next";

const {Panel} = Collapse;

const RawDataCollapsibleTable = ({dataSource, columns, exportTitle}) => {
  const {t} = useTranslation();
  const project = useSelector(projectsSelectors.selectEditing);

  return (
    <Collapse>
      <Panel
        header={
          <div className="containerH" style={{justifyContent: "space-between"}}>
            <span>{t("cockpit:rawDataExport.title")}</span>
            <CsvExportButton
              size="small"
              dataExportFunction={async () => dataSource}
              withCurrentDate
              getExportName={() => `${exportTitle} - ${project.name}`}>
              {t("cockpit:rawDataExport.export")}
            </CsvExportButton>
          </div>
        }>
        <TableElement.Simple
          showHeader
          columns={columns}
          dataSource={dataSource}
          scroll={{y: 700}}
        />
      </Panel>
    </Collapse>
  );
};

export default RawDataCollapsibleTable;
