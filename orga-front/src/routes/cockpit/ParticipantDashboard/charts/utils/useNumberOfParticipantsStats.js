import {useDispatch, useSelector} from "react-redux";
import {useMemo} from "react";
import dayjs, {dayjsBase} from "@shared/services/dayjs";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {filterSessionsByCategories} from "@routes/sessions/atoms/filterSessionsByCategories";
import {useLoadList} from "@shared/hooks/useLoadList";

export const useNumberOfParticipantsStats = (categoriesFilter: Array<string>) => {
  const dispatch = useDispatch();
  const sessions = useSelector(sessionsSelectors.selectList);

  useLoadList(() => {
    dispatch(sessionsActions.loadList());
  });

  return useMemo(() => {
    if (sessions.length === 0) return [];

    const numberOfParticipantsGroupedByHour = {};

    const filteredSessions = filterSessionsByCategories(sessions, categoriesFilter);

    // Calculate the earliest start and latest end dates
    const earliestStart = dayjsBase.min(...filteredSessions.map((session) => dayjs(session.start)));
    const latestEnd = dayjsBase.max(...filteredSessions.map((session) => dayjs(session.end)));

    // Fill maxNumberOfParticipantsByHour and numberOfParticipantsByHour with all possible hours
    let currentHour = earliestStart.startOf("hour");
    while (currentHour.isBefore(latestEnd) || currentHour.isSame(latestEnd)) {
      numberOfParticipantsGroupedByHour[currentHour.format("DD/MM HH:00")] = {
        maxNumberOfParticipants: 0,
        numberParticipants: 0,
      };
      currentHour = currentHour.add(1, "hour");
    }

    filteredSessions.forEach((session) => {
      // For each session, add the max number of participants for each hour
      const startTime = dayjs(session.start).startOf("hour");
      const endTime = dayjs(session.end).startOf("hour");
      const duration = endTime.diff(startTime, "hour");

      for (let i = 0; i < duration; i++) {
        const hour = startTime.add(i, "hour").format("DD/MM HH:00");
        numberOfParticipantsGroupedByHour[hour].maxNumberOfParticipants +=
          session.computedMaxNumberOfParticipants || 0;
        numberOfParticipantsGroupedByHour[hour].numberParticipants +=
          session.numberParticipants || 0;
      }
    });

    return Object.entries(numberOfParticipantsGroupedByHour).map(
      ([hour, {maxNumberOfParticipants, numberParticipants}]) => ({
        hour,
        maxNumberOfParticipants,
        numberParticipants,
      })
    );
  }, [sessions, categoriesFilter]);
};
