import dayjs from "@shared/services/dayjs";

export const getDateShort = (date) => dayjs(date).format("D MMM YYYY");

/**
 * Initializes dates for the graph based on registrations data and specified date-related functions.
 *
 * @param {Array<Object>} registrations - Array of registered participants' data.
 * @param {Function} getStartDate - Function to extract start date from a registration object.
 * @param {Function} getEndDate - Function to extract end date from a registration object.
 * @param datesBoundaries some dayjs date boundaries for the plot
 * @returns {Array<Object>} An array of date objects with formatted date information for graph display.
 */
export const initializeGraphDates = (registrations, getStartDate, getEndDate, datesBoundaries) => {
  // Obtenir les dates de début et de fin min/max de manière radicalement optimisée
  const minStartDate = dayjs(
    Math.max(
      Math.min(...registrations.map((r) => getStartDate(r).valueOf())),
      datesBoundaries?.start ? datesBoundaries.start.valueOf() : -Infinity
    )
  );
  const maxEndDate = dayjs(
    Math.min(
      Math.max(...registrations.map((r) => getEndDate(r).valueOf())),
      datesBoundaries?.end ? datesBoundaries.end.valueOf() : Infinity
    )
  );

  const arrayOfDates = [];

  // Calculate the total number of days to display on the graph
  const totalNumberOfDays = maxEndDate.startOf("day").diff(minStartDate.startOf("day"), "day") + 1;

  // Generate date objects and initialize statistics entry for each day
  for (let i = 0; i < totalNumberOfDays; i++) {
    arrayOfDates.push(minStartDate.add(i, "day"));
  }

  // Initialize dates for each date object
  return arrayOfDates.map((dateToDisplay) => ({
    dayjsDate: dateToDisplay,
    dateShort: getDateShort(dateToDisplay),
  }));
};
