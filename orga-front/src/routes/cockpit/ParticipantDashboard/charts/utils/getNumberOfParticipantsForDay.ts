/**
 * Retrieves the number of participants for a given date from meal and frequency statistics.
 *
 * @param dateShort - The date in short format (using the getDateShort() function).
 * @param formAnswerStats - The meal and frequency statistics as an array of objects.
 * @returns The number of participants for the given date. Returns 0 if no statistics are found for the date.
 */
export const getNumberOfParticipantsForDay = (
  dateShort: string,
  formAnswerStats: Array<{dateShort: string; participantsOnSite: number}>
): number => {
  const stat = formAnswerStats.find((stat) => stat.dateShort === dateShort);
  return stat ? stat.participantsOnSite : 0;
};
