import {getDateShort} from "./initializeGraphDates";

/**
 * Calculates stats for a specific day based on a registration object with the calculate function.
 *
 * @param {Object} registration - The registration object containing meals, tags, and specific keys information.
 * @param {Array<Object>} stats - The array containing meals and frequency statistics objects.
 * @param calculate function to compute stuff on dates of presence
 */
export const calculateStatsForEachDay = (
  registration,
  stats,
  calculate: (statForDay, dayOfPresence) => void
) => {
  // Iterate through each day of presence in the registration object
  registration.daysOfPresence.forEach((dayOfPresence, index) => {
    // Extract the current day from the day of presence information
    const currentDayShort = getDateShort(dayOfPresence.start);

    // Find the corresponding statistics object for the current day
    const statForDay = stats.find((st) => st.dateShort === currentDayShort);

    if (statForDay) {
      // Calculate stuff
      calculate(statForDay, dayOfPresence);
    }
  });
};
