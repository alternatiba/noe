import {Tag} from "antd";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {CardElement} from "@shared/components/CardElement";
import {SelectInput} from "@shared/inputs/SelectInput";
import {projectsActions, projectsSelectors} from "@features/projects";
import {PerDayFormAnswerPlot} from "./PerDayFormAnswerPlot";
import {AbsoluteFormAnswerPlot} from "./AbsoluteFormAnswerPlot";
import {FieldComp} from "@shared/components/FieldsBuilder/FieldComp/FieldComp";

const DISPLAY_MODE_OPTIONS = ["perDay", "absolute"];

const FormAnswerChart = ({
  field,
  displayMode,
}: {
  field: string,
  displayMode: "perDay" | "absolute",
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const flatFormComponents = useSelector(projectsSelectors.selectFlatFormComponents);
  const formComp: FieldComp = flatFormComponents.find((comp) => comp.key === field);

  const cockpitPreferences = useSelector(
    (s) => projectsSelectors.selectEditing(s).cockpitPreferences
  );

  const handleDisplayModeChange = (value) => {
    const updatedFormAnswers = cockpitPreferences.charts.answers.map((answer) =>
      answer.key === field ? {...answer, displayMode: value} : answer
    );

    dispatch(
      projectsActions.persist({
        cockpitPreferences: {
          ...cockpitPreferences,
          charts: {...cockpitPreferences.charts, answers: updatedFormAnswers},
        },
      })
    );
  };

  return formComp ? (
    <CardElement
      icon={
        <Tag style={{background: "var(--noe-bg)", color: "white"}} bordered={false}>
          {t("cockpit:formAnswerChart.formAnswer")}
        </Tag>
      }
      customButtons={
        <SelectInput
          defaultValue={displayMode}
          onChange={handleDisplayModeChange}
          options={DISPLAY_MODE_OPTIONS.map((displayMode) => ({
            label: t(`cockpit:formAnswerChart.${displayMode}`),
            value: displayMode,
          }))}
        />
      }
      title={formComp.label}>
      {displayMode === "absolute" && <AbsoluteFormAnswerPlot formComp={formComp} />}
      {displayMode === "perDay" && <PerDayFormAnswerPlot formComp={formComp} />}
    </CardElement>
  ) : null;
};

export default FormAnswerChart;
