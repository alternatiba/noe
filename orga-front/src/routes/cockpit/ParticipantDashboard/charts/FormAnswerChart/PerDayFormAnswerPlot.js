import BaseFormAnswerPlot from "./BaseFormAnswerPlot";
import {useMemo} from "react";
import {Pending} from "@shared/components/Pending";
import {addKeyToItemsOfList} from "@shared/utils/tableUtilities";
import {useDayStatColumn} from "../../../atoms/useDayStatColumn";
import {removeDuplicates} from "@shared/utils/utilities";
import {FieldComp} from "@shared/components/FieldsBuilder/FieldComp/FieldComp";
import {useFormAnswerStatsByDaysOfPresence} from "../utils/useFormAnswerStatsByDaysOfPresence";
import {useGetOptionLabel} from "../utils/useGetOptionLabel";

export const PerDayFormAnswerPlot = ({formComp}: {formComp: FieldComp}) => {
  const getOptionLabel = useGetOptionLabel();
  const field = formComp.key;
  const formAnswerStats = useFormAnswerStatsByDaysOfPresence(field) || {};

  const plottedStats = useMemo(
    () =>
      formAnswerStats.reduce((acc, stat) => {
        const statsArray = Object.entries(stat[field]).map(([name, count]) => ({
          dateShort: stat.dateShort,
          count,
          name: getOptionLabel(name, formComp),
          participantsOnSite: stat.participantsOnSite,
        }));
        return [...acc, ...statsArray];
      }, []),
    [formAnswerStats, field, formComp]
  );

  const uniqueAnswerKeys = useMemo(
    () =>
      removeDuplicates(
        formAnswerStats.reduce((acc, stat) => [...acc, ...Object.keys(stat[field])], [])
      ),
    [formComp, field]
  );

  const columns = [
    useDayStatColumn(),
    ...uniqueAnswerKeys.map((key) => ({
      dataIndex: key,
      title: getOptionLabel(key, formComp),
    })),
  ];

  if (!formAnswerStats) {
    return <Pending />;
  }

  return (
    <BaseFormAnswerPlot
      data={plottedStats}
      columns={columns}
      answerLabel={formComp.label}
      displayMode="perDay"
      daysOfPresenceStats={formAnswerStats}
      dataSourceTable={addKeyToItemsOfList(formAnswerStats)}
    />
  );
};
