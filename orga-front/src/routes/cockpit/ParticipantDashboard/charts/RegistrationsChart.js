import React from "react";
import {CardElement} from "@shared/components/CardElement";
import {Pending, PendingSuspense} from "@shared/components/Pending";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {addKeyToItemsOfList} from "@shared/utils/tableUtilities";
import RawDataCollapsibleTable from "./utils/RawDataCollapsibleTable";
import {useRegistrationsStats} from "./utils/useRegistrationsStats";
import {useTranslation} from "react-i18next";
import {sorter} from "@shared/utils/sorters";
import {Stack} from "@shared/layout/Stack";
import {useDayStatColumn} from "../../atoms/useDayStatColumn";

const DualAxesChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/charts/DualAxesChart")
);

export default function RegistrationsChart() {
  const {t} = useTranslation();
  const columnsRegistrationsStats = [
    useDayStatColumn(),
    {
      title: t("cockpit:registrationStats.newRegistrations"),
      dataIndex: "newRegistrations",
      sorter: (a, b) => sorter.number(a.newRegistrations, b.newRegistrations),
    },
    {
      title: t("cockpit:registrationStats.completedRegistrations"),
      dataIndex: "completedRegistrations",
      sorter: (a, b) => sorter.number(a.completedRegistrations, b.completedRegistrations),
    },
    {
      title: t("cockpit:registrationStats.newRegistrationsTotal"),
      dataIndex: "newRegistrationsTotal",
      sorter: (a, b) => sorter.number(a.newRegistrationsTotal, b.newRegistrationsTotal),
    },
    {
      title: t("cockpit:registrationStats.completedRegistrationsTotal"),
      dataIndex: "completedRegistrationsTotal",
      sorter: (a, b) => sorter.number(a.completedRegistrationsTotal, b.completedRegistrationsTotal),
    },
  ];

  const registrationsStats = useRegistrationsStats();

  if (!registrationsStats) return <Pending />;

  // Create the stats object for the Ant design plot
  const plottedRegistrationsStats = registrationsStats.reduce(
    (acc, stat) => [
      ...acc,
      {
        dateShort: stat.dateShort,
        count: stat.newRegistrations,
        name: t("cockpit:registrationStats.newRegistrations"),
      },
      {
        dateShort: stat.dateShort,
        count: stat.completedRegistrations,
        name: t("cockpit:registrationStats.completedRegistrations"),
      },
    ],
    []
  );

  const plottedRegistrationsTotalStats = registrationsStats.reduce(
    (acc, stat) => [
      ...acc,
      {
        dateShort: stat.dateShort,
        count: stat.newRegistrationsTotal,
        name: t("cockpit:registrationStats.newRegistrationsTotal"),
      },
      {
        dateShort: stat.dateShort,
        count: stat.completedRegistrationsTotal,
        name: t("cockpit:registrationStats.completedRegistrationsTotal"),
      },
    ],
    []
  );

  return (
    <>
      <CardElement title={t("cockpit:registrationStats.label")}>
        <Stack gap={2}>
          <PendingSuspense>
            <DualAxesChart
              config={{
                data: [plottedRegistrationsStats, plottedRegistrationsTotalStats],
                height: 300,
                xField: "dateShort",
                yField: ["count", "count"],
                geometryOptions: [
                  {geometry: "line", smooth: true, seriesField: "name"},
                  {geometry: "column", color: ["#B0ADFF", "#B5FFF6"], seriesField: "name"},
                ],
              }}
            />
          </PendingSuspense>
          <RawDataCollapsibleTable
            exportTitle={t("cockpit:registrationStats.label")}
            columns={columnsRegistrationsStats}
            dataSource={addKeyToItemsOfList(registrationsStats)}
          />
        </Stack>
      </CardElement>
    </>
  );
}
