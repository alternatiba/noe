import {CardElement} from "@shared/components/CardElement";
import {Pending, PendingSuspense} from "@shared/components/Pending";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {addKeyToItemsOfList} from "@shared/utils/tableUtilities";
import {removeDuplicates} from "@shared/utils/utilities";
import RawDataCollapsibleTable from "./utils/RawDataCollapsibleTable";
import {useTranslation} from "react-i18next";
import {useRegistrationsStatsByDaysOfPresence} from "./utils/useRegistrationsStatsByDaysOfPresence";
import {Stack} from "@shared/layout/Stack";
import {useDayStatColumn} from "../../atoms/useDayStatColumn";

const DualAxesChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/charts/DualAxesChart")
);

export default function TagsPerDayChart() {
  const {t} = useTranslation();
  const mealsAndFreqStats = useRegistrationsStatsByDaysOfPresence();
  const dayStatColumn = useDayStatColumn();

  if (!mealsAndFreqStats) return <Pending />;

  const columnsTagsStats = [
    dayStatColumn,
    ...removeDuplicates(
      mealsAndFreqStats.reduce((acc, stat) => [...acc, ...Object.keys(stat.tags)], [])
    ).map((tag) => ({dataIndex: ["tags", tag], title: tag})),
  ];

  // Create the stats objects for the Ant design plot
  const plottedFreqStats = mealsAndFreqStats.map((stat) => ({
    dateShort: stat.dateShort,
    participantsOnSite: stat.participantsOnSite,
    name: t("cockpit:presenceAndMealsStats.participantsOnSite"),
  }));

  // Create the stats objects for the Ant design plot
  const plottedTagsStats = mealsAndFreqStats.reduce(
    (acc, stat) => [
      ...acc,
      ...Object.entries(stat.tags).map(([tagName, count]) => ({
        dateShort: stat.dateShort,
        count,
        name: tagName,
      })),
    ],
    []
  );

  return (
    <>
      <CardElement title={t("cockpit:tagsStats.label")}>
        <Stack gap={2}>
          <PendingSuspense>
            <DualAxesChart
              config={{
                data: [plottedFreqStats, plottedTagsStats],
                height: 300,
                xField: "dateShort",
                yField: ["participantsOnSite", "count"],
                yAxis: [{min: 0}, {min: 0}],
                geometryOptions: [
                  {geometry: "column", seriesField: "name"},
                  {geometry: "line", smooth: true, seriesField: "name"},
                ],
              }}
            />
          </PendingSuspense>
          <RawDataCollapsibleTable
            exportTitle={t("cockpit:tagsStats.label")}
            columns={columnsTagsStats}
            dataSource={addKeyToItemsOfList(mealsAndFreqStats)}
          />
        </Stack>
      </CardElement>
    </>
  );
}
