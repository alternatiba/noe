import {SettingOutlined, CheckOutlined, UndoOutlined} from "@ant-design/icons";
import {Button, Form} from "antd";
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Drawer} from "@shared/components/Drawer";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {FormElement} from "@shared/inputs/FormElement";
import {projectsActions, projectsSelectors} from "@features/projects";
import ChartSelectionInputs from "./ParticipantDashboard/ChartSelectionInputs";
import FiguresSelect from "./ParticipantDashboard/FiguresSelect";
import {useTranslation} from "react-i18next";

// Same as in backend project.model.ts
export const DEFAULT_COCKPIT_PREFERENCES = {
  figures: ["notes", "registrations", "volunteering", "schedulingAndSupervision"],
  charts: {
    useful: [
      "presenceAndMealsStats",
      "numberOfParticipantsByHourStats",
      "tagsStats",
      "registrationStats",
    ],
    answers: [],
  },
};

/**
 * CockpitConfigDrawer component manages the configuration drawer for the chart view.
 * It allows users to select specific charts to be displayed.
 *
 * @returns {ReactNode} The configuration drawer component for the chart view.
 */
const CockpitConfigDrawer = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {isMobileView} = useWindowDimensions();
  const [cockpitPreferencesForm] = Form.useForm();
  const cockpitPreferences = useSelector(
    (s) => projectsSelectors.selectEditing(s).cockpitPreferences
  );
  const [settingsDrawerOpen, setSettingsDrawerOpen] = useState(false);

  /**
   * Handles saving the selected configuration and updates the Redux store.
   */
  const saveCockpitConfig = () => {
    const fieldsValues = cockpitPreferencesForm.getFieldsValue();

    // GO find the existing answer config if it already set, otherwise initialize it with the key
    const savedAnswers = fieldsValues?.charts?.answers?.map(
      (answerKey) =>
        cockpitPreferences.charts?.answers?.find((a) => a.key === answerKey) || {
          key: answerKey,
          displayMode: "perDay",
        }
    );

    dispatch(
      projectsActions.persist({
        cockpitPreferences: {
          ...fieldsValues,
          charts: {...fieldsValues.charts, answers: savedAnswers},
        },
      })
    );

    setSettingsDrawerOpen(false);
  };

  /**
   * Reset cockpit
   */
  const resetCockpitConfig = () => {
    dispatch(projectsActions.persist({cockpitPreferences: DEFAULT_COCKPIT_PREFERENCES}));
    setSettingsDrawerOpen(false);
  };

  return (
    <>
      <Button icon={<SettingOutlined />} onClick={() => setSettingsDrawerOpen(true)}>
        {!isMobileView && t("cockpit:config.customize")}
      </Button>

      <Drawer
        title={t("cockpit:config.customizeCockpit")}
        width={isMobileView ? undefined : "700px"}
        open={settingsDrawerOpen}
        setOpen={setSettingsDrawerOpen}
        extra={
          <Button onClick={saveCockpitConfig} type="primary" icon={<CheckOutlined />}>
            {t("common:editPage.buttonTitle.edit")}
          </Button>
        }>
        <div style={{padding: 16}}>
          <p style={{color: "gray", marginBottom: 26}}>{t("cockpit:config.helpMessage")}</p>
          <FormElement
            form={cockpitPreferencesForm}
            onValidate={saveCockpitConfig}
            initialValues={cockpitPreferences}>
            <FiguresSelect />
            <ChartSelectionInputs />
            <Button onClick={resetCockpitConfig} icon={<UndoOutlined />}>
              {t("cockpit:config.reset")}
            </Button>
          </FormElement>
        </div>
      </Drawer>
    </>
  );
};

export default CockpitConfigDrawer;
