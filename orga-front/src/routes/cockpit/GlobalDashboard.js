import React from "react";
import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {CardElement} from "@shared/components/CardElement";
import {projectsSelectors} from "@features/projects";
import RegistrationsInsightsCard from "./globalDashboard/RegistrationsInsights";
import {SchedulingInsights} from "./globalDashboard/SchedulingInsights";
import {VolunteeringInsights} from "./globalDashboard/VolunteeringInsights";
import {ProjectNotesEditor} from "./ProjectNotesEditor";

/**
 * Functional component for Global Dashboard.
 * @param {Object} props - Component props.
 * @param {Function} props.setActiveTab - Function to set active tab.
 * @returns {ReactNode} - JSX for Global Dashboard.
 */
function GlobalDashboard({setActiveTab}) {
  const {t} = useTranslation();
  const cockpitPreferencesFigures = useSelector(
    (s) => projectsSelectors.selectEditing(s).cockpitPreferences?.figures
  );

  const availableWidgets = {
    notes: <ProjectNotesEditor />,
    registrations: <RegistrationsInsightsCard setActiveTab={setActiveTab} />,
    volunteering: (
      <CardElement className="fade-in" title={t("cockpit:volunteering.label")}>
        <VolunteeringInsights />
      </CardElement>
    ),
    schedulingAndSupervision: (
      <CardElement className="fade-in" title={t("cockpit:schedulingAndSupervision.label")}>
        <div className="container-grid three-per-row">
          <SchedulingInsights />
        </div>
      </CardElement>
    ),
  };

  return (
    <div className="container-grid three-per-row">
      {cockpitPreferencesFigures?.map((widgetName) => availableWidgets[widgetName])}
    </div>
  );
}

export default GlobalDashboard;
