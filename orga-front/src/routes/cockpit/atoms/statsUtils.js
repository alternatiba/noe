import groupBy from "lodash/groupBy";
import {useSelector} from "react-redux";
import {registrationsSelectors} from "@features/registrations";
import {useMemo} from "react";
import {useTranslation} from "react-i18next";
import {getVolunteeringCoefficient} from "@shared/utils/sessionsUtilities";
import {sessionsSelectors} from "@features/sessions";
import {activitiesSelectors} from "@features/activities";

export const useRegistrationsWithDaysOfPresence = () =>
  useSelector(registrationsSelectors.selectListWithMetadata).filter(
    (registration) => registration.availabilitySlots?.length > 0
  );

export const useAverageNumberOfDaysOfPresence = () => {
  const registrationsWithDaysOfPresence = useRegistrationsWithDaysOfPresence();

  return useMemo(() => {
    return registrationsWithDaysOfPresence?.length > 0
      ? registrationsWithDaysOfPresence
          .map((el) => el.numberOfDaysOfPresence)
          .reduce((acc, el) => acc + el, 0) / registrationsWithDaysOfPresence.length
      : null;
  }, [registrationsWithDaysOfPresence]);
};

export const useVolunteeringData = () => {
  const sessions = useSelector(sessionsSelectors.selectList);
  const activities = useSelector(activitiesSelectors.selectList);

  return useMemo(() => {
    const volunteeringSessionsWithMetadata = sessions
      .map((el) => ({
        ...el,
        duration: el.slots.reduce((acc, slot) => acc + slot.duration, 0),
        volunteeringCoefficient: getVolunteeringCoefficient(el, activities),
      }))
      .filter((el) => el.volunteeringCoefficient > 0);

    const totalAmountOfVolunteering = volunteeringSessionsWithMetadata.reduce(
      (acc, el) => acc + el.duration * el.computedMaxNumberOfParticipants,
      0
    );
    return {volunteeringSessionsWithMetadata, totalAmountOfVolunteering};
  }, [activities, sessions]);
};

export const useRegistrationsGroupedByTypeAndRole = () => {
  const {t} = useTranslation();
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);

  return useMemo(() => {
    return Object.entries(
      groupBy(registrations, (el) =>
        el.invitationToken
          ? t("cockpit:registrations.invitations")
          : !el.everythingIsOk
          ? el.booked === false
            ? t("cockpit:registrations.unregistrations")
            : t("cockpit:registrations.incomplete")
          : t("cockpit:registrations.complete")
      )
    ).map(([name, value]) => ({
      name,
      value: value.length,
      children: Object.entries(
        groupBy(value, (el) =>
          el.role ? t("cockpit:registrations.orga") : t("cockpit:registrations.nonOrga")
        )
      ).map(([name, value]) => ({name, value: value.length, children: null})),
    }));
  }, [registrations, t]);
};
