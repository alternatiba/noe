import React, {useState} from "react";
import {ShareAltOutlined} from "@ant-design/icons";
import Paragraph from "antd/es/typography/Paragraph";
import {Button, Select} from "antd";
import {useSelector} from "react-redux";
import {useTranslation} from "react-i18next";
import {projectsSelectors} from "@features/projects";
import {URLS} from "@app/configuration";

export default function ShareEventButton() {
  const {t} = useTranslation();
  const project = useSelector(projectsSelectors.selectEditing);
  const [shareLinkEndPoint, setShareLinkEndPoint] = useState("welcome");
  const [displayShareLink, setDisplayShareLink] = useState(false);
  return displayShareLink ? (
    <div className="containerH buttons-container">
      <Select value={shareLinkEndPoint} onChange={setShareLinkEndPoint}>
        <Select.Option value="welcome">
          {t("cockpit:shareEventButton.options.welcome")}
        </Select.Option>
        <Select.Option value="login">{t("cockpit:shareEventButton.options.login")}</Select.Option>
      </Select>
      <Paragraph copyable style={{fontFamily: "monospace"}}>
        {`${URLS.INSCRIPTION_FRONT}/${project.slug || project._id}/${shareLinkEndPoint}`}
      </Paragraph>
    </div>
  ) : (
    <Button onClick={() => setDisplayShareLink(true)} icon={<ShareAltOutlined />} type="link">
      {t("cockpit:shareEventButton.label")}
    </Button>
  );
}
