import {getVolunteeringCoefficient} from "@shared/utils/sessionsUtilities";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {useTranslation} from "react-i18next";
import {
  useAverageNumberOfDaysOfPresence,
  useRegistrationsGroupedByTypeAndRole,
} from "../atoms/statsUtils";
import {InsightCard} from "../atoms/InsightCard";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {activitiesActions, activitiesSelectors} from "@features/activities";
import {PendingSuspense} from "@shared/components/Pending";
import {CardElement} from "@shared/components/CardElement";
import {Link} from "react-router-dom";
import {AreaChartOutlined} from "@ant-design/icons";
import {useLoadList} from "@shared/hooks/useLoadList";
import {clickOnTab} from "@shared/utils/userTourUtilities";

const SunburstChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/charts/SunburstChart")
);

const RegistrationsInsights = React.memo(
  () => {
    const {t} = useTranslation();
    const registrations = useSelector(registrationsSelectors.selectListWithMetadata);
    const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
    const sessions = useSelector(sessionsSelectors.selectList);
    const activities = useSelector(activitiesSelectors.selectList);

    const currentUserAloneInTheProject =
      registrations.filter((el) => el._id !== currentRegistration._id).length === 0;

    const averageNumberOfDaysOfPresence = useAverageNumberOfDaysOfPresence();

    const registrationsGroupedByTypeAndRole = useRegistrationsGroupedByTypeAndRole();

    const registrationsInsights = [
      {
        name: t("cockpit:registrations.label"),
        isPlot: true,
        calculateOnAll: () => ({
          name: t("cockpit:registrations.label"),
          children: registrationsGroupedByTypeAndRole,
        }),
        noAvailableData: currentUserAloneInTheProject,
        formatter: (data) => (
          <PendingSuspense>
            <SunburstChart
              config={{
                data,
                innerRadius: 0.3,
                label: {formatter: ({value}) => value, autoRotate: false},
                color: (item) => {
                  switch (item["ancestor-node"]) {
                    case t("cockpit:registrations.complete"):
                      return "green";
                    case t("cockpit:registrations.incomplete"):
                      return "red";
                    case t("cockpit:registrations.unregistrations"):
                      return "darkred";
                    case t("cockpit:registrations.invitations"):
                      return "#86c4ff";
                    case t("cockpit:registrations.orga"):
                      return "grey";
                    case t("cockpit:registrations.nonOrga"):
                      return "darkgrey";
                    default:
                      return "grey";
                  }
                },
              }}
            />
          </PendingSuspense>
        ),
        precisions: [
          {
            name: t("cockpit:registrations.complete"),
            endpoint: "participants",
            noAvailableData: currentUserAloneInTheProject,
            calculateOnAll: () =>
              registrationsGroupedByTypeAndRole.find(
                ({name}) => name === t("cockpit:registrations.complete")
              )?.value,
          },
          {
            name: t("cockpit:registrations.incomplete"),
            endpoint: "participants",
            noAvailableData: currentUserAloneInTheProject,
            calculateOnAll: () =>
              registrationsGroupedByTypeAndRole.find(
                ({name}) => name === t("cockpit:registrations.incomplete")
              )?.value,
          },
          {
            name: `${t("cockpit:registrations.complete")} + ${t(
              "cockpit:registrations.incomplete"
            )}`,
            endpoint: "participants",
            elements: registrations,
            filter: (el) => el.booked !== false && !el.invitationToken, // Remove also people who unregistered + with invitation token
            noAvailableData: currentUserAloneInTheProject,
            statType: "length",
          },
        ],
      },
      {
        name: t("cockpit:registrations.averageLengthOfStayOnSite"),
        endpoint: "participants",
        noAvailableData: averageNumberOfDaysOfPresence === null,
        calculateOnAll: () => averageNumberOfDaysOfPresence,
        suffix: t("cockpit:days"),
      },
      {
        name: t("cockpit:registrations.averageSessionFillingWithoutVolunteering"),
        endpoint: "sessions",
        elements: sessions,
        filter: (el) =>
          el.computedMaxNumberOfParticipants > 0 &&
          getVolunteeringCoefficient(el, activities) === 0,
        calculateOnEach: (el) => (el.numberParticipants / el.computedMaxNumberOfParticipants) * 100,
        statType: "average",
        suffix: "%",
      },
    ];

    return registrationsInsights?.map((insight, index) => <InsightCard key={index} {...insight} />);
  },
  (pp, np) => true
);

const RegistrationsInsightsCard = ({setActiveTab}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  useLoadList(() => {
    Promise.all([
      dispatch(sessionsActions.loadList()),
      dispatch(activitiesActions.loadList()),
      dispatch(registrationsActions.loadList()),
    ]);
  });

  return (
    <CardElement
      className="fade-in"
      title={t("cockpit:registrations.label")}
      subtitle={
        <p>
          <Link
            to="#attendance-data"
            onClick={(e) => {
              e.preventDefault(); // Do not navigate, only add the #hash it's enough
              clickOnTab("attendance-data");
            }}>
            <AreaChartOutlined style={{marginRight: 8}} />
            {t("cockpit:registrations.seeMoreFrequentationData")}
          </Link>
        </p>
      }>
      <RegistrationsInsights />
    </CardElement>
  );
};

export default RegistrationsInsightsCard;
