import React from "react";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {projectsSelectors} from "@features/projects";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {InsightCard} from "../atoms/InsightCard";
import {
  useAverageNumberOfDaysOfPresence,
  useRegistrationsWithDaysOfPresence,
  useVolunteeringData,
} from "../atoms/statsUtils";
import {useLoadList} from "@shared/hooks/useLoadList";
import {sessionsActions} from "@features/sessions";

import {timeFormatter} from "@shared/utils/formatters";

export const VolunteeringInsights = () => {
  const {t} = useTranslation();

  const dispatch = useDispatch();

  useLoadList(() => {
    Promise.all([dispatch(sessionsActions.loadList()), dispatch(registrationsActions.loadList())]);
  });

  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);
  const projectMinMaxVolunteering = useSelector(
    (state) => projectsSelectors.selectEditing(state).minMaxVolunteering
  );

  const registrationsWithDaysOfPresence = useRegistrationsWithDaysOfPresence();
  const averageNumberOfDaysOfPresence = useAverageNumberOfDaysOfPresence();

  const {totalAmountOfVolunteering, volunteeringSessionsWithMetadata} = useVolunteeringData();

  const volunteeringInsights = [
    {
      name: t("cockpit:volunteering.averageVolunteeringSessionsFilling"),
      endpoint: "sessions",
      elements: volunteeringSessionsWithMetadata,
      filter: (el) => el.computedMaxNumberOfParticipants > 0, // only sessions with at least one participant allowed
      calculateOnEach: (el) => (el.numberParticipants / el.computedMaxNumberOfParticipants) * 100,
      statType: "average",
      suffix: "%",
      isGood: (val) => (val > 90 ? 1 : -1),
    },
    {
      name: t("cockpit:volunteering.amountToFillInTheEvent"),
      endpoint: "sessions",
      noAvailableData: volunteeringSessionsWithMetadata.length === 0,
      calculateOnAll: () => totalAmountOfVolunteering,
      formatter: (val) => timeFormatter.duration(val, {short: true}),
      precisions: [
        {
          name: t("cockpit:volunteering.alreadyFilled"),
          elements: volunteeringSessionsWithMetadata,
          noAvailableData: volunteeringSessionsWithMetadata.length === 0,
          calculateOnEach: (el) => el.duration * el.numberParticipants,
          statType: "sum",
          formatter: (val) => timeFormatter.duration(val, {short: true}),
        },
        {
          name: `${t("cockpit:volunteering.alreadyFilled")} (%)`,
          elements: volunteeringSessionsWithMetadata,
          noAvailableData: volunteeringSessionsWithMetadata.length === 0,

          calculateOnEach: (el) => el.duration * el.numberParticipants,
          statType: "sum",
          formatter: (val) => Math.round((val / totalAmountOfVolunteering) * 100),
          suffix: "%",
        },
      ],
    },
    {
      name: t("cockpit:volunteering.averageVolunteeringAmountPerParticipant"),
      endpoint: "participants",
      elements: registrations,
      noAvailableData: registrations.filter((el) => el.everythingIsOk).length === 0,
      filter: (el) => el.everythingIsOk,
      calculateOnEach: (el) => el.voluntaryCounter,
      statType: "average",
      formatter: (val) => timeFormatter.duration(val, {short: true}),
      isGood: (val) =>
        val <= projectMinMaxVolunteering[0]
          ? -1 // Red if below
          : val >= projectMinMaxVolunteering[1]
          ? 0 // Neutral if above
          : 1, // Green if inside
      suffix: "/ " + t("cockpit:day"),
    },
    {
      name: t("cockpit:volunteering.optimalVolunteeringAmountPerParticipant"),
      endpoint: "participants",
      noAvailableData:
        volunteeringSessionsWithMetadata.length === 0 ||
        registrationsWithDaysOfPresence.length === 0 ||
        averageNumberOfDaysOfPresence === null,
      calculateOnAll: () => {
        return (
          totalAmountOfVolunteering /
          (registrationsWithDaysOfPresence.length * averageNumberOfDaysOfPresence)
        );
      },
      formatter: (val) => timeFormatter.duration(val, {short: true}),
      suffix: "/ " + t("cockpit:day"),
    },
  ];

  return volunteeringInsights?.map((insight, index) => <InsightCard key={index} {...insight} />);
};
