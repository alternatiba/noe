import {useDispatch, useSelector} from "react-redux";
import {Result} from "antd";
import {projectsActions, projectsSelectors} from "@features/projects";
import {EditPage} from "@shared/pages/EditPage";
import {CardElement} from "@shared/components/CardElement";
import {AdditionalFeatures} from "../config/advanced/FeaturesFormContent";
import {TextInput, TextInputEmail} from "@shared/inputs/TextInput";
import {useLoadEditing} from "@shared/hooks/useLoadEditing";
import {useNavigate} from "react-router-dom";
import {FormAvailabilitySlots} from "@shared/inputs/FormAvailabilitySlots";
import {useTranslation} from "react-i18next";
import {SvgImageContainer} from "@shared/components/SvgImageContainer";
import {ReactComponent as SvgPartying} from "@images/undraw/undraw_partying.svg";
import {currentUserSelectors} from "@features/currentUser";
import {SelectInput} from "@shared/inputs/SelectInput";
import {ProjectStatusTag, useProjectStatusesMapping} from "@shared/components/ProjectStatusTag";

function ProjectNew() {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const setIsModified = EditPage.useSetIsModified({resetOnUnmount: true});

  useLoadEditing(projectsActions, "new");

  const toggle = (key) => {
    setIsModified(true);
    dispatch(projectsActions.changeEditing({[key]: !project[key]}));
  };

  const projectStatusesMapping = useProjectStatusesMapping();
  const projectStatusesOptions = Object.keys(projectStatusesMapping).map((value) => ({
    label: <ProjectStatusTag status={value} />,
    value,
  }));

  return (
    <EditPage
      i18nNs="projects"
      record={project}
      initialValues={{
        ...project,
        status: currentUser.superAdmin ? "pendingDonation" : "demo",
      }}
      elementsActions={projectsActions}
      goBackAfterAction={false}
      onCreate={(projectSlug) => navigate(`/${projectSlug}/config`)}
      createAndStayButton={false}>
      <div className="container-grid two-per-row">
        <div className="container-grid">
          <CardElement>
            <div className="container-grid">
              <TextInput i18nNs={"projects"} name="name" required />

              <TextInputEmail i18nNs={"projects"} name="contactEmail" required />

              {currentUser.superAdmin && (
                <SelectInput
                  i18nNs={"projects"}
                  name="status"
                  options={projectStatusesOptions}
                  required
                />
              )}
            </div>
          </CardElement>

          <FormAvailabilitySlots
            label={t("projects:schema.availabilitySlots.label")}
            name={"availabilitySlots"}
            subtitle={
              "Ce sont les heures d'ouverture de votre événement. Vous pourrez revenir les modifier dans la page Configuration de votre événement."
            }
          />

          <AdditionalFeatures
            noTeams
            subtitle={
              <div style={{marginTop: -14, marginBottom: 26}}>
                Vous pouvez configurer NOÉ selon vos besoins, en activant ou en désactivant
                certaines fonctionnalités de la plateforme. Retrouvez ces contrôles dans les onglets
                "Avancé" > "Fonctionnalités" de la page Configuration.
              </div>
            }
            project={project}
            toggle={toggle}
            noWarning
          />
        </div>

        <Result
          style={{marginTop: "10vh"}}
          status="success"
          icon={<SvgImageContainer svg={SvgPartying} width={"70%"} />}
        />
      </div>
    </EditPage>
  );
}

export default ProjectNew;
