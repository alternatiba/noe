import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "@features/projects";
import {ListPage} from "@shared/pages/ListPage";
import {currentProjectActions, currentProjectSelectors} from "@features/currentProject";
import {
  CalendarOutlined,
  PlaySquareOutlined,
  QuestionCircleOutlined,
  SettingOutlined,
  PlusOutlined,
  EyeInvisibleOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import {Button, Modal, Checkbox, App} from "antd";
import {usersSelectors} from "@features/users";
import {registrationsActions} from "@features/registrations";
import {currentUserActions} from "@features/currentUser";
import {URLS} from "@app/configuration";
import {useTranslation} from "react-i18next";
import {BookmarkedProjectButton} from "@shared/components/buttons/BookmarkedProjectButton";
import {RoleTag} from "@shared/components/RoleTag";
import {useNavigate} from "react-router-dom";
import {sorter} from "@shared/utils/sorters";
import {useColumnsBlacklistingSelect} from "@shared/hooks/useColumnsBlacklistingSelect";
import {editableCellColumn} from "@shared/components/EditableCell";
import {dateFormatter} from "@shared/utils/formatters";
import {NOEHelpGuide, NOEUsefulLinksGuide} from "@shared/components/NOEHelpGuide";
import {ToggleTag} from "@shared/components/ToggleElement";
import {truncate} from "@shared/utils/stringUtilities";
import dayjs from "@shared/services/dayjs";
import {ProjectStatusTag, useProjectStatusesMapping} from "@shared/components/ProjectStatusTag";
export default function ProjectList() {
  const {message} = App.useApp();
  const navigate = useNavigate();
  const {t, i18n} = useTranslation();
  const dispatch = useDispatch();
  const projects = useSelector(projectsSelectors.selectList);
  const alreadyLoadedProject = useSelector(currentProjectSelectors.selectProject);
  const currentUser = useSelector(usersSelectors.selectEditing);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "projects",
  });
  const [showHelpModal, setShowHelpModal] = useState(false);
  const projectsAreLoaded = useSelector((s) =>
    projectsSelectors?.selectIsLoaded ? projectsSelectors.selectIsLoaded(s) : true
  );
  const noProjectsCreatedYet = projectsAreLoaded && projects.length === 0;

  const [showDemoProjects, setShowDemoProjects] = useState(false);

  useEffect(() => {
    dispatch(projectsActions.loadList());
  }, []);

  const projectsWithRegistrationRoles = projects.map((project) => ({
    ...project,
    registrationRole: currentUser.registrations.find((r) => r.project === project._id)?.role,
  }));

  const filteredProjects = projectsWithRegistrationRoles.filter((project) => {
    return project.status !== "demo" || showDemoProjects;
  });

  const projectStatusesMapping = useProjectStatusesMapping();
  const projectStatusesOptions = Object.keys(projectStatusesMapping).map((value) => ({
    label: <ProjectStatusTag status={value} />,
    value,
  }));

  const columns = [
    {
      title: t("common:schema.name.label"),
      dataIndex: "name",
      sorter: (a, b) => sorter.text(a.name, b.name),
      render: (text, record) => (
        <>
          {!currentUser.superAdmin && record.status === "demo" && (
            <ProjectStatusTag status="demo" />
          )}
          {text}
        </>
      ),
      searchable: true,
    },
    {
      title: "Accès interfaces",
      dataIndex: "access",
      render: (text, record) => (
        <>
          <Button
            icon={<SettingOutlined />}
            type="link"
            style={{marginRight: 8}}
            onClick={() => cleanStateAndNavigate(record.slug || record._id)}>
            Orga
          </Button>
          <Button
            type="link"
            href={`${URLS.INSCRIPTION_FRONT}/${record._id}`}
            icon={<PlaySquareOutlined />}>
            Participant⋅e
          </Button>
        </>
      ),
    },
    {
      title: t("common:availability.startEnd"),
      dataIndex: "start",
      sorter: (a, b) => sorter.date(a.start, b.start),
      width: i18n.language === "fr" ? 230 : 255,
      render: (text, record) =>
        dateFormatter.longDateRange(record.start, record.end, {short: true, withYear: true}),
      searchable: true,
      searchText: (record) =>
        dateFormatter.longDateRange(record.start, record.end, {short: true, withYear: true}),
    },
    {
      title: "Rôle",
      dataIndex: "role",
      render: (text, record) =>
        record.registrationRole ? (
          <RoleTag roleValue={record.registrationRole} />
        ) : (
          <Button onClick={() => onRequireAdmin(record)}>Devenir administrateur⋅ice</Button>
        ),
      sorter: (a, b) => sorter.text(a.registrationRole, b.registrationRole),
    },
    {
      title: "Stats",
      dataIndex: "stats",
      render: (_, {stats}) =>
        stats && (
          <div
            title={`${stats.participants} participant·es + ${stats.orgas} orgas, et ${stats.sessions} sessions`}>
            {stats.participants} px.{" "}
            <span style={{color: "grey"}}>
              | {stats.orgas} o. | {stats.sessions} s.
            </span>
          </div>
        ),
      ellipsis: true,
      sorter: (a, b) => sorter.number(a.stats?.participants, b.stats?.participants),
      width: 180,
    },
    currentUser.superAdmin && {
      ...editableCellColumn(
        {
          title: t("projects:schema.status.label"),
          dataIndex: "status",
          type: "select",
          options: projectStatusesOptions,
          placeholder: t("projects:schema.status.placeholder"),
          required: true,
          elementsActions: {
            persist: (fieldsToUpdate) => async (dispatch, getState) => {
              await dispatch(projectsActions.persist(fieldsToUpdate, {independentUpdate: true}));
            },
          },
        },
        {
          simpleDisplayCellRender: (record) => (
            <>
              <ProjectStatusTag status={record.status} />
              {record.status === "pendingDonation" && record.pendingDonationSince && (
                <div style={{fontSize: 12, color: "grey"}}>
                  {t("common:time.sinceXDays", {
                    count: dayjs().diff(dayjs(record.pendingDonationSince), "days"),
                  })}
                </div>
              )}
            </>
          ),
        }
      ),
      sorter: (a, b) =>
        sorter.text(
          projectStatusesMapping[a.status]?.label,
          projectStatusesMapping[b.status]?.label
        ),
      searchable: true,
      searchText: (record) => projectStatusesMapping[record.status]?.label,
      width: 140,
    },
    currentUser.superAdmin && {
      ...editableCellColumn({
        title: t("projects:schema.superAdminNotes.label"),
        dataIndex: "superAdminNotes",
        type: "longText",
        placeholder: t("common:schema.notes.placeholder"),
        elementsActions: {
          persist: (fieldsToUpdate) => async (dispatch, getState) => {
            await dispatch(projectsActions.persist(fieldsToUpdate, {independentUpdate: true}));
          },
        },
      }),
      sorter: (a, b) => sorter.text(a.superAdminNotes, b.superAdminNotes),
      searchable: true,
    },
    {
      dataIndex: "starred",
      width: 60,
      render: (text, record) => <BookmarkedProjectButton projectId={record._id} />,
    },
  ].filter((el) => !!el);

  const onRequireAdmin = async (project) => {
    try {
      const existingRegistration = currentUser.registrations.find(
        (registration) => registration.project === project._id
      );
      await dispatch(
        registrationsActions.persist({
          _id: existingRegistration?._id || "new",
          user: currentUser._id,
          project: project._id,
          role: "admin",
        })
      );
      dispatch(currentUserActions.refreshAuthTokens()); // Reload the user and all its registrations
    } catch {
      /* nothing */
    }
  };

  const cleanStateAndNavigate = async (projectId) => {
    const shouldReload =
      alreadyLoadedProject._id &&
      alreadyLoadedProject._id !== projectId &&
      alreadyLoadedProject.slug !== projectId;

    // If there is already a loaded project, and that it's not the same as the requested project, clean everything. Otherwise, keep the data
    if (shouldReload) await dispatch(currentProjectActions.cleanProject());

    if (projectId === "new") {
      navigate("/new");
    } else {
      const projectToNavigateTo = projects.find(
        (project) => project.slug === projectId || project._id === projectId
      );

      // Then navigate only after cleaning to the cockpit
      navigate(`/${projectToNavigateTo.slug || projectToNavigateTo._id}/cockpit`);
    }
  };

  // If a project has been bookmarked, then load the page directly. Only redirect if there is no "no-redir" in the URL
  useEffect(() => {
    projects?.length > 0 &&
      currentUser?.bookmarkedProject &&
      new URLSearchParams(window.location.search).get("no-redir") === null &&
      cleanStateAndNavigate(currentUser.bookmarkedProject);
  }, [projects, currentUser.bookmarkedProject]);

  const WelcomeGuide = () => {
    const [allowCreateProject, setAllowCreateProject] = useState(false);

    return (
      <div style={{textAlign: "initial", color: "initial", padding: 16}}>
        <NOEHelpGuide />

        <p>
          <Checkbox onChange={(e) => setAllowCreateProject(e.target.checked)}>
            <strong>{t("common:NOEHelpGuide.iHaveReadAndUnderstood")}</strong>
          </Checkbox>
        </p>
        <Button
          icon={<PlusOutlined />}
          type="primary"
          onClick={() =>
            allowCreateProject
              ? navigate("/new")
              : message.info(t("common:NOEHelpGuide.youShouldCheckTheCheckbox"))
          }>
          {t("projects:label_create")}
        </Button>
      </div>
    );
  };

  return (
    <>
      <ListPage
        icon={<CalendarOutlined />}
        i18nNs="projects"
        title={t("projects:labelMyProjects")}
        elementsActions={projectsActions}
        elementsSelectors={projectsSelectors}
        searchInFields={["name", "orgaEmails"]}
        customButtons={
          <>
            {currentUser.superAdmin && (
              <ToggleTag
                checked={showDemoProjects}
                onChange={(checked) => setShowDemoProjects(checked)}
                checkedIcon={<EyeOutlined />}
                uncheckedIcon={<EyeInvisibleOutlined />}>
                {t("projects:list.showDemoProjects")}
              </ToggleTag>
            )}
            <Button
              type="link"
              icon={<QuestionCircleOutlined />}
              onClick={() => setShowHelpModal(true)}>
              {t("common:NOEHelpGuide.findHelpButton")}
            </Button>
          </>
        }
        onNavigate={cleanStateAndNavigate}
        creatable={!noProjectsCreatedYet}
        customEmptyDataRender={projects.length === 0 ? () => <WelcomeGuide /> : undefined}
        noActionIcons
        settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
        columns={filterBlacklistedColumns(columns)}
        dataSource={filteredProjects}
      />

      <Modal
        title={t("common:NOEHelpGuide.welcomeOnNOE")}
        open={showHelpModal}
        onCancel={() => setShowHelpModal(false)}
        width={"min(90vw, 1200px)"}
        footer={null}>
        <NOEUsefulLinksGuide skipProjectCreation />
      </Modal>
    </>
  );
}
