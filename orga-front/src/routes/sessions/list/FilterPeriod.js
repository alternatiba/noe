import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {SelectInput} from "@shared/inputs/SelectInput";
import {viewActions, viewSelectors} from "@features/view";

export const FilterPeriod = () => {
  const dispatch = useDispatch();
  const filterPeriod = useSelector(viewSelectors.selectSessionFilterPeriod);

  return (
    <SelectInput
      bordered
      popupMatchSelectWidth={false}
      allowClear
      placeholder="Filtrer par période..."
      value={filterPeriod}
      options={[
        {value: "hidePastSessions", label: "Cacher sessions passées"},
        {value: "showOngoingSessions", label: "Sessions en cours"},
        {value: "showNextSessions", label: "Sessions à venir"},
      ]}
      onChange={(value) => dispatch(viewActions.changeFilter(value))}
    />
  );
};
