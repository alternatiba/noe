import {useSelector} from "react-redux";
import {sessionsSelectors} from "@features/sessions";
import {currentProjectSelectors} from "@features/currentProject";
import {useMemo} from "react";
import {getAgendaDisplayHours} from "../atoms/getAgendaDisplayHours";
import {AgendaParamsProps} from "@routes/sessions/agenda/useAgendaParams";

export const useAgendaDisplayHours = (
  forcedDisplayHours: AgendaParamsProps["forcedDisplayHours"]
) => {
  const allSlots = useSelector(sessionsSelectors.selectSlotsListForAgenda);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  return useMemo(() => {
    const computedHours = getAgendaDisplayHours(allSlots, currentProject);
    return {
      start: forcedDisplayHours?.start ?? computedHours.start,
      end: forcedDisplayHours?.end ?? computedHours.end,
    };
  }, [allSlots, currentProject, forcedDisplayHours?.start, forcedDisplayHours?.end]);
};
