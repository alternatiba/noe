import {Toolbar} from "@devexpress/dx-react-scheduler-material-ui";
import React from "react";

export const ToolbarRoot = (props) => (
  <Toolbar.Root
    {...props}
    style={{
      ...props.style,
      minHeight: "fit-content",
      paddingTop: 10,
      paddingBottom: 10,
    }}
  />
);
