import {resourcesMapping, useAgendaParams} from "./useAgendaParams";
import React, {useState} from "react";
import {App, Button, Drawer, Dropdown, Tag} from "antd";
import {SwitchInput} from "@shared/inputs/SwitchInput";
import {TableElement} from "@shared/components/TableElement";
import {paginationPageSizes} from "@features/view";
import {useSelector} from "react-redux";
import {sessionsSelectors} from "@features/sessions";
import {RECOMMENDED_LIMIT_OF_RESOURCE_COLUMNS} from "@utils/agendaUtilities";
import {DownOutlined} from "@ant-design/icons";
import {sorter} from "@shared/utils/sorters";
import {useTranslation} from "react-i18next";
import {ToggleTag} from "@shared/components/ToggleElement";
import {EyeOutlined, EyeInvisibleOutlined} from "@ant-design/icons";

const ResourceFilterSelector = ({resource, changeGrouping, inDropdown = false}) => {
  const {t} = useTranslation();
  const [
    {selectedResources, resourcesFilterSelections, showResourcesAvailabilities},
    setAgendaParams,
  ] = useAgendaParams();
  const {message} = App.useApp();
  const [tmpResourceFilterSelection, setTmpResourceFilterSelection] = useState(
    resourcesFilterSelections[resource.resourceName]?.map((el) => ({id: el})) || []
  );
  const [drawerOpen, setDrawerOpen] = useState(false);
  const checked = selectedResources.includes(resource.fieldName);

  const rowSelectionResourceFilterSelection = {
    onChange: (selectedRowKeys, selectedRowObject) =>
      setTmpResourceFilterSelection(selectedRowObject),
  };

  const validateFilter = () => {
    changeGrouping(resource, true, {
      ...resourcesFilterSelections,
      [resource.resourceName]: tmpResourceFilterSelection.map((r) => r.id),
    });
    setDrawerOpen(false);
  };

  const resetFilter = () => {
    changeGrouping(resource, false);
    setDrawerOpen(false);
  };

  return (
    <>
      <ToggleTag
        key={resource.fieldName}
        className={`${resource.resourceName}-grouping grouping-button`}
        checked={checked}
        {...(inDropdown
          ? {
              style: {width: "calc(100% + 24px)", padding: "5px 14px", margin: "-5px -12px"},
              checkedIcon: <EyeOutlined />,
              uncheckedIcon: <EyeInvisibleOutlined />,
              onChange: () =>
                resource.instances.length === 0
                  ? message.warning(
                      `Vous devez déjà avoir créé des ${resource.title.toLowerCase()} pour effectuer cette action.`
                    )
                  : !checked && selectedResources?.length >= 2
                  ? message.warning(
                      "Pas plus de deux filtres à la fois, autrement votre PC va exploser 😉"
                    )
                  : setDrawerOpen(true),
            }
          : {})}>
        {resource.title}
      </ToggleTag>
      <Drawer
        title={`Grouper par ${resource.title.toLowerCase()}`}
        placement="right"
        onClose={() => setDrawerOpen(false)}
        open={drawerOpen}
        zIndex={10000}>
        {/*Display availabilities or not*/}
        {resource.hasAvailabilities && (
          <SwitchInput
            label="Affichage des disponibilités"
            tooltip={
              <>
                Afficher les disponibilités des {resource.title} sur le planning lorsque l'on active
                le groupage.
                <br />- Le rose rayé représente les moment d'indisponibilité des salles.
                <br />- Le bleu rayé représente les moments d'indisponibilité des{" "}
                {t("stewards:labelDown_other")}.
              </>
            }
            checked={showResourcesAvailabilities[resource.fieldName]}
            onChange={(value) =>
              setAgendaParams({
                showResourcesAvailabilities: {
                  ...showResourcesAvailabilities,
                  [resource.fieldName]: value,
                },
              })
            }
          />
        )}
        <TableElement.Simple
          showHeader
          scroll={{y: "calc( 100vh - 340px)"}}
          pagination={{
            position: ["bottomCenter"],
            pageSize: 40,
            size: "small",
            pageSizeOptions: paginationPageSizes,
            showSizeChanger: true,
          }}
          rowSelection={rowSelectionResourceFilterSelection}
          selectedRowKeys={tmpResourceFilterSelection}
          columns={[
            {
              title: `Filtrage des ${resource.title}`,
              dataIndex: "text",
              searchable: true,
              searchText: (record) => record.text,
              sorter: (a, b) => sorter.text(a.text, b.text),
            },
          ]}
          rowKey="id"
          dataSource={resource.instances}
        />
        <Button
          type="primary"
          style={{width: "100%", marginTop: 8}}
          onClick={validateFilter}
          disabled={tmpResourceFilterSelection.length === 0}>
          Valider le groupage
        </Button>
        {checked && (
          <Button type="link" danger style={{width: "100%", marginTop: 8}} onClick={resetFilter}>
            Désactiver le groupage
          </Button>
        )}
      </Drawer>
    </>
  );
};
export const GroupByControls = () => {
  const {message} = App.useApp();

  const resources = useSelector(sessionsSelectors.selectAgendaResources);

  const [{selectedResources, numberOfDaysDisplayed}, setAgendaParams] = useAgendaParams();

  // Called by the grouping controls to change the state of the grouping
  const changeGrouping = (resource, value, newResourcesFilterSelections) => {
    const length = resource.instances.length;
    if (length === 0) {
      message.warning(
        `Vous devez déjà avoir créé des ${resource.title.toLowerCase()} pour effectuer cette action.`
      );
    } else {
      const selectedResourcesWithoutCurrent = selectedResources.filter(
        (t) => t !== resource.fieldName
      );
      const newSelectedResources = value
        ? [...selectedResourcesWithoutCurrent, resource.fieldName]
        : selectedResourcesWithoutCurrent;

      const displayNumberOfColumnsTooBig =
        value &&
        Object.entries(newResourcesFilterSelections)
          .map(([resourceName, resourcesSelected]) => [
            resourcesMapping[resourceName],
            resourcesSelected,
          ])
          .filter(([fieldName, resourcesSelected]) => selectedResources?.includes(fieldName))
          .reduce((acc, [_, resourcesSelected]) => resourcesSelected.length * acc, 1) *
          numberOfDaysDisplayed >
          RECOMMENDED_LIMIT_OF_RESOURCE_COLUMNS;

      if (displayNumberOfColumnsTooBig) {
        message.warning(
          `Vous avez beaucoup d'éléments sélectionnés. ` +
            `Pour éviter les ralentissements, décochez quelques éléments à afficher, ou abaissez le nombre de jours.`,
          6
        );
      }
      const params = {selectedResources: newSelectedResources};
      if (newResourcesFilterSelections)
        params.resourcesFilterSelections = newResourcesFilterSelections;
      setAgendaParams(params);
    }
  };

  const ResSelect = ({resource, inDropdown = false}) => (
    <ResourceFilterSelector
      key={resource.fieldName}
      resource={resource}
      changeGrouping={changeGrouping}
      inDropdown={inDropdown}
    />
  );

  return (
    <Dropdown
      trigger={["click"]}
      menu={{
        items: resources.map((resource) => ({
          label: <ResSelect resource={resource} inDropdown />,
        })),
      }}>
      <Button>
        <span style={{marginRight: -8}}>
          <span style={{marginRight: 8}}>Groupage</span>
          {selectedResources.length > 0 &&
            resources
              .filter((resource) => selectedResources.includes(resource.fieldName))
              .map((resource) => <ResSelect resource={resource} />)}
        </span>
        <DownOutlined />
      </Button>
    </Dropdown>
  );
};
