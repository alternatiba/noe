import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import React, {useState} from "react";
import {Button, Drawer} from "antd";
import {SearchOutlined} from "@ant-design/icons";
import {t} from "i18next";
import {NumberOfDaysSelect} from "./NumberOfDaysSelect";
import {SearchBox} from "../atoms/SearchBox";
import {CategoriesFilterSelect} from "../atoms/CategoriesFilterSelect";
import {GroupByControls} from "./GroupByControls";
import {CustomFieldsFilters} from "../atoms/CustomFieldsFilters";

export const AgendaControls = ({settingsDrawer}) => {
  const {isMobileView} = useWindowDimensions();
  const [sessionFilterDrawerOpen, setSessionFilterDrawerOpen] = useState(false);

  return isMobileView ? (
    <>
      {/*On mobile, display a button to open the drawer to filter the sessions*/}
      <Button
        type="primary"
        onClick={() => setSessionFilterDrawerOpen(true)}
        style={{marginRight: 10, marginLeft: 10, flexGrow: 1}}
        icon={<SearchOutlined />}>
        {t("sessions:agenda.filter")}
      </Button>

      <NumberOfDaysSelect isMobileView />

      {/*Drawer we can open on mobile view to get the controls*/}
      <Drawer
        placement="top"
        height="auto" // adjust to content
        closable={false}
        push={false} // Don't push the drawer when the nested Sesstings drawer opens
        onClose={() => setSessionFilterDrawerOpen(false)}
        open={sessionFilterDrawerOpen}>
        <div className="containerH buttons-container" style={{alignItems: "center"}}>
          <SearchBox />
          <CategoriesFilterSelect />
          <CustomFieldsFilters mode={"inOrgaFilters"} showIcon />

          {settingsDrawer}

          <GroupByControls />
        </div>
      </Drawer>
    </>
  ) : (
    <div
      style={{width: "100%", alignItems: "center", flexWrap: "nowrap", overflowX: "auto"}}
      className="containerH buttons-container">
      {/*Separator*/}
      <div style={{width: 20}} />

      <div className={"containerH buttons-container"} style={{flexGrow: 1, width: "100%"}}>
        <SearchBox />
        <CategoriesFilterSelect />
        <CustomFieldsFilters mode={"inOrgaFilters"} showIcon />
      </div>

      {/*Separator*/}
      <div style={{width: 12}} />
      <GroupByControls />

      {/*Separator*/}
      <div style={{width: 12}} />
      <NumberOfDaysSelect />

      {/*Separator*/}
      <div style={{width: 10}} />
      {settingsDrawer}
    </div>
  );
};
