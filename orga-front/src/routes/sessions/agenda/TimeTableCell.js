import React from "react";
import {useAgendaParams} from "./useAgendaParams";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {slotOverlapsAnOtherOne} from "@utils/slotsUtilities";
import {DayView} from "@devexpress/dx-react-scheduler-material-ui";
import {timezoneDiffManager} from "@shared/utils/timezoneDiffManager";

// Getting dates for each timetable cell is very expensive, so we cache it for more speed.
// Still not perfect, but it's a good improvement.
// The main bottleneck is the timezone diff calculation, which is done for each start and end date of each cell.
const startEndDatesCache = new Map();
const getStartEndDatesDatesFromCache = (startDate, endDate) => {
  const key = startDate.getTime() + endDate.getTime();

  // Check if result is already in cache
  if (startEndDatesCache.has(key)) return startEndDatesCache.get(key);

  // Calculate if cell overlaps with availability slots
  const agendaCellSlot = {
    start: timezoneDiffManager.keepDateFormat.unset(startDate),
    end: timezoneDiffManager.keepDateFormat.unset(endDate),
  };
  // Store result in cache
  startEndDatesCache.set(key, agendaCellSlot);

  return agendaCellSlot;
};

export const TimeTableCell = React.memo(({filteredResources, ...props}) => {
  const [{cellDisplayHeight, showResourcesAvailabilities}] = useAgendaParams();
  const currentProjectAvailabilitySlots = useSelector(
    (s) => currentProjectSelectors.selectProject(s).availabilitySlots
  );

  // Useful to calculate overlaps with different availabilities from the resources
  const filteredResourcesWithAvailabilities = filteredResources.filter(
    (resource) => resource.hasAvailabilities
  );

  // Get the cell start and end hour
  const agendaCellSlot = getStartEndDatesDatesFromCache(props.startDate, props.endDate);
  const overlapsWithElements = [];

  // Check if the cell is out of project availabilities
  if (!slotOverlapsAnOtherOne(agendaCellSlot, currentProjectAvailabilitySlots)) {
    overlapsWithElements.push("project");
  }

  // Check if the cell is out of the other entities availabilities (only for resources with availabilities
  for (const resourceInfo of filteredResourcesWithAvailabilities) {
    if (showResourcesAvailabilities[resourceInfo.fieldName]) {
      const resourceGrouping = props.groupingInfo?.find(
        (info) => info.fieldName === resourceInfo.fieldName
      );
      if (resourceGrouping) {
        const resource = resourceInfo.instances.find((res) => res.id === resourceGrouping.id);
        if (!slotOverlapsAnOtherOne(agendaCellSlot, resource?.availabilitySlots)) {
          overlapsWithElements.push(resourceInfo.resourceName);
        }
      }
    }
  }

  // Add the appropriate classes to the cell, so it can be painted if there are some overlaps
  const className =
    overlapsWithElements.length > 0 ? `${overlapsWithElements.join("-")}-disabled-date` : undefined;
  return (
    <DayView.TimeTableCell {...props} style={{height: cellDisplayHeight}} className={className} />
  );
});
