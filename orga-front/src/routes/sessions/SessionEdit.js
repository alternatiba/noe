import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {displayInconsistencies, sessionsActions, sessionsSelectors} from "@features/sessions";
import {stewardsActions} from "@features/stewards";
import {placesActions} from "@features/places";
import {currentProjectSelectors} from "@features/currentProject";
import {activitiesActions, activitiesSelectors} from "@features/activities";
import {App, Button, Form} from "antd";
import {BookOutlined, ClockCircleOutlined, ScheduleOutlined, TeamOutlined} from "@ant-design/icons";
import dayjs from "@shared/services/dayjs";
import {CardElement} from "@shared/components/CardElement";
import {EditPage, EditPageProps, ElementEditProps} from "@shared/pages/EditPage";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {teamsActions, teamsSelectors} from "@features/teams";
import {pick} from "@shared/utils/utilities";
import {useColumnsBlacklistingSelect} from "@shared/hooks/useColumnsBlacklistingSelect";
import {TextInput} from "@shared/inputs/TextInput";
import {NumberInput} from "@shared/inputs/NumberInput";
import {SwitchInput} from "@shared/inputs/SwitchInput";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {getSessionSubscription} from "@utils/registrationsUtilities";
import {useLoadEditing} from "@shared/hooks/useLoadEditing";
import {RichTextInput} from "@shared/inputs/RichTextInput";
import {SelectInput} from "@shared/inputs/SelectInput";
import {WithEntityLinks} from "@shared/components/WithEntityLinks";
import {TagsSelectInput} from "@shared/components/TagsSelectInput";
import {generateRegistrationDispoColumn} from "@utils/columns/generateRegistrationDispoColumn";
import {useRegistrationsColumns} from "@utils/columns/useRegistrationsColumns";
import {generateSubscriptionInfo} from "@utils/columns/generateSubscriptionInfo";
import {searchInRegistrationFields} from "@shared/utils/searchInFields/searchInRegistrationFields";
import {GroupSmsButtons} from "@shared/components/buttons/GroupSmsButtons";
import {useLocation} from "react-router-dom";
import {withFallBackOnUrlId} from "@shared/utils/withFallbackOnUrlId";
import {generateSessionNoShowColumn} from "@shared/utils/columns/generateSessionNoShowColumn";
import {sorter} from "@shared/utils/sorters";
import {ElementsTableWithModal} from "@shared/components/ElementsTableWithModal";
import {Stack} from "@shared/layout/Stack";
import {PlacesTableWithModal} from "./sessionEditAtoms/PlacesTableWithModal";
import {StewardsTableWithModal} from "./sessionEditAtoms/StewardsTableWithModal";
import {AdvancedSlots} from "./sessionEditAtoms/AdvancedSlots";
import {RecapSlot} from "./sessionEditAtoms/RecapSlot";
import {SessionSmallPreview} from "./sessionEditAtoms/SessionSmallPreview";
import {SlotStartAndDurationInput} from "./sessionEditAtoms/SlotStartAndDurationInput";
import {CustomFieldsInputs} from "@shared/inputs/CustomFieldsInputs";
import {useTranslation} from "react-i18next";
import {getSlotsStartAndEnd} from "@utils/slotsUtilities";

const ActivityEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../activities/ActivityEdit")
);

const TeamEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../teams/TeamEdit"));

const SessionGroupSmsButtons = () => {
  const sessionVal = Form.useWatch();
  const activitiesById = useSelector(activitiesSelectors.selectById);
  const activityName = activitiesById[sessionVal?.activity]?.name;

  return (
    <GroupSmsButtons
      registrations={sessionVal?.registrations}
      session={{
        ...sessionVal,
        activity: {name: activityName},
        ...getSlotsStartAndEnd(sessionVal?.slots),
      }}
    />
  );
};

export const hereditaryPlaceholder = (value: string | number, defaultPlaceholder: string) =>
  "↖ " + (value ? value : defaultPlaceholder);

function SessionEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {message} = App.useApp();
  const location = useLocation();

  const endpoint = "sessions";
  const setIsModified = EditPage.useSetIsModified({resetOnUnmount: true, endpoint, id});
  const [form] = Form.useForm();
  const [filterBlacklistedParticipantsColumns] = useColumnsBlacklistingSelect({
    endpoint: "participants",
  });

  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const session = useSelector(sessionsSelectors.selectEditing);
  const activities = useSelector(activitiesSelectors.selectList);
  const activitiesById = useSelector(activitiesSelectors.selectById);
  const sessionActivityVal = activitiesById[Form.useWatch("activity", form)];
  const sessionEverybodyIsSubscribedVal = Form.useWatch("everybodyIsSubscribed", form);
  const teams = useSelector(teamsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    sessionsActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      dispatch(activitiesActions.loadList());
      dispatch(teamsActions.loadList());
      dispatch(placesActions.loadList());
      dispatch(registrationsActions.loadList());
      dispatch(sessionsActions.loadList()); // For tags
    },
    clonedElement
  );

  const onValuesChange: EditPageProps["onValuesChange"] = (changedValues) => {
    // Auto-update the default slots of the session when we change the activity, except if group editing
    if (Object.keys(changedValues).includes("activity") && !groupEditing) {
      const newActivity = activitiesById[changedValues.activity];

      // Only auto-update slots if :
      if (
        session._id === "new" && // - the session is new
        !form.isFieldTouched("slots") && // - slots have not been touched yet
        newActivity?.slots?.length > 0 // and if the newly selected activity has some default slots
      ) {
        // Start building the new slots by adding basic stuff and activity duration
        const defaultSlots = newActivity.slots.map((s) => ({
          duration: s.duration,
        }));

        // Then we will add start and end dates :
        // Get timetrace starting from session start, or project start
        let timeTrace = dayjs(form.getFieldValue("start") || currentProject.start);
        // and compute start and end dates
        for (const slot of defaultSlots) {
          slot.start = timeTrace.toISOString();
          timeTrace = timeTrace.add(slot.duration, "minute");
          slot.end = timeTrace.toISOString();
        }

        // update form values
        form.setFieldValue("slots", defaultSlots);
        if (defaultSlots?.length > 1) setSimpleSlotsMode(false);
      }
    }

    if (Object.keys(changedValues).includes("slots")) {
      displayInconsistencies(form.getFieldsValue(), currentProject._id);
    }
  };

  // Initialize the slots mode, simple or advanced, depending on the initial number of slots
  const [simpleSlotsMode, setSimpleSlotsMode] = useState(true);
  useEffect(() => {
    if (session._id) setSimpleSlotsMode(session.slots.length < 2);
  }, [session._id, session.slots?.length]);

  const sessionRegistrationsColumns = [
    ...filterBlacklistedParticipantsColumns(useRegistrationsColumns("./../..", currentProject)),
    generateRegistrationDispoColumn("./../.."),
  ];
  const additionalRegistrationsColumns = [
    generateSessionNoShowColumn(session, registrations, dispatch),
    {
      title: t("sessions:schema.subscriptionInfo.label"),
      render: (text, record) =>
        generateSubscriptionInfo(
          record,
          getSessionSubscription(record, session),
          registrations,
          true
        ),
      sorter: (a, b) =>
        sorter.date(
          getSessionSubscription(a, session).updatedAt,
          getSessionSubscription(b, session).updatedAt
        ),
    },
  ];

  // REGISTRATIONS STUFF

  const sessionRegistrations = registrations.filter((r) =>
    r.sessionsSubscriptions?.find((ss) => ss.session === session._id)
  );

  // Add registrations to the session (don't do this in cloning mode)
  useEffect(() => {
    if (!clonedElement && session._id) form.setFieldValue("registrations", sessionRegistrations);
  }, [JSON.stringify(sessionRegistrations), session._id, clonedElement]);

  // GLOBAL STUFF

  const onValidation = async (formData, fieldsKeysToUpdate) => {
    const payload = {...session, ...formData};

    if (fieldsKeysToUpdate) {
      const onlyFieldsToUpdate = pick(payload, fieldsKeysToUpdate);

      for (const entityId of groupEditing.elements) {
        await dispatch(sessionsActions.persist({...onlyFieldsToUpdate, _id: entityId}));
      }
    } else {
      if (payload.slots?.length > 0) {
        return dispatch(sessionsActions.persist(payload));
      } else {
        message.error("Vous devez créer au moins une plage horaire pour créer cette session.");
      }
    }
  };

  const sessionPostTransform = (values) => {
    if (values.start) values.start = values.start.toISOString();
    if (values.end) values.end = values.end.toISOString();

    return values;
  };

  const SimpleSlot = () => (
    <CardElement
      title="Plage horaire"
      icon={<ClockCircleOutlined />}
      customButtons={
        <Button type={"link"} onClick={() => setSimpleSlotsMode(false)}>
          Plages multiples
        </Button>
      }>
      <div className="container-grid">
        <Stack row columnGap={5} rowGap={2} wrap>
          <SlotStartAndDurationInput name={["slots", 0]} />
        </Stack>

        <RecapSlot name={["slots", 0]} row style={{marginBottom: 0}} size={"small"} />
      </div>
    </CardElement>
  );

  return (
    <EditPage
      icon={<ScheduleOutlined />}
      i18nNs={endpoint}
      form={form}
      onValidation={onValidation}
      clonable
      clonedElement={clonedElement}
      asModal={asModal}
      modalOpen={modalOpen}
      setModalOpen={setModalOpen}
      onCreate={onCreate}
      deletable
      onValuesChange={onValuesChange}
      elementsActions={sessionsActions}
      record={session}
      initialValues={{
        ...session,
        start: session.start && dayjs(session.start),
        end: session.end && dayjs(session.end),
        activity: session.activity?._id,
        team: session.team?._id,
      }}
      postTransform={sessionPostTransform}
      groupEditing={groupEditing}>
      <div className="container-grid two-thirds-one-third">
        <div className="container-grid">
          <CardElement>
            <div className="container-grid two-per-row">
              <WithEntityLinks
                endpoint="activities"
                name="activity"
                createButtonText="Créer une nouvelle activité"
                ElementEdit={ActivityEdit}>
                <SelectInput
                  label="Activité"
                  icon={<BookOutlined />}
                  options={activities.map((d) => ({value: d._id, label: d.name}))}
                  placeholder="sélectionnez une activité"
                  required
                  showSearch
                />
              </WithEntityLinks>

              {currentProject.useTeams && (
                <WithEntityLinks
                  endpoint="teams"
                  name="team"
                  createButtonText="Créer une nouvelle équipe"
                  ElementEdit={TeamEdit}>
                  <SelectInput
                    label="Équipe"
                    icon={<TeamOutlined />}
                    placeholder="sélectionnez une équipe"
                    options={[
                      {value: null, label: "- Pas d'équipe -"},
                      ...teams.map((d) => ({
                        value: d._id,
                        label: d.name + (d.activity?.name ? ` (${d.activity?.name})` : ""),
                      })),
                    ]}
                    showSearch
                  />
                </WithEntityLinks>
              )}

              <NumberInput
                label="Coefficient de bénévolat de la session"
                tooltip={
                  <>
                    ↖ Champ hérité de l'activité : Le coefficient de bénévolat de la session est
                    égal au coefficient de bénévolat de l'activité, mais vous pouvez le redéfinir si
                    besoin.
                  </>
                }
                name="volunteeringCoefficient"
                step={0.1}
                placeholder={hereditaryPlaceholder(
                  sessionActivityVal?.volunteeringCoefficient,
                  "coef"
                )}
                min={0}
                max={5}
              />

              <SwitchInput
                label="Inscrire tout le monde"
                name="everybodyIsSubscribed"
                tooltip={
                  "Rendre la session visible dans le planning de tous vos participants sans qu'iels s'y inscrivent."
                }
              />

              <NumberInput
                label="Nombre maximum de participant⋅es"
                name="maxNumberOfParticipants"
                disabled={sessionEverybodyIsSubscribedVal}
                tooltip={
                  <>
                    <p>
                      ↖ Champ hérité de l'activité : Le nombre maximum de participant⋅es de la
                      session est égal au nombre maximum de participant⋅es de l'activité, mais vous
                      pouvez le redéfinir si besoin.
                    </p>
                    Valeurs spéciales:
                    <ul>
                      <li>
                        Rien = valeur de l'activité (et si pas de valeur dans l'activité =
                        inscription libre)
                      </li>
                      <li>
                        0 = Session invisible pour les participant.es, seulement visible par les
                        orgas et les personnes inscrites
                      </li>
                    </ul>
                  </>
                }
                min={0}
                placeholder={hereditaryPlaceholder(
                  sessionActivityVal?.maxNumberOfParticipants,
                  "max"
                )}
              />
            </div>
          </CardElement>

          <CardElement>
            <div className="container-grid two-per-row">
              <RichTextInput i18nNs="sessions" name="notes" />

              <TagsSelectInput
                i18nNs="sessions"
                name="tags"
                elementsSelectors={sessionsSelectors}
              />
            </div>
          </CardElement>

          <CustomFieldsInputs form={form} endpoint={endpoint} />
        </div>

        <div className="container-grid">
          <CardElement>
            <TextInput i18nNs="sessions" name="name" />
          </CardElement>

          <SessionSmallPreview />
        </div>
      </div>

      <div
        className={
          "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
        }>
        <div className="containerV" style={{flexBasis: "33%"}}>
          {simpleSlotsMode ? <SimpleSlot /> : <AdvancedSlots />}
        </div>

        <div className="containerV" style={{flexBasis: "33%"}}>
          <StewardsTableWithModal
            onChange={() => {
              setIsModified(true);
              displayInconsistencies(form.getFieldsValue(), currentProject._id);
            }}
            sessionActivityVal={sessionActivityVal}
          />
        </div>

        {currentProject.usePlaces && (
          <div className="containerV" style={{flexBasis: "33%"}}>
            <PlacesTableWithModal
              onChange={() => {
                setIsModified(true);
                displayInconsistencies(form.getFieldsValue(), currentProject._id);
              }}
              sessionActivityVal={sessionActivityVal}
            />
          </div>
        )}
      </div>

      <ElementsTableWithModal
        name={"registrations"}
        onChange={() => setIsModified(true)}
        allElements={registrations}
        title="Participant⋅es"
        icon={<TeamOutlined />}
        customButtons={<SessionGroupSmsButtons />}
        subtitle="En rouge clair figurent les participant⋅es qui ne sont pas encore arrivé⋅es à l'événement."
        showHeader
        buttonTitle="Ajouter un⋅e participant⋅e"
        rowClassName={(record) => (record.hasCheckedIn ? "" : " ant-table-row-danger")}
        navigableRootPath="./../../participants"
        columns={[...sessionRegistrationsColumns, ...additionalRegistrationsColumns]}
        elementSelectionModalProps={{
          large: true,
          title: "Ajouter des participant⋅es à la session",
          searchInFields: searchInRegistrationFields,
          columns: sessionRegistrationsColumns,
        }}
      />
    </EditPage>
  );
}

export default withFallBackOnUrlId(SessionEdit);
