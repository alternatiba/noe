import {ScheduleOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import CsvExportButton from "@shared/components/buttons/CsvExportButton";
import {useColumnsBlacklistingSelect} from "@shared/hooks/useColumnsBlacklistingSelect";
import {useLoadList} from "@shared/hooks/useLoadList";
import {ListPage} from "@shared/pages/ListPage";
import {categoriesActions} from "@features/categories";
import {currentProjectSelectors} from "@features/currentProject";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {useSessionsColumns} from "@utils/columns/useSessionsColumns";
import {searchInSessionFields} from "@shared/utils/searchInFields/searchInSessionFields";
import {CategoriesFilterSelect} from "./atoms/CategoriesFilterSelect";
import {useBuildSessionsExport} from "./list/useBuildSessionsExport.js";
import {FilterPeriod} from "./list/FilterPeriod";
import {CustomFieldsFilters} from "./atoms/CustomFieldsFilters";
import {useTranslation} from "react-i18next";

/**
 * Functional component representing custom action buttons for session list operations.
 * This component provides buttons for category filtering, filter period selection, and CSV export of sessions.
 * @component
 * @returns {ReactNode} - Rendered component of custom action buttons for session list operations.
 */
const SessionListCustomButtons = () => {
  const {t} = useTranslation();
  const currentProjectName = useSelector((s) => currentProjectSelectors.selectProject(s).name);
  const buildSessionsExport = useBuildSessionsExport();

  return (
    <>
      <CategoriesFilterSelect />
      <FilterPeriod />
      <CustomFieldsFilters mode={"inOrgaFilters"} showIcon />

      <CsvExportButton
        tooltip={t("sessions:sessions.csvExportButton.tooltip")}
        getExportName={() => `Export des sessions - ${currentProjectName}`}
        withCurrentDate
        dataExportFunction={buildSessionsExport}>
        {t("sessions:sessions.csvExportButton.title")}
      </CsvExportButton>
    </>
  );
};

/**
 * Functional component representing a list of sessions.
 * This component fetches and displays a list of sessions, allowing users to filter, search,
 * export data, and perform various actions on the sessions.
 * @component
 * @returns {ReactNode} - Rendered component of the session list.
 */
function SessionList() {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "sessions",
    defaultBlacklisting: [
      "everybodyIsSubscribed",
      "maxNumberOfParticipants",
      "volunteeringCoefficient",
    ],
  });
  const registrations = useSelector(registrationsSelectors.selectList);

  const filteredSessions = useSelector(sessionsSelectors.selectListFiltered);

  const columns = useSessionsColumns(
    "./..",
    currentProject.usePlaces,
    currentProject.useTeams,
    false,
    false,
    registrations,
    true
  );

  useLoadList(() => {
    dispatch(sessionsActions.loadList());
    dispatch(registrationsActions.loadList());
    dispatch(categoriesActions.loadList());
  });

  return (
    <ListPage
      icon={<ScheduleOutlined />}
      i18nNs="sessions"
      searchInFields={searchInSessionFields}
      elementsActions={sessionsActions}
      elementsSelectors={sessionsSelectors}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      columns={filterBlacklistedColumns(columns)}
      customButtons={<SessionListCustomButtons />}
      dataSource={filteredSessions}
      groupEditable
      groupImportable
    />
  );
}

export default SessionList;
