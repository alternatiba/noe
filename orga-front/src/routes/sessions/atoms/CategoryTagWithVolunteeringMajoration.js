import {Tag} from "antd";
import React from "react";
import {useTranslation} from "react-i18next";

export const CategoryTagWithVolunteeringMajoration = ({
  volunteeringCoefficient,
  category,
  children,
  invertedColors = false,
  ...props
}) => {
  const {t} = useTranslation();
  let volunteeringMajoration;
  if (volunteeringCoefficient > 0 && volunteeringCoefficient !== 1) {
    const percent = Math.round((volunteeringCoefficient - 1) * 100);
    volunteeringMajoration = `${Math.sign(percent) === 1 ? "+" : "-"}${Math.abs(percent)}%`;
  }

  if (invertedColors) {
    props.style.color = category.color;
    props.style.background = "white";
  }

  return children || volunteeringMajoration ? (
    <Tag
      title={
        volunteeringMajoration
          ? t("sessions:volunteeringMajorationTag", {volunteeringMajoration})
          : undefined
      }
      color={category.color}
      {...props}>
      {children}
      {volunteeringMajoration && <strong style={{fontSize: 15}}> {volunteeringMajoration}</strong>}
    </Tag>
  ) : null;
};
