import {CategoriesSelectComponent} from "@shared/components/CategoriesSelectComponent";
import {useAgendaParams} from "../agenda/useAgendaParams";

export const CategoriesFilterSelect = () => {
  const [{categoriesFilter}, setAgendaParams] = useAgendaParams();

  return (
    <CategoriesSelectComponent
      value={categoriesFilter}
      onChange={(value) => setAgendaParams({categoriesFilter: value})}
    />
  );
};
