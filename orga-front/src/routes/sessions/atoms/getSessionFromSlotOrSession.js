export const getSessionFromSlotOrSession = (slotOrSession) =>
  slotOrSession.session ? slotOrSession.session : slotOrSession;
