import {ElementSelectionModalProps} from "@shared/components/ElementSelectionModal";
import {useSelector} from "react-redux";
import {stewardsSelectors} from "@features/stewards";
import {ElementsTableWithModal} from "@shared/components/ElementsTableWithModal";
import {InfoCircleOutlined, UserOutlined} from "@ant-design/icons";
import {useStewardsColumns} from "@utils/columns/useStewardsColumns";
import React from "react";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {useTranslation} from "react-i18next";

const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../../stewards/StewardEdit")
);
export const StewardsTableWithModal = ({
  onChange,
  sessionActivityVal,
}: Pick<ElementSelectionModalProps, "onChange"> & {sessionActivityVal: any}) => {
  const {t} = useTranslation();
  const stewards = useSelector(stewardsSelectors.selectList);

  const stewardsColumns = useStewardsColumns();

  return (
    <ElementsTableWithModal
      name={"stewards"}
      onChange={onChange}
      allElements={stewards}
      title={t("stewards:label_other")}
      icon={<UserOutlined />}
      preselectedElements={sessionActivityVal?.stewards}
      buttonTitle={`Ajouter un⋅e ${t("stewards:labelDown")}`}
      navigableRootPath="./../../stewards"
      columns={stewardsColumns}
      ElementEdit={StewardEdit}
      elementSelectionModalProps={{
        title: `Ajouter des ${t("stewards:labelDown_other")} en charge de cette session`,
        subtitle:
          sessionActivityVal?.stewards?.length > 0 ? (
            <>
              <InfoCircleOutlined /> Les {t("stewards:labelDown_other")} éligibles de l'activité ont
              été pré-sélectionné⋅es pour plus de facilités.
            </>
          ) : undefined,
        searchInFields: ["firstName", "lastName"],
        createNewElementButtonTitle: t("stewards:label_create"),
      }}
    />
  );
};
