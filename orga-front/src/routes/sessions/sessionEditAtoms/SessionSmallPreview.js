import {Form} from "antd";
import {useSelector} from "react-redux";
import {sessionsSelectors} from "@features/sessions";
import {activitiesSelectors} from "@features/activities";
import {registrationsSelectors} from "@features/registrations";
import {PendingSuspense} from "@shared/components/Pending";
import {CardElement} from "@shared/components/CardElement";
import {getSlotsStartAndEnd} from "@utils/slotsUtilities";
import React from "react";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {Stack} from "@shared/layout/Stack";

const SessionShowSmall = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../atoms/SessionShowSmall")
);
export const SessionSmallPreview = () => {
  const PreviewContent = () => {
    const form = Form.useFormInstance();
    const sessionVal = Form.useWatch(undefined, form);
    const session = useSelector(sessionsSelectors.selectEditing);
    const activitiesById = useSelector(activitiesSelectors.selectById);
    const registrations = useSelector(registrationsSelectors.selectList);

    return sessionVal ? (
      <SessionShowSmall
        preview
        session={{
          ...sessionVal,
          ...getSlotsStartAndEnd(sessionVal?.slots),
          activity: activitiesById[sessionVal.activity],
          numberParticipants: sessionVal?.registrations?.length,
          computedMaxNumberOfParticipants: session.computedMaxNumberOfParticipants,
        }}
        registrations={registrations}
      />
    ) : null;
  };

  return (
    <CardElement greyedOut bodyStyle={{height: "100%"}}>
      <Stack justifyCenter height={"100%"}>
        <PendingSuspense>
          <PreviewContent />
        </PendingSuspense>
      </Stack>
    </CardElement>
  );
};
