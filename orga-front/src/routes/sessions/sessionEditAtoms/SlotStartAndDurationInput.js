import {NamePath} from "rc-field-form/es/interface";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import useFormInstance from "antd/es/form/hooks/useFormInstance";
import {Form} from "antd";
import dayjs, {dayjsBase} from "@shared/services/dayjs";
import {DateTimeInput} from "@shared/inputs/DateTimeInput";
import {DurationSelectInput} from "@shared/inputs/DurationSelectInput";
import {FormItem} from "@shared/inputs/FormItem";
import {useWatch} from "antd/es/form/Form";

export const SlotStartAndDurationInput = ({name}: {name: NamePath}) => {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const form = useFormInstance();
  const slot = useWatch(name, form);
  const slotVal = Form.useWatch(name, form);

  const computeSlotEnd = (start, duration) => {
    if (start && duration) {
      form.setFieldValue([...name, "end"], dayjs(start).add(duration, "minute").toISOString());
    }
  };

  return (
    <>
      <DateTimeInput
        label={"Début"}
        name={[...name, "start"]}
        disableDatesIfOutOfProject
        disableDatesBeforeNow={false}
        format={dayjsBase.localeData().longDateFormat("llll")}
        defaultPickerValue={currentProject.start}
        required
        onChange={(newStart) => computeSlotEnd(newStart, slotVal.duration)}
      />

      <DurationSelectInput
        name={[...name, "duration"]}
        required
        formItemProps={{style: {flexGrow: 1}}}
        onChange={(newDuration) => {
          computeSlotEnd(slotVal.start, newDuration);
        }}
      />

      {/* Hidden fields */}
      <FormItem name={[...name, "end"]} noStyle />
      <FormItem name={[...name, "_id"]} noStyle />
    </>
  );
};
