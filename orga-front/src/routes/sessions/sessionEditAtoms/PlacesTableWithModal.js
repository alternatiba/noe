import {ElementSelectionModalProps} from "@shared/components/ElementSelectionModal";
import {useSelector} from "react-redux";
import {placesSelectors} from "@features/places";
import {sessionsSelectors} from "@features/sessions";
import {Alert} from "antd";
import {getMaxParticipantsBasedOnPlaces} from "@shared/utils/sessionsUtilities";
import {ElementsTableWithModal} from "@shared/components/ElementsTableWithModal";
import {EnvironmentOutlined, InfoCircleOutlined} from "@ant-design/icons";
import {usePlacesColumns} from "@utils/columns/usePlacesColumns";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";

const PlaceEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../../places/PlaceEdit"));

export const PlacesTableWithModal = ({
  onChange,
  sessionActivityVal,
}: Pick<ElementSelectionModalProps, "onChange"> & {sessionActivityVal: any}) => {
  const places = useSelector(placesSelectors.selectList);
  const session = useSelector(sessionsSelectors.selectEditing);
  const sessionMax = session.maxNumberOfParticipants;
  const activityMax = sessionActivityVal?.maxNumberOfParticipants;
  const sessionMaxIsDefined = sessionMax !== undefined && sessionMax !== null;
  const maxParticipantsBasedOnPlaces = getMaxParticipantsBasedOnPlaces(session);
  const numberOfParticipantsSetIsAbovePlacesCapacity =
    sessionMax > maxParticipantsBasedOnPlaces || activityMax > maxParticipantsBasedOnPlaces;

  const placesColumns = usePlacesColumns();

  return (
    <ElementsTableWithModal
      name={"places"}
      onChange={onChange}
      allElements={places}
      preselectedElements={sessionActivityVal?.places}
      title="Espaces"
      icon={<EnvironmentOutlined />}
      subtitle={
        numberOfParticipantsSetIsAbovePlacesCapacity && (
          <Alert
            message={`La jauge finale de la session est contrainte par la taille des espaces définis dans la session.
                 La jauge de participants définie dans ${
                   sessionMaxIsDefined ? "la session" : "l'activité"
                 } est contrainte, et la jauge finale sera de ${maxParticipantsBasedOnPlaces}.`}
            type="warning"
            showIcon
          />
        )
      }
      buttonTitle="Ajouter un espace"
      navigableRootPath="./../../places"
      columns={placesColumns}
      ElementEdit={PlaceEdit}
      elementSelectionModalProps={{
        title: "Ajouter les espaces utilisés pour cette session",
        subtitle:
          sessionActivityVal?.places?.length > 0 ? (
            <>
              <InfoCircleOutlined /> Les espaces éligibles de l'activité ont été pré-sélectionnés
              pour plus de facilités.
            </>
          ) : undefined,
        searchInFields: ["name"],
        createNewElementButtonTitle: "Créer un espace",
      }}
    />
  );
};
