import {ClockCircleOutlined} from "@ant-design/icons";
import {CardElement} from "@shared/components/CardElement";
import {
  AvailabilitySlotsTableProps,
  useAvailabilitySlotsTable,
} from "@shared/hooks/useAvailabilitySlotsTable";

type AvailabilitySlotsTableCardProps = AvailabilitySlotsTableProps & {subtitle?: string};

export function AvailabilitySlotsTableCard(props: AvailabilitySlotsTableCardProps) {
  const {controlButtons, table} = useAvailabilitySlotsTable(props);

  const showTitle = props.title || !props.disabled;

  return (
    <CardElement
      title={showTitle ? props.title : undefined}
      icon={<ClockCircleOutlined />}
      subtitle={props.subtitle}
      borderless
      style={{overflow: "hidden"}}
      customButtons={controlButtons}>
      {table}
    </CardElement>
  );
}
