import {
  AvailabilitySlotsTableProps,
  useAvailabilitySlotsTable,
} from "@shared/hooks/useAvailabilitySlotsTable";
import {H3Title} from "@shared/layout/typography";
import React from "react";

export function AvailabilitySlotsTableSimple(props: AvailabilitySlotsTableProps) {
  const {controlButtons, table} = useAvailabilitySlotsTable(props);

  const showTitle = props.title || !props.disabled;

  return (
    <div>
      {showTitle && (
        <div className="header-space-between list-element-header">
          <H3Title>{props.title}</H3Title>
          <div className="containerH buttons-container">{controlButtons}</div>
        </div>
      )}

      {table}
    </div>
  );
}
