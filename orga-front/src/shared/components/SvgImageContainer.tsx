import React, {CSSProperties} from "react";

type SvgReactComponent = React.FunctionComponent<React.SVGProps<SVGSVGElement>>;

export const SvgImageContainer = ({
  svg: SvgImageComp,
  width = "auto",
  height = "auto",
  color = "var(--noe-primary)",
  style,
}: {
  svg: SvgReactComponent;
  width?: CSSProperties["width"];
  height?: CSSProperties["height"];
  color?: CSSProperties["color"];
  style?: CSSProperties;
}) => <SvgImageComp style={{color, width, height, ...style}} />;
