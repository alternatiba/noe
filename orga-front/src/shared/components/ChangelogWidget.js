import React, {useEffect, useState} from "react";
import {Button} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBell} from "@fortawesome/free-regular-svg-icons";
import {useTranslation} from "react-i18next";

const widgetClassName = `whatsNewButton`;
const widgetSelector = `.${widgetClassName}`;

/**
 * Create a Headway widget to display news about NOÉ (https://headwayapp.co)
 */
export default function ChangelogWidget() {
  const {t} = useTranslation();
  const [hasUnreadNews, setHasUnreadNews] = useState(false);

  useEffect(() => {
    const head = document.getElementsByTagName("head")[0];
    const script = document.createElement("script");

    script.type = "text/javascript";
    script.src = "https://cdn.headwayapp.co/widget.js";
    script.async = true;
    script.onload = () => {
      window.Headway.getNewWidget().init({
        selector: widgetSelector,
        account: "JVmrW7",
        trigger: widgetSelector,
        translations: {
          title: t("common:changelog.title"),
          readMore: t("common:changelog.readMore"),
          labels: {
            new: t("common:changelog.labels.new"),
            improvement: t("common:changelog.labels.improvement"),
            fix: t("common:changelog.labels.fix"),
          },
          footer: t("common:changelog.footer"),
        },
        callbacks: {
          onWidgetReady: function (widget) {
            if (widget.getUnseenCount() > 0) setHasUnreadNews(true);
          },
        },
        __component: true,
      });
    };

    head.appendChild(script);
  }, [t]);

  return (
    <div className={"fade-in"} style={{position: "fixed", bottom: 10, right: 10, zIndex: 110}}>
      <Button
        title={t("common:changelog.seeLatestNewsAboutNOE")}
        type={hasUnreadNews ? "primary" : undefined}
        size={hasUnreadNews ? undefined : "small"}
        shape="circle"
        icon={<FontAwesomeIcon icon={faBell} />}
        className={`${widgetClassName} shadow`}
      />
    </div>
  );
}
