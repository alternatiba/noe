import {addKeyToItemsOfList} from "@shared/utils/tableUtilities";
import {Form} from "antd";
import React from "react";
import {TableElement, TableWithTitleProps} from "./TableElement";

/**
 * A Table, plugged to a form field
 * @param name the form field name
 * @param props some props for the table
 * @constructor
 */
export type WithKey<T> = T & {key: number};

export type FormTableProps<T> = {name: string} & TableWithTitleProps<WithKey<T>>;

export function FormTable<T>({name, rowKey, ...props}: FormTableProps<T>) {
  const dataSource = Form.useWatch(name) || [];
  const dataSourceWithKey = addKeyToItemsOfList(dataSource);

  return <TableElement.WithTitle {...props} dataSource={dataSourceWithKey} rowKey={rowKey} />;
}
