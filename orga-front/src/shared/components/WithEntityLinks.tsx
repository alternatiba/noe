import {EyeOutlined} from "@ant-design/icons";
import {Button, Form, Tooltip} from "antd";
import useFormInstance from "antd/es/form/hooks/useFormInstance";
import React, {ReactElement} from "react";
import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router-dom";
import {useNewElementModal} from "../hooks/useNewElementModal";
import {ElementEditProps} from "../pages/EditPage";

type WithEntityLinksProps<T extends {_id: string}> = {
  endpoint: string;
  name: string;
  children: ReactElement;
  createButtonText?: string;
  ElementEdit?: React.FC<ElementEditProps<T>>;
};

export const WithEntityLinks = <T extends {_id: string}>({
  endpoint,
  name,
  children,
  createButtonText,
  ElementEdit,
}: WithEntityLinksProps<T>) => {
  const form = useFormInstance();
  const entityId = Form.useWatch(name, form);
  const navigate = useNavigate();
  const {t} = useTranslation();
  const [setShowNewElementModal, NewElementModal] = useNewElementModal(ElementEdit);

  return (
    <div>
      <div
        className="containerH buttons-container"
        style={{alignItems: "start", flexWrap: "nowrap", marginBottom: 24}}>
        {React.cloneElement(children, {name})}
        {entityId && (
          <Tooltip title={t("common:inputElement.accessToSelectedElement")} placement="topRight">
            <Button
              style={{marginTop: 35, flexGrow: 0, paddingRight: 0, marginRight: 0}}
              type="link"
              onClick={() => navigate(`./../../${endpoint}/${entityId}`)}
              icon={<EyeOutlined />}
            />
          </Tooltip>
        )}
        {createButtonText && NewElementModal && (
          <>
            <Tooltip title={createButtonText} placement="topRight">
              <Button
                type="link"
                onClick={() => setShowNewElementModal(true)}
                style={{
                  marginTop: 35,
                  flexGrow: 0,
                  paddingLeft: 5,
                  paddingRight: 0,
                  marginRight: 0,
                }}>
                {createButtonText.split(" ")[0]}
              </Button>
            </Tooltip>

            <NewElementModal onCreate={(elementId) => form.setFieldValue(name, elementId)} />
          </>
        )}
      </div>
    </div>
  );
};
