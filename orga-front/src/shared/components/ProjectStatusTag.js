import {useTranslation} from "react-i18next";
import {Tag} from "antd";
import React from "react";

export const useProjectStatusesMapping = () => {
  const {t} = useTranslation();

  return {
    demo: {
      label: t("projects:schema.status.options.demo"),
      color: "#ff4d4f",
    },
    example: {
      label: t("projects:schema.status.options.example"),
      color: "cyan",
    },
    pendingDonation: {
      label: t("projects:schema.status.options.pendingDonation"),
      color: "warning",
    },
    validated: {
      label: t("projects:schema.status.options.validated"),
      color: "success",
    },
    validatedForFree: {
      label: t("projects:schema.status.options.validatedForFree"),
      color: "success",
    },
  };
};

export const ProjectStatusTag = ({status}) => {
  const statusesMapping = useProjectStatusesMapping();
  return status ? (
    <Tag color={statusesMapping[status].color}>{statusesMapping[status].label}</Tag>
  ) : null;
};
