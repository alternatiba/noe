import {useTranslation} from "react-i18next";
import {Button, Popconfirm} from "antd";
import {DeleteOutlined, QuestionCircleOutlined} from "@ant-design/icons";
import React from "react";

export const DeleteButton = ({onConfirm, disabled}: {onConfirm: any, disabled?: boolean}) => {
  const {t} = useTranslation();
  return disabled ? (
    <Button style={{flexGrow: 0}} danger disabled type="link" icon={<DeleteOutlined />} />
  ) : (
    <Popconfirm
      title={t("common:areYouSure")}
      placement="topRight"
      onConfirm={onConfirm}
      okText={t("common:yesDelete")}
      okButtonProps={{danger: true}}
      icon={<QuestionCircleOutlined style={{color: "red"}} />}
      cancelText={t("common:no")}>
      <Button style={{flexGrow: 0}} danger type="link" icon={<DeleteOutlined />} />
    </Popconfirm>
  );
};
