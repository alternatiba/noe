import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import React, {useRef} from "react";
import {currentProjectSelectors} from "@features/currentProject";
import {App, Button} from "antd";
import {FilePdfOutlined} from "@ant-design/icons";
import {normalize} from "@shared/utils/stringUtilities";

import {WithTooltipOrNot} from "@shared/components/WithTooltipOrNot";

export const GetPdfPlanningButton = ({
  id, // can be either an id, an array of ids, or the keyword "all"
  elementsActions,
  tooltip,
  noText,
  type,
  customFileName,
  onClick,
  noTooltip,
  ...props
}) => {
  const {message} = App.useApp();
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const downloadButton = useRef();
  const currentProjectName = useSelector(
    (state) => currentProjectSelectors.selectProject(state).name
  );

  tooltip ||= t("common:pdfButton.tooltip");

  const getPdfPlanning = () => {
    if (id === "all") {
      message.loading({
        key: "fetchWithMessagesKey",
        content: t("common:pdfButton.warningAllPlannings"),
        duration: 8,
      });
    }

    dispatch(elementsActions.getPdfPlanning(id))
      .then((pdfFile) => {
        let pdfPlanningUrl = URL.createObjectURL(pdfFile);
        const customResult = customFileName?.();
        const fileName = normalize(
          customResult
            ? t("common:pdfButton.filenameWithCustomName", {
                custom: customResult,
                projectName: currentProjectName,
              })
            : t("common:pdfButton.filename", {projectName: currentProjectName}),
          true
        ).replace(/[^\w ]/g, "-"); // Sanitize the filename

        downloadButton.current.setAttribute("href", pdfPlanningUrl);
        downloadButton.current.setAttribute("download", fileName);
        downloadButton.current.click();
        onClick?.();
      })
      .catch((err) => {
        console.log(err);
        message.error(t("common:pdfButton.pdfCouldNotBeGenerated"));
      });
  };

  return (
    <>
      <a ref={downloadButton} style={{display: "none"}} />
      <WithTooltipOrNot noTooltip={noTooltip} placement="bottomLeft" title={tooltip}>
        <Button
          title={noTooltip && tooltip}
          className={"userTourSubscribedSessionsPdfExport"}
          style={{flexGrow: 0}}
          type={type || "link"}
          onClick={getPdfPlanning}
          disabled={id?.length === 0}
          icon={<FilePdfOutlined />}
          {...props}>
          {!noText && t("common:pdfButton.label")}
        </Button>
      </WithTooltipOrNot>
    </>
  );
};
