import React from "react";
import {Button, Dropdown, Tooltip} from "antd";
import {CalendarOutlined, FileOutlined, GoogleOutlined, WindowsOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import {getFullSessionName} from "@shared/utils/sessionsUtilities";
import dayjs from "@shared/services/dayjs";
import {teamsSelectors} from "@features/teams";
import {useSelector} from "react-redux";
import {URLS} from "@app/configuration";
import {currentProjectSelectors} from "@features/currentProject";
import {dateFormatter} from "@shared/utils/formatters";

export const generateEventDescription = (session) => {
  const summary = session.activity.summary ? `<p>${session.activity.summary}</p>` : "";
  const sessionLink = `<p><a href="${URLS.INSCRIPTION_FRONT}/sessions/${session._id}">Voir la session dans NOÉ</a></p>`;
  const description = session.activity?.description
    ? `<p>---</p>${session.activity.description}`
    : "";
  return summary + sessionLink + description;
};

export const AddSessionToCalendarButton = ({session, ...buttonProps}) => {
  const {t} = useTranslation();
  const teams = useSelector(teamsSelectors.selectList);
  const {sessionNameTemplate, name: projectName} = useSelector(
    currentProjectSelectors.selectProject
  );

  const getEventDetails = () => {
    const title = getFullSessionName(session, sessionNameTemplate, teams) + ` [${projectName}]`;
    const description = generateEventDescription(session);
    const location = session.places?.length
      ? session.places.map((place) => place.name).join(", ")
      : "";
    return {title, description, location};
  };

  const generateGoogleCalendarUrl = (session) => {
    const startDate = dayjs(session.start).utc().format("YYYYMMDDTHHmmss[Z]");
    const endDate = dayjs(session.end).utc().format("YYYYMMDDTHHmmss[Z]");
    const {title, description, location} = getEventDetails(session);
    return (
      `https://www.google.com/calendar/render?action=TEMPLATE` +
      `&text=${encodeURIComponent(title)}` +
      `&dates=${startDate}/${endDate}` +
      `&location=${encodeURIComponent(location)}` +
      `&details=${encodeURIComponent(description)}`
    );
  };

  const generateOutlookCalendarUrl = (session) => {
    const startDate = dayjs(session.start).format("YYYY-MM-DDTHH:mm:ss");
    const endDate = dayjs(session.end).format("YYYY-MM-DDTHH:mm:ss");
    const {title, description, location} = getEventDetails(session);
    return (
      `https://outlook.live.com/calendar/0/deeplink/compose` +
      `?subject=${encodeURIComponent(title)}` +
      `&startdt=${startDate}&enddt=${endDate}` +
      `&body=${encodeURIComponent(description)}` +
      `&location=${encodeURIComponent(location)}`
    );
  };

  const downloadICSFile = (session) => {
    const {title, description, location} = getEventDetails(session);
    const startDate = dayjs(session.start).utc().format("YYYYMMDDTHHmmss[Z]");
    const endDate = dayjs(session.end).utc().format("YYYYMMDDTHHmmss[Z]");
    const icsContent = `BEGIN:VCALENDAR
VERSION:2.0
BEGIN:VEVENT
DTSTART:${startDate}
DTEND:${endDate}
SUMMARY:${title}
DESCRIPTION:${description}
LOCATION:${location}
END:VEVENT
END:VCALENDAR`;
    const blob = new Blob([icsContent], {type: "text/calendar;charset=utf-8"});
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.download = `${title} - ${dateFormatter.longDateTimeRange(session.start, session.end, {
      short: true,
      withYear: true,
    })}.ics`;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const menuItems = [
    {
      key: "google",
      label: t("sessions:addToCalendarButton.google"),
      onClick: () => window.open(generateGoogleCalendarUrl(session), "_blank"),
      icon: <GoogleOutlined />,
    },
    {
      key: "outlook",
      label: t("sessions:addToCalendarButton.outlook"),
      onClick: () => window.open(generateOutlookCalendarUrl(session), "_blank"),
      icon: <WindowsOutlined />,
    },
    {
      key: "ics",
      label: t("sessions:addToCalendarButton.ics"),
      onClick: () => downloadICSFile(session),
      icon: <FileOutlined />,
    },
  ];

  return (
    <Tooltip title={t("sessions:addToCalendarButton.title")} placement="bottom">
      <Dropdown menu={{items: menuItems}} trigger={["click"]}>
        <Button style={{flexGrow: 0}} {...buttonProps}>
          <CalendarOutlined />
        </Button>
      </Dropdown>
    </Tooltip>
  );
};
