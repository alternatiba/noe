import {Button, Popconfirm} from "antd";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {useTranslation} from "react-i18next";

export const ImportProjectAvailabilitiesButton = ({changeAvailabilitySlots}) => {
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  return (
    <Popconfirm
      title={t("projects:importAvailabilities.overwriteWarning")}
      okText={t("projects:importAvailabilities.importAnyway")}
      okButtonProps={{danger: true}}
      cancelText={t("common:cancel")}
      onConfirm={() =>
        changeAvailabilitySlots(
          currentProject.availabilitySlots.map((slot) => {
            const {_id, ...slotWithoutId} = slot;
            return slotWithoutId;
          })
        )
      }>
      <Button type="link">{t("projects:importAvailabilities.importSlots")}</Button>
    </Popconfirm>
  );
};
