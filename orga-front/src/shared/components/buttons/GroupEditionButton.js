import React, {useCallback, useState} from "react";
import {useSelector} from "react-redux";
import {App, Button, Tooltip} from "antd";
import {EditOutlined} from "@ant-design/icons";

import {useNavigate} from "react-router-dom";
import {currentProjectSelectors} from "@features/currentProject";
import {useTranslation} from "react-i18next";
import {ElementSelectionModal} from "@shared/components/ElementSelectionModal";

import {useEntitySchema} from "@shared/hooks/useEntitySchema";
import {sorter} from "@shared/utils/sorters";

export type GroupEditionNavigationState = {
  fieldsToUpdate: Array<{key: string, label: string}>, // The fields key and labels that should be edited
  elements: Array<string>, // The ids of elements to be group edited
};

export default function GroupEditionButton({selectedRowKeys, forceEndpoint}) {
  const {message} = App.useApp();
  const navigate = useNavigate();
  const {t} = useTranslation();
  const [open, setOpen] = useState(false);
  const {usePlaces, useTeams} = useSelector(currentProjectSelectors.selectProject);
  const [selectedFields, setSelectedFields] = useState([]);

  const filterSchema = useCallback(
    (field) => {
      if (
        (!usePlaces && field.usePlaces) ||
        (!useTeams && field.useTeams) ||
        field.noGroupEditing
      ) {
        return false;
      } else {
        return true;
      }
    },
    [usePlaces, useTeams]
  );

  const [entitySchema] = useEntitySchema({
    filterSchema,
    forceEndpoint,
  });

  const rowSelection = {
    onChange: (_, selectedRowObject) => {
      setSelectedFields(selectedRowObject);
    },
  };

  return (
    <>
      <Tooltip title={t("common:listPage.groupedEdition.tooltip")}>
        <Button
          icon={<EditOutlined />}
          type="link"
          disabled={selectedRowKeys.length === 0}
          onClick={() => setOpen(true)}
        />
      </Tooltip>

      <ElementSelectionModal
        title={t("common:listPage.groupedEdition.modalTitle")}
        subtitle={t("common:listPage.groupedEdition.modalSubtitle")}
        open={open}
        onOk={() => {
          selectedFields.length > 0
            ? navigate(`./groupedit`, {
                state: {
                  groupEditing: {
                    fieldsToUpdate: selectedFields,
                    elements: selectedRowKeys,
                  },
                },
              })
            : message.error(t("common:listPage.groupedEdition.selectAtLeastOne"));
        }}
        onCancel={() => setOpen(false)}
        rowSelection={rowSelection}
        selectedRowKeys={selectedFields}
        rowKey="key"
        size="small"
        showHeader={false}
        columns={[
          {
            dataIndex: "label",
            defaultSortOrder: "ascend",
            sorter: (a, b) => sorter.text(a.label, b.label),
          },
        ]}
        dataSource={entitySchema}
      />
    </>
  );
}
