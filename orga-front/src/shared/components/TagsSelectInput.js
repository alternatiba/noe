import {FormItemProps, SelectProps} from "antd";
import {useSelector} from "react-redux";
import {removeDuplicates} from "@shared/utils/utilities";
import {SelectComponent} from "../inputs/SelectInput";
import React from "react";
import {sorter} from "@shared/utils/sorters";
import {FormItem} from "../inputs/FormItem";

export const TagsSelectComponent = ({elementsSelectors, ...props}: FormItemProps<SelectProps>) => {
  const elements = useSelector(elementsSelectors.selectList);
  const tagsOptions = removeDuplicates(
    elements?.reduce((acc, a) => (a[props.name] ? [...a[props.name], ...acc] : acc), [])
  )
    .sort(sorter.text)
    .map((string) => ({
      key: string,
      value: string,
      label: string,
    }));

  return <SelectComponent mode="tags" options={tagsOptions} {...props} />;
};

export const TagsSelectInput = (props: FormItemProps<SelectProps>) => {
  return (
    <FormItem {...props}>
      <TagsSelectComponent name={props.name} bordered={false} />
    </FormItem>
  );
};
