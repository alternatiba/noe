import {useTranslation} from "react-i18next";
import {Tag} from "antd";
import {MailOutlined} from "@ant-design/icons";

export const WaitingInvitationTag = () => {
  const {t} = useTranslation();
  return (
    <Tag icon={<MailOutlined />} color="processing">
      {t("users:waitingInvitation")}
    </Tag>
  );
};
