import {Button, Form} from "antd";
import React, {useState} from "react";
import {useNewElementModal} from "../hooks/useNewElementModal";
import {ElementSelectionModal, ElementSelectionModalProps} from "./ElementSelectionModal";
import {FormTable, FormTableProps} from "./FormTable";

/**
 * Displays a form field as a small table, with a selection modal to select new elements.
 * @param name the field name
 * @param onChange on values change, what to do
 * @param columns the columns to display in the table and in the modal
 * @param allElements all the elements to select from
 * @param filteredAddableElements addable elements will be managed by default, but if you wanna add additional filters on the addable elements, come here.
 * @param ElementEdit an optional component to create new elements
 * @param elementSelectionModalProps some element selection modal props
 * @param props some additional props for the table
 * @returns {JSX.Element}
 * @constructor
 */
type ElementTableWithModalProps<T extends {_id: string}> = {
  name: string;
  onChange: () => void;
  allElements: Array<T>;
  filteredAddableElements?: Array<T>;
  preselectedElements?: FormTableProps<T>["selectedRowKeys"];
  ElementEdit?: React.FC;
  elementSelectionModalProps: Omit<
    ElementSelectionModalProps<T>,
    "open" | "setOpen" | "selectedRowKeys" | "setSelectedRowKeys" | "onOk" | "onCancel"
  > & {createNewElementButtonTitle?: string};
} & FormTableProps<T>;

export const ElementsTableWithModal = <T extends {_id: string}>({
  name,
  onChange,
  columns,
  allElements,
  filteredAddableElements = allElements,
  ElementEdit,
  preselectedElements,
  elementSelectionModalProps,
  ...props
}: ElementTableWithModalProps<T>) => {
  const form = Form.useFormInstance();
  const [elementsToAdd, setElementsToAdd] = useState<Array<T>>([]);
  const [showModal, setShowModal] = useState(false);
  const [setShowNewElementModal, NewElementModal] = useNewElementModal<T>(ElementEdit);

  // Get the selected elements from form data
  const selectedElements: Array<T> = Form.useWatch(name);

  // Get the addable elements (ie. the ones not already selected)
  const addableElements =
    selectedElements === undefined
      ? filteredAddableElements
      : filteredAddableElements.filter((s) => !selectedElements.find((as) => as._id === s._id));

  // Add and remove functions
  const addElements = () => {
    form.setFieldValue(name, [...form.getFieldValue(name), ...elementsToAdd]);
    setElementsToAdd([]);
    onChange?.();
  };
  const removeElement = (element: T) => {
    form.setFieldValue(
      name,
      form.getFieldValue(name).filter((s: T) => s._id !== element._id)
    );
    onChange?.();
  };

  const openModal = () => {
    if (preselectedElements?.length && preselectedElements.length > 0) {
      setElementsToAdd(
        addableElements.filter((el) =>
          preselectedElements.find((preselected) => preselected._id === el._id)
        )
      );
    }
    setShowModal(true);
  };

  return (
    <>
      {/* Insert the table inside a form list to connect it to the Form instance data */}
      <Form.List name={name}>
        {() => (
          <FormTable
            name={name}
            columns={columns}
            onClickButton={openModal}
            onDelete={removeElement}
            {...props}
          />
        )}
      </Form.List>

      <ElementSelectionModal
        open={showModal}
        customButtons={
          elementSelectionModalProps.createNewElementButtonTitle &&
          ElementEdit && (
            <Button type="link" onClick={() => setShowNewElementModal(true)}>
              {elementSelectionModalProps.createNewElementButtonTitle}
            </Button>
          )
        }
        onOk={() => {
          addElements();
          setShowModal(false);
        }}
        onCancel={() => setShowModal(false)}
        {...elementSelectionModalProps}
        columns={elementSelectionModalProps.columns || columns}
        rowSelection={{
          onChange: (selectedRowKeys, selectedRowObject) => setElementsToAdd(selectedRowObject),
        }}
        selectedRowKeys={elementsToAdd}
        dataSource={addableElements}
      />
      <NewElementModal />
    </>
  );
};
