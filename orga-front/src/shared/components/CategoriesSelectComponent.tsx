import {categoriesActions, categoriesSelectors} from "@features/categories";
import {categoriesOptionsWithColorDot} from "@routes/sessions/atoms/categoriesOptionsWithColorDot";
import {Select, SelectProps} from "antd";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {useLoadList} from "../hooks/useLoadList";

export const CategoriesSelectComponent = ({value, ...props}: SelectProps) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const categories = useSelector(categoriesSelectors.selectList);

  useLoadList(() => {
    dispatch(categoriesActions.loadList());
  });

  // Check if categories are available and prepare options for the select input
  const categoriesReady = categories?.length > 0;
  const categoriesOptions = [
    {
      key: "volunteering",
      value: "volunteering",
      label: <em>- {t("sessions:searchControls.categoriesFilter.options.volunteering")} -</em>,
    },
    {
      key: "allTheRest",
      value: "allTheRest",
      label: <em>- {t("sessions:searchControls.categoriesFilter.options.allTheRest")} -</em>,
    },
    ...categoriesOptionsWithColorDot(categories),
  ];

  return (
    <Select
      value={categoriesReady ? value : undefined}
      allowClear
      mode="multiple"
      placeholder={t("sessions:searchControls.categoriesFilter.placeholder")}
      options={categoriesOptions}
      {...props}
      style={{minWidth: 300, maxWidth: 700, ...props.style}}
    />
  );
};
