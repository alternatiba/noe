import {QuestionCircleOutlined} from "@ant-design/icons";

import {SelectInput} from "@shared/inputs/SelectInput";
import {TextInput} from "@shared/inputs/TextInput";
import {flattenFieldComponents} from "@shared/utils/flattenFieldComponents";
import {Alert, Collapse, Form} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import React from "react";
import {Trans, useTranslation} from "react-i18next";
import {CardElement} from "../../CardElement";
import {useFieldCompsFormInstance} from "../atoms/useFieldCompsFormInstance";

export function ConditionalFieldCompConfig({rootName, name}: {rootName: NamePath; name: number}) {
  const {t} = useTranslation();
  const form = useFieldCompsFormInstance();
  const fieldComponents = Form.useWatch(rootName, form);

  return (
    <div className="container-grid" style={{width: "100%"}}>
      <div className="containerH" style={{gap: 8, flexWrap: "wrap", marginBottom: 16}}>
        <SelectInput
          name={[name, "conditional", "show"]}
          placeholder={t("fieldsBuilder:schema.conditional.showPlaceholder")}
          options={[
            {
              value: "show",
              label: t("fieldsBuilder:schema.conditional.show"),
            },
            {
              value: "doNotShow",
              label: t("fieldsBuilder:schema.conditional.doNotShow"),
            },
          ]}
          formItemProps={{style: {marginBottom: 0, minWidth: 100, maxWidth: 300}}}
          allowClear
          popupMatchSelectWidth={false}
        />
        <div className={"containerH"} style={{gap: 8, alignItems: "baseline"}}>
          {t("fieldsBuilder:schema.conditional.whenQuestion")}

          <SelectInput
            name={[name, "conditional", "when"]}
            placeholder={t("fieldsBuilder:schema.conditional.question")}
            formItemProps={{style: {marginBottom: 0, minWidth: 100, maxWidth: 300}}}
            options={flattenFieldComponents(fieldComponents).map((component) => ({
              value: component.key,
              label: component.label,
            }))}
            showSearch
            allowClear
            popupMatchSelectWidth={300}
          />
        </div>

        <div className={"containerH"} style={{gap: 8, alignItems: "baseline", flexGrow: 1}}>
          <span style={{whiteSpace: "nowrap"}}>
            {t("fieldsBuilder:schema.conditional.isEqualTo")}
          </span>
          <TextInput
            name={[name, "conditional", "eq"]}
            placeholder={t("fieldsBuilder:schema.conditional.conditions")}
            inputProps={{style: {width: "100%"}}}
            formItemProps={{style: {marginBottom: 0, width: "100%"}}}
          />
        </div>
      </div>

      <Collapse>
        <Collapse.Panel
          key={1}
          header={
            <>
              <QuestionCircleOutlined style={{marginRight: 8}} />
              {t("fieldsBuilder:schema.conditional.helpForDisplayConditions")}
            </>
          }>
          <ConditionalFieldsHelpContent />
        </Collapse.Panel>
      </Collapse>
    </div>
  );
}

const ConditionalFieldsHelpContent = () => {
  const {t} = useTranslation();
  return (
    <>
      <Alert message={t("fieldsBuilder:displayConditionsHelp.notice")} style={{marginBottom: 26}} />

      <CardElement
        size={"small"}
        title={t("fieldsBuilder:displayConditionsHelp.simpleConditions.title")}>
        <Trans
          ns={"fieldsBuilder"}
          i18nKey={"displayConditionsHelp.simpleConditions.text"}
          components={{exampleText: <span style={{color: "grey"}} />}}
        />
      </CardElement>
      <CardElement
        size={"small"}
        title={t("fieldsBuilder:displayConditionsHelp.advancedConditions.title")}>
        <Trans
          ns={"fieldsBuilder"}
          i18nKey={"displayConditionsHelp.advancedConditions.text"}
          components={{exampleText: <span style={{color: "grey"}} />}}
        />
      </CardElement>
    </>
  );
};
