import {NamePath} from "rc-field-form/es/interface";
import {createDatabaseKey} from "../FieldComp/FieldCompConfig";
import {useFieldCompsFormInstance} from "./useFieldCompsFormInstance";

export const useSafeAutoModifField = (
  rootName: NamePath,
  target: NamePath,
  isNewElement: boolean
) => {
  const form = useFieldCompsFormInstance();

  const targetFieldNamePath = [...rootName, target];

  const onChange = (value: string) => {
    // If the key is not defined in the component value, it means it's a new component.
    // So pre-fill it unless the user touches the field
    if (isNewElement && !form.isFieldTouched(targetFieldNamePath)) {
      form.setFieldValue(targetFieldNamePath, createDatabaseKey(value));
    }
  };

  return onChange;
};
