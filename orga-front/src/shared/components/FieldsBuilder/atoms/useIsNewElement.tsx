import {Form} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import {useEffect, useState} from "react";
import {useFieldCompsFormInstance} from "./useFieldCompsFormInstance";
import {useHasFirstReRendered} from "./useHasFirstReRendered";

export const useIsNewElement = (rootName: NamePath) => {
  const form = useFieldCompsFormInstance();
  const [isNew, setIsNew] = useState(false);

  const isNewFieldNamePath = [...rootName, "isNew"];
  const isNewVal = !!Form.useWatch(isNewFieldNamePath);

  const hasFirstReRendered = useHasFirstReRendered();

  // On second re-render, when isNewVal is know, detect if it is new or not
  useEffect(() => {
    if (isNewVal) {
      setIsNew(true);
      form.setFieldValue(isNewFieldNamePath, undefined);
    }
  }, [isNewVal]);

  // On first render, isNew is false and isNewVal is undefined (cause not known yet.
  // On second re-render, isNewVal is set to the right value, so if this is a new question, isNewVAl is true
  // on third re-render, the isNewVal has been deleted, but now the isNew state is true too.
  // So basically if a question is new, we have isNewElement set to true from the second re-render
  return {
    isNewElement: isNewVal || isNew,
    hasFirstReRendered,
  } as const;
};
