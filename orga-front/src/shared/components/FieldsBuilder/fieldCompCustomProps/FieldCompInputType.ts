export const fieldCompInputTypes = [
  "text",
  "longText",
  "phoneNumber",
  "checkbox",
  "radioGroup",
  "select",
  "checkboxGroup",
  "multiSelect",
  "url",
  "email",
  "number",
  "datetime",
  "day",
] as const;

export const fieldCompLayoutTypes = ["content", "panel"] as const;

export type FieldCompInputType = (typeof fieldCompInputTypes)[number];
export type FieldCompLayoutType = (typeof fieldCompLayoutTypes)[number];
export type FieldCompType = FieldCompInputType | FieldCompLayoutType;
