import {
  faCircleDot,
  faClock,
  faEnvelope,
  faSquareCaretDown as faSquareCaretDownRegular,
} from "@fortawesome/free-regular-svg-icons";
import {
  faCalendarDay,
  faCheck,
  faFont,
  faHashtag,
  faLink,
  faParagraph,
  faPhone,
  faQuoteLeft,
  faSquareCaretDown,
  faSquareCheck,
  faVectorSquare,
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ReactNode} from "react";
import {FieldCompType} from "./FieldCompInputType";

export const fieldCompIcons: Record<FieldCompType, ReactNode> = {
  text: <FontAwesomeIcon icon={faFont} />,
  longText: <FontAwesomeIcon icon={faParagraph} />,
  url: <FontAwesomeIcon icon={faLink} />,
  email: <FontAwesomeIcon icon={faEnvelope} />,
  phoneNumber: <FontAwesomeIcon icon={faPhone} />,
  number: <FontAwesomeIcon icon={faHashtag} />,
  checkbox: <FontAwesomeIcon icon={faCheck} />,
  radioGroup: <FontAwesomeIcon icon={faCircleDot} />,
  select: <FontAwesomeIcon icon={faSquareCaretDownRegular} />,
  checkboxGroup: <FontAwesomeIcon icon={faSquareCheck} />,
  multiSelect: <FontAwesomeIcon icon={faSquareCaretDown} />,
  datetime: <FontAwesomeIcon icon={faClock} />,
  day: <FontAwesomeIcon icon={faCalendarDay} />,
  panel: <FontAwesomeIcon icon={faVectorSquare} />,
  content: <FontAwesomeIcon icon={faQuoteLeft} />,
} as const;
