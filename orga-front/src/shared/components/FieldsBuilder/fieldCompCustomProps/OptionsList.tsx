import {CloseOutlined, PlusOutlined} from "@ant-design/icons";
import {FormItem} from "@shared/inputs/FormItem";
import {TextInput} from "@shared/inputs/TextInput";
import {Button, Divider, Form, FormListFieldData, FormListOperation} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import React from "react";
import {useTranslation} from "react-i18next";
import {useIsNewElement} from "../atoms/useIsNewElement";
import {useSafeAutoModifField} from "../atoms/useSafeAutoModifField";

import {CustomPropsGroup} from "./fieldCompCustomPropsConfig";

const OptionsListComponent = React.memo(
  ({
    rootName,
    optionFields,
    remove,
    name,
  }: {
    rootName: NamePath;
    optionFields: FormListFieldData;
    remove: FormListOperation["remove"];
    name: NamePath;
  }) => {
    const {t} = useTranslation();
    const thisCompRootName = [...rootName, name, "options", optionFields.name];
    const {isNewElement} = useIsNewElement(thisCompRootName);
    const modifValueBasedOnLabel = useSafeAutoModifField(thisCompRootName, "value", isNewElement);

    return (
      <div
        className={"containerH buttons-container"}
        key={optionFields.key}
        style={{flexWrap: "nowrap", gap: 26}}>
        {/* Option Label */}
        <span className={"containerH"} style={{flexBasis: "66%", gap: 4, alignItems: "baseline"}}>
          <span style={{flexShrink: 0, color: "gray"}}>• </span>
          <TextInput
            name={[optionFields.name, "label"]}
            required
            formItemProps={{style: {marginBottom: 0, width: "100%"}}}
            placeholder={t("fieldsBuilder:schema.options.optionLabel")}
            autoFocus
            onChange={(event) => modifValueBasedOnLabel(event.target.value)}
          />
        </span>

        {/* Option Value (always disabled, cannot be edited manually */}
        <span className={"containerH"} style={{flexBasis: "33%", gap: 4, alignItems: "baseline"}}>
          <span style={{flexShrink: 0, color: "gray"}}>
            {t("fieldsBuilder:schema.options.optionKey")}
          </span>
          <TextInput
            name={[optionFields.name, "value"]}
            formItemProps={{style: {marginBottom: 0, opacity: 0.4}}}
            readOnly
            placeholder="-"
          />
        </span>
        <CloseOutlined onClick={() => remove(optionFields.name)} />
      </div>
    );
  }
);

export const OptionsList: CustomPropsGroup = ({rootName, name}) => {
  const {t} = useTranslation();
  const initialOptionValue = {label: "", value: "", isNew: true};

  return (
    <FormItem label={t("fieldsBuilder:schema.options.label")}>
      <Form.List
        name={[name, "options"]}
        rules={[
          {
            validator: async (_, options) => {
              if (!options || options.length < 1) {
                return Promise.reject(
                  new Error(t("fieldsBuilder:schema.options.atLeastOneOptionNeeded"))
                );
              }
            },
          },
        ]}>
        {(optionFieldsList, {add, remove}, {errors}) => (
          <div style={{display: "flex", flexDirection: "column", rowGap: 16, margin: "4px 11px"}}>
            {optionFieldsList.map((optionFields, index) => (
              <React.Fragment key={optionFields.key}>
                {index !== 0 && <Divider style={{margin: 0}} />}
                <OptionsListComponent
                  optionFields={optionFields}
                  remove={remove}
                  name={name}
                  rootName={rootName}
                />
              </React.Fragment>
            ))}
            <Button
              icon={<PlusOutlined />}
              type="dashed"
              onClick={() => add(initialOptionValue)}
              block>
              {t("fieldsBuilder:schema.options.addOption")}
            </Button>
            <Form.ErrorList errors={errors} />
          </div>
        )}
      </Form.List>
    </FormItem>
  );
};
