import {Form, FormInstance} from "antd";
import {useEffect, useMemo, useState} from "react";
import {useTranslation} from "react-i18next";
import {useDebounce} from "../../hooks/useDebounce";
import RegistrationForm from "../RegistrationForm";
import {FieldComp, FieldInputComps} from "./FieldComp/FieldComp";

export const FieldsBuilderLivePreview = ({
  fieldsComponentsForm,
  filterField,
  name,
}: {
  name: string;
  filterField?: (fields: FieldComp) => boolean;
  fieldsComponentsForm: FormInstance<{[key: string]: FieldInputComps}>;
}) => {
  const {t} = useTranslation();
  const fieldCompsWatched = Form.useWatch(name, fieldsComponentsForm);
  const [previewForm] = Form.useForm();

  const [fieldComps, setFieldComps] = useState<Array<FieldComp>>();
  const debouncedSetFormComps = useDebounce(setFieldComps);

  useEffect(() => {
    const displayableFormComponents = fieldCompsWatched?.filter(
      (comp: FieldComp) => comp.key !== "" && (filterField ? filterField(comp) : true)
    );

    // If values are undefined, initialize directly without debounce, else, change with debounce
    if (fieldComps === undefined) setFieldComps(displayableFormComponents);
    else debouncedSetFormComps(displayableFormComponents);
  }, [fieldCompsWatched, filterField]);

  return useMemo(() => {
    return <RegistrationForm formComponents={fieldComps || []} form={previewForm} />;
  }, [fieldComps, previewForm]);
};
