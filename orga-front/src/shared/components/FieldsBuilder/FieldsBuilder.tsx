import {PlusOutlined} from "@ant-design/icons";
import {Stack} from "@shared/layout/Stack";
import {Button, ButtonProps, Dropdown, Form} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import React from "react";
import {useTranslation} from "react-i18next";
import {
  initialContentFieldCompValues,
  initialInputFieldCompValues,
  initialPanelFieldCompValues,
} from "./FieldComp/FieldComp";
import {FieldCompConfig} from "./FieldComp/FieldCompConfig";
import {
  CustomPropsGroup,
  FieldCompCustomPropsConfig,
} from "./fieldCompCustomProps/fieldCompCustomPropsConfig";
import {fieldCompIcons} from "./fieldCompCustomProps/fieldCompIcons";

export type FieldsBuilderI18n = {
  addAQuestion: string;
  newFieldInputComp: string;
  addAPanel: string;
  newFieldPanelComp: string;
  labelLabel: string;
  labelPlaceholder: string;
  typeLabel: string;
  typePlaceholder: string;
};

export type FieldsBuilderConfig = {
  nested?: boolean;
  allowPanel?: boolean;
  allowFreeContent?: boolean;
  allowConditionals?: boolean;
  customPropsConfig: FieldCompCustomPropsConfig;
  customDisplayConfig: CustomPropsGroup;
  customGeneralConfig: CustomPropsGroup;
  i18nOverrides?: Partial<FieldsBuilderI18n>;
  FieldCompTitleExtras?: React.FC<{componentRootName: NamePath}>;
};

export const FieldsBuilder = ({
  rootName,
  name,
  config: {
    nested = false,
    allowPanel = true,
    allowFreeContent = true,
    allowConditionals = true,
    i18nOverrides,
    ...restConfig
  },
}: {
  rootName?: NamePath;
  name: NamePath;
  config: FieldsBuilderConfig;
}) => {
  const {t} = useTranslation();

  const i18n: FieldsBuilderI18n = {
    addAQuestion: i18nOverrides?.addAQuestion || t("fieldsBuilder:addAQuestion"),
    addAPanel: i18nOverrides?.addAPanel || t("fieldsBuilder:addAPanel"),
    newFieldInputComp: i18nOverrides?.newFieldInputComp || t("fieldsBuilder:newFieldInputComp"),
    newFieldPanelComp: i18nOverrides?.newFieldPanelComp || t("fieldsBuilder:newFieldPanelComp"),
    labelLabel: i18nOverrides?.labelLabel || t("fieldsBuilder:schema.label.label"),
    labelPlaceholder:
      i18nOverrides?.labelPlaceholder || t("fieldsBuilder:schema.label.placeholder"),
    typeLabel: i18nOverrides?.typeLabel || t("fieldsBuilder:schema.type.label"),
    typePlaceholder: i18nOverrides?.typePlaceholder || t("fieldsBuilder:schema.type.placeholder"),
  };

  // If rootName is not given, defaults to the name
  rootName ||= Array.isArray(name) ? name : [name];

  const NewCompButton = (props: ButtonProps) => (
    <Button {...props} style={{height: nested ? 52 : 64}} type="dashed" block />
  );

  return (
    <Form.List name={name}>
      {(fields, {add, remove, move}) => (
        <>
          {fields.map(({name, key}, index) => (
            <React.Fragment key={index}>
              {/* Add button in the middle of two questions */}
              {index !== 0 && (
                <div
                  style={{margin: "2px auto", justifyContent: "center"}}
                  className={"containerH reveal-on-hover-container"}>
                  <Dropdown
                    menu={{
                      items: [
                        {
                          label: i18n.addAQuestion,
                          key: "fieldInputComp",
                          onClick: () => add(initialInputFieldCompValues, index),
                        },
                        ...((allowFreeContent && [
                          {
                            label: t("fieldsBuilder:addAFreeContent"),
                            key: "fieldContentComp",
                            onClick: () => add(initialContentFieldCompValues, index),
                          },
                        ]) ||
                          []),
                        ...((allowPanel && [
                          {
                            label: i18n.addAPanel,
                            key: "panelComp",
                            onClick: () => add(initialPanelFieldCompValues, index),
                          },
                        ]) ||
                          []),
                      ],
                    }}
                    trigger={["click"]}>
                    <Button
                      type={"text"}
                      size={"small"}
                      className={"reveal-on-hover-item"}
                      icon={<PlusOutlined />}
                    />
                  </Dropdown>
                </div>
              )}

              {/* Question config component */}
              <FieldCompConfig
                rootName={rootName}
                name={name}
                config={{
                  nested,
                  allowPanel,
                  allowFreeContent,
                  allowConditionals,
                  i18n,
                  ...restConfig,
                }}
                remove={remove}
                moveUp={index !== 0 ? () => move(index, index - 1) : undefined}
                moveDown={index !== fields.length - 1 ? () => move(index, index + 1) : undefined}
              />
            </React.Fragment>
          ))}

          <Stack gap={2} mt={fields.length > 0 ? 3 : 0}>
            <div className={"userTourAddFormQuestion"}>
              <NewCompButton
                onClick={() => add(initialInputFieldCompValues)}
                icon={<PlusOutlined />}>
                {i18n.addAQuestion}
              </NewCompButton>
            </div>

            {(allowFreeContent || allowPanel) && (
              <Stack
                row
                wrap={{xs: "wrap", sm: "nowrap"}}
                gap={2}
                className="userTourAddOtherComponents">
                {allowFreeContent && (
                  <NewCompButton
                    onClick={() => add(initialContentFieldCompValues)}
                    icon={fieldCompIcons.content}>
                    {t("fieldsBuilder:addAFreeContent")}
                  </NewCompButton>
                )}

                {allowPanel && (
                  <NewCompButton
                    onClick={() => add(initialPanelFieldCompValues)}
                    icon={fieldCompIcons.panel}>
                    {i18n.addAPanel}
                  </NewCompButton>
                )}
              </Stack>
            )}
          </Stack>
        </>
      )}
    </Form.List>
  );
};
