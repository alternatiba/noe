import React from "react";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {Progress} from "antd";
import {useTranslation} from "react-i18next";
import {timeFormatter} from "@shared/utils/formatters";

import {WithTooltipOrNot} from "./WithTooltipOrNot";

export default function VolunteeringGauge({registration, style, noTooltip}) {
  const {t} = useTranslation();
  const minMaxVolunteering = useSelector(
    (state) => currentProjectSelectors.selectProject(state)?.minMaxVolunteering
  );

  if (minMaxVolunteering && registration) {
    const [minVolunteering, maxVolunteering] = minMaxVolunteering;
    const {volunteering, numberOfDaysOfPresence} = registration;
    // If number of days of presence is zero, count the volunteering in absolute mode, like if the usesr was present for one day
    const countInAbsoluteMode = numberOfDaysOfPresence === 0;
    const volunteeringWeightedByDay = countInAbsoluteMode
      ? volunteering.weighted
      : volunteering.weighted / numberOfDaysOfPresence;

    if (volunteering?.real || volunteering?.real === 0) {
      const max = minVolunteering + maxVolunteering;

      let color, contextualMessage;
      if (volunteeringWeightedByDay < minVolunteering) {
        color = "red";
        contextualMessage = t("registrations:volunteeringGauge.hint.moreNeeded");
      } else {
        if (volunteeringWeightedByDay > maxVolunteering) {
          color = "dodgerblue";
          contextualMessage = t("registrations:volunteeringGauge.hint.lessNeeded");
        } else {
          color = "#52c41a"; // Same color as success buttons
          contextualMessage = t("registrations:volunteeringGauge.hint.itsPerfect");
        }
      }

      const tooltipMessages = {
        userRecap: countInAbsoluteMode
          ? t("registrations:volunteeringGauge.fullRegistrationRecapAbsolute", {
              total: timeFormatter.duration(volunteering.weighted),
            })
          : t("registrations:volunteeringGauge.fullRegistrationRecap", {
              numberOfDaysOfPresence: numberOfDaysOfPresence,
              total: timeFormatter.duration(volunteering.weighted),
              totalPerDay: timeFormatter.duration(volunteeringWeightedByDay),
            }),
        projectRecap: t("registrations:volunteeringGauge.projectRecap", {
          min: timeFormatter.duration(minVolunteering),
          max: timeFormatter.duration(maxVolunteering),
        }),
      };

      const tooltipMessage = noTooltip ? (
        `${contextualMessage} ${tooltipMessages.userRecap} ${tooltipMessages.projectRecap}`
      ) : (
        <>
          <p>
            <strong>{contextualMessage}</strong>
          </p>
          <p>{tooltipMessages.userRecap}</p>
          {tooltipMessages.projectRecap}
        </>
      );

      let gaugePercentage = (100 * volunteeringWeightedByDay) / max;
      if (gaugePercentage < 1) gaugePercentage = 3; // Always see a little bit of red, even if the jauge is empty

      return (
        <WithTooltipOrNot noTooltip={noTooltip} title={tooltipMessage} placement="right">
          <Progress
            style={style}
            percent={gaugePercentage}
            strokeWidth={15}
            trailColor="rgba(170, 170, 170, 0.30)"
            showInfo={false}
            strokeColor={color}
          />
        </WithTooltipOrNot>
      );
    }
    // Fallback: don't display anything if there is no loaded project
    return null;
  }
}
