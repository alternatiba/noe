import React from "react";
import {useSelector} from "react-redux";
import {viewSelectors} from "@features/view";

export const ChartContainer = React.memo(
  ({chart: Chart, config}) => {
    const darkMode = useSelector(viewSelectors.selectDarkMode);

    config = Object.assign(
      {
        interactions: [{type: "element-active"}],
        theme: darkMode ? "dark" : "default",
        slider: {start: 0, end: 1},
      },
      config
    );

    return <Chart {...config} />;
  },
  (pp, np) => JSON.stringify(pp) === JSON.stringify(np)
);
