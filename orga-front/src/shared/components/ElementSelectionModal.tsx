import {paginationPageSizes} from "@features/view";
import {SearchInFieldsValue, searchInObjectsList} from "@shared/utils/searchInObjectsList";
import {Button, Modal} from "antd";
import Search from "antd/es/input/Search";
import React, {ReactNode, useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {useDebounce} from "../hooks/useDebounce";
import type {SimpleTableProps} from "./TableElement";
import {TableElement} from "./TableElement";

export type ElementSelectionModalProps<T> = {
  title: string;
  subtitle?: string;
  open: boolean;
  onOk: () => void;
  onCancel: () => void;
  large?: boolean;
  customButtons?: ReactNode;
  searchInFields?: SearchInFieldsValue<T>;
  okText?: string;
  dataSource?: Array<T>;
} & SimpleTableProps<T>;

export const ElementSelectionModal = <T,>({
  title,
  subtitle,
  open,
  onOk,
  onCancel,
  large,
  customButtons,
  searchInFields = [],
  okText,
  dataSource,
  ...tableProps
}: ElementSelectionModalProps<T>) => {
  const {t} = useTranslation();
  const [filteredDataSource, setFilteredDataSource] = useState<Array<T> | undefined>([]);
  const [searchValue, setSearchValue] = useState("");

  const debouncedSetFilteredDataSource = useDebounce((searchValue: string, dataSource: Array<T>) =>
    setFilteredDataSource(searchInObjectsList(searchValue, dataSource, searchInFields))
  );

  useEffect(
    () => debouncedSetFilteredDataSource(searchValue, dataSource),
    [dataSource, searchValue]
  );

  return (
    <Modal
      className="element-selection-modal"
      title={title}
      open={open}
      width={large ? "98%" : undefined}
      centered
      footer={
        <>
          {customButtons}
          <Button onClick={onCancel}>{t("common:cancel")}</Button>
          <Button type="primary" onClick={onOk}>
            {okText || t("common:ok")}
          </Button>
        </>
      }
      onCancel={onCancel}
      styles={{body: {margin: -24}}}>
      {(subtitle || searchInFields.length > 0) && (
        <div style={{padding: "10px 24px"}}>
          {subtitle}
          {searchInFields.length > 0 && (
            <Search
              allowClear
              autoFocus
              onChange={(event) => setSearchValue(event.target.value)}
              placeholder={t("common:search")}
            />
          )}
        </div>
      )}

      <TableElement.Simple
        showHeader
        scroll={{
          x: (tableProps.columns.length - 1) * 160 + 70,
          y: window.innerHeight - 260 - (subtitle ? 45 : 0) - (searchInFields.length > 0 ? 45 : 0),
        }}
        pagination={{
          position: ["bottomCenter"],
          pageSize: 40,
          size: "small",
          pageSizeOptions: paginationPageSizes,
          showSizeChanger: true,
        }}
        dataSource={filteredDataSource}
        {...tableProps}
      />
    </Modal>
  );
};
