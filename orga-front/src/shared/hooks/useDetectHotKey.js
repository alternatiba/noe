import isHotkey from "is-hotkey";
import {useEffect, useRef, useState} from "react";

/**
 * @param hotKey the hot key
 * @return {boolean} if the hot key is triggered or not
 */
export const useDetectHotKey = (hotKey) => {
  const isHotKeyPressed = isHotkey(hotKey);
  const [isTriggered, setIsTriggered] = useState(false);
  const hotKeyPressed = useRef();

  const onKeyUpOrDown = (event) => {
    hotKeyPressed.current = isHotKeyPressed(event);
    setTriggered();
  };

  const setTriggered = () => {
    setIsTriggered(hotKeyPressed.current);
  };

  useEffect(() => {
    window.addEventListener("keydown", onKeyUpOrDown);
    window.addEventListener("keyup", onKeyUpOrDown);
    return () => {
      window.removeEventListener("keydown", onKeyUpOrDown);
      window.removeEventListener("keyup", onKeyUpOrDown);
    };
  }, []);

  return isTriggered;
};
