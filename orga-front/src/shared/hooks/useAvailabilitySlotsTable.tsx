import {dateFormatter} from "@shared/utils/formatters";
import {sorter} from "@shared/utils/sorters";
import {
  AvailabilitySlot,
  slotEndIsBeforeBeginning,
  slotOverlapsAnOtherOne,
} from "@utils/slotsUtilities";
import {App, Button, ConfigProvider, Form, Input, Modal} from "antd";
import {ColumnProps} from "antd/es/table";
import React, {ReactNode, useState} from "react";
import {useTranslation} from "react-i18next";
import {TableElement} from "../components/TableElement";
import {DateTimeRangeInput, DateTimeRangeInputProps} from "../inputs/DateTimeRangeInput";
import {FormElement, safeValidateFields} from "../inputs/FormElement";
import {PlusOutlined} from "@ant-design/icons";

export type AvailabilitySlotsTableProps = {
  title: string;
  customButtons?: ReactNode;
  defaultPickerValue?: DateTimeRangeInputProps["defaultPickerValue"];
  disableDatesIfOutOfProject?: boolean; // True: We grey out all the dates out of the project scope / False: only dates in the past
  blockDisabledDates?: boolean; // We prevent from choosing grayed out dates
  disabled?: boolean;
  useMinutes?: boolean;
  onChange?: (availabilitySlots: Array<AvailabilitySlot>) => void;
  availabilitySlots?: Array<AvailabilitySlot>;
};

export const useAvailabilitySlotsTable = ({
  customButtons,
  defaultPickerValue,
  disableDatesIfOutOfProject,
  blockDisabledDates,
  disabled,
  useMinutes,
  onChange,
  availabilitySlots = [],
}: Omit<AvailabilitySlotsTableProps, "title">) => {
  const {message} = App.useApp();
  const {t} = useTranslation();

  const [showAddSlotInput, setShowAddSlotInput] = useState(false);
  const [showUpdateSlotModal, setShowUpdateSlotModal] = useState(false);
  const [addSlotForm] = Form.useForm();
  const [updateSlotForm] = Form.useForm();

  const validateSlotFn =
    (ignoreThisSlot?: AvailabilitySlot) => (_: any, value: AvailabilitySlot) => {
      // If null, OK
      if (value == null) return Promise.resolve();

      // Make various checks
      if (slotOverlapsAnOtherOne(value, availabilitySlots, ignoreThisSlot)) {
        return Promise.reject(t("common:availability.errors.slotOverlapsAnotherOne"));
      }
      if (slotEndIsBeforeBeginning(value)) {
        return Promise.reject(t("common:availability.errors.slotEndIsBeforeBeginning"));
      }

      // If it passes, it's ok
      return Promise.resolve();
    };

  const updateSlot = () =>
    safeValidateFields(
      updateSlotForm,
      () => {
        const updatedSlot: AvailabilitySlot = updateSlotForm.getFieldValue("slot");

        const oldSlot: AvailabilitySlot = updateSlotForm.getFieldValue("oldSlot");
        let newData: Array<AvailabilitySlot> = [...availabilitySlots];
        const slotIndexToModify = availabilitySlots.findIndex(
          (s) => s.start === oldSlot.start && s.end === oldSlot.end
        );
        newData[slotIndexToModify] = updatedSlot;
        updateSlotForm.resetFields();
        onChange?.(newData);

        setShowUpdateSlotModal(false);
      },
      () => message.error(t("common:availability.errors.oopsDateIncorrect"))
    );

  const addSlot = () =>
    safeValidateFields(
      addSlotForm,
      () => {
        const slot: AvailabilitySlot = addSlotForm.getFieldValue("slot");

        const newData: Array<AvailabilitySlot> = [...availabilitySlots, slot];
        addSlotForm.resetFields();
        onChange?.(newData);
      },
      () => message.error(t("common:availability.errors.oopsDateIncorrect"))
    );

  const onEditSlot = (record: AvailabilitySlot) => {
    updateSlotForm.setFieldsValue({
      slot: record,
      oldSlot: record,
    });
    setShowUpdateSlotModal(true);
  };

  const onDeleteSlot = (removedSlot: AvailabilitySlot) => {
    const newData = availabilitySlots.filter(
      (slot) => !(slot.start === removedSlot.start && slot.end === removedSlot.end)
    );
    onChange?.(newData);
  };

  const columns: Array<ColumnProps<AvailabilitySlot>> = [
    {
      title: t("common:availability.startEnd"),
      key: "dateRange",
      sorter: (a, b) => sorter.date(a.start, b.start),
      defaultSortOrder: "ascend",
      render: (text, record) =>
        dateFormatter.longDateTimeRange(record.start, record.end, {short: false, withYear: true}),
    },
  ];

  const datePickerProps = {
    useMinutes,
    defaultPickerValue,
    disableDatesIfOutOfProject,
    blockDisabledDates,
  };

  const controlButtons = (
    <>
      {customButtons}
      {!disabled && (
        <FormElement form={addSlotForm} style={{flexGrow: 1}}>
          <div className="containerH buttons-container">
            {showAddSlotInput && (
              <>
                <DateTimeRangeInput
                  autoFocus
                  bordered
                  name="slot"
                  {...datePickerProps}
                  required
                  rules={[{validator: validateSlotFn()}]}
                />

                <Button onClick={addSlot}>{t("common:add")}</Button>
              </>
            )}
            {availabilitySlots?.length > 0 && !showAddSlotInput && (
              <Button onClick={() => setShowAddSlotInput(true)} icon={<PlusOutlined />}>
                {t("common:availability.addNewSlots")}
              </Button>
            )}
          </div>
        </FormElement>
      )}
    </>
  );

  const table = (
    <>
      <ConfigProvider
        renderEmpty={
          !showAddSlotInput && !disabled
            ? () => (
                <Button onClick={() => setShowAddSlotInput(true)} icon={<PlusOutlined />}>
                  {t("common:availability.addNewSlots")}
                </Button>
              )
            : undefined
        }>
        <TableElement.Simple
          columns={columns}
          rowKey="start"
          scroll={{x: 300, y: "50vh"}}
          dataSource={availabilitySlots}
          onEdit={!disabled ? onEditSlot : undefined}
          onDelete={!disabled ? onDeleteSlot : undefined}
        />
      </ConfigProvider>

      <Modal
        title={t("common:availability.modifyASlot")}
        open={showUpdateSlotModal}
        onOk={updateSlot}
        onCancel={() => setShowUpdateSlotModal(false)}>
        <FormElement form={updateSlotForm}>
          <div className="containerH" style={{flexWrap: "wrap"}}>
            <DateTimeRangeInput
              label={t("common:availability.slot")}
              name="slot"
              bordered
              {...datePickerProps}
              required
              rules={[{validator: validateSlotFn(updateSlotForm.getFieldValue("oldSlot"))}]}
            />
            <div style={{display: "none"}}>
              <Form.Item hidden name="oldSlot">
                <Input />
              </Form.Item>
            </div>
          </div>
        </FormElement>
      </Modal>
    </>
  );

  return {controlButtons, table};
};
