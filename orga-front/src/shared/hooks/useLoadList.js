import {useEffect} from "react";

export const useLoadList = (loader) => {
  useEffect(() => {
    // First run initialization
    loader();

    // TODO check if there is a need to have auto refresh or not. Maybe the websocket way to do is enough ? For now, let's comment that.
    //
    // const loadIfActive = () => {
    //   // Only trigger load if the page is active
    //   if (!document.hidden) loader();
    // };
    //
    // // Reload changed data every X seconds when the page is open...
    // const intervalId = setInterval(loadIfActive, AUTO_RELOAD_SECONDS * 5 * 1000);
    //
    // // ... And reload changed data each time the page has been inactive and becomes active again.
    // window.addEventListener("visibilitychange", loadIfActive);
    //
    // // Remove listener and interval on unmount
    // return () => {
    //   window.removeEventListener("visibilitychange", loadIfActive);
    //   clearInterval(intervalId);
    // };
  }, []);
};
