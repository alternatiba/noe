import {paginationPageSizes} from "@features/view";
import {useLocalStorageState} from "@shared/utils/localStorageUtilities";
import type {PaginationProps} from "antd";
import {useTranslation} from "react-i18next";

export const usePagination = (columnsFilteredDataSource: Array<any> | undefined) => {
  const [preferredTablePageSize, setPreferredTablePageSize] = useLocalStorageState(
    "preferredTablePageSize",
    10
  );
  const {t} = useTranslation();

  return {
    position: ["bottomCenter"],
    defaultPageSize: preferredTablePageSize,
    size: "small",
    pageSizeOptions: paginationPageSizes,
    total: columnsFilteredDataSource?.length,
    showSizeChanger: true,
    showTotal: (total, range) =>
      t("common:listPage.xtoYOnZElements", {
        from: range[0],
        to: range[1] > total ? total : range[1],
        total,
        context: "short",
      }),
    onShowSizeChange: (current, size) => setPreferredTablePageSize(size),
  } as PaginationProps;
};
