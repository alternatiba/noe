import {useTranslation} from "react-i18next";
import {dateFormatter} from "@shared/utils/formatters";
import {SwapOutlined} from "@ant-design/icons";

export const useRenderHtmlTicket = () => {
  const {t} = useTranslation();

  return (ticket) =>
    ticket.transferInfos ? (
      <div
        title={
          ticket.name +
          "\n\n" +
          t("registrations:ticketing.transfer.transferredFromEmailOnDate", {
            fromEmail: ticket.transferInfos.fromEmail,
            date: dateFormatter.longDateTime(ticket.transferInfos.date, {
              displayInTimeZone: "user",
            }),
          })
        }
        style={{display: "inline", wordBreak: "break-all"}}>
        <span style={{color: "gray", opacity: 0.8}}>
          <SwapOutlined />
        </span>{" "}
        {ticket.id}
      </div>
    ) : (
      <div title={ticket.name} style={{display: "inline", wordBreak: "break-all"}}>
        {ticket.id}
      </div>
    );
};
