import {
  browserTracingIntegration,
  createReduxEnhancer,
  init,
  replayIntegration,
  setUser,
  withProfiler,
} from "@sentry/react";

export const initSentry = (tracesSampleRate = 0.02) =>
  process.env.NODE_ENV === "production" &&
  init({
    dsn: process.env.REACT_APP_SENTRY_DSN,
    environment: process.env.REACT_APP_SENTRY_ENVIRONMENT,
    tunnel: "/sentry-tunnel",

    // Sentry Tracing: performance and links between front an back transactions
    tracesSampleRate,

    // Sentry replay feature: replay stuff when there is an error
    replaysOnErrorSampleRate: 1.0,

    // Add soe depth for the Redux state to be recorded
    normalizeDepth: 10,

    integrations: [browserTracingIntegration(), replayIntegration()],

    // Ignore some Error types to prevent cluttering Sentry with bad errors.
    // Inspired by https://gist.github.com/impressiver/5092952
    ignoreErrors: [
      // ResizeObserverLoops
      "ResizeObserver loop limit exceeded",
      "ResizeObserver loop completed with undelivered notifications.",
      // Random plugins/extensions
      "top.GLOBALS",
      // See: http://blog.errorception.com/2012/03/tale-of-unfindable-js-error.html
      "originalCreateNotification",
      "canvas.contentDocument",
      "MyApp_RemoveAllHighlights",
      "http://tt.epicplay.com",
      "Can't find variable: ZiteReader",
      "jigsaw is not defined",
      "ComboSearch is not defined",
      "http://loading.retry.widdit.com/",
      "atomicFindClose",
      // Facebook borked
      "fb_xd_fragment",
      // ISP "optimizing" proxy - `Cache-Control: no-transform` seems to reduce this. (thanks @acdha)
      // See http://stackoverflow.com/questions/4113268/how-to-stop-javascript-injection-from-vodafone-proxy
      "bmi_SafeAddOnload",
      "EBCallBackMessageReceived",
      // See http://toolbar.conduit.com/Developer/HtmlAndGadget/Methods/JSInjection.aspx
      "conduitPage",
      // Generic error code from errors outside the security sandbox
      // You can delete this if using raven.js > 1.0, which ignores these automatically.
      "Script error.",
      // Avast extension error
      "_avast_submit",
      // CSS chunk load errors (even if that is not optimal)
      "Loading CSS chunk",
    ],
    denyUrls: [
      // Google Adsense
      /pagead\/js/i,
      // Facebook flakiness
      /graph\.facebook\.com/i,
      // Facebook blocked
      /connect\.facebook\.net\/en_US\/all\.js/i,
      // Woopra flakiness
      /eatdifferent\.com\.woopra-ns\.com/i,
      /static\.woopra\.com\/js\/woopra\.js/i,
      // Chrome extensions
      /extensions\//i,
      /^chrome:\/\//i,
      // Other plugins
      /127\.0\.0\.1:4001\/isrunning/i, // Cacaoweb
      /webappstoolbarba\.texthelp\.com\//i,
      /metrics\.itunes\.apple\.com\.edgesuite\.net\//i,
    ],
  });

export const getSentryReduxEnhancer = () =>
  createReduxEnhancer({
    stateTransformer: (state) => {
      // If we do it like that, it's because there is a reason.... :)

      try {
        // Transform the state to remove sensitive information
        let transformedState = {...state};

        if (transformedState.currentUser?.user)
          transformedState = Object.assign({}, transformedState, {
            currentUser: {
              ...transformedState.currentUser,
              user: {...transformedState.currentUser.user, xsrfToken: null},
            },
          });

        if (transformedState.currentProject?.project) {
          if (transformedState.currentProject.project.helloAsso)
            transformedState = Object.assign({}, transformedState, {
              currentProject: {
                ...transformedState.currentProject,
                project: {
                  ...transformedState.currentProject.project,
                  helloAsso: {
                    ...transformedState.currentProject.project.helloAsso,
                    clientId: null,
                    clientSecret: null,
                  },
                },
              },
            });

          if (transformedState.currentProject.project.tiBillet)
            transformedState = Object.assign({}, transformedState, {
              currentProject: {
                ...transformedState.currentProject,
                project: {
                  ...transformedState.currentProject.project,
                  tiBillet: {
                    ...transformedState.currentProject.project.tiBillet,
                    apiKey: null,
                  },
                },
              },
            });
        }
        return transformedState;
      } catch (e) {
        console.error(
          "Error when transforming Redux state for Sentry. Sending without currentUser and currentProjet.",
          e
        );
        return {...state, currentUser: null, currentProject: null};
      }
    },
  });

export const changeSentryUser = (user: {email?: string; _id?: string}) =>
  setUser(user?.email ? {email: user.email, id: user._id} : null);

export const withSentryProfiler = withProfiler;
