export const addKeyToItemsOfList = (data, keyName = undefined) => {
  return data ? data?.map((t, i) => ({...t, key: keyName ? t[keyName] : i})) : [];
};

export const fieldToData = (fields, acc = {}) => {
  return fields.reduce((accumulator, currentValue) => {
    accumulator[currentValue.name] = currentValue.value;
    return accumulator;
  }, acc);
};

export const dataToFields = (data, formatFieldsFn = undefined) => {
  let clone = {...data};

  formatFieldsFn && formatFieldsFn(clone);

  const fields = Object.keys(clone).reduce((accumulator, currentValue) => {
    accumulator.push({name: currentValue, value: clone[currentValue]});
    return accumulator;
  }, []);
  return fields;
};
