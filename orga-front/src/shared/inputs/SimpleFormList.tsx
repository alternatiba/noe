import {MinusCircleOutlined, PlusOutlined} from "@ant-design/icons";
import {Button, Divider, Form} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import React, {ReactNode} from "react";

export const SimpleFormList = ({
  name,
  noItemText,
  addButtonText,
  children,
  maxHeight,
  getDefaultValue,
}: {
  name: NamePath;
  noItemText: string;
  addButtonText: string;
  children: (name: number) => ReactNode;
  maxHeight?: number;
  getDefaultValue?: () => Record<string, any>;
}) => {
  return (
    <Form.List name={name}>
      {(fields, {add, remove}) => (
        <>
          {/* Add button */}
          <Button onClick={() => add(getDefaultValue?.() || undefined, 0)} icon={<PlusOutlined />}>
            {addButtonText}
          </Button>

          {fields.length > 0 ? (
            <>
              {fields.length > 0 && <Divider />}
              <div style={{maxHeight, overflowY: "auto"}}>
                {fields.map(({key, name}, index) => (
                  <div key={key} className={"containerV"}>
                    {/* Divider between lines */}
                    {index !== 0 && <Divider style={{marginTop: 0}} />}

                    {/* Form list line */}
                    <div className="containerH" style={{alignItems: "top", columnGap: 26}}>
                      {/* Form list fields on one side */}
                      <div style={{width: "100%"}}>{children(name)}</div>

                      {/* Delete button on the other */}
                      <Button
                        type="link"
                        style={{flexGrow: 0, flexShrink: 0, marginRight: 8}}
                        danger
                        icon={<MinusCircleOutlined />}
                        onClick={() => remove(name)}
                      />
                    </div>
                  </div>
                ))}
              </div>
            </>
          ) : (
            <div style={{color: "grey", marginTop: 16}}>{noItemText}</div>
          )}
        </>
      )}
    </Form.List>
  );
};
