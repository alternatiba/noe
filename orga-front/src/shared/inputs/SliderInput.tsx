import {Slider} from "antd";
import type {SliderRangeProps, SliderSingleProps} from "antd/es/slider";
import {FormItem, FormItemProps} from "./FormItem";

export const SliderInput = (props: FormItemProps<SliderSingleProps | SliderRangeProps>) => (
  <FormItem {...props}>
    <Slider />
  </FormItem>
);
