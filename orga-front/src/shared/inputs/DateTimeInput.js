import dayjs, {ConfigType, dayjsBase} from "@shared/services/dayjs";
import {useSelector} from "react-redux";
import {DatePicker, DatePickerProps} from "antd";
import {ReactNode} from "react";
import {FormItem, FormItemProps} from "./FormItem";
import {currentProjectSelectors} from "@features/currentProject";
import {useWindowDimensions} from "../hooks/useWindowDimensions";
import type {RangePickerProps} from "antd/es/date-picker";
import {smoothScrollIntoView} from "@shared/utils/smoothScrollIntoView";
import {timezoneDiffManager} from "@shared/utils/timezoneDiffManager";

// The date picker will ALWAYS return dates as strings.
type DateInputProps = {
  isRange?: boolean,
  DatePickerComponent?: ReactNode,
  blockDisabledDates?: boolean,
  disableDatesIfOutOfProject?: boolean,
  disableDatesBeforeNow?: boolean,
  useMinutes?: boolean,
  value?: ConfigType | {start: ConfigType, end: ConfigType},
  defaultPickerValue?: ConfigType,
};

export type DateTimeInputProps = Omit<DateInputProps, "isRange" | "DatePickerComponent">;

const disabledDateBeforeNow = (current) => {
  const now = dayjs();
  return current && current.isBefore(now, "day");
};

const disabledDateOutOfProject = (current, project) => {
  const start = dayjs(project.start),
    end = dayjs(project.end);
  return current && (current.isBefore(start, "day") || current.isAfter(end, "day"));
};

const isADisabledDate = (project, disableDatesIfOutOfProject, disableDatesBeforeNow) => {
  return (current) => {
    return (
      (disableDatesIfOutOfProject ? disabledDateOutOfProject(current, project) : false) ||
      (disableDatesBeforeNow !== false ? disabledDateBeforeNow(current) : false)
    );
  };
};

const setGreyedOutDates =
  (project, disableDatesIfOutOfProject, disableDatesBeforeNow) => (current) => {
    const disabled = isADisabledDate(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    )(current);
    return (
      <div className={`ant-picker-cell-inner ${disabled ? "disabled-date" : undefined}`}>
        {current.date()}
      </div>
    );
  };

/**
 * Raw date picker input
 */
export function DatePickerComponent({
  isRange = false,
  DatePickerComponent = DatePicker, //Display range picker or date picker
  blockDisabledDates, // Block dates out of the project
  disableDatesIfOutOfProject, // grey out either only days before today, or also the dates out of the project
  disableDatesBeforeNow,
  useMinutes = true,
  popupClassName,
  value,
  defaultPickerValue,
  ...props
}: (DatePickerProps | RangePickerProps) & DateInputProps) {
  const {isMobileView} = useWindowDimensions();
  const project = useSelector(currentProjectSelectors.selectProject);

  const convert = (val) => dayjsBase(timezoneDiffManager.set(val));

  // Make sure those are dayjs dates.
  // If range, then use the {start, end} format
  if (isRange && value) value = [value?.start, value?.end]?.map(convert);
  else if (value) value = convert(value);
  if (isRange && defaultPickerValue) defaultPickerValue = [convert(defaultPickerValue)];
  else if (defaultPickerValue) defaultPickerValue = convert(defaultPickerValue);

  const cleanDateValue = (value) => {
    value = value.set("millisecond", 0).set("second", 0);
    if (!useMinutes) value = value.set("minute", 0);
    return value;
  };

  // If we don't use minutes, display them like there are "00"
  let timeFormat = dayjsBase.localeData().longDateFormat("LT");
  // If we don't use minutes, just show zeros instead
  if (!useMinutes) timeFormat = timeFormat.replaceAll("m", "0");

  const defaultProps = {
    ...(isMobileView && {inputReadOnly: true}), // Disable virtual keyboard from appearing on mobile
    bordered: false,
    format: `${dayjsBase.localeData().longDateFormat("LL")} ${timeFormat}`,
    showTime: {minuteStep: 5, format: timeFormat, showToday: false},
    popupClassName: "date-picker-panel" + (popupClassName ? ` ${popupClassName}` : ""),
    showNow: props.showNow ?? false,
    onOpenChange: function (open) {
      open &&
        setTimeout(() => {
          const popups = document.getElementsByClassName("date-picker-panel");
          for (const popupElement of popups) smoothScrollIntoView(popupElement);
        }, 300);
    },
  };

  const onChange = (value) => {
    // If a value is given, clean it
    if (value !== null) {
      if (isRange) {
        value = value?.map((v) => timezoneDiffManager.unset(cleanDateValue(v).toISOString()));
        value = {start: value[0], end: value[1]};
      } else {
        value = timezoneDiffManager.unset(cleanDateValue(value).toISOString());
      }
    }
    props.onChange(value);
  };

  if (blockDisabledDates) {
    defaultProps.disabledDate = isADisabledDate(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    );
  } else {
    defaultProps.dateRender = setGreyedOutDates(
      project,
      disableDatesIfOutOfProject,
      disableDatesBeforeNow
    );
  }

  return (
    <DatePickerComponent
      {...defaultProps}
      {...props}
      value={value}
      defaultPickerValue={defaultPickerValue}
      onChange={onChange}
    />
  );
}

/**
 * Basic Datetime input
 */
export const DateTimeInput = (props: FormItemProps<DateTimeInputProps & DatePickerProps>) => (
  <FormItem {...props}>
    <DatePickerComponent />
  </FormItem>
);
