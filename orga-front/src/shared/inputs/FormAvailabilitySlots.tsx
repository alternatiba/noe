import {AvailabilitySlotsTableCard} from "@shared/components/AvailabilitySlotsTable/AvailabilitySlotsTableCard";
import {ImportProjectAvailabilitiesButton} from "@shared/components/buttons/ImportProjectAvailabilitiesButton";
import {NamePath} from "rc-field-form/es/interface";
import {AvailabilitySlotsTableProps} from "../hooks/useAvailabilitySlotsTable";
import {FormItem} from "./FormItem";

type FormAvailabilitySlotsProps = {
  label: string;
  name: NamePath;
  allowImportProjectDates?: boolean;
} & Omit<AvailabilitySlotsTableProps, "title">;

/**
 * Form wrapper for Availability Slots table. We need to refactor the AvailabilitySlotsTable component anyways at some point
 */
export const FormAvailabilitySlots = ({
  label,
  name,
  allowImportProjectDates = false,
  ...availabilitySlotsTableProps
}: FormAvailabilitySlotsProps) => {
  const InnerAvailabilitySlots = ({
    value,
    onChange,
  }: {
    value?: AvailabilitySlotsTableProps["availabilitySlots"];
    onChange?: AvailabilitySlotsTableProps["onChange"];
  }) => {
    return (
      <AvailabilitySlotsTableCard
        title={label}
        onChange={onChange}
        customButtons={
          allowImportProjectDates ? (
            <ImportProjectAvailabilitiesButton changeAvailabilitySlots={onChange} />
          ) : undefined
        }
        availabilitySlots={value}
        {...availabilitySlotsTableProps}
      />
    );
  };

  return (
    <FormItem name={name} formItemProps={{style: {marginBottom: 0}}}>
      <InnerAvailabilitySlots />
    </FormItem>
  );
};
