import {ColorPicker} from "antd";
import {FormItem, FormItemProps} from "./FormItem";
import {ColorPickerProps} from "antd/es/color-picker/ColorPicker";

export const ColorInput = (props: FormItemProps<ColorPickerProps>) => (
  <FormItem
    {...{
      ...props,
      formItemProps: {
        normalize: (value) => {
          let hexVal = typeof value === "string" ? value : value?.toHex();
          if (hexVal.length === 8 && hexVal.substring(6, 8) === "00") return undefined;
          if (hexVal) return "#" + hexVal.substring(0, 6);
        },
        ...props.formItemProps,
      },
    }}>
    <ColorPicker allowClear disabledAlpha />
  </FormItem>
);
