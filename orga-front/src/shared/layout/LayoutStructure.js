import {Suspense} from "react";
import {Layout} from "antd";
import {useWindowDimensions} from "../hooks/useWindowDimensions";
import {useTranslation} from "react-i18next";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import Sidebar from "./Sidebar";
import {ErrorBoundary} from "../components/ErrorBoundary";
import {OfflineModeBanner} from "@shared/utils/offlineModeUtilities";
import {AnnouncementMessageBanner} from "@shared/components/AnnouncementMessageBanner/AnnouncementMessageBanner";

const MobileNavbar = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "low" */
    "./MobileNavbar"
  )
);
const SidebarMenuContent = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "low" */
    "./SidebarMenuContent"
  )
);

const {Content} = Layout;

export function LayoutStructure({
  title,
  ribbon,
  menu,
  showSocialIcons = true,
  children,
  profileUser,
  displayButtonBadge,
  collapsedSidebar,
}) {
  const {t} = useTranslation();
  const {isMobileView} = useWindowDimensions();

  const menuComponent = title && (
    <Suspense fallback={null}>
      <SidebarMenuContent
        collapsedSidebar={collapsedSidebar}
        showSocialIcons={showSocialIcons}
        profileUser={profileUser}
        menu={menu}
      />
    </Suspense>
  );

  return (
    <Layout>
      {isMobileView && (
        <Suspense fallback={null}>
          <MobileNavbar displayButtonBadge={displayButtonBadge} title={title} ribbon={ribbon}>
            {menuComponent}
          </MobileNavbar>
        </Suspense>
      )}
      {/* Page content: sidebar and main content */}
      <Layout>
        {!isMobileView && (
          <Sidebar forceCollapsed={collapsedSidebar} title={title} ribbon={ribbon}>
            {menuComponent}
          </Sidebar>
        )}

        <ErrorBoundary>
          <Content className="page-content">
            <AnnouncementMessageBanner />
            <OfflineModeBanner />

            {title && children}
          </Content>
        </ErrorBoundary>
      </Layout>
    </Layout>
  );
}
