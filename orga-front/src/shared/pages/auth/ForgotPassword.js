import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserActions, currentUserSelectors} from "@features/currentUser";
import {Button, Form} from "antd";
import {TextInputEmail} from "../../inputs/TextInput";
import {AuthPage} from "../AuthPage";
import {viewSelectors} from "@features/view";
import {useTranslation} from "react-i18next";
import {safeValidateFields} from "../../inputs/FormElement";
import {useNavigate} from "react-router-dom";

export default function ForgotPassword({footer}) {
  const navigate = useNavigate();
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const dispatch = useDispatch();
  const [hideSendEmailButton, setHideSendEmailButton] = useState(false);

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changeLogin(allFields[0].value));
  };

  const sendPasswordRecoveryMail = () => {
    safeValidateFields(form, () => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.changeConnectionNotice(undefined));
      dispatch(currentUserActions.forgotPassword());
      setHideSendEmailButton(true);
    });
  };

  const goToLogInPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("./../login");
  };

  return (
    <AuthPage
      form={form}
      dataFormType={"forgot_password"}
      footer={footer}
      subtitle={t("common:connectionPage.forgottenPassword")}
      initialValues={{
        email: currentUser.email || searchParamsInfo.email,
      }}
      onFieldsChange={onChange}
      onValidate={sendPasswordRecoveryMail}
      buttons={
        <>
          {!hideSendEmailButton && (
            <Button type="primary" onClick={sendPasswordRecoveryMail} htmlType="submit">
              {t("common:connectionPage.sendPasswordRecoveryEmail")}
            </Button>
          )}
          <Button type="link" onClick={goToLogInPage}>
            {t("common:connectionPage.backToAuthPage")}
          </Button>
        </>
      }>
      <TextInputEmail
        label={t("common:connectionPage.recoveryEmail.label")}
        name="email"
        placeholder={t("common:connectionPage.recoveryEmail.placeholder")}
        required
        bordered
      />
    </AuthPage>
  );
}
