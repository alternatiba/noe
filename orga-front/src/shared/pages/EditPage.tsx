import {CheckOutlined, CopyOutlined, PlusOutlined} from "@ant-design/icons";
import {viewActions, viewSelectors} from "@features/view";
import {DeleteButton} from "@shared/components/buttons/DeleteButton";
import {GroupEditionNavigationState} from "@shared/components/buttons/GroupEditionButton";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {listOfClickableElements} from "@shared/utils/listOfClickableElements";
import {OFFLINE_MODE} from "@shared/utils/offlineModeUtilities";
import {pick} from "@shared/utils/utilities";
import {Alert, App, Button, Collapse, Form, FormInstance, FormProps, Modal, Tooltip} from "antd";
import {ReactNode, useEffect} from "react";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import {PageHeading} from "../components/PageHeading";
import {PendingSuspense} from "../components/Pending";
import {FormElement} from "../inputs/FormElement";

const ModificationsHistoryTimeline = lazyWithRetry(
  () => import(/* webpackPrefetch: true */ "../components/ModificationsHistoryTimeline.js")
);

export type EditPageProps<T extends {_id: string} & Record<string, any>> = {
  title?: string;
  icon?: ReactNode;

  // The optional namespace for translations
  i18nNs?: string;

  // The object we want to modify
  record: T;

  // The redux actions for this element
  elementsActions: {
    loadEditingHistory?: (entityId: string) => any; // Optional: if the redux action exists, we're able to display the history timeline
    persist: (fieldsToUpdate?: Partial<T>) => Promise<string>; // The redux action to persist the modifications on the object
    remove: (entityId?: string) => void; // The redux action to persist the modifications on the object
  };

  // function activated when the user validates the form  onFieldsChange. Returns the id of the newly created element
  onValidation?: (formData: any, fieldKeysToUpdate?: Array<string>) => Promise<string>;

  // A function to be called to post transform the validated object
  postTransform?: (record: any) => any;

  // Display the delete button or not
  deletable?: boolean;

  // Display a back button, or not
  backButton?: boolean;
  // The path where to navigate after the validation. Special values false means no navigate.
  goBackAfterAction?: boolean;

  // custom buttons if you need to display some
  customButtons?: ReactNode;

  // Display the "Create and stay" button
  createAndStayButton?: boolean;

  // Action to do with the newly created element id
  onCreate?: (elementId: string) => void;

  // Enables the cloning feature (and displays the cloning button)
  clonable?: boolean;
  // Tells if we are currently cloning the element
  clonedElement?: Omit<T, "_id"> & {_id: "new"};

  // If you wanna pass a Form instance directly
  form?: FormInstance<T>;
  // The payload to give to the EditPage if you wanna group edit some elements
  groupEditing?: GroupEditionNavigationState;

  // Tell if the EditPage is displayed in a modal or not
  asModal?: boolean;
  // If the EditPage modal is open or not
  modalOpen?: boolean;
  // The function to set if the modal is open or not
  setModalOpen?: (modalOpen: boolean) => void;

  // Content of the edit page form
  children: ReactNode;
} & FormProps; // props for the Form Element if needed

export type ElementEditProps<T extends {_id: string}> = {
  id?: string;
} & Pick<EditPageProps<T>, "asModal" | "modalOpen" | "setModalOpen" | "onCreate">;

const buttonIconMapping = {
  create: <PlusOutlined />,
  clone: <CopyOutlined />,
  edit: <CheckOutlined />,
  groupEdit: <CheckOutlined />,
};

export function EditPage<T extends {_id: string} & Record<string, any>>({
  title,
  icon,
  i18nNs,

  record,
  elementsActions,
  onValidation,
  postTransform,

  deletable = false,
  backButton = true,
  goBackAfterAction = true,
  customButtons,
  createAndStayButton = true,
  onCreate,

  clonable,
  clonedElement,

  form: formOverride,
  groupEditing,

  asModal,
  modalOpen,
  setModalOpen,

  children,

  initialValues,
  ...formElementProps
}: EditPageProps<T>) {
  const {message} = App.useApp();
  const {t} = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const setIsModified = EditPage.useSetIsModified({
    resetOnUnmount: true,
    endpoint: i18nNs,
    id: record?._id,
  });

  let [form] = Form.useForm();
  if (formOverride) form = formOverride;

  // If cloning, use the cloned element as initial values
  if (clonedElement) initialValues = clonedElement;

  const isGroupEditingPage = !!(groupEditing && !asModal);
  // Create page is when the record id is "new" and we are not in group editing mode
  const isCreatePage = !isGroupEditingPage && record._id === "new";

  const context = isCreatePage
    ? clonedElement
      ? "clone"
      : "create"
    : isGroupEditingPage
    ? "groupEdit"
    : "edit";
  title = title || t(`${i18nNs}:label`, {context});

  // Initialize values each time the id changes
  useEffect(() => form.setFieldsValue(initialValues), [record._id]);

  // If in group edit mode, display a special "save" button title
  const buttonTitle =
    context === "groupEdit"
      ? t("common:editPage.buttonTitle.groupEdit", {count: groupEditing?.elements.length})
      : t(`common:editPage.buttonTitle.${context}`);

  const shouldDisplayHistory = !isCreatePage && elementsActions?.loadEditingHistory;

  const validateAndNavigate = (navigateToElementAfterValidation: boolean) => {
    const fieldsKeysToUpdate = isGroupEditingPage
      ? groupEditing.fieldsToUpdate.map((field) => field.key)
      : undefined;
    form
      .validateFields(fieldsKeysToUpdate)
      .then(async () => {
        let elementId;
        const formData = postTransform
          ? postTransform(form.getFieldsValue())
          : form.getFieldsValue();

        if (onValidation) {
          elementId = await onValidation(formData, fieldsKeysToUpdate);
          onCreate?.(elementId);
        } else {
          if (isGroupEditingPage) {
            const onlyFieldsToUpdate = pick(formData, fieldsKeysToUpdate);
            for (const entityId of groupEditing.elements) {
              await dispatch(
                elementsActions.persist({...onlyFieldsToUpdate, _id: entityId} as Partial<T>)
              );
            }
          } else {
            elementId = await dispatch(elementsActions.persist({...formData, _id: record._id}));
            onCreate?.(elementId);
          }
        }

        setIsModified(false);

        if (asModal) setModalOpen?.(false);
        // If in a modal, just close the modal
        else if (navigateToElementAfterValidation) navigate(`./../${elementId}`, {replace: true});
        else if (goBackAfterAction && !clonedElement) navigate(-1);
      })
      .catch(() => message.error(t("common:editPage.formInvalid")));
  };

  const onValuesChange: FormProps["onValuesChange"] = (changedValues, values) => {
    formElementProps.onValuesChange?.(changedValues, values);
    setIsModified(true);
  };

  const onDelete = () => {
    dispatch(elementsActions.remove(record._id));
    goBackAfterAction && navigate(-1);
  };

  const Header = () => {
    const isModified = EditPage.useIsModified({endpoint: i18nNs, id: record?._id});

    return (
      <PageHeading
        icon={icon}
        backButton={backButton && !asModal} // Display the back button only if we are not in a modal and if the backButton prop is true
        className="edit-page-header"
        title={title as string}
        buttonTitle={buttonTitle}
        buttonIcon={buttonIconMapping[context]}
        onButtonClick={() => validateAndNavigate(false)}
        buttonDisabled={!isModified && !clonedElement} // Disable if not modified and if not in cloning mode (because we don't want it disabled in cloning mode)
        customButtons={
          !isCreatePage ? (
            <>
              {!isGroupEditingPage && (
                <>
                  {customButtons}
                  {clonable && (
                    <Tooltip title={t("common:editPage.cloneFromCurrentInfo")}>
                      <Button
                        type="link"
                        style={{flexGrow: 0}}
                        icon={<CopyOutlined />}
                        onClick={() => {
                          navigate("./../clone", {
                            replace: true,
                            state: {clonedElement: {...form.getFieldsValue(), _id: "new"} as T},
                          });
                        }}
                      />
                    </Tooltip>
                  )}
                  {deletable && <DeleteButton onConfirm={onDelete} />}
                </>
              )}
            </>
          ) : (
            createAndStayButton &&
            !clonedElement &&
            !asModal && (
              <Tooltip title={t("common:editPage.createAndStayButton.tooltip")}>
                <Button onClick={() => validateAndNavigate(true)} disabled={!isModified}>
                  {t("common:editPage.createAndStayButton.label")}
                </Button>
              </Tooltip>
            )
          )
        }
      />
    );
  };

  const editPageContent = (
    <div className={"page-container"}>
      <Header />

      {isGroupEditingPage && (
        <Alert
          style={{marginBottom: 26}}
          message={t("common:editPage.youAreEditingDocumentsInTheSameTime.message", {
            count: groupEditing.elements.length,
          })}
          description={
            <>
              {t("common:editPage.youAreEditingDocumentsInTheSameTime.description.1")}{" "}
              {listOfClickableElements(
                groupEditing.fieldsToUpdate.map((field) => field.label),
                (el) => (
                  <strong style={{color: "darkred"}}>{el}</strong>
                )
              )}
              . {t("common:editPage.youAreEditingDocumentsInTheSameTime.description.2")}
            </>
          }
        />
      )}

      {clonedElement && (
        <Alert
          style={{marginBottom: 26}}
          message={t("common:editPage.cloningInfoAlert.message")}
          description={t("common:editPage.cloningInfoAlert.description")}
        />
      )}

      {record._id && (
        <div className="fade-in">
          <FormElement
            form={form}
            onValidate={validateAndNavigate}
            requiredMark={isCreatePage}
            initialValues={initialValues}
            {...formElementProps}
            onValuesChange={onValuesChange}>
            {children}
          </FormElement>

          {shouldDisplayHistory && !OFFLINE_MODE && (
            <Collapse className="fade-in">
              <Collapse.Panel header={t("common:editPage.modificationsHistory")} key={"history"}>
                <PendingSuspense>
                  <ModificationsHistoryTimeline record={record} elementsActions={elementsActions} />
                </PendingSuspense>
              </Collapse.Panel>
            </Collapse>
          )}
        </div>
      )}
    </div>
  );

  return asModal ? (
    <Modal
      width="min(90vw, 1000pt)"
      open={modalOpen}
      onCancel={() => setModalOpen?.(false)}
      footer={null}>
      <div className="modal-page-content">{editPageContent}</div>
    </Modal>
  ) : (
    editPageContent
  );
}

EditPage.useSetIsModified = ({
  resetOnUnmount = false,
  endpoint,
  id,
}: {resetOnUnmount?: boolean; endpoint?: string; id?: string} = {}) => {
  const dispatch = useDispatch();
  const givenKey = endpoint && id && `${endpoint}-${id}`;
  const setIsModified = (value: boolean) => {
    dispatch(
      viewActions.changeIsModified({
        value,
        key: givenKey || window.location.pathname,
      })
    );
  };

  // Safely reset the isModified value when unmounting the component if resetOnUnmount is true
  useEffect(() => {
    const savedLocation = window.location.pathname;
    if (resetOnUnmount) {
      return () => {
        dispatch(viewActions.changeIsModified({value: false, key: givenKey || savedLocation}));
      };
    }
  }, []);

  return setIsModified;
};

EditPage.useIsModified = ({endpoint, id}: {endpoint?: string; id?: string} = {}) => {
  const givenKey = endpoint && id && `${endpoint}-${id}`;

  return useSelector((s) => viewSelectors.selectIsModified(s, givenKey));
};
