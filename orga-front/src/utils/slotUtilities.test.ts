import {Dayjs} from "@shared/services/dayjs";
import {
  AvailabilitySlot,
  getSlotsStartAndEnd,
  hasSameStartAndEnd,
  slotEndIsBeforeBeginning,
  slotOverlapsAnOtherOne,
} from "./slotsUtilities";

// Helper function to create AvailabilitySlot objects
const createSlot = (
  start: string | Dayjs,
  end: string | Dayjs,
  duration: number,
  _id?: string
): AvailabilitySlot => ({
  start,
  end,
  duration,
  _id,
});

describe("slotsUtilities", () => {
  describe("getSlotsStartAndEnd", () => {
    it("should return correct start and end for valid slots", () => {
      const slots: AvailabilitySlot[] = [
        createSlot("2023-01-01T10:00:00.000Z", "2023-01-01T11:00:00.000Z", 60, "1"),
        createSlot("2023-01-01T12:00:00.000Z", "2023-01-01T14:00:00.000Z", 120, "2"),
      ];
      const result = getSlotsStartAndEnd(slots);
      expect(result.start).toBe("2023-01-01T10:00:00.000Z");
      expect(result.end).toBe("2023-01-01T14:00:00.000Z");
    });

    it("should return undefined for empty slots array", () => {
      const result = getSlotsStartAndEnd([]);
      expect(result.start).toBeUndefined();
      expect(result.end).toBeUndefined();
    });

    it("should use defaultStart when provided and slots are empty", () => {
      const result = getSlotsStartAndEnd([], "2023-01-01T09:00:00.000Z");
      expect(result.start).toBe("2023-01-01T09:00:00.000Z");
      expect(result.end).toBeUndefined();
    });
  });

  describe("slotEndIsBeforeBeginning", () => {
    it("should return true when end is before start", () => {
      const slot = createSlot("2023-01-01T11:00:00.000Z", "2023-01-01T10:00:00.000Z", 60);
      expect(slotEndIsBeforeBeginning(slot)).toBe(true);
    });

    it("should return false when end is after start", () => {
      const slot = createSlot("2023-01-01T10:00:00.000Z", "2023-01-01T11:00:00.000Z", 60);
      expect(slotEndIsBeforeBeginning(slot)).toBe(false);
    });
  });

  describe("hasSameStartAndEnd", () => {
    it("should return true for slots with same start and end", () => {
      const slotA = createSlot("2023-01-01T10:00:00.000Z", "2023-01-01T11:00:00.000Z", 60, "1");
      const slotB = createSlot("2023-01-01T10:00:00.000Z", "2023-01-01T11:00:00.000Z", 60, "2");
      expect(hasSameStartAndEnd(slotA, slotB)).toBe(true);
    });

    it("should return false for slots with different start or end", () => {
      const slotA = createSlot("2023-01-01T10:00:00.000Z", "2023-01-01T11:00:00.000Z", 60, "1");
      const slotB = createSlot("2023-01-01T10:00:00.000Z", "2023-01-01T12:00:00.000Z", 120, "2");
      expect(hasSameStartAndEnd(slotA, slotB)).toBe(false);
    });
  });

  describe("slotOverlapsAnOtherOne", () => {
    const existingSlots: AvailabilitySlot[] = [
      createSlot("2023-01-01T10:00:00.000Z", "2023-01-01T11:00:00.000Z", 60, "1"),
      createSlot("2023-01-01T12:00:00.000Z", "2023-01-01T13:00:00.000Z", 60, "2"),
    ];

    it("should return true for overlapping slot", () => {
      const newSlot = createSlot("2023-01-01T10:30:00.000Z", "2023-01-01T11:30:00.000Z", 60);
      expect(slotOverlapsAnOtherOne(newSlot, existingSlots)).toBe(true);
    });

    it("should return false for non-overlapping slot", () => {
      const newSlot = createSlot("2023-01-01T11:00:00.000Z", "2023-01-01T12:00:00.000Z", 60);
      expect(slotOverlapsAnOtherOne(newSlot, existingSlots)).toBe(false);
    });

    it("should ignore specified slot when checking for overlap", () => {
      const slotToIgnore = existingSlots[0];
      const newSlotOverlappingSlotToIgnore = createSlot(
        "2023-01-01T10:30:00.000Z",
        "2023-01-01T11:30:00.000Z",
        60
      );
      expect(
        slotOverlapsAnOtherOne(newSlotOverlappingSlotToIgnore, existingSlots, slotToIgnore)
      ).toBe(false);
    });

    it("should return false when availabilitySlots is undefined", () => {
      const newSlot = createSlot("2023-01-01T10:00:00.000Z", "2023-01-01T11:00:00.000Z", 60);
      expect(slotOverlapsAnOtherOne(newSlot, undefined)).toBe(false);
    });
  });
});
