import {Link} from "react-router-dom";
import {Tag} from "antd";
import {getCategoryTitle} from "../features/getCategoryTitle";
import {sorter} from "@shared/utils/sorters";
import {editableCellColumn} from "@shared/components/EditableCell";
import {teamsActions} from "@features/teams";
import {useTranslation} from "react-i18next";
import {useCustomFieldsColumns} from "@shared/utils/columns/useCustomFieldsColumns";

export const useTeamsColumns = (path) => {
  const {t} = useTranslation();
  return [
    {
      title: t("categories:label"),
      dataIndex: "category",
      sorter: (a, b) => sorter.text(getCategoryTitle(a), getCategoryTitle(b)),
      render: (text, record) => (
        <Link to={`${path}/categories/${record.activity?.category?._id}`}>
          <Tag
            style={{
              cursor: "pointer",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              overflow: "hidden",
              maxWidth: 125,
            }}
            color={record.activity?.category?.color}>
            {getCategoryTitle(record)}
          </Tag>
        </Link>
      ),
      searchable: true,
      width: 140,
      ellipsis: true,
      searchText: getCategoryTitle,
    },
    {
      title: t("teams:schema.activity.label"),
      dataIndex: "activity",
      render: (text, record) => (
        <Link to={`${path}/activities/${record.activity?._id}`}>{record.activity?.name}</Link>
      ),
      sorter: (a, b) => sorter.text(a.activity?.name, b.activity?.name),
      searchable: true,
      searchText: (record) => record.activity?.name,
    },
    {
      ...editableCellColumn({
        title: t("common:schema.name.label"),
        dataIndex: "name",
        type: "text",
        placeholder: t("common:schema.name.placeholder"),
        elementsActions: teamsActions,
      }),
      sorter: (a, b) => sorter.text(a.name, b.name),
      searchable: true,
    },
    ...useCustomFieldsColumns({
      endpoint: "teams",
      elementsActions: teamsActions,
    }),
  ];
};
