import React from "react";
import {sorter} from "@shared/utils/sorters";
import {useTranslation} from "react-i18next";

export const usePlacesColumns = () => {
  const {t} = useTranslation();

  return [
    {
      title: t("common:schema.name.label"),
      dataIndex: "name",
      sorter: (a, b) => sorter.text(a.name, b.name),
      defaultSortOrder: "ascend",
      searchable: true,
    },
    {
      title: t("places:schema.maxNumberOfParticipants.labelShort"),
      dataIndex: "maxNumberOfParticipants",
      render: (text) =>
        text ? (
          t("places:schema.maxNumberOfParticipants.jaugeOfX", {count: text})
        ) : (
          <span style={{color: "gray"}}>
            {t("places:schema.maxNumberOfParticipants.noMaxNumber")}
          </span>
        ),
      sorter: (a, b) => sorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
    },
  ];
};
