import React from "react";
import dayjs from "@shared/services/dayjs";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {sessionsSelectors} from "@features/sessions";

const SOON_OCCUPIED_THRESHOLD = 120;

const NextOrCurrentSessionDisplay = React.memo(({sessionsSubscriptions, path}) => {
  const sessionsList = useSelector(sessionsSelectors.selectList);

  const getNextOrCurrentSlot = () => {
    const now = dayjs();
    return (
      sessionsSubscriptions
        // Get sessions from sessionsSubscriptions
        .map((ss) => sessionsList.find((s) => s._id === ss.session))
        // Filter sessions that are still ongoing
        .filter((s) => s && dayjs(s.end).isAfter(now))
        // Get slots from those sessions, keep the session id
        .flatMap((s) => s.slots.map((slot) => ({...slot, session: s._id})))
        // Filter slots that are still ongoing
        .filter((slot) => dayjs(slot.end).isAfter(now))
        // Sort slots by start date and take the first one which is the one that will come next for the participant
        .sort((slotA, slotB) => dayjs(slotA.start).diff(dayjs(slotB.start)))[0]
    );
  };

  const nextOrCurrentSlot = getNextOrCurrentSlot();

  if (nextOrCurrentSlot) {
    const nextSlotIn = -dayjs().diff(nextOrCurrentSlot.start, "minute");
    if (nextSlotIn <= 0) {
      return <Link to={`${path}/sessions/${nextOrCurrentSlot.session}`}>Occupé·e</Link>;
    } else if (nextSlotIn < SOON_OCCUPIED_THRESHOLD) {
      return <Link to={`${path}/sessions/${nextOrCurrentSlot.session}`}>Bientôt occupé·e</Link>;
    }
  }

  return "Libre";
});

export const generateRegistrationDispoColumn = (path) => ({
  title: "Disponibilité",
  dataIndex: "dispo",
  render: (text, record) => (
    <NextOrCurrentSessionDisplay sessionsSubscriptions={record.sessionsSubscriptions} path={path} />
  ),
});
