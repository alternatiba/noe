import {TagsSelectInput} from "@shared/components/TagsSelectInput";
import {Tag} from "antd";
import {sorter} from "@shared/utils/sorters";
import {editableCellColumn} from "@shared/components/EditableCell";

export const editableTagsColumn = ({title, dataIndex, elementsActions, elementsSelectors}) => ({
  ...editableCellColumn(
    {
      title,
      dataIndex,
      elementsActions,
    },
    {
      simpleDisplayCellRender: (record) => (
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
            rowGap: 5,
          }}>
          {record[dataIndex]?.map((tagName, index) => (
            <Tag key={index} style={{marginRight: 5}}>
              {tagName}
            </Tag>
          ))}
        </div>
      ),
      editCellRender: () => (
        <TagsSelectInput name={dataIndex} elementsSelectors={elementsSelectors} noStyle bordered />
      ),
    }
  ),
  sorter: (a, b) => sorter.text(a[dataIndex]?.join(" "), b[dataIndex]?.join(" ")),
  searchable: true,
  searchText: (record) => record[dataIndex]?.join(" "),
});
