import {t} from "i18next";
import {RoleTag} from "@shared/components/RoleTag";
import {listOfClickableElements} from "@shared/utils/listOfClickableElements";
import {WaitingInvitationTag} from "@shared/components/WaitingInvitationTag";
import {useParticipantsFormAnswersColumns} from "./useParticipantsFormAnswersColumns";
import {editableTagsColumn} from "./editableTagsColumn";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {Link} from "react-router-dom";
import {sorter} from "@shared/utils/sorters";
import {useCustomFieldsColumns} from "@shared/utils/columns/useCustomFieldsColumns";

export const useRegistrationsColumns = (path, project, {start, middle, end} = {}) =>
  [
    ...(start || []),
    {
      title: t("users:schema.firstName.label"),
      dataIndex: "firstName",
      render: (text, record) => {
        const sessionsNoShows = record.sessionsSubscriptions?.filter(
          (sessionSubscription) => sessionSubscription.hasNotShownUp
        );
        return (
          <>
            <RoleTag roleValue={record.role} small />
            {sessionsNoShows?.length > 0 && (
              <span title={`Ce·tte participant·e a fait défaut ${sessionsNoShows.length} fois.`}>
                {sessionsNoShows.map(() => "❗").join("")}{" "}
              </span>
            )}
            {record.user?.firstName}
          </>
        );
      },
      sorter: (a, b) => sorter.text(a.user?.firstName, b.user?.firstName),
      searchable: true,
      searchText: (record) => record.user?.firstName,
    },
    {
      title: t("users:schema.lastName.label"),
      dataIndex: "lastName",
      render: (text, record) => record.user?.lastName,
      sorter: (a, b) => sorter.text(a.user?.lastName, b.user?.lastName),
      defaultSortOrder: "ascend",
      searchable: true,
      searchText: (record) => record.user?.lastName,
    },
    {
      title: t("users:schema.email.label"),
      dataIndex: "email",
      render: (text, record) => (
        <>
          {record.invitationToken && <WaitingInvitationTag />}
          {record.user.email}
        </>
      ),
      sorter: (a, b) => sorter.text(a.user.email, b.user.email),
      searchable: true,
      searchText: (record) => record.user.email,
    },
    ...(middle || []),
    ...useParticipantsFormAnswersColumns(),
    ...useCustomFieldsColumns({
      endpoint: "registrations",
      customFieldsComponents: project.customFieldsComponents,
      elementsActions: registrationsActions,
    }),
    editableTagsColumn({
      title: t("common:schema.tags.label"),
      dataIndex: "tags",
      elementsActions: registrationsActions,
      elementsSelectors: registrationsSelectors,
    }),
    project.useTeams && {
      title: t("teams:label"),
      dataIndex: "team",
      render: (text, record) =>
        listOfClickableElements(record.teamsSubscriptions, (el, index) => (
          <Link to={`${path}/teams/${el.team._id}`} key={index}>
            {el.team.name}
          </Link>
        )),
      sorter: (a, b) => sorter.text(a.teamsSubscriptionsNames, b.teamsSubscriptionsNames),
      searchable: true,
      searchText: (record) => record.teamsSubscriptionsNames,
    },
    {
      title: t("registrations:numberOfDaysOfPresence.label"),
      dataIndex: "numberOfDaysOfPresence",
      sorter: (a, b) => sorter.number(a.numberOfDaysOfPresence, b.numberOfDaysOfPresence),
      searchable: true,
      searchText: (record) => record.numberOfDaysOfPresence,
      width: 95,
    },
    ...(end || []),
  ].filter((el) => !!el);
