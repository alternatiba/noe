import {useSelector} from "react-redux";
import {projectsSelectors} from "@features/projects";
import {personName} from "@shared/utils/utilities";
import {useRenderHtmlTicket} from "@shared/hooks/useRenderHtmlTicket";
import {useTranslation} from "react-i18next";

export const useRegistrationTicketsColumns = () => {
  const project = useSelector(projectsSelectors.selectEditing);
  const renderHtmlTicket = useRenderHtmlTicket();
  const {t} = useTranslation();

  return [
    {
      title: t("registrations:ticketing.schema.id"),
      dataIndex: "id",
      render: (text, record) => renderHtmlTicket(record),
    },
    project.ticketingMode === "helloAsso" && {
      title: t("registrations:ticketing.schema.userName"),
      dataIndex: "username",
      render: (text, record) => personName(record.user),
    },
    {
      title: t("registrations:ticketing.schema.article"),
      dataIndex: "name",
    },
    {
      title: t("registrations:ticketing.schema.amount"),
      dataIndex: "amount",
      render: (text, record) => `${record?.amount} €`,
    },
    {
      title: t("registrations:ticketing.schema.options"),
      dataIndex: "options",
      render: (text, record) =>
        record.options?.map((option) => `${option.name} (${option.amount} €)`).join(", "),
    },
  ].filter((el) => !!el);
};
