import {editableCellColumn, EditableCellColumnOptions} from "@shared/components/EditableCell";
import {listOfClickableElements} from "@shared/utils/listOfClickableElements";
import {Link} from "react-router-dom";
import {
  EntitiesSelectComponent,
  EntitiesSelectComponentProps,
} from "@shared/inputs/EntitiesSelectInput";
import {sorter} from "@shared/utils/sorters";
import {EntitiesSelectors} from "../features/types";
import {FormItem} from "@shared/inputs/FormItem";

type EditableEntitiesColumnProps = Pick<
  EditableCellColumnOptions,
  "title" | "dataIndex" | "elementsActions" | "beforeChange"
> &
  Pick<
    EntitiesSelectComponentProps,
    "getEntityLabel" | "deselectEntityLabel" | "ElementEdit" | "mode"
  > & {
    endpoint?: string,
    entityElementsActions: any,
    entityElementsSelectors: EntitiesSelectors<any, any>,
    simpleMode?: boolean,
    path?: string,
  };

export const editableEntitiesColumn = ({
  title,
  dataIndex,
  endpoint = dataIndex,
  getEntityLabel,
  deselectEntityLabel,
  elementsActions,
  beforeChange,
  entityElementsActions,
  entityElementsSelectors,
  ElementEdit,
  mode,
  simpleMode = false,
  path = "./..",
}: EditableEntitiesColumnProps) => {
  const getJoinedStringValues = (element) => element[dataIndex]?.map(getEntityLabel).join(" ");

  const renderOneElement = (el, index?: number) =>
    simpleMode ? (
      getEntityLabel(el)
    ) : (
      <Link to={`${path}/${endpoint}/${el._id}`} key={index}>
        {getEntityLabel(el)}
      </Link>
    );

  return {
    ...editableCellColumn(
      {
        title,
        dataIndex,
        elementsActions,
        beforeChange,
      },
      {
        simpleDisplayCellRender: (record) => (
          <div style={{overflow: "hidden", whiteSpace: "wrap"}}>
            {listOfClickableElements(
              mode === "multiple" ? record[dataIndex] : [record[dataIndex]],
              renderOneElement
            )}
          </div>
        ),
        editCellRender: () => (
          <FormItem name={dataIndex} noStyle>
            <EntitiesSelectComponent
              elementsActions={entityElementsActions}
              elementsSelectors={entityElementsSelectors}
              getEntityLabel={getEntityLabel}
              deselectEntityLabel={deselectEntityLabel}
              mode={mode}
              autoFocus
              maxTagTextLength={15}
              ElementEdit={ElementEdit}
            />
          </FormItem>
        ),
      }
    ),
    sorter: (a, b) =>
      mode === "multiple"
        ? sorter.text(getJoinedStringValues(a), getJoinedStringValues(b))
        : sorter.text(getEntityLabel(a[dataIndex]), getEntityLabel(b[dataIndex])),
    searchable: true,
    searchText: (record) =>
      mode === "multiple" ? getJoinedStringValues(record) : getEntityLabel(record[dataIndex]),
  };
};
