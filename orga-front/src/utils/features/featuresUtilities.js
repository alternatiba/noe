// Update all dependencies next time
export const resetDependenciesContext = (dispatch, ...elementsActionsArray: Array<any>) =>
  elementsActionsArray.forEach((elementsActions) => {
    dispatch(elementsActions.resetContext());
    dispatch(elementsActions.loadList({silent: true}));
  });

// Only refresh if forceLoad is true, if there was already a date, and if there is some data. else, don't.
export const shouldAutoRefreshDependencies = (forceLoad, data) => forceLoad && data.length > 0;
