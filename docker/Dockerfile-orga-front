FROM node:20-alpine

# Install git and bash which are needed in Webpack's custom processes
RUN apk add --update --no-cache git bash

# Install the latest version of NPM
RUN ["npm", "install", "-g", "npm@10.4.0"]

# Don't run as root
USER node

# Set working directory to where source code is located
WORKDIR /home/node/app

# Install node modules from package.json and package-lock.json files (in the shared folder)
COPY --chown=app:node shared/front/package.json .
COPY --chown=app:node shared/front/package-lock.json .

# Install node modules
RUN ["npm", "ci", "--legacy-peer-deps"]

# Copy source code
COPY --chown=app:node inscription-front .

# Copy shared folder and files (but not the node_modules in the shared folder)
COPY --chown=app:node shared/front/craco.config.js .
COPY --chown=app:node shared/ ../shared
COPY --chown=app:node shared/front/config ./src/app/config

# By default, set the image to strat in production mode
CMD ["npm", "start"]