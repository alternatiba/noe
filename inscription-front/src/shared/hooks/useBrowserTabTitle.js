import {useEffect} from "react";

/**
 * Browser tab title changer
 * @param baseName root name that appears all the time
 * @param currentPageRoot the variable containing the current page uri
 * @param titlesForPages key/value pairs of page titles
 * @param suffix end name that appears all the time
 */
export const useBrowserTabTitle = (currentPageRoot, titlesForPages, {suffix, baseName} = {}) => {
  useEffect(() => {
    let currentPageTitle = titlesForPages[currentPageRoot];
    if (currentPageTitle) {
      if (baseName) currentPageTitle = `${baseName} - ${currentPageTitle}`;
      document.title = `${currentPageTitle} | ${suffix}`;
    }
  }, [currentPageRoot, titlesForPages, suffix, baseName]);
};
