import {useRef, useState} from "react";
import Search from "antd/es/input/Search";
import {Button} from "antd";
import {useTranslation} from "react-i18next";
import {SearchOutlined} from "@ant-design/icons";
import {normalize} from "@shared/utils/stringUtilities";

/**
 * Enables the use of text search in each column that has a searchable = true value and optionally a searchText defined.
 * @param columns the initial columns
 * @returns the columns with search enabled
 */
export function useSearchInColumns<T>(columns: Array<T>): Array<T> {
  const {t} = useTranslation();
  const [, setSearchText] = useState("");
  const [, setSearchedColumn] = useState("");
  let searchInput = useRef(null);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters, confirm) => {
    clearFilters();
    setSearchText("");
    confirm();
  };

  const enableColumnSearchProps = (dataIndex, columnTitle, searchText) => ({
    filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
      <div className="containerH" style={{padding: 8}}>
        <Search
          autoComplete="new-password" // Prevent any autocomplete service to complete this, cause autoComplete="off" doesn't seem to work
          enterButton
          style={{maxWidth: 250}}
          value={selectedKeys[0]}
          // On press search button
          onSearch={() => handleSearch(selectedKeys, confirm, dataIndex)}
          // On keyboard input
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          // on press enter
          onPressEnter={(e) => {
            e.stopPropagation();
            handleSearch(selectedKeys, confirm, dataIndex);
          }}
          placeholder={t("common:filterIn", {name: columnTitle})}
          ref={searchInput}
        />
        <Button type="link" onClick={() => clearFilters && handleReset(clearFilters, confirm)}>
          {t("common:erase")}
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <div
        className={"containerH"}
        style={{
          borderRadius: 8,
          border: filtered && "1px solid var(--noe-primary)",
          background: filtered && "var(--noe-primary)",
          padding: filtered && 3.5,
        }}>
        <SearchOutlined
          style={{
            color: filtered ? "white" : "#888888",
          }}
        />
      </div>
    ),
    onFilter: (text, record) => {
      if (searchText) return normalize(searchText(record)).includes(normalize(text));
      else if (record[dataIndex]) {
        return normalize(record[dataIndex]).includes(normalize(text));
      } else {
        return "";
      }
    },
    onFilterDropdownOpenChange: (open) =>
      open && setTimeout(() => searchInput.current?.select(), 100),
  });

  return columns.map((column) => ({
    // The column data
    ...column,
    // Enable search options if needed
    ...(column.searchable
      ? enableColumnSearchProps(column.dataIndex, column.title, column.searchText)
      : undefined),
  }));
}
