import {viewSelectors} from "@features/view";
import {normalize} from "@shared/utils/stringUtilities";
import {App, TableProps} from "antd";
import {ColumnType} from "antd/es/table";
import {FilterValue} from "antd/es/table/interface";
import {t} from "i18next";
import {useState} from "react";
import {useSelector} from "react-redux";

type SearchTextFn<T> = (record: T) => string;

export type SearchableColumn<T> = ColumnType<T> & {
  searchText?: SearchTextFn<T>;
};

/**
 * Enables the ability to copy the content of a column when clicking on the header of the column (or long press on mobile)
 * @param dataSource the data
 * @param columns the columns definition
 * @param handleDisplayConfigChangeFn the function to manage display config changes that need to be passed if there is one
 * @returns {{
 *      columns: *,
 *      columnsFilteredDataSource,
 *      handleDisplayConfigChange
 *    }}
 *    the columns with the copy ability,
 *    the datasource filtered by columns search
 *    the supercharged handleDisplayConfigChange function
 */
export const useCopyColumns = <T>(
  dataSource: Array<T>,
  columns: Array<SearchableColumn<T>>,
  handleDisplayConfigChangeFn?: TableProps<T>["onChange"]
): {
  columnsFilteredDataSource: Array<T>;
  columns: Array<SearchableColumn<T>>;
  handleDisplayConfigChange?: TableProps<T>["onChange"];
} => {
  const {message} = App.useApp();

  // Update the redux state, but rely on a useState because sometimes there is no proper dedicated redux state for this
  const reduxSorting = useSelector(viewSelectors.selectSorting);
  const [filtering, setFiltering] = useState<Record<string, FilterValue | null>>(
    reduxSorting?.filteredInfo
  );
  // Getting all search texts that are set up for each column
  const columnsSearchTextFunctions: Record<keyof T, SearchTextFn<T>> = Object.fromEntries(
    columns.map((column) => [
      column.dataIndex,
      column.searchText || ((record: T) => record[column.dataIndex as keyof T]),
    ])
  );

  // Function that takes all the currently available filtering information from redux,
  let columnsFilteredDataSource = dataSource;
  if (filtering) {
    for (const [displayName, searchTextArray] of Object.entries(filtering)) {
      if (searchTextArray) {
        const value = searchTextArray[0];
        columnsFilteredDataSource = columnsFilteredDataSource?.filter((record) =>
          normalize(columnsSearchTextFunctions[displayName as keyof T]?.(record))?.includes(
            normalize(value)
          )
        );
      }
    }
  }

  return {
    columns: columns.map((column) => ({
      ...column,
      onHeaderCell: (computedColumn: ColumnType<T>) => ({
        ...computedColumn.onHeaderCell,
        onContextMenu: (e) => {
          // Prevent right click menu from appearing when right clicking on header cell
          e.preventDefault();
          // Get the function that will collect the text to search into for each item of the list
          const searchTextFn = columnsSearchTextFunctions[computedColumn.dataIndex as keyof T];
          // Map elements with that function and assemble them with a comma
          const copyableText = columnsFilteredDataSource?.map(searchTextFn).join(", ");
          // Write that in the clipboard
          copyableText &&
            navigator.clipboard.writeText(copyableText).then(() =>
              message.success(
                t("common:messages.elementsFromColumnCopiedToClipboard", {
                  count: columnsFilteredDataSource?.length,
                  columnTitle: column.title,
                })
              )
            );
        },
      }),
    })),
    handleDisplayConfigChange: (pagination, filters, sorter, extra) => {
      handleDisplayConfigChangeFn?.(pagination, filters, sorter, extra);
      setFiltering(filters);
    },
    columnsFilteredDataSource,
  };
};
