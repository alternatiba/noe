import {useDispatch, useSelector} from "react-redux";
import {viewActions, viewSelectors} from "@features/view";
import {useTranslation} from "react-i18next";
import {useState} from "react";
import {TableElement} from "../components/TableElement";
import {useParams} from "react-router-dom";
import {useDebounce} from "./useDebounce";

export const useColumnsBlacklistingSelect = ({
  endpoint,
  defaultBlacklisting = [],
}: {
  endpoint: string,
  defaultBlacklisting?: Array<string>,
}) => {
  const {projectId} = useParams();
  const key = projectId + "/" + endpoint;
  const columnsBlacklist =
    useSelector(viewSelectors.selectColumnsBlacklist(projectId + "/" + endpoint)) ||
    defaultBlacklisting;

  const filterBlacklistedColumns = (columns) =>
    columns.filter((c) => !columnsBlacklist.includes(c.dataIndex));

  const ColumnsBlacklistingSelector = ({columns}) => {
    const {t} = useTranslation();
    const dispatch = useDispatch();

    const allDisplayableColumns = columns.filter((c) => c.title);

    const [displayedColumns, setDisplayedColumns] = useState(
      columns.filter((c) => c.title && !columnsBlacklist.includes(c.dataIndex))
    );

    const debouncedPersistColumns = useDebounce(
      (newDisplayedColumnsValues) =>
        dispatch(
          viewActions.changeColumnsBlacklist({
            key,
            blacklist: allDisplayableColumns
              .map((opt) => opt.dataIndex)
              .filter(
                (dataIndex) => !newDisplayedColumnsValues.find((col) => col.dataIndex === dataIndex)
              ),
          })
        ),
      1000
    );

    const rowSelection = {
      onChange: (_, newDisplayedColumnsValues) => {
        setDisplayedColumns(newDisplayedColumnsValues);
        debouncedPersistColumns(newDisplayedColumnsValues);
      },
    };

    return (
      <TableElement.WithTitle
        title={t("common:listPage.settings.columns")}
        size={"small"}
        rowKey={"dataIndex"}
        dataSource={allDisplayableColumns}
        columns={[{dataIndex: "title"}]}
        selectedRowKeys={displayedColumns}
        rowSelection={rowSelection}
      />
    );
  };

  return [filterBlacklistedColumns, ColumnsBlacklistingSelector];
};
