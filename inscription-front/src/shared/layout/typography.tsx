import {DetailedHTMLProps, HTMLAttributes} from "react";
import {useThemeToken} from "../hooks/useThemeToken";

type HeadingProps = DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;

export const H1Title = (props: HeadingProps) => {
  const {colorTextHeading} = useThemeToken();
  return <h1 {...props} style={{color: colorTextHeading, ...props.style}} />;
};

export const H2Title = (props: HeadingProps) => {
  const {colorTextHeading} = useThemeToken();
  return <h2 {...props} style={{color: colorTextHeading, ...props.style}} />;
};

export const H3Title = (props: HeadingProps) => {
  const {colorTextHeading} = useThemeToken();
  return <h3 {...props} style={{color: colorTextHeading, ...props.style}} />;
};
