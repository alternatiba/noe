import React, {useState} from "react";
import {useSelector} from "react-redux";
import {useOnlineStatus} from "../hooks/useOnlineStatus";
import {OFFLINE_MODE} from "@shared/utils/offlineModeUtilities";
import {currentProjectSelectors} from "@features/currentProject";
import {CloseOutlined, DownloadOutlined, MenuOutlined, WifiOutlined} from "@ant-design/icons";
import {Badge, Button, Drawer} from "antd";
import {Header} from "antd/es/layout/layout";
import {useTranslation} from "react-i18next";
import {ProjectStatusTag} from "@shared/components/ProjectStatusTag";

export default function MobileNavbar({title, children, displayButtonBadge, ribbon}) {
  const {t} = useTranslation();
  const [open, setOpen] = useState(false);
  const projectStatus = useSelector((s) => currentProjectSelectors.selectProject(s)?.status);

  const online = useOnlineStatus();

  const IconContainer = ({className, style, children}) => (
    <div
      className={className}
      style={{
        borderRadius: 50,
        border: "1px solid var(--noe-bg)",
        minHeight: 32,
        minWidth: 32,
        marginBottom: 3,
        marginRight: 8,
        paddingLeft: 6,
        paddingTop: 6,
        ...style,
      }}>
      {children}
    </div>
  );

  const header = (
    <Header
      className="containerH mobile-navbar-container"
      style={{backgroundColor: "var(--noe-bg)"}}>
      <h4
        style={{
          fontSize: "18px",
          color: "white",
          display: "flex",
          alignItems: "baseline",
          whiteSpace: "wrap",
          margin: "auto",
          paddingTop: 5,
          fontWeight: "bold",
        }}>
        {OFFLINE_MODE && ( // Offline mode activated icon
          <IconContainer className="bg-info" style={{zIndex: 1}}>
            <DownloadOutlined />
          </IconContainer>
        )}
        {OFFLINE_MODE &&
          online && ( // Available internet connection while in offline mode
            <IconContainer className="bg-success" style={{marginLeft: -20}}>
              <WifiOutlined />
            </IconContainer>
          )}

        {projectStatus === "demo" && <ProjectStatusTag status="demo" />}

        {title}
      </h4>
    </Header>
  );

  return (
    <>
      <Badge
        className="navbar-menu-button"
        dot
        count={displayButtonBadge && !open ? 1 : 0}
        style={{top: 5, right: 5, width: 10, height: 10}}>
        <Button
          type="primary"
          size="large"
          icon={open ? <CloseOutlined /> : <MenuOutlined />}
          onClick={() => setOpen(!open)}
          // Make the button yellow if no connection while not in offline mode
          className={"shadow" + (online || OFFLINE_MODE ? "" : " warning-button")}
        />
      </Badge>
      <div style={{overflow: "hidden"}}>
        {ribbon ? (
          <Badge.Ribbon text={<span style={{marginRight: 5}}>{ribbon}</span>} color="#6cdac5">
            {header}
          </Badge.Ribbon>
        ) : (
          header
        )}
      </div>
      <Drawer
        forceRender // Force render so we can run some tour popups in the sidebar
        width={210}
        rootClassName="mobile-sidebar-drawer"
        placement="left"
        zIndex={99}
        onClose={() => setOpen(false)}
        open={open}
        styles={{
          header: {background: "var(--noe-bg)", borderRadius: 0},
          body: {
            background: "var(--noe-bg)",
            padding: 0,
          },
        }}>
        <div className="sidebar-container" onClick={() => setOpen(false)}>
          {children}
        </div>
      </Drawer>
    </>
  );
}
