import {Grid2Props as MuiGridProps, Unstable_Grid2 as MuiGrid} from "@mui/material";

type GridProps = Omit<MuiGridProps, "gap">;
export default function Grid(props: GridProps) {
  return <MuiGrid columnSpacing={{md: 6, lg: 8}} rowSpacing={6} {...props} />;
}
