import {Stack as MuiStack, StackProps as MuiStackProps} from "@mui/material";

const ALIGN_ITEMS_MAPPING = {
  alignCenter: "center",
  alignStart: "flex-start",
  alignEnd: "flex-end",
  alignStretch: "stretch",
  alignBaseline: "baseline",
} as const;
type AlignItems = keyof typeof ALIGN_ITEMS_MAPPING;

const JUSTIFY_CONTENT_MAPPING = {
  justifyCenter: "center",
  justifyStart: "start",
  justifyEnd: "end",
  justifyStretch: "stretch",
  justifySpaceBetween: "space-between",
  justifySpaceAround: "space-around",
  justifySpaceEvenly: "space-evenly",
} as const;
type JustifyContent = keyof typeof JUSTIFY_CONTENT_MAPPING;

type SxDynmamicProps = {xs?: any; sm?: any; md?: any; lg?: any; xl?: any};

type StackProps = Omit<MuiStackProps, "flexWrap" | "direction" | "flexDirection"> & {
  // Align items props
  [str in AlignItems]?: boolean | SxDynmamicProps;
} & {
  // Justify content props
  [str in JustifyContent]?: boolean | SxDynmamicProps;
} & {
  wrap?: boolean | SxDynmamicProps;
  row?: boolean | SxDynmamicProps;
};

const getValueFromProps = (MAPPING: Record<string, string>, props: Record<string, any>) => {
  const validPropNames = Object.keys(MAPPING);
  const activatedPropName = Object.entries(props).find(
    ([propName, value]) => validPropNames.includes(propName) && !!value
  )?.[0];
  return activatedPropName ? MAPPING[activatedPropName] : undefined;
};

export const Stack = ({wrap = false, row = false, ...props}: StackProps) => (
  <MuiStack
    {...props}
    direction={row === true ? "row" : row === false ? "column" : row}
    flexWrap={wrap === true ? "wrap" : wrap === false ? "nowrap" : wrap}
    alignItems={getValueFromProps(ALIGN_ITEMS_MAPPING, props)}
    justifyContent={getValueFromProps(JUSTIFY_CONTENT_MAPPING, props)}
  />
);
