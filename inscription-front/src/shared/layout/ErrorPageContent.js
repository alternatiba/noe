import React from "react";
import {Result} from "antd";
import {useTranslation} from "react-i18next";
import {ContactNOELink} from "../components/NOESocialIcons";
export default function ErrorPageContent({extra}) {
  const {t} = useTranslation();
  return (
    <Result
      style={{margin: "auto"}}
      status="404"
      title={t("common:errorBoundary.title")}
      subTitle={
        <>
          <p>{t("common:errorBoundary.subTitle.helpUs")}</p>

          <p>
            <ContactNOELink
              href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=${t(
                "common:mailtoSubjectBugNOE",
                {url: window.location.href}
              )}`}
              rel="noreferrer">
              {t("common:errorBoundary.subTitle.sendEmail")}
            </ContactNOELink>
          </p>
          <p>
            <a href="https://m.me/noeappio" target="_blank" rel="noreferrer">
              {t("common:errorBoundary.subTitle.socialNetworks")}
            </a>
          </p>
          <p>{t("common:errorBoundary.subTitle.takesOneMinute")}</p>
          <p>{t("common:thanks")} 🥰</p>
        </>
      }
      extra={extra}
    />
  );
}
