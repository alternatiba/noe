import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {useOnlineStatus} from "../hooks/useOnlineStatus";
import React, {Suspense, useRef} from "react";
import {OFFLINE_MODE} from "@shared/utils/offlineModeUtilities";
import {DownloadOutlined, WarningOutlined, WifiOutlined} from "@ant-design/icons";
import {SidebarDivider} from "../components/Menu";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {useLocation} from "react-router-dom";
import {currentProjectSelectors} from "@features/currentProject";

const ProfileSidebarMenuItem = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "low" */
    "./ProfileSidebarMenuItem"
  )
);
const NOESocialIcons = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "low" */
    "../components/NOESocialIcons"
  )
);
const MyAccount = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "low" */
    "@routes/myAccount/MyAccount"
  )
);

/**
 * SIDEBAR
 */
const SidebarBannerText = ({displayCondition, children, className}) => (
  <div
    className={`text-white d-flex justify-content-center align-items-center ${className}`}
    style={{
      minHeight: displayCondition ? 25 : 0,
      maxHeight: displayCondition ? 25 : 0,
      opacity: displayCondition ? 1 : 0,
      overflow: "hidden",
      transition: "all 0.8s ease-in-out",
    }}>
    <strong>{children}</strong>
  </div>
);

export default function SidebarMenuContent({collapsedSidebar, showSocialIcons, menu, profileUser}) {
  const {t} = useTranslation();
  const online = useOnlineStatus();
  const projectStatus = useSelector((s) => currentProjectSelectors.selectProject(s)?.status);

  // Show MyAccount modal if there is #my-account at the end of the URL
  const {hash} = useLocation();
  const showMyAccount = hash === "#my-account";
  let tempStoreHash = useRef();
  const setShowMyAccount = (val: boolean) => {
    if (val) {
      tempStoreHash.current = window.location.hash;
      window.location.hash = "#my-account";
    } else {
      window.location.hash = tempStoreHash.current || "";
    }
  };

  return (
    <>
      {/* Online/offline small banner */}
      <div
        style={{
          position: "sticky",
          zIndex: 1000,
          top: 0,
          flexGrow: 0,
          flexShrink: 0,
          marginBottom: 5,
          display: "flex",
          flexDirection: "column",
          gap: 3,
        }}>
        {/* Offline mode banners */}
        <SidebarBannerText displayCondition={OFFLINE_MODE} className={"bg-info"}>
          <DownloadOutlined />
          {!collapsedSidebar && ` ${t("common:offlineMode.sidebar.offlineModeActivated")}`}
        </SidebarBannerText>
        <SidebarBannerText displayCondition={online && OFFLINE_MODE} className={"bg-success"}>
          <WifiOutlined />
          {!collapsedSidebar && ` ${t("common:offlineMode.sidebar.connectionAvailable")}`}
        </SidebarBannerText>
        <SidebarBannerText displayCondition={!online && !OFFLINE_MODE} className={"bg-warning"}>
          <WarningOutlined />
          {!collapsedSidebar && ` ${t("common:offlineMode.sidebar.youAreOffline")}`}
        </SidebarBannerText>

        {/* Demo mode banner */}
        <SidebarBannerText className={"bg-danger"} displayCondition={projectStatus === "demo"}>
          {t("common:demo").toUpperCase()}
        </SidebarBannerText>
      </div>

      <div className="sidebar-top">
        <div className="fade-in">{menu.top}</div>

        {showSocialIcons && (
          <>
            <SidebarDivider color="rgba(255,255,255,0.9)" />
            <div className="social-icons-container fade-in" style={{marginTop: 30}}>
              <NOESocialIcons linkClassname="no-noe-link-color" />
            </div>
          </>
        )}
      </div>
      <Suspense fallback={null}>
        <div className="sidebar-footer" style={{marginTop: "15px"}}>
          <div className={"fade-in"}>
            {menu.footer}
            <ProfileSidebarMenuItem
              user={profileUser}
              collapsedSidebar={collapsedSidebar}
              setShowProfile={setShowMyAccount}
            />
          </div>
        </div>
      </Suspense>
      <Suspense fallback={null}>
        <MyAccount openState={[showMyAccount, setShowMyAccount]} />
      </Suspense>
    </>
  );
}
