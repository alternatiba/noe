import {PlusOutlined, SelectOutlined} from "@ant-design/icons";
import {paginationPageSizes, viewActions, viewSelectors} from "@features/view";
import {DeleteButton} from "@shared/components/buttons/DeleteButton";
import {ListSettingsDrawerButton} from "@shared/components/buttons/ListSettingsDrawerButton";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {useLocalStorageState} from "@shared/utils/localStorageUtilities";
import {SearchInFieldsValue, searchInObjectsList} from "@shared/utils/searchInObjectsList";
import {getFullHeightWithMargins} from "@shared/utils/viewUtilities";
import {Button, ConfigProvider, Table, Tooltip} from "antd";
import {AnyObject} from "antd/es/_util/type";
import {ExpandableConfig, RowClassName} from "rc-table/lib/interface";
import React, {ReactNode, Suspense, useEffect, useLayoutEffect, useMemo, useState} from "react";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import {ListSearchBar} from "../components/ListSearchBar";
import {PageHeading, PageHeadingProps} from "../components/PageHeading";
import {SavedWindowScroll} from "../components/SavedWindowScroll";
import {clickIsInsideSelectionColumn, getTableWidth} from "../components/TableElement";
import {SearchableColumn, useCopyColumns} from "../hooks/useCopyColumns";
import {useMultipleSelection} from "../hooks/useMultipleSelection";
import {useSavedPagination} from "../hooks/useSavedPagination";
import {useSearchInColumns} from "../hooks/useSearchInColumns";
import {useWindowDimensions} from "../hooks/useWindowDimensions";
import {Pending} from "@shared/components/Pending";
import {RenderEmptyHandler} from "antd/es/config-provider";

const CsvGroupImportButton = lazyWithRetry(
  () => import(/* webpackPrefetch: true */ "@shared/components/buttons/CsvGroupImportButton")
);
const GroupEditionButton = lazyWithRetry(
  () => import(/* webpackPrefetch: true */ "@shared/components/buttons/GroupEditionButton")
);

type ListPageProps<T extends AnyObject> = Pick<
  PageHeadingProps,
  "title" | "icon" | "buttonTitle" | "customButtons"
> & {
  i18nNs?: string;
  subtitle?: ReactNode;

  multipleActionsButtons?: React.FC<{selectedRowKeys: Array<any>; setSelectedRowKeys: any}>;
  settingsDrawerContent?: ReactNode;

  elementsActions: any;
  elementsSelectors?: any;
  columns: Array<SearchableColumn<T>>;
  dataSource: Array<T>;
  customEmptyDataRender?: RenderEmptyHandler;

  noActionIcons?: boolean;
  creatable?: boolean;
  editable?: boolean;
  deletable?: boolean;
  groupImportable?: boolean;
  groupEditable?: boolean;
  forceEndpoint?: string;

  onNavigate?: (recordIdOrSlug: string) => void;
  searchInFields?: SearchInFieldsValue<T>;

  rowClassName?: RowClassName<T>;
  expandable?: ExpandableConfig<T>;
};

export function ListPage<T extends AnyObject>({
  i18nNs,
  title,
  icon,
  subtitle,
  customButtons,
  multipleActionsButtons: MultipleActionsButtons,
  buttonTitle,
  settingsDrawerContent,

  elementsActions,
  elementsSelectors,
  columns: rawColumns,
  dataSource,
  customEmptyDataRender,

  creatable = true,
  editable = true,
  groupEditable = false,
  deletable = true,
  groupImportable = false,
  onNavigate,
  noActionIcons = false,
  forceEndpoint,
  searchInFields = [],

  rowClassName,
  expandable,
}: ListPageProps<T>) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const navigateFn = onNavigate || navigate;
  const [pagination, setPagination] = useSavedPagination();
  const {isMobileView} = useWindowDimensions();
  const sorting = useSelector(viewSelectors.selectSorting);
  const search = useSelector(viewSelectors.selectSearch);
  const [displaySize, setDisplaySize] = useLocalStorageState(
    "listDisplaySize",
    isMobileView ? "small" : "middle"
  );

  const isLoaded = useSelector((s) =>
    elementsSelectors?.selectIsLoaded ? elementsSelectors.selectIsLoaded(s) : true
  );

  // If an i18n namespace is given, get the title and button name from there
  if (i18nNs) {
    title = title || t(`${i18nNs}:label_other`);
    buttonTitle = creatable ? buttonTitle || t(`${i18nNs}:label`, {context: "create"}) : undefined;
    if (buttonTitle && isMobileView) buttonTitle = buttonTitle.split(" ")[0];
  }

  const globalSearchFilteredDataSource = useMemo(
    () => searchInObjectsList(search, dataSource, searchInFields),
    [search, dataSource, searchInFields]
  );

  /*********** Multiple selection **************************/

  const {
    enableMultipleSelection,
    setEnableMultipleSelection,
    selectedRowKeys,
    setSelectedRowKeys,
    rowSelection,
    onBatchDelete,
  } = useMultipleSelection((id) => dispatch(elementsActions.remove(id)));

  /*********** Sorting / Filtering memorization ************/

  let {columns, handleDisplayConfigChange, columnsFilteredDataSource} = useCopyColumns(
    globalSearchFilteredDataSource || [],
    rawColumns,
    (pagination, filters, sorter) => {
      dispatch(viewActions.changeSorting({filteredInfo: filters, sortedInfo: sorter}));
    }
  );

  const clearDisplayConfig = () => {
    dispatch(viewActions.changeSorting({}));
  };

  columns = columns?.map((column) => {
    const savedSortingOrder =
      sorting?.sortedInfo?.column?.dataIndex === column.dataIndex && sorting?.sortedInfo?.order;
    return {
      // The column data
      ...column,
      // Add the saved sorting order if found
      sortOrder: savedSortingOrder || column.sortOrder,
      filteredValue: sorting?.filteredInfo?.[column.dataIndex as keyof T],
    };
  });

  columns = useSearchInColumns(columns);

  if (!noActionIcons && (editable || deletable)) {
    columns.push({
      key: "action",
      width: 9 + (deletable ? 32 + 9 : 0),
      render: (text, record) => (
        <div
          className="containerH buttons-container"
          style={{justifyContent: "flex-end", flexWrap: "nowrap"}}>
          {deletable && (
            <DeleteButton onConfirm={() => dispatch(elementsActions.remove(record._id))} />
          )}
        </div>
      ),
    });
  }

  const getHeightOffset = () =>
    getFullHeightWithMargins(document.querySelector(".full-list-page-header"));

  const [heightOffset, setHeightOffset] = useState(getHeightOffset());

  // Compute the height offset for the table sticky header
  useEffect(() => {
    const newHeightOffset = getHeightOffset();
    if (newHeightOffset !== heightOffset) setHeightOffset(newHeightOffset);
  });

  // Make the table body at least fill the page event if there is only few data. So the layout is prettier.
  useLayoutEffect(() => {
    const tableBody: HTMLElement | null = document.querySelector(".ant-table-body");
    const paginationHeight = getFullHeightWithMargins(
      document.querySelector(".ant-table-pagination")
    );
    const headerHeight = getFullHeightWithMargins(document.querySelector(".ant-table-header")); // pagination height
    if (tableBody) {
      tableBody.style.minHeight = `calc(100vh - ${
        heightOffset + paginationHeight + headerHeight
      }px)`;
    }
  });

  return (
    <div
      className={"containerV full-width-content list-page"}
      // Fill up the gap that is not filled with container color
      style={{minHeight: "100vh", background: "var(--colorBgContainer)"}}>
      <SavedWindowScroll />
      <div className={"full-list-page-header sticky-element"}>
        <PageHeading
          className="list-page-header"
          title={title}
          icon={icon}
          customButtons={
            <>
              {!enableMultipleSelection && customButtons}

              {sorting?.sortedInfo?.column && ( // There is a sorter activated
                <Button type="link" onClick={clearDisplayConfig}>
                  {t("common:listPage.clearSorting")}
                </Button>
              )}

              <Suspense fallback={null}>
                {(deletable || MultipleActionsButtons || groupEditable) &&
                  (enableMultipleSelection ? (
                    <>
                      <span style={{marginRight: 8}}>
                        {t("common:listPage.xSelected", {count: selectedRowKeys.length})}
                      </span>

                      {MultipleActionsButtons && (
                        <MultipleActionsButtons
                          selectedRowKeys={selectedRowKeys}
                          setSelectedRowKeys={setSelectedRowKeys}
                        />
                      )}

                      {groupEditable && (
                        <GroupEditionButton
                          selectedRowKeys={selectedRowKeys}
                          forceEndpoint={forceEndpoint}
                        />
                      )}

                      {deletable && (
                        <DeleteButton
                          onConfirm={onBatchDelete}
                          disabled={selectedRowKeys.length === 0}
                        />
                      )}

                      <Button
                        type="link"
                        danger
                        onClick={() => {
                          setEnableMultipleSelection(false);
                          setSelectedRowKeys([]);
                        }}>
                        {t("common:cancel")}
                      </Button>
                    </>
                  ) : (
                    <Tooltip title={t("common:listPage.groupedSelection.tooltip")}>
                      <Button
                        type="link"
                        style={{marginRight: 12}}
                        icon={<SelectOutlined />}
                        onClick={() => setEnableMultipleSelection(true)}
                      />
                    </Tooltip>
                  ))}
                {groupImportable && (
                  <CsvGroupImportButton
                    onOk={(entitiesToImport: Array<any>) =>
                      entitiesToImport.forEach((el) =>
                        dispatch(elementsActions.persist({_id: "new", ...el}))
                      )
                    }
                    elementsName={title}
                  />
                )}
              </Suspense>

              {searchInFields?.length > 0 && <ListSearchBar />}
            </>
          }
          buttonTitle={buttonTitle}
          buttonIcon={<PlusOutlined />}
          onButtonClick={() => navigateFn("new")}
        />
        {subtitle && <div className="subtitle with-margins">{subtitle}</div>}
        <div style={{position: "absolute", right: 10}}>
          <ListSettingsDrawerButton
            settingsDrawerContent={settingsDrawerContent}
            localStorageDisplaySize={[displaySize, setDisplaySize]}
          />
        </div>
      </div>

      <div style={{background: "var(--colorBgLayout)"}}>
        {isLoaded ? (
          <div className="fade-in">
            <ConfigProvider renderEmpty={customEmptyDataRender}>
              <Table
                scroll={{x: getTableWidth(columns)}}
                sticky={{offsetHeader: heightOffset}}
                columns={columns}
                rowClassName={(record, index, indent) =>
                  (rowClassName?.(record, index, indent) ||
                    (record.updatedAt && "ant-table-row-updated")) +
                  (editable ? " cursor-pointer" : "")
                }
                pagination={{
                  position: ["bottomCenter"],
                  current: pagination.current,
                  pageSize: pagination.pageSize,
                  pageSizeOptions: paginationPageSizes,
                  total: columnsFilteredDataSource.length,
                  showSizeChanger: true,
                  showTotal: (total, range) =>
                    t("common:listPage.xtoYOnZElements", {
                      from: range[0],
                      to: range[1] > total ? total : range[1],
                      total,
                      elementsLabel: t(`${i18nNs}:label`, {count: total}),
                      context: isMobileView ? "short" : "",
                    }).toLowerCase(),
                  onChange: (current, pageSize) => {
                    setPagination(current, pageSize);
                    window.scrollTo({top: 0}); // Scroll back to page top
                  },
                }}
                onRow={(record) => ({
                  onDoubleClick: (e) => {
                    if (clickIsInsideSelectionColumn(e)) return;
                    navigateFn(record.slug || record._id);
                  },
                })}
                expandable={expandable}
                size={displaySize}
                rowKey="_id"
                dataSource={globalSearchFilteredDataSource}
                rowSelection={rowSelection}
                onChange={handleDisplayConfigChange}
              />
            </ConfigProvider>
          </div>
        ) : (
          <Pending minHeight={`calc(100vh - ${heightOffset + 20}px)`} />
        )}
      </div>
    </div>
  );
}
