import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserActions, currentUserSelectors} from "@features/currentUser";
import {Button, Form} from "antd";
import {TextInputEmail, TextInputPassword} from "../../inputs/TextInput";
import {AuthPage} from "../AuthPage";
import {viewSelectors} from "@features/view";
import {useTranslation} from "react-i18next";
import {safeValidateFields} from "../../inputs/FormElement";
import {useNavigate, useParams} from "react-router-dom";

export default function LogIn({subtitle, footer}) {
  const navigate = useNavigate();
  const {projectId} = useParams();
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const dispatch = useDispatch();

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changeLogin(allFields[0].value));
    dispatch(currentUserActions.changePassword(allFields[1].value));
  };

  const cleanConnectionMessages = () => {
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
  };

  const logIn = () =>
    safeValidateFields(form, () => {
      cleanConnectionMessages();
      dispatch(currentUserActions.logIn());
    });

  const goToSignInPage = () => {
    cleanConnectionMessages();
    navigate("./../signup");
  };

  const goToForgotPasswordPage = () => {
    cleanConnectionMessages();
    navigate("./../forgotpassword");
  };

  return (
    <AuthPage
      form={form}
      dataFormType={"login"}
      className="fade-in"
      projectId={projectId}
      footer={footer}
      subtitle={subtitle}
      initialValues={{
        email: currentUser.email || searchParamsInfo.email,
        password: currentUser.password,
      }}
      onFieldsChange={onChange}
      onValidate={logIn}
      buttons={
        <>
          <Button type="primary" onClick={logIn} htmlType="submit">
            {t("common:login")}
          </Button>
          <Button onClick={goToSignInPage}>{t("common:createAccount")}</Button>
          <Button type="link" onClick={goToForgotPasswordPage}>
            {t("common:connectionPage.passwordForgottenQuestion")}
          </Button>
        </>
      }>
      <TextInputEmail
        i18nNs="users"
        name="email"
        autoComplete="email"
        data-form-type="email"
        required
        bordered
      />
      <TextInputPassword
        i18nNs="users"
        name="password"
        autoComplete="current-password"
        data-form-type="password"
        required
        bordered
      />
    </AuthPage>
  );
}
