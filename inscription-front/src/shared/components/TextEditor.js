import "react-quill/dist/quill.snow.css"; // Base theme
import "@styles/quill-editor.less"; // Our custom overload
import React, {useEffect, useMemo, useRef} from "react";
import {Form, Modal} from "antd";
import {FormElement} from "../inputs/FormElement";
import {NumberInput} from "../inputs/NumberInput";
import {useTranslation} from "react-i18next";
import {TextInput} from "../inputs/TextInput";
import {trySeveralTimes} from "@shared/utils/trySeveralTimes";

import ReactQuill, {Quill, ReactQuillProps} from "react-quill";
import {AppMethods} from "../hooks/useThemedAppMethods";

const toolbar = [
  [{size: []}],
  ["bold", "italic", "underline"],
  ["blockquote"],
  [{list: "ordered"}, {list: "bullet"}],
  ["link", "image", "video"],
  [{color: []}, {background: []}],
  [{align: []}],
];

export type TextEditorProps = {
  onChange?: ReactQuillProps["onChange"],
  placeholder?: string,
  value?: string,
  toolbarSupercharge?: Array<Array<any>>,
};

const TextEditor = ({onChange, placeholder, value, toolbarSupercharge = []}: TextEditorProps) => {
  const {t} = useTranslation();
  const reactQuillRef = useRef();
  const [form] = Form.useForm();

  // **** Image Handler stuff ****
  const imageHandler = async () => {
    const quillEditor = reactQuillRef.current.editor;
    const range = quillEditor.getSelection();

    const onFinish = async (values) => {
      quillEditor.clipboard.dangerouslyPasteHTML(
        range.index,
        `<img src="${values.url}" width="${values.width}%" height="${
          values.height ? `${values.height}px` : "auto"
        }">`,
        Quill.sources.USER
      );

      // Reset fields and close modal
      form.resetFields();
      Modal.destroyAll();
    };

    const ImageHandlerModalContent = () => {
      useEffect(() => {
        trySeveralTimes(() => document.getElementsByClassName("url-input")[0].focus());
      }, []);

      return (
        <FormElement form={form} initialValues={{width: 100}} onFinish={onFinish}>
          <div className="container-grid">
            <TextInput
              className="url-input"
              label={t("common:textEditor.imageUrl.label")}
              name="url"
              placeholder={t("common:textEditor.imageUrl.placeholder")}
              bordered
              required
              rules={[{type: "url"}]}
            />
            <div className="container-grid two-per-row">
              <NumberInput
                label={t("common:textEditor.imageWidth.label")}
                name="width"
                bordered
                required
                placeholder={t("common:textEditor.imageWidth.placeholder")}
                addonAfter="%"
              />
              <NumberInput
                label={t("common:textEditor.imageHeight.label")}
                name="height"
                bordered
                placeholder={t("common:textEditor.imageHeight.placeholder")}
                addonAfter="px"
              />
            </div>
          </div>
        </FormElement>
      );
    };

    AppMethods.modal.confirm({
      icon: null,
      content: <ImageHandlerModalContent />,
      okButtonProps: {
        onClick: form.submit, // Force not closing the modal
      },
    });
  };

  // **** Quill Editor ****
  return useMemo(
    () => (
      <ReactQuill
        ref={reactQuillRef}
        theme="snow"
        onChange={
          onChange
            ? (value, ...rest) => {
                if (value === "<p><br></p>") value = ""; // When the text is blank, then just remove everything
                onChange(value, ...rest);
              }
            : undefined
        }
        modules={{
          toolbar: {
            container: [...toolbarSupercharge, ...toolbar],
            handlers: {image: imageHandler},
          },
        }}
        placeholder={placeholder}
        value={value}
      />
    ),
    []
  );
};

export default TextEditor;
