import React, {useEffect, useMemo} from "react";
import FormRenderer from "./FormRenderer";
import {FormElement} from "../inputs/FormElement";
import {flattenFieldComponents} from "@shared/utils/flattenFieldComponents";
import {useDebounce} from "../hooks/useDebounce";
import {FormInstance} from "antd";
import {FieldComp} from "./FieldsBuilder/FieldComp/FieldComp";

type RegistrationFormProps = {
  initialValues?: any,
  formComponents: Array<FieldComp>,
  form: FormInstance,
  onChange?: any,
  saveData?: any,
};

export default function RegistrationForm({
  initialValues = {},
  formComponents,
  form,
  onChange,
  saveData,
}: RegistrationFormProps) {
  const forceUpdate = FormRenderer.useForceUpdate();

  // saveData on unmount
  useEffect(() => {
    if (saveData) {
      return () => saveData();
    }
  }, []);

  const flatFormComponents = useMemo(
    () => flattenFieldComponents(formComponents, false),
    [formComponents]
  );

  // Rerender the form :
  // - after a 400ms debounce time
  // - AND only if a field with dependencies has been touched
  const debouncedForceUpdateOnValuesChange = useDebounce(
    (values) => {
      const modifiedFields = Object.keys(values).map((key) =>
        flatFormComponents.find((comp) => comp.key === key)
      );
      // If we can find one field that depends on it, we have to update
      const hasDependencies = (modifiedField) =>
        flatFormComponents.find((field) => field.conditional?.when === modifiedField.key);
      if (modifiedFields.find(hasDependencies)) forceUpdate();
    },
    400,
    [flatFormComponents]
  );

  return (
    <FormElement
      form={form}
      requiredMark
      onValuesChange={(values) => {
        onChange?.(values);
        debouncedForceUpdateOnValuesChange(values);
      }}
      initialValues={initialValues}>
      <FormRenderer fields={formComponents} form={form} />
    </FormElement>
  );
}
