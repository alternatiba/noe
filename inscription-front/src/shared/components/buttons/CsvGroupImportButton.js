import React, {ReactNode, useState} from "react";
import {Alert, App, Button, Modal, Tooltip, Upload} from "antd";
import {FileExcelOutlined, ImportOutlined, InboxOutlined} from "@ant-design/icons";
import {BetaTag} from "@shared/components/BetaTag";

import {addKeyToItemsOfList} from "@shared/utils/tableUtilities";
import {Trans, useTranslation} from "react-i18next";
import {TableElement} from "@shared/components/TableElement";
import CsvExportButton from "./CsvExportButton";
import {useEntitySchema} from "@shared/hooks/useEntitySchema";

export default function CsvGroupImportButton({
  title,
  elementsName,
  forceEndpoint,
  forceSchema,
  keepUnknownEntries,
  onOk,
  customInfo,
  customExampleImportFileFunction,
  validate = () => [],
}: {
  title: string,
  elementsName: string,
  forceEndpoint: string,
  forceSchema: Array<{key: string, label: string}>,
  keepUnknownEntries: boolean,
  onOk: (entitiesToImport: any, allowedFields: any) => void,
  customInfo?: ReactNode,
  customExampleImportFileFunction?: () => Promise<Array<any>>,
  validate: (entitiesToImport: any, allowedFields: any) => Array<{row: number, message: string}>,
}) {
  const {message} = App.useApp();
  const {t} = useTranslation();
  const [modalOpen, setModalOpen] = useState(false);
  const [entitiesToImport, setEntitiesToImport] = useState([]);
  const [parsingErrors, setParsingErrors] = useState([]);
  const [validationErrors, setValidationErrors] = useState([]);
  const [columnsFromImport, setColumnsFromImport] = useState([]);
  const [entitySchema, frontendEndpoint] = useEntitySchema({forceEndpoint, forceSchema});

  const renderArrayField = (record, field) => record[field.key]?.join(", ");

  const allowedFields = entitySchema.map((field) => field.key);
  const schemaColumns = entitySchema.map((field) => ({
    dataIndex: field.key,
    title: field.label,
    ...(field.isArray && {render: (_, record) => renderArrayField(record, field)}),
  }));

  const cleanQuotes = (string) => string.replace(/[“”]/g, '"').replace(/[‘’]/g, "'");

  const parseJsonFromCsvEntry = (element, field) => {
    // If is array but properly formatted, or the opposite :
    const arrayRegExp = /\[([^,]+)(,[^,]+)*\]/;
    if (field.isArray && !arrayRegExp.exec(element)) {
      throw TypeError(t("common:csvImportButton.badFormatShouldBeArray", {field: field.key}));
    } else if (!field.isArray && arrayRegExp.exec(element)) {
      throw TypeError(t("common:csvImportButton.badFormatShouldNotBeArray", {field: field.key}));
    }

    // Then if it's JSONable, turn it into a JS object or variable
    if (typeof element === "string" && (element[0] === "{" || element[0] === "[")) {
      return JSON.parse(cleanQuotes(element));
    } else {
      // Else, keep the element intact (papaparse casts automatically booleans so no need to worry about them)
      return element;
    }
  };

  const parseCsvFile = (file) => {
    let reader = new FileReader();
    setEntitiesToImport([]);
    setParsingErrors([]);
    setValidationErrors([]);
    reader.onload = async function (e) {
      // Import papaparse dynamically
      const {parse} = await import("papaparse");

      parse(e.target.result?.trim(), {
        header: true,
        dynamicTyping: true,
        complete: (result) => {
          const rowsWithErrors = result.errors.map((error) => error.row);

          const additionalParsingErrors = [];
          const cleanAndParseRow = (row, index) => {
            if (rowsWithErrors.includes(index)) return;
            try {
              return Object.fromEntries(
                Object.entries(row)
                  // If the value is not valid, remove it
                  .filter(
                    ([key, val]) =>
                      (keepUnknownEntries || allowedFields.includes(key)) &&
                      val !== "" &&
                      val !== null
                  )
                  // If the string is JSON, then parse it
                  .map(([key, val]) => [
                    key,
                    parseJsonFromCsvEntry(
                      val,
                      entitySchema.find((field) => field.key === key)
                    ),
                  ])
              );
            } catch (e) {
              additionalParsingErrors.push({row: index, type: e.name, message: e.message});
            }
          };

          setColumnsFromImport([
            ...schemaColumns,
            ...result.meta.fields
              .filter((field) => !allowedFields.includes(field))
              .map((field) => ({dataIndex: field, title: field})),
          ]);

          // Set the entities to import
          const entitiesToImport = result.data.map(cleanAndParseRow).filter((row) => !!row);
          setEntitiesToImport(entitiesToImport);

          const parsingErrors = [...result.errors, ...additionalParsingErrors];
          if (parsingErrors.length > 0) {
            // If parsing errors, add them to the state
            setParsingErrors(parsingErrors);
          } else {
            // Else, validate the import data
            const errors = validate(entitiesToImport, allowedFields);
            if (errors?.length > 0) setValidationErrors(errors);
          }
        },
      });
    };
    reader.readAsText(file);
  };

  const handleImport = () => {
    if (parsingErrors.length > 0 || validationErrors.length > 0) {
      message.error(t("common:csvImportButton.youHaveErrorsPleaseCorrectThem"));
      return;
    }
    onOk(entitiesToImport, allowedFields);
    setEntitiesToImport([]);
    setModalOpen(false);
  };

  const ExampleImportFileButton = () => (
    <CsvExportButton
      getExportName={() => t("common:csvImportButton.filename", {elementsName})}
      dataExportFunction={
        customExampleImportFileFunction ||
        (async () => [
          entitySchema.reduce((acc, field) => {
            acc[field.key] = t("common:csvImportButton.rowPlaceholder", {
              fieldLabel: field.label.toLowerCase(),
            });
            return acc;
          }, {}),
        ])
      }
      icon={<FileExcelOutlined />}
      type="primary"
      style={{marginBottom: 15}}>
      {t("common:csvImportButton.downloadTheExampleFile")}
    </CsvExportButton>
  );

  return (
    <>
      <Tooltip title={t("common:csvImportButton.importFromCsvFile")}>
        <Button type="link" icon={<ImportOutlined />} onClick={() => setModalOpen(true)}>
          {title}
        </Button>
      </Tooltip>

      <Modal
        centered
        title={t("common:csvImportButton.importInCsv")}
        open={modalOpen}
        onCancel={() => setModalOpen(false)}
        onOk={handleImport}
        width="98%"
        okText={t("common:csvImportButton.importElements")}>
        {/* BETA ALERT */}
        <Alert
          type="warning"
          icon={<BetaTag />}
          message={t("common:csvImportButton.experimentalFeatureAlert.message")}
          description={t("common:csvImportButton.experimentalFeatureAlert.description")}
          showIcon
          style={{marginBottom: 26}}
        />

        {/* CUSTOM HELP CONTENT */}
        {customInfo}

        {/* EXAMPLE FILE */}
        <Alert
          type="info"
          showIcon
          message={t("common:csvImportButton.exampleFileAlert.message")}
          description={
            <>
              <p>
                {t("common:csvImportButton.exampleFileAlert.description", {
                  elementsName: elementsName.toLowerCase(),
                })}
              </p>
              <ExampleImportFileButton />
            </>
          }
          style={{marginBottom: 26}}
        />

        {/* ERRORS REPORTS */}
        {(parsingErrors.length > 0 || validationErrors.length > 0) && (
          <Alert
            type="error"
            message={t("common:csvImportButton.errorsFound.message")}
            description={
              <>
                <p>{t("common:csvImportButton.errorsFound.description")}</p>
                <ul>
                  {parsingErrors.map((error) => (
                    <li>
                      <strong>
                        {t("common:csvImportButton.errorsFound.lineNumber", {
                          number: error.row + 1,
                        })}{" "}
                        - {error.type} :
                      </strong>{" "}
                      {error.message}
                    </li>
                  ))}
                  {validationErrors.map((error) => (
                    <li>
                      <strong>
                        {t("common:csvImportButton.errorsFound.lineNumber", {
                          number: error.row + 1,
                        })}{" "}
                        :
                      </strong>{" "}
                      {error.message}
                    </li>
                  ))}
                </ul>
              </>
            }
            showIcon
            style={{marginBottom: 26}}
          />
        )}

        {/* UPLOAD DRAGGER */}
        <div style={{marginBottom: 26}}>
          <Upload.Dragger
            beforeUpload={(file) => {
              parseCsvFile(file);
              return false;
            }}
            maxCount={1}
            accept="text/csv"
            showUploadList={true}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">{t("common:csvImportButton.clickOrDragFile")}</p>
          </Upload.Dragger>
        </div>

        {/* PREVIEW OF IMPORTED CONTENT */}
        {entitiesToImport.length > 0 && (
          <TableElement.WithTitle
            title={t("common:csvImportButton.importPreviewTitle")}
            dataSource={addKeyToItemsOfList(entitiesToImport)}
            columns={[{title: "N°", width: 50, render: (a, b, i) => i + 1}, ...columnsFromImport]}
            rowClassName={(row, index) =>
              [...validationErrors, ...parsingErrors].find((error) => error.row === index)
                ? " ant-table-row-danger"
                : ""
            }
            rowKey="key"
            scroll={{
              x: (columnsFromImport.length - 1) * 160,
              y: window.innerHeight - 500,
            }}
            showHeader
            pagination
          />
        )}
      </Modal>
    </>
  );
}
