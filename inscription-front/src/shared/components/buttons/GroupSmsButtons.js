import React from "react";
import {App, Button, Tooltip} from "antd";
import {MessageOutlined} from "@ant-design/icons";
import {replaceInText} from "@shared/utils/stringUtilities";
import {useTranslation} from "react-i18next";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";

import {getFullSessionName} from "@shared/utils/sessionsUtilities";
import {flattenFieldComponents} from "@shared/utils/flattenFieldComponents";
import {dateFormatter, timeFormatter} from "@shared/utils/formatters";

const useCreateSmsMessageForSession = (session) => {
  const {t} = useTranslation();
  const sessionNameTemplate = useSelector(
    (state) => currentProjectSelectors.selectProject(state).sessionNameTemplate
  );
  const smsMessageTemplate = useSelector(
    (state) => currentProjectSelectors.selectProject(state).smsMessageTemplate
  );

  return () =>
    replaceInText(smsMessageTemplate || t("projects:schema.smsMessageTemplate.defaultValue"), {
      sessionName: getFullSessionName(session, sessionNameTemplate),
      startDate: dateFormatter.longDate(session.start),
      endDate: dateFormatter.longDate(session.end),
      startTime: timeFormatter.time(session.start),
      endTime: timeFormatter.time(session.end),
      startDateTime: dateFormatter.longDateTime(session.start),
      endDateTime: dateFormatter.longDateTime(session.end),
    });
};

export const usePhoneFormCompsInfo = () => {
  const formComponents = useSelector(
    (state) => currentProjectSelectors.selectProject(state).formComponents
  );
  const flatFormComponents = flattenFieldComponents(formComponents);

  return flatFormComponents.filter((formComp) => formComp.type === "phoneNumber");
};

export const GroupSmsButtons = ({registrations, session}) => {
  const {message} = App.useApp();
  const {t} = useTranslation();
  const {isMobileView} = useWindowDimensions();
  const createSmsMessage = useCreateSmsMessageForSession(session);
  const phoneFormComps = usePhoneFormCompsInfo();
  const multiplePhoneFormComps = phoneFormComps?.length > 1;

  return phoneFormComps.map((formComp) => (
    <Tooltip
      title={
        t("registrations:groupedSmsButtons.tooltip") +
        (multiplePhoneFormComps ? ` (${formComp.label})` : "")
      }
      placement="topLeft"
      key={formComp.key}>
      <Button
        style={{flexGrow: 0}}
        type={"link"}
        icon={<MessageOutlined />}
        disabled={!registrations?.length > 0}
        onClick={() => {
          const phoneNumbers = registrations
            .map((r) => r.formAnswers?.[formComp.key])
            .filter((el) => el && el !== "");

          if (phoneNumbers.length === 0) {
            message.warning(t("registrations:groupedSmsButtons.noPhoneNumbersFound"));
            return;
          }

          if (!isMobileView) {
            message.warning(t("registrations:groupedSmsButtons.useThisFeatureOnAPhone"));
          }

          const textMessage = createSmsMessage();
          console.debug("Recipients :", phoneNumbers);
          console.debug("Message :", textMessage);

          const phoneNumbersEncoded = phoneNumbers.join(",").replaceAll(" ", "");

          const link = document.createElement("a");
          link.href = `sms:${phoneNumbersEncoded}?body=${encodeURIComponent(textMessage)}`;
          link.click();
        }}
      />
    </Tooltip>
  ));
};
