import {useTranslation} from "react-i18next";
import {Button, Tooltip} from "antd";
import {StarFilled, StarOutlined} from "@ant-design/icons";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserSelectors} from "@features/currentUser";
import {usersActions} from "@features/users";

export const BookmarkedProjectButton = ({projectId}) => {
  const dispatch = useDispatch();
  const {t} = useTranslation();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const isBookmarked = currentUser.bookmarkedProject === projectId;
  return (
    <Tooltip title={t("projects:list.bookmarkProjectTooltip")} placement={"topLeft"}>
      <Button
        className={"userTourBookmarkProject"}
        shape={"circle"}
        type={"text"}
        style={{opacity: 0.6}}
        icon={isBookmarked ? <StarFilled /> : <StarOutlined />}
        onClick={(e) => {
          e.stopPropagation();
          dispatch(
            usersActions.persist(
              {
                _id: currentUser._id,
                bookmarkedProject: isBookmarked ? null : projectId,
              },
              false
            )
          );
        }}
      />
    </Tooltip>
  );
};
