import {ArrowDownOutlined} from "@ant-design/icons";
import {Collapse as CollapseEffect} from "@mui/material";
import {Button} from "antd";
import React, {ReactNode, useState} from "react";

export const HideableContent = ({
  buttonOpenTitle,
  buttonCloseTitle = buttonOpenTitle,
  children,
}: {
  buttonOpenTitle: ReactNode;
  buttonCloseTitle?: ReactNode;
  children: ReactNode;
}) => {
  const [open, setOpen] = useState(false);

  const handleToggle = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setOpen(!open);
  };

  return (
    <div>
      <Button
        type="link"
        style={{paddingLeft: 0}}
        onClick={handleToggle}
        icon={
          <ArrowDownOutlined
            style={{
              transition: "all 0.3s ease-in-out",
              transform: open ? "rotate(0)" : "rotate(-90deg)",
            }}
          />
        }>
        {open ? buttonOpenTitle : buttonCloseTitle}
      </Button>
      <CollapseEffect in={open}>
        <div style={{marginTop: 16}}>{children}</div>
      </CollapseEffect>
    </div>
  );
};
