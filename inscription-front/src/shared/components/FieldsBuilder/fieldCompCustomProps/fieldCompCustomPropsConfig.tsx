import {DisplayInput} from "@shared/inputs/DisplayInput";
import {NOEObjectSelectInput} from "@shared/inputs/NOEObjectSelectInput";
import {NumberInput} from "@shared/inputs/NumberInput";
import {RichTextInput} from "@shared/inputs/RichTextInput";
import {TextAreaInput} from "@shared/inputs/TextAreaInput";
import {Stack} from "@shared/layout/Stack";
import {Form} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import React from "react";
import {useTranslation} from "react-i18next";
import {useFieldCompsFormInstance} from "../atoms/useFieldCompsFormInstance";
import {FieldsBuilder, FieldsBuilderConfig} from "../FieldsBuilder";
import {FieldCompInputType, fieldCompInputTypes, FieldCompLayoutType} from "./FieldCompInputType";
import {OptionsList} from "./OptionsList";

type CustomPropsGroupProps = {
  rootName: NamePath;
  name: number;
  modifKeyBasedOnValue?: (value: string) => void;
  config: FieldsBuilderConfig;
};

export type CustomPropsGroup = React.FC<CustomPropsGroupProps>;
export type MainControlsCustomPropsGroup = React.FC<
  CustomPropsGroupProps & {isNewElement?: boolean}
>;

export type FieldComponentCustomProps = {
  MainControls?: MainControlsCustomPropsGroup;
  generalTab?: CustomPropsGroup;
};

export type FieldCompCustomPropsConfig = Partial<
  Record<FieldCompInputType | FieldCompLayoutType, FieldComponentCustomProps>
>;

const ContentEditor: MainControlsCustomPropsGroup = ({name, modifKeyBasedOnValue}) => {
  const {t} = useTranslation();
  return (
    <RichTextInput
      label={t("fieldsBuilder:schema.content.label")}
      placeholder={t("fieldsBuilder:schema.content.placeholder")}
      name={[name, "content"]}
      onChange={modifKeyBasedOnValue}
      toolbarSupercharge={[[{header: [1, 2, 3, 4, 5, 6, false]}]]}
    />
  );
};

const PanelEditor: MainControlsCustomPropsGroup = ({
  rootName,
  name,
  modifKeyBasedOnValue,
  isNewElement,
  config,
}) => {
  const {t} = useTranslation();

  return (
    <>
      <TextAreaInput
        label={t("fieldsBuilder:schema.title.label")}
        autoFocus={!!isNewElement} // Focus if new
        placeholder={t("fieldsBuilder:schema.title.placeholder")}
        autoSize={{minRows: 1}}
        name={[name, "label"]}
        required
        onChange={(event) => {
          // If the key is not defined in the component value, it means it's a new component.
          // So pre-fill it unless the user touches the field
          modifKeyBasedOnValue?.(event.target.value);
        }}
      />
      <div
        style={{
          border: "2px dashed var(--colorBorderSecondary)",
          margin: "24px -4px",
          padding: 18,
          borderRadius: 12,
        }}>
        <FieldsBuilder
          config={{...config, allowPanel: false, nested: true}}
          name={[name, "components"]}
          rootName={[...rootName, name, "components"]}
        />
      </div>
    </>
  );
};

const MinMaxBase = ({
  label,
  name,
  required,
}: {
  label: string;
  name: NamePath;
  required?: boolean;
}) => {
  const {t} = useTranslation();
  return (
    <DisplayInput label={label}>
      <Stack row gap={1}>
        <NumberInput
          noStyle
          placeholder={t("fieldsBuilder:schema.minMax.placeholderMin")}
          name={[...name, "min"]}
          min={0}
          disabled={!required}
          title={!required ? t("fieldsBuilder:schema.minMax.minDisabledIfNotRequired") : undefined}
        />
        -
        <NumberInput
          noStyle
          placeholder={t("fieldsBuilder:schema.minMax.placeholderMax")}
          name={[...name, "max"]}
          min={0}
        />
      </Stack>
    </DisplayInput>
  );
};

const MinMaxMultipleChoiceProps: CustomPropsGroup = ({rootName, name}) => {
  const {t} = useTranslation();
  const form = useFieldCompsFormInstance();
  const requiredVal = Form.useWatch([...rootName, name, "required"], form);
  return (
    <MinMaxBase
      name={[name, "selectedCount"]}
      label={t("fieldsBuilder:schema.minMax.labelSelectedCount")}
      required={requiredVal}
    />
  );
};

const MinMaxNumberProps: CustomPropsGroup = ({name}) => {
  const {t} = useTranslation();
  return (
    <MinMaxBase
      name={[name, "minMaxNumber"]}
      label={t("fieldsBuilder:schema.minMax.labelNumber")}
    />
  );
};

const LinkToObjectProps: CustomPropsGroup = ({name}) => {
  const {t} = useTranslation();
  return (
    <NOEObjectSelectInput
      name={[name, "meta", "endpoint"]}
      label={t("projects:schema.customFieldsComponents.fieldsBuilder.endpoint.label")}
      placeholder={t("projects:schema.customFieldsComponents.fieldsBuilder.endpoint.placeholder")}
      extended
      required
    />
  );
};

export const formCompCustomPropsConfig: FieldCompCustomPropsConfig = {
  // Inputs
  number: {generalTab: MinMaxNumberProps},
  radioGroup: {MainControls: OptionsList},
  select: {MainControls: OptionsList},
  checkboxGroup: {MainControls: OptionsList, generalTab: MinMaxMultipleChoiceProps},
  multiSelect: {MainControls: OptionsList, generalTab: MinMaxMultipleChoiceProps},

  // Layout comps
  content: {MainControls: ContentEditor},
  panel: {MainControls: PanelEditor},
};

export const customFieldCompCustomPropsConfig: FieldCompCustomPropsConfig = Object.fromEntries(
  fieldCompInputTypes.map((fieldCompInputName) => {
    const config = {...formCompCustomPropsConfig[fieldCompInputName]};
    const MainControls = config.MainControls;
    config.MainControls = (props) => (
      <>
        <LinkToObjectProps {...props} /> {MainControls && <MainControls {...props} />}
      </>
    );

    return [fieldCompInputName, config];
  })
);
