import {Form} from "antd";
import {FieldInputComps} from "../FieldComp/FieldComp";

export type FieldCompsFormData = {
  [key: string]: FieldInputComps;
};

export const useFieldCompsFormInstance = () => Form.useFormInstance<FieldCompsFormData>();
