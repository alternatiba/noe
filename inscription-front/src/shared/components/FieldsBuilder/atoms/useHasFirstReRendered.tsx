import {useEffect, useState} from "react";

// AntdForm Form.useWatch() is great, but values are systematically undefined on first re-render.
// This is a workaround to only start rendering on the second re-render
export const useHasFirstReRendered = () => {
  const [hasFirstReRendered, setHasFirstReRendered] = useState(false);
  useEffect(() => setHasFirstReRendered(true), []);
  return hasFirstReRendered;
};
