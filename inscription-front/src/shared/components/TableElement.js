import React from "react";
import type {ButtonProps, PaginationProps, TableColumnProps, TableProps, TooltipProps} from "antd";
import {Button, Table, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined, QuestionCircleOutlined} from "@ant-design/icons";
import type {CardElementProps} from "./CardElement";
import {CardElement} from "./CardElement";
import {useSearchInColumns} from "../hooks/useSearchInColumns";
import {useCopyColumns} from "../hooks/useCopyColumns";
import {useNavigate} from "react-router-dom";
import {usePagination} from "../hooks/usePagination";
import {RowClassName} from "rc-table/lib/interface";

export const TableElement = () => null;

export const getTableWidth = (columns: Array<TableColumnProps>) =>
  columns
    .map((col) => (typeof col.width === "number" ? col.width : 200))
    .reduce((acc, width) => acc + width, 0) - 150; // 150 as a safety for small tables in mobile mode. Otherwise, tables get too large too quick

export const clickIsInsideSelectionColumn = (e) => {
  return [...document.querySelectorAll(".ant-table-selection-column")]?.find((el) =>
    el.contains(e.target)
  );
};

export type SimpleTableProps<T> = Omit<TableProps<T>, "rowClasName"> & {
  onEdit?: (record: T) => void,
  onDelete?: (record: T) => void,
  navigableRootPath?: string,
  showHeader?: boolean,
  columns: Array<TableColumnProps<T>>,
  selectedRowKeys?: Array<T>,
  setSelectedRowKeys?: (selectedRows: Array<T>) => void,
  pagination?: true | false | PaginationProps, // true for default pagination, false for no pagination at all, and some PaginationProps to override the default pagination settings
  handleDisplayConfigChange?: TableProps<T>["onChange"],
  rowClassName?: RowClassName<T>,
};

function SimpleTable<T>({
  onEdit,
  onDelete,
  navigableRootPath,
  showHeader = false,
  columns: rawColumns,
  selectedRowKeys,
  rowSelection,
  pagination = false,
  rowClassName,
  handleDisplayConfigChange: handleDisplayConfigChangeFn,
  ...otherProps
}: SimpleTableProps<T>) {
  const navigate = useNavigate();

  let {columns, handleDisplayConfigChange, columnsFilteredDataSource} = useCopyColumns(
    otherProps.dataSource,
    rawColumns,
    handleDisplayConfigChangeFn
  );
  columns = useSearchInColumns(columns);
  const defaultPagination = usePagination(columnsFilteredDataSource);

  if (onEdit || onDelete) {
    columns.push({
      key: "action",
      width: 15 + (onEdit ? 32 + 15 : 0) + (onDelete ? 32 + 15 : 0),
      render: (text, record) => (
        <div
          className="containerH buttons-container"
          style={{justifyContent: "flex-end"}}
          onDoubleClick={(e) => e.stopPropagation()}>
          {onEdit && (
            <Button
              style={{flexGrow: 0}}
              type="link"
              icon={<EditOutlined />}
              onClick={() => onEdit(record)}
            />
          )}
          {onDelete && (
            <Button
              style={{flexGrow: 0}}
              danger
              type="link"
              icon={<DeleteOutlined />}
              onClick={() => onDelete(record)}
            />
          )}
        </div>
      ),
    });
  }

  const onRowDoubleClickFn = (record) => (e) => {
    if (clickIsInsideSelectionColumn(e)) return;
    onEdit ? onEdit(record) : navigableRootPath && navigate(`${navigableRootPath}/${record._id}`);
  };

  let onRowActions;
  if (selectedRowKeys && rowSelection) {
    // If all the needed elements for a selection are given, activate the selection by clicking on the row

    const rowKey = otherProps.rowKey;
    const selectRow = (record) => {
      let newSelectedRowKeys;
      if (rowSelection.type === "radio") {
        // If we have radio buttons (able to select only one row)
        newSelectedRowKeys = [record];
      } else {
        // If we have checkboxes (able to select only multiple rows)
        newSelectedRowKeys = [...selectedRowKeys];
        const recordIndex = newSelectedRowKeys.findIndex(
          (el) => (el._id || el.key || el[rowKey]) === (record._id || record.key || record[rowKey])
        );
        if (recordIndex >= 0) {
          newSelectedRowKeys.splice(recordIndex, 1);
        } else {
          newSelectedRowKeys.push(record);
        }
      }

      rowSelection.onChange(
        selectedRowKeys.map((el) => el._id || el.key || el[rowKey]),
        newSelectedRowKeys
      );
    };

    onRowActions = (record) => ({
      onDoubleClick: onRowDoubleClickFn(record),
      onClick: () => selectRow(record),
    });

    rowSelection.selectedRowKeys = selectedRowKeys.map((el) => el._id || el.key || el[rowKey]);
  } else {
    onRowActions = (record) => ({
      onDoubleClick: onRowDoubleClickFn(record),
    });
  }

  return (
    <Table
      columns={columns}
      scroll={{x: getTableWidth(columns)}}
      onRow={onRowActions}
      showHeader={showHeader}
      pagination={
        pagination === false ? false : pagination === true ? defaultPagination : pagination
      }
      rowSelection={rowSelection}
      rowKey="_id"
      onChange={handleDisplayConfigChange}
      {...otherProps}
      rowClassName={(record, index, indent) =>
        rowClassName?.(record, index, indent) +
        (onEdit || navigableRootPath ? " cursor-pointer" : "")
      }
    />
  );
}
TableElement.Simple = SimpleTable;

export type TableWithTitleProps<T> = SimpleTableProps<T> &
  Pick<CardElementProps, "title" | "subtitle" | "icon"> & {
    tooltip?: TooltipProps["title"],
    onClickButton?: ButtonProps["onClick"],
    buttonTitle?: string,
    buttonClassName?: string,
  };

function TableElementWithTitle<T>({
  title,
  subtitle,
  tooltip,
  icon,
  onClickButton,
  buttonTitle,
  buttonClassName,
  customButtons,
  ...otherProps
}: TableWithTitleProps<T>) {
  return (
    <CardElement
      title={
        tooltip ? (
          <>
            {title}{" "}
            <Tooltip title={tooltip}>
              <QuestionCircleOutlined
                style={{color: "rgba(46, 48, 56, 0.6)", cursor: "help", fontSize: 14}}
              />
            </Tooltip>
          </>
        ) : (
          title
        )
      }
      icon={icon}
      subtitle={subtitle}
      customButtons={
        <>
          {customButtons}
          {onClickButton && buttonTitle && (
            <Tooltip title={buttonTitle}>
              <Button className={buttonClassName} onClick={onClickButton}>
                {buttonTitle.split(" ")[0]}
              </Button>
            </Tooltip>
          )}
        </>
      }
      style={{overflow: "hidden"}}
      borderless>
      <TableElement.Simple {...otherProps} style={{...otherProps.style, height: "100%"}} />
    </CardElement>
  );
}
TableElement.WithTitle = TableElementWithTitle;
