import {flattenFieldComponents} from "@shared/utils/flattenFieldComponents";
import {Conditional, FieldComp} from "../FieldsBuilder/FieldComp/FieldComp";

const isDefinedConditional = (conditional: Conditional) =>
  conditional &&
  ["show", "doNotShow"].includes(conditional.show) &&
  !!conditional.when &&
  !!conditional.eq;

export const shouldShowField = (
  conditional: Conditional,
  fields: Array<FieldComp>,
  dependencyFieldValue: any
) => {
  // If the conditional parameters are incomplete, just show the field
  if (!isDefinedConditional(conditional)) return true;

  // If the dependency link is broken and dependencyField is undefined, just show the field
  const flatFieldComponents = flattenFieldComponents(fields);
  const dependencyField = flatFieldComponents.find((field) => field.key === conditional.when);
  if (!dependencyField) return true;

  const equalityFunction: (value: any) => boolean =
    // For checkboxes
    dependencyField.type === "checkbox"
      ? (value: boolean) => (conditional.eq?.toLowerCase() !== "false" ? value : !value)
      : // For numbers
      dependencyField.type === "number"
      ? (value: number) => value === parseFloat(conditional.eq)
      : // For select boxes. You can make AND assertions using " AND ", and OR assertions with ";".
      // Ex: "one;two AND three;four;five" means we need at least "one" or "two" AND "three", "four" or "five" to be valid
      dependencyField.type === "checkboxGroup" || dependencyField.type === "multiSelect"
      ? (value: Array<string>) =>
          conditional.eq
            .split(" AND ")
            .every((andCondition) =>
              andCondition.split(";").some((eqString) => value?.includes(eqString))
            )
      : // For everything else, that is stored as string: text, select, radio, etc.
        // If there are multiple possible values separated by ";", compare to each of them:
        // Ex: "one" / "one;two" / "heyHowAreYou;someThing;someStuff"
        (value: string) => conditional.eq.split(";").some((eqString) => eqString === value);

  const shouldShow = dependencyField ? conditional.show === "show" : true;

  return equalityFunction(dependencyFieldValue) ? shouldShow : !shouldShow;
};
