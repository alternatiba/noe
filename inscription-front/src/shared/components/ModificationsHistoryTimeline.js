import {useTranslation} from "react-i18next";
import {useDispatch} from "react-redux";
import React, {useEffect} from "react";
import {Collapse, Timeline} from "antd";
import {Link} from "react-router-dom";
import {personName} from "@shared/utils/utilities";
import {CardElement} from "./CardElement";
import {dateFormatter} from "@shared/utils/formatters";

const ModificationsHistoryTimeline = ({record, elementsActions, path = "./../.."}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const {history, ...recordWithoutHistory} = record || {};

  useEffect(() => {
    dispatch(elementsActions.loadEditingHistory(record?._id));
  }, [JSON.stringify(recordWithoutHistory)]);

  if (!history) return null;
  if (!(history.length > 0)) return t("common:editPage.noHistory");

  return (
    <>
      <Timeline style={{marginTop: 15}}>
        {history.map(({date, registration, diff}, index) => (
          <Timeline.Item key={index}>
            <strong>{dateFormatter.longDateTime(date, {displayInTimeZone: "user"})}</strong>
            {registration && (
              <span>
                {" – "}
                <Link to={`${path}/participants/${registration._id}`}>
                  {personName(registration.user)}
                </Link>
              </span>
            )}
            <CardElement
              size="small"
              borderless
              bodyStyle={{paddingTop: 5, marginBottom: -20}}
              style={{overflow: "hidden", marginTop: 10}}>
              {Object.entries(diff).map(([key, values], index) => (
                <div key={index}>
                  <span style={{marginLeft: 12}}>{key}</span>
                  {Array.isArray(values) ? (
                    <div
                      className="container-grid two-per-row"
                      style={{gap: 0, maxHeight: 300, overflow: "auto"}}>
                      {values[1] !== undefined ? (
                        // Modification
                        <>
                          <pre style={{background: "#fdf5f5", padding: "5px 12px"}}>
                            {JSON.stringify(values[0], null, 2)}
                          </pre>
                          <pre style={{background: "#f3fcf3", padding: "5px 12px"}}>
                            {JSON.stringify(values[1], null, 2)}
                          </pre>
                        </>
                      ) : (
                        // New stuff
                        <pre style={{background: "#f3fcf3", padding: "5px 12px"}}>
                          {JSON.stringify(values[0], null, 2)}
                        </pre>
                      )}
                    </div>
                  ) : (
                    // Deep object
                    <pre style={{background: "#f7f7f7", padding: "5px 12px", maxHeight: 300}}>
                      {JSON.stringify(values, null, 2)}
                    </pre>
                  )}
                </div>
              ))}
            </CardElement>
          </Timeline.Item>
        ))}
      </Timeline>
      <Collapse style={{marginBottom: 26}}>
        <Collapse.Panel header={t("common:rawData")} key={1}>
          <pre>{JSON.stringify(history, null, 2)}</pre>
        </Collapse.Panel>
      </Collapse>
    </>
  );
};

export default ModificationsHistoryTimeline;
