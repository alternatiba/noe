import React, {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "antd";
import {SearchOutlined} from "@ant-design/icons";

import {useWindowDimensions} from "../hooks/useWindowDimensions";
import {viewActions, viewSelectors} from "@features/view";
import {useTranslation} from "react-i18next";
import Search from "antd/es/input/Search";
import isHotkey from "is-hotkey";
import {useDebounce} from "../hooks/useDebounce";

const isCtrlFPressed = isHotkey("mod+F");

export const ListSearchBar = () => {
  const dispatch = useDispatch();
  const {t} = useTranslation();
  const searchBarRef = useRef();

  const selectSearchBarOnFKeyPressed = (event) => {
    if (isCtrlFPressed(event)) {
      event.preventDefault(); // Prevent default browser text search feature
      searchBarRef.current?.select(); // Focus the search bar and select existing text if any
    }
  };

  useEffect(() => {
    window.addEventListener("keydown", selectSearchBarOnFKeyPressed);
    return () => {
      window.removeEventListener("keydown", selectSearchBarOnFKeyPressed);
    };
  }, []);

  const {isMobileView} = useWindowDimensions();
  const [showSearch, setShowSearch] = useState(false);
  const search = useSelector(viewSelectors.selectSearch);

  const debouncedChangeSearch = useDebounce((searchValue) =>
    dispatch(viewActions.changeSearch(searchValue))
  );

  return isMobileView && !showSearch ? (
    <Button icon={<SearchOutlined />} onClick={() => setShowSearch(true)} />
  ) : (
    <Search
      ref={searchBarRef}
      defaultValue={search}
      style={{width: 250}}
      allowClear
      autoFocus={isMobileView}
      onChange={(event) => debouncedChangeSearch(event.target.value)}
      placeholder={t("common:search")}
    />
  );
};
