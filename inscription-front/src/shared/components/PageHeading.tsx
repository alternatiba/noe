import {ArrowLeftOutlined} from "@ant-design/icons";
import {H1Title} from "@shared/layout/typography";
import {Button} from "antd";
import React, {ReactNode} from "react";
import {useNavigate} from "react-router-dom";

export type PageHeadingProps = {
  title: ReactNode;
  icon?: any;
  buttonTitle?: string;
  buttonIcon?: ReactNode;
  onButtonClick?: any;
  customButtons?: ReactNode;
  className?: string;
  backButton?: boolean;
  buttonDisabled?: boolean;
};

export const PageHeading = ({
  title,
  icon,
  buttonTitle,
  buttonIcon,
  onButtonClick,
  customButtons,
  className,
  backButton = false,
  buttonDisabled,
}: PageHeadingProps) => {
  const navigate = useNavigate();
  return (
    <div className={`header-space-between ${className}`} style={{gap: 10}}>
      {/*flexGrow is forced to 1000 to give better rendering in mobile view mode. No better solution found up to now.*/}
      <div style={{flexGrow: 1000, paddingBottom: 5}}>
        {backButton && (
          <Button
            type="link"
            onClick={() => navigate(-1)}
            style={{paddingRight: "20pt", paddingLeft: 5, display: "inline"}}>
            <ArrowLeftOutlined style={{fontSize: "20pt"}} />
          </Button>
        )}
        <H1Title style={{margin: 0, display: "inline"}}>
          {icon && <span style={{paddingRight: 15, opacity: 0.8}}>{icon}</span>}
          {title}
        </H1Title>
      </div>
      <div className="containerH buttons-container" style={{flexGrow: 1, alignItems: "center"}}>
        {customButtons}
        {buttonTitle && (
          <Button
            type="primary"
            disabled={buttonDisabled}
            onClick={onButtonClick}
            icon={buttonIcon}>
            {buttonTitle}
          </Button>
        )}
      </div>
    </div>
  );
};
