import {theme} from "antd";

export const RequiredMark = () => {
  const {token} = theme.useToken();
  const requiredColor = token.colorError;

  // Imitate the default required mark from AntDesign
  return (
    <span
      style={{
        color: requiredColor,
        fontFamily: "SimSun,sans-serif",
        lineHeight: 1,
        paddingRight: 4,
      }}>
      *
    </span>
  );
};
