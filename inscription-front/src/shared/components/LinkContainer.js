import {Tooltip} from "antd";
import React from "react";

export const LinkContainer = ({href, icon, children, showTitle = true, style, ...props}) => {
  const link = (
    <a
      href={href}
      target="_blank"
      rel="noreferrer"
      style={{...style, textDecoration: "none"}}
      {...props}>
      {icon} {showTitle && children}
    </a>
  );

  return showTitle ? (
    link
  ) : (
    <Tooltip title={children} zIndex={100000} mouseEnterDelay={0.6}>
      {link}
    </Tooltip>
  );
};
