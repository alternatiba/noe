import {Column} from "@ant-design/plots";
import {ChartContainer} from "./ChartContainer";

export default function ColumnChart({config}) {
  return <ChartContainer chart={Column} config={config} />;
}
