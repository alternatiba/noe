import {Sunburst} from "@ant-design/plots";
import {ChartContainer} from "./ChartContainer";

export default function SunburstChart({config}) {
  return <ChartContainer chart={Sunburst} config={config} />;
}
