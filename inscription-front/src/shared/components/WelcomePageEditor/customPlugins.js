// The image plugin
import {imagePlugin} from "@react-page/plugins-image";
import "@react-page/plugins-image/lib/index.css";

// The background plugin
import background, {ModeEnum} from "@react-page/plugins-background";
import "@react-page/plugins-background/lib/index.css";

export const backgroundP = background({
  enabledModes: ModeEnum.COLOR_MODE_FLAG | ModeEnum.IMAGE_MODE_FLAG | ModeEnum.GRADIENT_MODE_FLAG,
});
export const imageP = imagePlugin();
