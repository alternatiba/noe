import {Form, FormListOperation, Modal, ModalProps} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import React, {ReactNode, useState} from "react";
import {FormElement, safeValidateFields} from "../inputs/FormElement";
import {FormTable, FormTableProps, WithKey} from "./FormTable";

/**
 * A Table that lets you add some items via a modal
 * @param props props for the table and the modal (to be defined, see usages)
 * @returns {JSX.Element}
 * @constructor
 */

type TableWithEditModalProps<T, InitialT = {}> = {
  name: NamePath;
  modalTitle: ReactNode;
  children: ReactNode;
  postTransform?: (values: InitialT | T) => T;
  validate?: (values: InitialT | T) => void;
  modalProps: ModalProps;
} & FormTableProps<T>;

export const TableWithEditModal = <T,>(props: TableWithEditModalProps<WithKey<T>>) => {
  const form = Form.useFormInstance();

  const InnerTableWithEditModal = ({
    add,
    remove,
    name,
    onClickButton,
    modalTitle,
    children,
    postTransform,
    validate,
    modalProps,
    ...props
  }: TableWithEditModalProps<WithKey<T>> & Pick<FormListOperation, "add" | "remove">) => {
    const [modalForm] = Form.useForm();
    const [currentElementKey, setCurrentElementKey] = useState<number | "new">();
    const modalIsOpen = currentElementKey !== undefined;

    const onOpenCreateModal = () => {
      setCurrentElementKey("new");
    };

    const onOpenEditModal = (record: WithKey<T>) => {
      modalForm.setFieldsValue(form.getFieldValue([name, record.key]));
      setCurrentElementKey(record.key);
    };

    const onCloseModal = () => {
      modalForm.resetFields();
      setCurrentElementKey(undefined);
    };

    const onDeleteElement = (record: WithKey<T>) => {
      remove(record.key);
    };

    const onSaveElement = () =>
      safeValidateFields(modalForm, () => {
        let values = modalForm.getFieldsValue();

        if (postTransform) values = postTransform(values);
        if (validate) validate(values);

        if (currentElementKey === "new") add(values);
        else {
          // We have to do this because we can't find a way to trigger the form onValuesChange() stuff with form.setFieldsValue()
          // https://github.com/ant-design/ant-design/issues/22168
          remove(currentElementKey as number);
          add(values, currentElementKey);
        }

        onCloseModal();
      });

    return (
      <>
        <FormTable
          name={name}
          {...props}
          onClickButton={onClickButton || onOpenCreateModal}
          onEdit={onOpenEditModal}
          onDelete={onDeleteElement}
        />

        <Modal
          destroyOnClose
          title={modalTitle}
          open={modalIsOpen}
          onOk={onSaveElement}
          onCancel={onCloseModal}
          {...modalProps}>
          <FormElement form={modalForm} onValidate={onSaveElement}>
            {children}
          </FormElement>
        </Modal>
      </>
    );
  };

  return (
    <Form.List name={props.name}>
      {(fields, {add, remove}) => <InnerTableWithEditModal add={add} remove={remove} {...props} />}
    </Form.List>
  );
};
