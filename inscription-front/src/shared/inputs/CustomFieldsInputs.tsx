import {EyeOutlined} from "@ant-design/icons";
import {FormInstance} from "antd";
import {useTranslation} from "react-i18next";
import {BetaTag} from "../components/BetaTag";
import {CardElement} from "../components/CardElement";
import FormRenderer from "../components/FormRenderer/FormRenderer";
import {useGetCustomFieldsForEndpointAndMode} from "../hooks/useGetCustomFieldsForEndpointAndMode";
import {NOEEndpointExtended} from "./NOEObjectSelectInput";

type CustomFieldsInputsProps = {
  form: FormInstance<any>;
  endpoint: NOEEndpointExtended;
};

export const CustomFieldsInputs = ({form, endpoint}: CustomFieldsInputsProps) => {
  const {t} = useTranslation();
  const customFieldCompsForEndpoint = useGetCustomFieldsForEndpointAndMode({
    endpoint,
    mode: "inEditPage",
  });

  return customFieldCompsForEndpoint.length > 0 ? (
    <CardElement
      icon={<EyeOutlined />}
      title={
        <span>
          <BetaTag title={"beta"} /> {t("projects:schema.customFieldsComponents.label")}
        </span>
      }>
      <div className={"container-grid two-per-row"}>
        <FormRenderer
          fields={customFieldCompsForEndpoint.map((customFielComp) => ({
            ...customFielComp,
            key: ["customFields", customFielComp.key],
            bordered: false,
          }))}
          form={form}
        />
      </div>
    </CardElement>
  ) : null;
};
