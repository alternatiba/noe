import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import React from "react";
import type {PendingProps} from "../components/Pending";
import {PendingSuspense} from "../components/Pending";
import type {TextEditorProps} from "../components/TextEditor";
import {FormItem, FormItemProps} from "./FormItem";

const TextEditor = lazyWithRetry(
  () => import(/* webpackPreload: true */ "../components/TextEditor")
);

export const RichTextInput = ({
  noFadeIn,
  ...props
}: FormItemProps<TextEditorProps> & {noFadeIn?: PendingProps["noFadeIn"]}) => (
  // Need to add the suspense around the form tag, because if in between the form data doesn't pass and it creates bugs
  <PendingSuspense animationDelay={"800ms"} minHeight={100} noFadeIn={noFadeIn}>
    <FormItem {...props}>
      <TextEditor />
    </FormItem>
  </PendingSuspense>
);
