import {ReactElement} from "react";
import type {FormItemProps} from "./FormItem";
import {FormItem} from "./FormItem";

/**
 * This component renders a form item layout around something, but just for the layout purpose.
 * It does not pass any props to its children, avoiding some errors if it was to do so.
 */
export const DisplayInput = ({
  children,
  ...props
}: Omit<FormItemProps<{}>, "name" | "children"> & {
  name?: FormItemProps<any>["name"];
  children: ReactElement;
}) => {
  // We make a child component to wrap the children, so that props are not passed to children
  const ChildComp = () => <>{children}</>;
  return (
    <FormItem {...props}>
      <ChildComp />
    </FormItem>
  );
};
