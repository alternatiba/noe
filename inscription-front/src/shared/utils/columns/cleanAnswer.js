import React from "react";

// Same function as in api/src/controllers/pdf.ts
export const cleanAnswer = (
  answer,
  separator = ",\n",
  fieldComp = {},
  renderMode: "text" | "html" = "text"
) => {
  const {type, options} = fieldComp;

  const findLabelForOption = (optValue) =>
    options.find((opt) => opt.value === optValue)?.label || optValue;

  if (type === "phoneNumber") {
    return renderMode === "text" ? answer : <a href={`tel:${answer}`}>{answer}</a>;
  } else if (type === "checkboxGroup" || type === "multiSelect") {
    return answer?.map(findLabelForOption).join(separator);
  } else if (type === "checkbox") {
    return answer ? "✔️" : "❌";
  } else if (type === "select" || type === "radioGroup") {
    return findLabelForOption(answer);
  } else {
    return answer;
  }
};
