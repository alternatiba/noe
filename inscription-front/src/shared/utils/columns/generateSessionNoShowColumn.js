import {SessionNoShowCheckbox} from "@shared/components/SessionNoShowCheckbox";
import {getSessionSubscription} from "@utils/registrationsUtilities";
import React from "react";

export const generateSessionNoShowColumn = (session, registrations, dispatch) => ({
  title: "No show",
  dataIndex: "hasNotShownUp",
  render: (text, record) => (
    <SessionNoShowCheckbox
      session={session}
      registration={record}
      registrations={registrations}
      dispatch={dispatch}
    />
  ),
  filters: [
    {text: "Oui", value: true},
    {text: "Non", value: false},
  ],
  onFilter: (value, record) => {
    const noShow = getSessionSubscription(record, session).hasNotShownUp;
    return value ? noShow : !noShow; // hasNotShownUp is a user id (not a boolean), so we need to make this condition to make the filter work
  },
});
