import {EyeOutlined} from "@ant-design/icons";
import {EditableCell} from "@shared/components/EditableCell";
import {useGetCustomFieldsForEndpointAndMode} from "@shared/hooks/useGetCustomFieldsForEndpointAndMode";
import {NOEEndpointExtended} from "@shared/inputs/NOEObjectSelectInput";
import {cleanAnswer} from "@shared/utils/columns/cleanAnswer";
import {TableColumnProps} from "antd";
import {useTranslation} from "react-i18next";
import {useDispatch} from "react-redux";
import {sorter} from "../sorters";

type GetCustomFieldsColumnsParams = {
  endpoint: NOEEndpointExtended;
  elementsActions?: {persist: (fieldsToUpdate?: any) => any};
};

// Get the columns generated from the participant custom data
export const useCustomFieldsColumns = ({
  endpoint,
  elementsActions,
}: GetCustomFieldsColumnsParams): TableColumnProps<any>[] => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const customFieldCompsForEndpoint = useGetCustomFieldsForEndpointAndMode({
    endpoint,
    mode: "inListView",
  });

  return customFieldCompsForEndpoint.map((customFieldComp) => {
    const cleanAnswerFormComp = (record: any) =>
      cleanAnswer(record.customFields?.[customFieldComp.key], undefined, customFieldComp);

    return {
      title: (
        <>
          <EyeOutlined
            title={t("projects:schema.customFieldsComponents.label")}
            style={{opacity: 0.8}}
          />{" "}
          {customFieldComp.displayName}
        </>
      ),
      dataIndex: customFieldComp.displayName,
      render: (text, record) => (
        <EditableCell
          value={record.customFields?.[customFieldComp.key]}
          fieldComp={customFieldComp}
          onChange={
            elementsActions
              ? async (value) => {
                  await dispatch(
                    elementsActions.persist({
                      _id: record._id,
                      customFields: {...record.customFields, [customFieldComp.key]: value},
                    })
                  );
                }
              : undefined
          }
        />
      ),
      // TODO : text sorter and test filter are not very adapted. Do better
      sorter: (a, b) => sorter.text(cleanAnswerFormComp(a), cleanAnswerFormComp(b)),
      searchable: true,
      searchText: cleanAnswerFormComp,
    };
  });
};
