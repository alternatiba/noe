import {t} from "i18next";

export const checkPasswordsAreSame = (form) => (_, value) => {
  if (!value || form.getFieldValue("password") === value) {
    return Promise.resolve();
  }
  return Promise.reject(new Error(t("common:formValidation.password.dontMatch")));
};

export const validatePassword = (form) => (_, value) => {
  if (!value || value.length === 0) return Promise.resolve();

  const containsDigit = /\d/.test(value);
  const containsLowerCaseLetter = /[a-z]/.test(value);
  const containsUpperCaseLetter = /[A-Z]/.test(value);

  const missing = [];
  containsDigit || missing.push(t("common:formValidation.password.aNumber"));
  containsLowerCaseLetter || missing.push(t("common:formValidation.password.aLowerCaseLetter"));
  containsUpperCaseLetter || missing.push(t("common:formValidation.password.anUpperCaseLetter"));

  if (missing.length === 0) return Promise.resolve();

  return Promise.reject(
    t("common:formValidation.password.mustContainList", {
      field: form.getFieldInstance("password").input.labels[0].innerText,
      missing,
    })
  );
};

export const checkOldPasswordIsNotTheSameValidator = ({getFieldValue}) => ({
  validator(_, value) {
    const newPasswordGiven = getFieldValue("oldPassword");
    const oldPasswordGiven = value;

    return newPasswordGiven.length && !oldPasswordGiven.length
      ? Promise.reject(new Error("Vous devez saisir votre ancien mot de passe pour le changer."))
      : newPasswordGiven === oldPasswordGiven
      ? Promise.reject(new Error("Le nouveau mot de passe doit être différent du précédent."))
      : Promise.resolve();
  },
});
