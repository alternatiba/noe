import {
  capitalize,
  isValidObjectId,
  normalize,
  replaceInText,
  replaceInTextWithJSX,
  truncate,
} from "./stringUtilities";

describe("stringUtilities", () => {
  describe("truncate", () => {
    it("truncates a string to the given length", () => {
      expect(truncate("Hello World", 5)).toBe("Hello...");
    });
  });

  describe("capitalize", () => {
    it("capitalizes the first letter of a string", () => {
      expect(capitalize("hello how are you")).toBe("Hello how are you");
    });
  });

  describe("isValidObjectId", () => {
    it("returns true for a valid object id", () => {
      expect(isValidObjectId("507f191e810c19729de860ea")).toBe(true);
    });

    it("returns false for an invalid object id", () => {
      expect(isValidObjectId("invalid")).toBe(false);
    });
  });

  describe("normalize", () => {
    it("normalizes the string", () => {
      expect(normalize("Résumé")).toBe("resume");
    });

    it("optionally keeps case", () => {
      expect(normalize("Résumé", true)).toBe("Resume");
    });
  });

  describe("replaceInText", () => {
    it("replaces values in text with delimiter", () => {
      const text = "Hello {{name}}. Today is {{date}}";
      expect(
        replaceInText(text, {
          name: "John",
          date: "01/01/2020",
        })
      ).toBe("Hello John. Today is 01/01/2020");
    });

    it("uses custom delimiters", () => {
      expect(replaceInText("Hello [[name]]", {name: "John"}, {start: "[[", end: "]]"})).toBe(
        "Hello John"
      );
    });

    it("returns original text if no match", () => {
      const text = "Some text";
      const replaced = replaceInText(text, {"{{name}}": "John"});
      expect(replaced).toBe("Some text");
    });

    it("handles empty text input", () => {
      const text = "";
      const replaced = replaceInText(text, {"{{name}}": "John"});
      expect(replaced).toBe(undefined);
    });
  });

  describe("replaceInTextWithJSX", () => {
    it("replaces text with JSX", () => {
      const text = "Hello {{name}}";
      const values = {
        name: <b>John</b>,
      };

      const result = replaceInTextWithJSX(text, values);

      expect(<>{result}</>).toMatchObject(
        <>
          Hello <b>John</b>
        </>
      );
    });

    it("handles empty text", () => {
      const text = "";
      const values = {
        name: <b>John</b>,
      };

      const result = replaceInTextWithJSX(text, values);

      expect(<>{result}</>).toMatchObject(<></>);
    });

    it("replaces multiple values", () => {
      const text = "Hello {{name}}, today is {{date}}";
      const values = {
        name: <b>John</b>,
        date: <i>01/01/2020</i>,
      };

      const result = replaceInTextWithJSX(text, values);

      expect(<>{result}</>).toMatchObject(
        <>
          Hello <b>John</b>, today is <i>01/01/2020</i>
        </>
      );
    });
  });
});
