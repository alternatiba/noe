import React, {Suspense, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {App, Badge, Button, Tooltip} from "antd";
import {PendingSuspense} from "@shared/components/Pending";
import {
  CarryOutOutlined,
  DoubleLeftOutlined,
  FormOutlined,
  HomeOutlined,
  PlaySquareOutlined,
  ScheduleOutlined,
  UserSwitchOutlined,
} from "@ant-design/icons";
import {currentUserActions, currentUserSelectors} from "@features/currentUser";
import {currentProjectActions, currentProjectSelectors} from "@features/currentProject";
import {LayoutStructure} from "@shared/layout/LayoutStructure";
import {displayNotification} from "@shared/utils/displayNotification";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {viewSelectors} from "@features/view";
import {sessionsActions} from "@features/sessions";
import {
  ACTIVATING_OFFLINE_MODE,
  DISPLAY_OFFLINE_MODE_FEATURE,
  OFFLINE_MODE,
  OnlineOfflineSwitch,
  useOfflineMode,
} from "@shared/utils/offlineModeUtilities";
import {personName} from "@shared/utils/utilities";
import {URLS} from "@app/configuration";
import {Trans, useTranslation} from "react-i18next";
import {MenuLayout} from "@shared/components/Menu";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {useBrowserTabTitle} from "@shared/hooks/useBrowserTabTitle";
import {Route, Routes} from "react-router";
import {useNavigate, useParams} from "react-router-dom";
import {Redirect} from "@shared/pages/Redirect";
import {DynamicProjectThemeProvider} from "@shared/layout/DynamicProjectThemeProvider";
import {useProjectOpeningStateAccessRights} from "@utils/useProjectOpeningStateAccessRights";
import {useNOEInstanceName} from "@shared/hooks/useNOEInstanceName";
import {useProjectStatusNotification} from "@shared/hooks/useProjectStatusNotification";

const VolunteeringGauge = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "low" */
    "@shared/components/VolunteeringGauge"
  )
);
const Registration = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "high" */
    "./registration/Registration"
  )
);
const SessionAgenda = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionAgenda")
);
const SessionList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionList")
);
const SessionShow = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionShow")
);
const ParticipantList = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "low" */
    "./participants/ParticipantList"
  )
);
const Welcome = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "high" */
    "./welcome/Welcome"
  )
);

export default function ProjectLayout() {
  const {notification} = App.useApp();
  const {t} = useTranslation();
  const navigate = useNavigate();
  const {projectId, "*": page} = useParams();
  const dispatch = useDispatch();

  // ****** SELECTORS & STATES ******

  const currentProject = useSelector(currentProjectSelectors.selectProject);

  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);
  const authenticatedRegistration = useSelector(registrationsSelectors.selectAuthenticated);

  const dirty = useSelector(currentUserSelectors.selectDirty);

  // ****** DATA FETCHING ******

  // Load project and project registration
  useEffect(() => {
    // We should not fire these actions if the offline mode is already activated.
    // Instead, fire it when offline is disabled OR when it is activating offline mode and needs to load all the data
    const shouldLoadProjectData = !OFFLINE_MODE || ACTIVATING_OFFLINE_MODE;

    if (currentUser._id && shouldLoadProjectData) {
      dispatch(currentProjectActions.load(projectId))
        // Then only, load project registration, because it is dependent from the project
        // (on inscription front, we need the project to be loaded to get the registration metadata)
        .then(async () => {
          dispatch(registrationsActions.loadCurrent());

          setTimeout(() => {
            dispatch(sessionsActions.loadList({silent: true}));
          }, 3000);
        })
        .catch(() => navigate("/projects"));
    }
  }, [currentUser?._id, projectId]);

  useOfflineMode(authenticatedRegistration?._id);

  useProjectStatusNotification();

  // ****** DATA DEFINITION ******

  const projectAndUserValid =
    currentUser._id &&
    currentProject?._id &&
    currentRegistration?._id &&
    authenticatedRegistration?._id;

  // Registration completion
  const {formIsOk, ticketingIsOk, everythingIsOk, firstVisit, booked} = currentRegistration || {};
  const everythingIsOkInDatabase = currentRegistration?.inDatabase?.everythingIsOk;

  // User rights and registration
  const isConnectedAsAnotherUser = authenticatedUser._id !== currentUser._id;

  // Project opening state
  const {
    giveAccessToSessionsPage,
    openingStateAllowsAccessToSessions,
    userHasAccessToSecretSchedule,
    authenticatedUserIsProjectOrga,
  } = useProjectOpeningStateAccessRights();

  const agendaMode = useSelector(viewSelectors.selectSessionsViewMode) === "agenda";
  // Utilities
  const pageRoot = page?.split("/").slice(0, 2).join("/");
  const registrationPagName = everythingIsOkInDatabase
    ? t("registrations:labelMyRegistration")
    : t("registrations:labelRegister");

  // ****** TAB TITLE ******

  useBrowserTabTitle(
    pageRoot,
    {
      welcome: t("welcome:label"),
      registration: registrationPagName,
      "sessions/all": t("sessions:labelAll"),
      "sessions/subscribed": t("sessions:labelSubscribed"),
      participants: t("registrations:actions.changeIdentity"),
    },
    {
      baseName: currentProject.name,
      suffix: useNOEInstanceName("inscriptionFront"),
    }
  );

  // ****** NOTIFICATIONS ******

  // TODO hookify this
  // Persistent notif if registration incomplete
  useEffect(() => {
    if (projectAndUserValid && booked !== false && !firstVisit && pageRoot !== "registration") {
      everythingIsOkInDatabase // Those can be undefined at the beginning
        ? notification.destroy("notifRegistrationIncomplete")
        : displayNotification("info", "notifRegistrationIncomplete", {
            message: t("registrations:notifIncomplete.title"),
            description: (
              <ul style={{margin: 0}}>
                {!(formIsOk !== false) && (
                  <li>
                    <Trans i18nKey="notifIncomplete.form" ns="registrations" />
                  </li>
                )}
                {!(ticketingIsOk !== false) && (
                  <li>
                    <Trans i18nKey="notifIncomplete.ticketing" ns="registrations" />
                  </li>
                )}
              </ul>
            ),
            buttonText: t("registrations:notifIncomplete.completeMyRegistration"),
            onClickButton: () => navigate(`/${projectId}/registration`),
          });
    } else {
      notification.destroy("notifRegistrationIncomplete");
    }
  }, [projectAndUserValid, booked, ticketingIsOk, formIsOk, pageRoot]);

  // ****** SIDE MENU ******

  const collapsedSidebar = page?.includes("agenda");

  const menu = {
    top: (
      <>
        <MenuLayout
          rootNavUrl={`/${projectId}/`}
          selectedItem={pageRoot}
          items={[
            userHasAccessToSecretSchedule && {
              label: t("welcome:label"),
              key: "welcome",
              icon: <HomeOutlined />,
            },
            {
              label: (
                <>
                  {registrationPagName}
                  {!dirty && (
                    <Badge
                      dot
                      count={everythingIsOk && everythingIsOkInDatabase ? 0 : 1}
                      style={{marginBottom: 10, width: 8, height: 8}}
                    />
                  )}
                </>
              ),
              key: "registration",
              icon: <FormOutlined />,
              disabled: dirty,
            },
          ]}
        />
        <Tooltip
          title={
            currentProject.secretSchedule
              ? t("common:menuNavigation.noAccess.secretSchedule")
              : !openingStateAllowsAccessToSessions
              ? t("common:menuNavigation.noAccess.projectOpeningState")
              : undefined
          }
          placement="right">
          <MenuLayout
            rootNavUrl={`/${projectId}/`}
            selectedItem={page}
            disabled={!giveAccessToSessionsPage}
            divider
            items={[
              // All sessions
              {
                label:
                  everythingIsOkInDatabase && !currentProject.blockSubscriptions
                    ? t("common:menuNavigation.subscribeToActivities")
                    : t("common:menuNavigation.seeAllActivities"),
                key: `sessions/all${agendaMode ? "/agenda" : ""}`,
                icon: <ScheduleOutlined />,
                disabled: dirty || !giveAccessToSessionsPage,
              },

              // Participant planning
              everythingIsOkInDatabase && {
                label: t("sessions:labelSubscribed"),
                key: `sessions/subscribed${agendaMode ? "/agenda" : ""}`,
                icon: <CarryOutOutlined />,
                disabled: dirty || !giveAccessToSessionsPage,
              },
            ]}
          />

          {/*Volunteering jauge*/}
          {!currentProject.hideVolunteeringJaugeForParticipants &&
            giveAccessToSessionsPage &&
            everythingIsOkInDatabase && (
              <Suspense fallback={null}>
                <MenuLayout
                  divider
                  title={
                    collapsedSidebar
                      ? t("registrations:schema.voluntaryCounter.label").slice(0, -2)
                      : t("registrations:schema.voluntaryCounter.label")
                  }>
                  <VolunteeringGauge
                    style={{
                      paddingBottom: "5pt",
                      paddingLeft: "1em",
                      width: "calc(100% - 1em)",
                      alignSelf: "center",
                    }}
                    registration={currentRegistration}
                  />
                </MenuLayout>
              </Suspense>
            )}
        </Tooltip>
      </>
    ),

    footer: (
      <MenuLayout
        selectedItem={page}
        items={[
          // Identity swap
          authenticatedUserIsProjectOrga && {
            label: isConnectedAsAnotherUser ? (
              <span style={{fontWeight: "bold"}}>{personName(currentUser)}</span>
            ) : (
              t("registrations:actions.changeIdentity")
            ),
            key: "participants",
            onClick: () => navigate(`/${projectId}/participants`),
            icon: <UserSwitchOutlined />,
            danger: isConnectedAsAnotherUser,
            disabled: OFFLINE_MODE,
          },

          // Stop identity swap button
          isConnectedAsAnotherUser && {
            label: (
              <Button
                danger
                ghost
                block
                onClick={() => {
                  navigate(`/${projectId}/participants`);
                  dispatch(currentUserActions.changeConnectedAsUser(undefined));
                }}>
                {t("common:stop")}
              </Button>
            ),
            url: false,
          },

          // Orga front
          authenticatedUserIsProjectOrga && {
            label: t("common:pagesNavigation.orgaFront"),
            url: `${URLS.ORGA_FRONT}/${projectId}/${page}`,
            icon: <PlaySquareOutlined />,
          },

          // Back to events page
          {
            label: t("projects:labelMyProjects"),
            onClick: () => navigate({pathname: "/projects", search: "?no-redir"}),
            icon: <DoubleLeftOutlined />,
            disabled: OFFLINE_MODE,
          },

          // Offline mode
          DISPLAY_OFFLINE_MODE_FEATURE &&
            !collapsedSidebar && {
              label: <OnlineOfflineSwitch />,
              url: false,
            },
        ]}
      />
    ),
  };

  return (
    <DynamicProjectThemeProvider>
      <LayoutStructure
        title={currentProject.name}
        menu={menu}
        profileUser={authenticatedUser}
        displayButtonBadge={
          pageRoot !== "registration" && !(everythingIsOk && everythingIsOkInDatabase)
        }
        collapsedSidebar={collapsedSidebar}>
        {projectAndUserValid && ( // If we don't know if the user has registered to the project, we wait
          <PendingSuspense minHeight={"100vh"}>
            <Routes style={{height: "100%"}}>
              {/* Redirect when switching from one frontend to another*/}
              <Route path="/sessions/agenda" element={<Redirect to="./../all/agenda" />} />

              {userHasAccessToSecretSchedule && <Route path="/welcome" element={<Welcome />} />}
              <Route
                path="/registration"
                element={<Registration giveAccessToSessionsPage={giveAccessToSessionsPage} />}
              />
              {giveAccessToSessionsPage && (
                <>
                  <Route path="/sessions/all" element={<SessionList />} />
                  <Route path="/sessions/all/agenda" element={<SessionAgenda />} />
                  {everythingIsOkInDatabase && [
                    <Route path="/sessions/subscribed" element={<SessionList key="1" />} />,
                    <Route
                      path="/sessions/subscribed/agenda"
                      element={<SessionAgenda key="2" />}
                    />,
                  ]}
                  <Route path="/sessions/:id" element={<SessionShow />} />
                </>
              )}
              {authenticatedUserIsProjectOrga && (
                <Route path="/participants" element={<ParticipantList />} />
              )}

              <Route
                path="/*"
                element={
                  <Redirect
                    to={
                      everythingIsOkInDatabase && giveAccessToSessionsPage
                        ? "./sessions/subscribed"
                        : "./registration"
                    }
                  />
                }
              />
            </Routes>
          </PendingSuspense>
        )}
      </LayoutStructure>
    </DynamicProjectThemeProvider>
  );
}
