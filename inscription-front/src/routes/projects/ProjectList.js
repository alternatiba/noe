import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "@features/projects";
import {currentProjectActions, currentProjectSelectors} from "@features/currentProject";
import {currentUserSelectors} from "@features/currentUser";
import {GridPage} from "@shared/pages/GridPage";
import {CardElement} from "@shared/components/CardElement";
import WelcomePageEditor from "@shared/components/WelcomePageEditor/WelcomePageEditor";
import {
  CalendarFilled,
  CalendarOutlined,
  SaveOutlined,
  EyeOutlined,
  EyeInvisibleOutlined,
} from "@ant-design/icons";
import {Tag, Tooltip} from "antd";
import {useTranslation} from "react-i18next";
import dayjs from "@shared/services/dayjs";
import {BookmarkedProjectButton} from "@shared/components/buttons/BookmarkedProjectButton";
import {useUserTour} from "@shared/utils/userTourUtilities";
import projectsListsUserTour from "@utils/userTours/projectsListsUserTour";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {useNavigate} from "react-router-dom";
import {dateFormatter} from "@shared/utils/formatters";
import {isInStandaloneMode} from "@shared/utils/isInStandaloneMode";
import {ToggleButton} from "@shared/components/ToggleElement";

export default function ProjectList({displayAllPublicProjects = false}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const projects = useSelector(projectsSelectors.selectList);
  const publicProjects = useSelector(projectsSelectors.selectPublicList);
  const {isMobileView} = useWindowDimensions();
  const currentlyDisplayedProjects = displayAllPublicProjects ? publicProjects : projects;

  const [showPastEvents, setShowPastEvents] = useState(false);

  let dataSource = currentlyDisplayedProjects;
  dataSource = showPastEvents
    ? dataSource
    : dataSource.filter((project) => dayjs(project.end).isAfter(dayjs()));
  const alreadyLoadedProject = useSelector(currentProjectSelectors.selectProject);
  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);

  useEffect(() => {
    dispatch(projectsActions.loadList({displayAllPublicProjects}));
  }, []);

  const cleanStateAndNavigate = async (projectId) => {
    const shouldReload =
      alreadyLoadedProject._id &&
      alreadyLoadedProject._id !== projectId &&
      alreadyLoadedProject.slug !== projectId;
    // If there is already a loaded project, and that it's not the same as the requested project, clean everything. Otherwise, keep the data
    if (shouldReload) await dispatch(currentProjectActions.cleanProject());

    const projectToNavigateTo = currentlyDisplayedProjects.find(
      (project) => project.slug === projectId || project._id === projectId
    );

    projectToNavigateTo &&
      navigate(
        // Then navigate only after cleaning (to welcome page if no registration, or if there is a registration, let the ProjectLayout decide)
        authenticatedUser.registrations?.find(
          (registration) => registration.project === projectToNavigateTo._id
        )
          ? `/${projectToNavigateTo.slug || projectToNavigateTo._id}`
          : `/${projectToNavigateTo.slug || projectToNavigateTo._id}/welcome`
      );
  };

  // If a project has been bookmarked, then load the page directly. Only redirect if there is no "no-redir" in the URL
  useEffect(() => {
    currentlyDisplayedProjects?.length > 0 &&
      authenticatedUser?.bookmarkedProject &&
      new URLSearchParams(window.location.search).get("no-redir") === null &&
      cleanStateAndNavigate(authenticatedUser.bookmarkedProject);
  }, [currentlyDisplayedProjects, authenticatedUser.bookmarkedProject]);

  useUserTour("projectsLists", projectsListsUserTour(isMobileView), {
    shouldShowNow: () =>
      isInStandaloneMode() && // We are in the installed PWA app
      currentlyDisplayedProjects.length > 0 && // some projects are available
      authenticatedUser._id && // is connected
      !authenticatedUser.bookmarkedProject, // has not used the feature yet
    deps: [currentlyDisplayedProjects, authenticatedUser],
    delayBeforeShow: 2000,
  });

  return (
    <GridPage
      icon={displayAllPublicProjects ? <CalendarFilled /> : <CalendarOutlined />}
      title={
        displayAllPublicProjects ? t("projects:labelPublicProjects") : t("projects:labelMyProjects")
      }
      subtitle={
        <div style={{marginBottom: 15}}>
          {displayAllPublicProjects
            ? t("projects:subtitlePublicProjects")
            : t("projects:subtitleMyProjects")}
        </div>
      }
      customControls={
        <ToggleButton
          checked={showPastEvents}
          onChange={setShowPastEvents}
          checkedIcon={<EyeOutlined />}
          uncheckedIcon={<EyeInvisibleOutlined />}>
          {t("projects:pastEvents")}
        </ToggleButton>
      }
      searchInFields={(project) => [
        project.name,
        dateFormatter.longDateRange(project.start, project.end, {short: false, withYear: true}),
      ]}
      renderItem={(item) => (
        <CardElement
          title={
            <>
              {/* Only display bookmarks if connected */}
              {authenticatedUser._id && (
                <div style={{position: "absolute", top: 0, right: 0}}>
                  <BookmarkedProjectButton projectId={item._id} />
                </div>
              )}

              <div style={{textAlign: "center", paddingTop: 12, paddingBottom: 12, width: "100%"}}>
                <div
                  style={{
                    // Allow activity name to go on 3 lines not more
                    whiteSpace: "normal",
                    fontWeight: "bold",
                    overflow: "hidden",
                    display: "-webkit-box",
                    WebkitLineClamp: "3",
                    WebkitBoxOrient: "vertical",
                  }}>
                  <strong>
                    {item.name}
                    {alreadyLoadedProject._id === item._id && authenticatedUser.superAdmin && (
                      <Tooltip title={t("projects:list.alreadySavedContentIconTooltip")}>
                        <SaveOutlined style={{marginLeft: 8, opacity: 0.6}} />
                      </Tooltip>
                    )}
                  </strong>
                </div>
                {item.start && item.end && (
                  <Tag style={{width: "auto"}}>
                    {dateFormatter.longDateRange(item.start, item.end, {
                      short: true,
                      withYear: true,
                    })}
                  </Tag>
                )}
              </div>
            </>
          }
          borderless
          hoverable
          onClick={() => cleanStateAndNavigate(item.slug || item._id)}
          style={{margin: 15, overflow: "hidden", height: 300, borderColor: "#c9c9c9"}}
          bodyStyle={{overflow: "hidden"}}>
          <div
            style={{
              zoom: 0.3,
              // Fixes for the Firefox browser, cause zoom is not supported
              ...(navigator.userAgent.toLowerCase().indexOf("firefox") > -1 && {
                "-moz-transform": "scale(0.3)",
                "-moz-transform-origin": "-140px 0",
                width: 1920,
              }),
              overflow: "hidden",
            }}>
            <WelcomePageEditor readOnly value={item.content} />
          </div>
        </CardElement>
      )}
      dataSource={dataSource}
    />
  );
}
