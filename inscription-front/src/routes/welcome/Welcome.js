import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentProjectActions, currentProjectSelectors} from "@features/currentProject";
import {RegisterToProjectPopup, WelcomeMessagePopup} from "./WelcomeMessagePopup";
import {currentUserSelectors} from "@features/currentUser";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {useTranslation} from "react-i18next";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {useBrowserTabTitle} from "@shared/hooks/useBrowserTabTitle";
import {useNavigate, useParams} from "react-router-dom";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {DynamicProjectThemeProvider} from "@shared/layout/DynamicProjectThemeProvider";
import {Redirect} from "@shared/pages/Redirect";
import {useNOEInstanceName} from "@shared/hooks/useNOEInstanceName";

const WelcomePageEditor = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/WelcomePageEditor/WelcomePageEditor")
);

function Welcome() {
  const navigate = useNavigate();
  const {projectId} = useParams();
  const {t} = useTranslation();
  const {isMobileView} = useWindowDimensions();
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  const connected = useSelector(currentUserSelectors.selectConnected);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  const userCanViewWelcomePage =
    !currentProject.secretSchedule || !!currentRegistration?.inDatabase?.everythingIsOk;

  // When connected and on desktop, there is the sidebar next to the welcome page, we should take this into account.
  const isDisplayingNextToSidebar = connected && !isMobileView;

  // Necessary when the user is not connected at all
  useEffect(() => {
    if (
      (currentProject._id !== projectId && currentProject.slug !== projectId) || // if the project to load is not the same as the current project, it has to be loaded
      (currentProject.public && connected) || //Or... if it's loaded in public mode whereas the user is now connected, we have to reload it again also
      (!currentProject.public && !connected) // And the opposite : if user is not connected but project in connected mode
    ) {
      dispatch(currentProjectActions.load(projectId)).catch(() => navigate("/projects"));
    }
  }, [projectId, connected, currentProject]);

  useBrowserTabTitle(
    "welcome",
    {welcome: t("welcome:label")},
    {
      baseName: currentProject.name,
      suffix: useNOEInstanceName("inscriptionFront"),
    }
  );

  // Load project registration
  useEffect(() => {
    // Only load the registration if we are connected and if it is not loaded yet
    connected &&
      currentProject._id &&
      !currentRegistration?._id &&
      dispatch(registrationsActions.loadCurrent());
  }, [connected, currentProject._id, currentRegistration?._id, dispatch]);

  // If the user has no right to see the welcome page, send them to the login page
  if (!userCanViewWelcomePage) {
    return (
      <Redirect from={"/:projectId/welcome"} to={"/:projectId/login"} paramsNames={["projectId"]} />
    );
  }

  return (
    <DynamicProjectThemeProvider>
      {/* Welcome message when not connected at all */}
      {!connected && currentProject._id && <WelcomeMessagePopup project={currentProject} />}
      {/* Reminder message to tell people to register to the event */}
      {connected && currentProject._id && <RegisterToProjectPopup />}
      <WelcomePageEditor
        value={currentProject.content}
        readOnly
        roundedBorders={isDisplayingNextToSidebar}
        className={connected ? "full-width-content" : ""}
      />
    </DynamicProjectThemeProvider>
  );
}

export default Welcome;
