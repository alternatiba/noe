import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "antd";
import {LogoutOutlined, UserSwitchOutlined} from "@ant-design/icons";
import {currentUserActions, currentUserSelectors} from "@features/currentUser";
import {ListPage} from "@shared/pages/ListPage";
import {CardElement} from "@shared/components/CardElement";
import VolunteeringGauge from "@shared/components/VolunteeringGauge";
import {currentProjectSelectors} from "@features/currentProject";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {FormElement} from "@shared/inputs/FormElement";
import {useColumnsBlacklistingSelect} from "@shared/hooks/useColumnsBlacklistingSelect";
import {personName} from "@shared/utils/utilities";
import dayjs from "@shared/services/dayjs";
import {useTranslation} from "react-i18next";
import {SwitchInput} from "@shared/inputs/SwitchInput";
import {ConnectedAsAlert} from "./atoms/ConnectedAsAlert";
import {useRegistrationsColumns} from "@utils/columns/useRegistrationsColumns";
import {generateYesNoColumn} from "@shared/utils/columns/generateYesNoColumn";
import {searchInRegistrationFields} from "@shared/utils/searchInFields/searchInRegistrationFields";
import {useNavigate} from "react-router-dom";
import {sorter} from "@shared/utils/sorters";
import {dateFormatter} from "@shared/utils/formatters";
import {listOfClickableElements} from "@shared/utils/listOfClickableElements";
import {useRenderHtmlTicket} from "@shared/hooks/useRenderHtmlTicket";

function ParticipantList() {
  const navigate = useNavigate();
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const project = useSelector(currentProjectSelectors.selectProject);
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "participants",
  });
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);
  const [displayUnbookedUsers, setDisplayUnbookedUsers] = useState(false);
  const [advancedMode, setAdvancedMode] = useState(false);

  useEffect(() => {
    dispatch(registrationsActions.loadList());
  }, []);

  useEffect(() => {
    const connectedAsParticipantId = new URLSearchParams(window.location.search).get("connectedAs");
    const requestedConnectedAsRegistration = registrations.find(
      (registration) => registration._id === connectedAsParticipantId
    );
    if (requestedConnectedAsRegistration) {
      const url = new URL(window.location);
      url.searchParams.delete("connectedAs");
      window.history.replaceState(null, null, url.toString());
      activateConnectionAsUserForRegistration(requestedConnectedAsRegistration._id);
    }
  }, [registrations]);

  const registrationsExceptAuthenticatedUser = registrations.filter(
    (r) =>
      // Don't include yourself
      r.user._id !== authenticatedUser._id &&
      // Display unbooked users or not
      (displayUnbookedUsers || r.availabilitySlots?.length > 0)
  );

  const activateConnectionAsUserForRegistration = (registrationId) => {
    const connectedAsUser = registrationsExceptAuthenticatedUser.find(
      (r) => r._id === registrationId
    ).user;
    dispatch(currentUserActions.changeConnectedAsUser(connectedAsUser));
    navigate("./../registration");
  };

  const returnToNormal = () => dispatch(currentUserActions.changeConnectedAsUser(undefined));

  const renderHtmlTicket = useRenderHtmlTicket();
  const getTicketsList = (record, html = false) => {
    const tickets = record[`${project.ticketingMode}Tickets`];
    return tickets?.length > 0
      ? html
        ? listOfClickableElements(tickets, renderHtmlTicket, {lineBreaks: true})
        : tickets.map((ticket) => ticket.id).join(", ")
      : undefined;
  };

  const columns = useRegistrationsColumns({
    middle: [
      {
        title: t("stewards:label"),
        dataIndex: "steward",
        sorter: (a, b) => sorter.text(personName(a.steward), personName(b.steward)),
        render: (text, record) => personName(record.steward),
        searchText: (record) => personName(record.steward),
        searchable: true,
      },
    ],
    end: [
      {
        title: t("registrations:schema.hasCheckedIn.label"),
        ...generateYesNoColumn("hasCheckedIn"),
        width: 100,
      },
      {
        title: t("registrations:schema.arrivalDateTime.label"),
        dataIndex: "arrivalDateTime",
        render: (text, record) => {
          // If arrived, green. If not arrived and date is past, red. Else, no color
          const color = record.hasCheckedIn
            ? "green"
            : dayjs().isAfter(record.arrivalDateTime) && "red";
          return (
            <span style={{color}}>
              {dateFormatter.longDateTime(record.arrivalDateTime, {short: true})}
            </span>
          );
        },
        sorter: (a, b) => sorter.date(a.arrivalDateTime, b.arrivalDateTime),
        searchable: true,
        searchText: (record) => dateFormatter.longDateTime(record.arrivalDateTime, {short: true}),
        width: 155,
      },
      {
        title: t("registrations:schema.departureDateTime.label"),
        dataIndex: "departureDateTime",
        render: (text, record) => {
          // If arrived, green. If not arrived and date is past, red. Else, no color
          const color = record.hasCheckedIn && dayjs().isAfter(record.departureDateTime) && "green";
          return (
            <span style={{color}}>
              {dateFormatter.longDateTime(record.departureDateTime, {short: true})}
            </span>
          );
        },
        sorter: (a, b) => sorter.date(a.departureDateTime, b.departureDateTime),
        searchable: true,
        searchText: (record) => dateFormatter.longDateTime(record.departureDateTime, {short: true}),
        width: 155,
      },
      advancedMode && {
        title: t("registrations:schema.hidden.label"),
        ...generateYesNoColumn("hidden"),
        width: 100,
      },
      {
        title: t("registrations:schema.everythingIsOk.label"),
        ...generateYesNoColumn("everythingIsOk"),
        width: 115,
      },
      project.ticketingMode && {
        title: t("registrations:schema.tickets.label"),
        dataIndex: `${project.ticketingMode}Tickets`,
        render: (text, record) => getTicketsList(record, true),
        sorter: (a, b) => sorter.text(getTicketsList(a), getTicketsList(b)),
        searchable: true,
        searchText: getTicketsList,
        width: 140,
      },
      {
        title: t("registrations:schema.voluntaryCounter.label"),
        dataIndex: "voluntaryCounter",
        render: (text, record) => <VolunteeringGauge registration={record} />,
        sorter: (a, b) => sorter.number(a.voluntaryCounter, b.voluntaryCounter),
      },
      {
        key: "action",
        fixed: "right",
        render: (text, record) => (
          <div style={{margin: 4, textAlign: "right"}}>
            {record.user._id === currentUser?._id ? (
              <Button
                title={t("registrations:actions.stopChangeIdentity")}
                style={{marginRight: 8}}
                danger
                type="primary"
                onClick={() => returnToNormal(dispatch)}
                icon={<LogoutOutlined />}
              />
            ) : (
              <Button
                title={t("registrations:actions.changeIdentity")}
                type="link"
                icon={<UserSwitchOutlined />}
                onClick={() => activateConnectionAsUserForRegistration(record._id)}
              />
            )}
          </div>
        ),
        width: 56,
      },
    ],
  });

  return (
    <ListPage
      i18nNs="registrations"
      searchInFields={searchInRegistrationFields}
      icon={<UserSwitchOutlined />}
      title={t("registrations:actions.changeIdentity")}
      subtitle={
        <ConnectedAsAlert
          fallback={<p>{t("registrations:actions.changeIdentityDescription")}</p>}
        />
      }
      rowClassName={(record) =>
        record.user._id === currentUser?._id
          ? "ant-table-row-selected"
          : record.everythingIsOk
          ? ""
          : "ant-table-row-danger"
      }
      creatable={false}
      editable={false}
      deletable={false}
      onNavigate={activateConnectionAsUserForRegistration}
      columns={filterBlacklistedColumns(columns)}
      dataSource={registrationsExceptAuthenticatedUser}
      settingsDrawerContent={
        <FormElement>
          <ColumnsBlacklistingSelector columns={columns} />
          <CardElement title={t("registrations:list.dataDisplayOptionsTitle")}>
            <div className="container-grid">
              <SwitchInput
                label={t("registrations:list.displayUnbookedUsers")}
                checked={displayUnbookedUsers}
                onChange={setDisplayUnbookedUsers}
              />
              <SwitchInput
                label={t("registrations:list.advancedMode")}
                checked={advancedMode}
                onChange={setAdvancedMode}
              />
            </div>
          </CardElement>
        </FormElement>
      }
    />
  );
}

export default ParticipantList;
