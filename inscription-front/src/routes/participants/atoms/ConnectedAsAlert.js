import {useSelector} from "react-redux";
import {currentUserSelectors} from "@features/currentUser";
import {Alert} from "antd";
import {Trans, useTranslation} from "react-i18next";
import {personName} from "@shared/utils/utilities";
import {useProjectOpeningStateAccessRights} from "@utils/useProjectOpeningStateAccessRights";

export const ConnectedAsAlert = ({fallback = null}) => {
  const {t} = useTranslation();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);

  const {authenticatedUserIsProjectOrga, giveAccessToSessionsPageForNormalUsers} =
    useProjectOpeningStateAccessRights();

  return currentUser._id !== authenticatedUser._id ? (
    <div style={{marginBottom: 26, alignItems: "stretch"}} className={"containerV flex-space"}>
      <Alert
        type="info"
        showIcon
        message={
          <Trans
            i18nKey="alerts.currentlyConnectedAs.description"
            ns="users"
            values={{personName: personName(currentUser), email: currentUser?.email}}
          />
        }
      />
      {authenticatedUserIsProjectOrga && !giveAccessToSessionsPageForNormalUsers && (
        <Alert
          type="warning"
          showIcon
          message={t("users:alerts.currentlyConnectedAs.yourParticipantsCannotRegister")}
          description={
            <Trans ns="users" i18nKey="alerts.currentlyConnectedAs.onlyOrgasCurrentlyCanDoThis" />
          }
        />
      )}
    </div>
  ) : (
    fallback
  );
};
