import {useSelector} from "react-redux";
import {registrationsSelectors} from "@features/registrations";
import {teamsSelectors} from "@features/teams";
import {Button, Popconfirm, Tooltip} from "antd";
import {CheckOutlined, QuestionCircleOutlined, TeamOutlined, UserOutlined} from "@ant-design/icons";
import {t} from "i18next";
import {Trans} from "react-i18next";
import React from "react";
import {currentProjectSelectors} from "@features/currentProject";

export const SubscribeUnsubscribeButton = ({
  sessionSubscription,
  onSubscribe,
  onUnsubscribe,
  sessionIsFull,
  registrationIncomplete,
  participantIsNotAvailable,
  isSteward,
  alreadySubscribedToOtherSession,
  alreadyStewardOnOtherSession,
  tooltipMessage,
  shouldBeAddedAutomagicallyInUsersPlanning,
  volunteeringBeginsSoon,
  block = false,
}) => {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const authenticatedUserRegistration = useSelector(registrationsSelectors.selectAuthenticated);
  const teams = useSelector(teamsSelectors.selectList);

  const blockSubscriptionsOnProject =
    currentProject.blockSubscriptions &&
    authenticatedUserRegistration &&
    !authenticatedUserRegistration.role;

  const blockVolunteeringUnsubscribeIfBeginsSoon =
    currentProject.blockVolunteeringUnsubscribeIfBeginsSoon;

  const registrationButtonClassName =
    alreadySubscribedToOtherSession || alreadyStewardOnOtherSession
      ? "warning-button"
      : "success-button";

  const subscriptionIsDisabled =
    sessionIsFull ||
    registrationIncomplete ||
    participantIsNotAvailable ||
    isSteward ||
    shouldBeAddedAutomagicallyInUsersPlanning ||
    ((alreadySubscribedToOtherSession || alreadyStewardOnOtherSession) &&
      currentProject.notAllowOverlap);

  const disabledStyle = {borderColor: "rgb(100,100,100)", color: "#555"};

  if (isSteward) {
    return (
      <Tooltip title={tooltipMessage}>
        <Button icon={<UserOutlined />} type="dashed" disabled style={disabledStyle} block={block}>
          {t("stewards:label")}
        </Button>
      </Tooltip>
    );
  }
  if (shouldBeAddedAutomagicallyInUsersPlanning) {
    return (
      <Button icon={<CheckOutlined />} type="dashed" disabled style={disabledStyle} block={block}>
        {t("sessions:filling.everybodyIsSubscribed")}
      </Button>
    );
  } else if (sessionSubscription) {
    // -------- USER IS SUBSCRIBED : show UNSUBSCRIBE button ------------

    // Case for team members
    const teamLinkedToRegistration = teams.find((t) => t._id === sessionSubscription.team);
    if (teamLinkedToRegistration) {
      return (
        <Tooltip title={tooltipMessage}>
          <Button
            icon={<TeamOutlined />}
            type="dashed"
            disabled
            style={disabledStyle}
            block={block}>
            {t("teams:teamMember")}
          </Button>
        </Tooltip>
      );
    }

    if (blockSubscriptionsOnProject) {
      return (
        <Button icon={<CheckOutlined />} disabled block={block}>
          Inscrit⋅e
        </Button>
      );
    }

    // Case for volunteering that begins soon but not allowed
    if (
      volunteeringBeginsSoon &&
      blockVolunteeringUnsubscribeIfBeginsSoon &&
      !authenticatedUserRegistration.role
    )
      return (
        <Tooltip title={t("sessions:subscribeUnsubscribeButton.volunteeringSoonWarning.tooltip")}>
          <Button disabled block={block}>
            {t("sessions:unsubscribe")}
          </Button>
        </Tooltip>
      );

    const confirmUnsubscribeMessage = volunteeringBeginsSoon ? (
      <div style={{maxWidth: 450}}>
        <Trans
          ns="sessions"
          i18nKey="subscribeUnsubscribeButton.volunteeringSoonWarning.confirmText"
        />
      </div>
    ) : (
      t("sessions:subscribeUnsubscribeButton.confirmUnsubscribe")
    );

    return (
      <Popconfirm
        title={confirmUnsubscribeMessage}
        onConfirm={onUnsubscribe}
        okText={t("common:yes")}
        okButtonProps={{danger: true}}
        icon={<QuestionCircleOutlined style={{color: "red"}} />}
        cancelText={t("common:no")}>
        <Button type={teamLinkedToRegistration && "dashed"} danger block={block}>
          {t("sessions:unsubscribe")}
        </Button>
      </Popconfirm>
    );
  } else {
    // -------- USER IS UNSUBSCRIBED : show SUBSCRIBE button ------------

    const subscribeButton = (
      <Button
        icon={<CheckOutlined />}
        type="primary"
        onClick={tooltipMessage ? undefined : onSubscribe}
        className={subscriptionIsDisabled ? undefined : registrationButtonClassName}
        disabled={subscriptionIsDisabled}
        block={block}>
        {t("sessions:subscribe")}
      </Button>
    );

    return tooltipMessage ? (
      <Popconfirm
        title={
          <>
            <p>{tooltipMessage}</p>
            {t("sessions:stillWantToRegisterConfirm")}
          </>
        }
        onConfirm={onSubscribe}
        okText={t("common:yes")}
        okButtonProps={{className: registrationButtonClassName}}
        cancelText={t("common:no")}
        disabled={subscriptionIsDisabled}>
        <Tooltip title={tooltipMessage} zIndex={20}>
          {subscribeButton}
        </Tooltip>
      </Popconfirm>
    ) : (
      subscribeButton
    );
  }
};
