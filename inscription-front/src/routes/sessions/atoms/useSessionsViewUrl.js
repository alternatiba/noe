import {useMemo} from "react";
import {useLocation} from "react-router-dom";

export const useSessionsViewUrl = () => {
  const location = useLocation();
  return useMemo(
    () =>
      location.pathname.includes("/sessions/all")
        ? "all"
        : location.pathname.includes("/sessions/subscribed")
        ? "subscribed"
        : null,
    [location.pathname]
  );
};
