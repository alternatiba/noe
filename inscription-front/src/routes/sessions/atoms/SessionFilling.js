import {useTranslation} from "react-i18next";
import {FillingInput} from "@shared/inputs/FillingInput";

export const SessionFilling = ({
  computedMaxNumberOfParticipants,
  numberOfParticipants,
  showLabel = true,
  style,
  ...otherProps
}) => {
  const {t} = useTranslation();
  // cf. getMaxParticipantsBasedOnPlaces() function in backend
  if (computedMaxNumberOfParticipants === null) return t("sessions:filling.freeSubscription");

  const fillingText =
    numberOfParticipants !== undefined && computedMaxNumberOfParticipants
      ? numberOfParticipants + "/" + computedMaxNumberOfParticipants
      : 0;

  return (
    <FillingInput
      formItemProps={{
        style: {maxWidth: 250, marginBottom: showLabel ? undefined : 0, ...style},
        ...otherProps.formItemProps,
      }}
      label={showLabel ? t("sessions:filling.label") : undefined}
      percent={(numberOfParticipants / computedMaxNumberOfParticipants) * 100}
      status={numberOfParticipants >= computedMaxNumberOfParticipants ? "exception" : ""}
      text={fillingText}
      {...otherProps}
    />
  );
};
