import {EyeOutlined} from "@ant-design/icons";
import {FieldCompInput} from "@shared/components/FieldsBuilder/FieldComp/FieldComp";
import FormRenderer from "@shared/components/FormRenderer";
import {
  CustomFieldDisplayMode,
  useGetCustomFieldsForEndpointAndMode,
} from "@shared/hooks/useGetCustomFieldsForEndpointAndMode";
import {FormElement} from "@shared/inputs/FormElement";
import {Form} from "antd";
import React from "react";
import type {CustomFieldFilter} from "./filterSessionsByCustomFields";
import {useSessionsFilter} from "./useSessionsFilter";

type CustomFieldsFiltersProps = {
  mode: Extract<CustomFieldDisplayMode, "inOrgaFilters" | "inParticipantFilters">;
  showIcon?: boolean;
};

const fromFiltersToFormValues = (customFieldsFilters: Array<CustomFieldFilter>) =>
  Object.fromEntries(
    customFieldsFilters?.map((filterComp) => [filterComp.key, filterComp.filterValues])
  );

const fromFormValuesToFilters = (
  allValues: Record<string, Array<string>>,
  customFieldComponents: Array<FieldCompInput>
): Array<CustomFieldFilter> =>
  Object.entries(allValues).map(([key, filterValues]) => {
    const customFieldComp = customFieldComponents.find(
      (comp) => comp.key === key
    ) as FieldCompInput;

    return {
      key,
      filterValues,
      type: customFieldComp.type,
      endpoint: customFieldComp.meta.endpoint,
    };
  });

export const CustomFieldsFilters = ({mode, showIcon}: CustomFieldsFiltersProps) => {
  const customFieldComponentsForAgendaFilters = useGetCustomFieldsForEndpointAndMode({mode});
  const [form] = Form.useForm();
  const [{customFieldsFilters}, setSessionsFilter] = useSessionsFilter();

  return customFieldComponentsForAgendaFilters.map((customFieldComp) => (
    <FormElement
      form={form}
      initialValues={fromFiltersToFormValues(customFieldsFilters)}
      onValuesChange={(_, allValues) =>
        setSessionsFilter({
          customFieldsFilters: fromFormValuesToFilters(
            allValues,
            customFieldComponentsForAgendaFilters
          ),
        })
      }>
      <FormRenderer
        fields={[
          {
            ...customFieldComp,
            type: "multiSelect",
            placeholder: (
              <>
                {showIcon && <EyeOutlined />} {customFieldComp.displayName}…
              </>
            ),
            style: {minWidth: 200, flexBasis: "15%"},

            // Force the required value to false to prevent weird behavior https://noe-app.notion.site/champs-personnalis-s-d-sactiv-dans-vue-session-agenda-vider-les-filtres-bddd55e64d824933ae330c97178c82e6?pvs=4
            required: false,
          },
        ]}
        form={form}
        minimalist
      />
    </FormElement>
  ));
};
