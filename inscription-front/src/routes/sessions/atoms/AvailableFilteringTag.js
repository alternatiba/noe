import {useTranslation} from "react-i18next";
import {useSessionsFilter} from "./useSessionsFilter";
import {useSessionsViewUrl} from "./useSessionsViewUrl";
import {Tag, Tooltip} from "antd";

export const AvailableFilteringTag = () => {
  const {t} = useTranslation();
  const [{availableFilter}, setSessionsFilter] = useSessionsFilter();

  const viewUrl = useSessionsViewUrl();
  const viewOnlySubscribed = viewUrl === "subscribed";

  return !viewOnlySubscribed ? (
    <Tooltip title={t("sessions:searchControls.availableFilter.tooltip")}>
      <Tag.CheckableTag
        checked={availableFilter}
        onChange={(value) => setSessionsFilter({availableFilter: value})}>
        {t("sessions:searchControls.availableFilter.label")}
      </Tag.CheckableTag>
    </Tooltip>
  ) : null;
};
