import {useTranslation} from "react-i18next";
import {useDebounce} from "@shared/hooks/useDebounce";
import {Input} from "antd";
import React from "react";

import {useSessionsFilter} from "./useSessionsFilter";

export const SearchBox = () => {
  const {t} = useTranslation();
  const [{searchBarFilter}, setSessionsFilter] = useSessionsFilter();

  const debouncedSetSearchBarValue = useDebounce((value) =>
    setSessionsFilter({searchBarFilter: value})
  );

  return (
    <Input.Search
      placeholder={t("sessions:searchControls.searchBar.placeholder")}
      style={{minWidth: 200, flexGrow: 4, flexBasis: 3}}
      defaultValue={searchBarFilter}
      onChange={({target: {value: searchText}}) => debouncedSetSearchBarValue(searchText)}
      enterButton
    />
  );
};
