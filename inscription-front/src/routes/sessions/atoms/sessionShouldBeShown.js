import {isParticipantAvailable} from "@shared/utils/getSessionMetadata";

export const sessionShouldBeShown = (viewUrl, session, currentRegistration) => {
  // If the session is "hidden" (ie. participants jauge = to zero), display it only if the user is subscribed to it.
  // But don't do this if everybodyIsSubscribed is true because everybodyIsSubscribed has priority over computedMaxNumberOfParticipants
  if (!session.everybodyIsSubscribed && session.computedMaxNumberOfParticipants === 0) {
    return session.subscribed;
  }

  if (viewUrl === "all") {
    // If viewing "all" sessions... then show all sessions
    return true;
  } else {
    // If viewing "subscribed" only... then that's a little more complicated

    // Display in My Planning if the user is subscribed or is a steward of the session
    if (session.isSteward || session.subscribed) return true;

    // If the session should be in everybody's agenda, and if the participant is available on the session's dates, show it too
    if (
      session.everybodyIsSubscribed &&
      isParticipantAvailable(session.slots, currentRegistration.availabilitySlots)
    ) {
      return true;
    }

    // In any other case, don't show in My Planning
    return false;
  }
};
