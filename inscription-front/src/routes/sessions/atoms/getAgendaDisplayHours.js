// Returns the hour of the day based on hours and minutes
import dayjs from "@shared/services/dayjs";

const hoursOfDay = (dayjsDate) => dayjsDate.minute() / 60.0 + dayjsDate.hour();

// Tells if a slot is located on two different days
const slotEndsAfterMidnight = (slot) => {
  const start = dayjs(slot.start);
  const end = dayjs(slot.end);
  const length = end.diff(start, "minute");
  return start.hour() * 60 + start.minute() + length > 1439;
};

// Get the min and max hour of all slots
const getSlotsMinMaxHours = (slots) => {
  // If no slots, return
  if (!slots || !(slots?.length > 0)) return;

  let minStart, maxEnd;
  for (const slot of slots) {
    if (slotEndsAfterMidnight(slot)) {
      // If any of the slots ends after midnight, display all the hours
      return {start: 0, end: 24};
    } else {
      // Else, compute dates depending on the
      const slotStart = hoursOfDay(dayjs(slot.start));
      const slotEnd = hoursOfDay(dayjs(slot.end));
      minStart = minStart && minStart < slotStart ? minStart : slotStart;
      maxEnd = maxEnd && maxEnd > slotEnd ? maxEnd : slotEnd;
    }
  }

  return {start: minStart, end: maxEnd};
};

// Get the min and max hour of the project availability slots
const getProjectMinMaxHours = (project) => {
  // If there is no availability slots, don't return anything, we can't calculate.
  if (!(project?.availabilitySlots?.length > 0)) return;

  const projectSlotThatEndsAfterMidnight = project.availabilitySlots.find((slot) =>
    slotEndsAfterMidnight(slot)
  );
  // If some slots pass through midnight, we have to display everything.
  if (projectSlotThatEndsAfterMidnight) return;

  // If not, then just take the event's slots as reference
  return {
    start: hoursOfDay(dayjs(project.start)),
    end: hoursOfDay(dayjs(project.end)),
  };
};

const MIN_START_DISPLAY_HOUR = 8;
const MIN_END_DISPLAY_HOUR = 22;
const SAFE_HOUR_DISTANCE_FROM_SESSIONS_TOP = 1;
const SAFE_HOUR_DISTANCE_FROM_SESSIONS_BOTTOM = 2;

const halfRound = (funcName: "floor" | "ceil" | "round", val) => Math[funcName](val * 2) / 2;

// Get the display parameters to give to the Agenda view
export const getAgendaDisplayHours = (slots, project) => {
  let displayHours;
  const projectMinMaxHours = getProjectMinMaxHours(project);
  const slotsMinMaxHours = getSlotsMinMaxHours(slots);

  if (projectMinMaxHours && slotsMinMaxHours) {
    // If there are both slots and project hours, calculate based on both

    const slotsExistBeforeProjectHours = slotsMinMaxHours.start < projectMinMaxHours.start;
    const slotsExistAfterProjectHours = slotsMinMaxHours.end > projectMinMaxHours.end;

    displayHours = {
      start: halfRound(
        "floor",
        slotsExistBeforeProjectHours ? slotsMinMaxHours.start : projectMinMaxHours.start
      ),
      end: halfRound(
        "ceil",
        slotsExistAfterProjectHours ? slotsMinMaxHours.end : projectMinMaxHours.end
      ),
    };
  } else if (slotsMinMaxHours || projectMinMaxHours) {
    // If there are only one of them, just use the ones we have
    displayHours = {
      start: halfRound(
        "floor",
        slotsMinMaxHours?.start !== undefined ? slotsMinMaxHours?.start : projectMinMaxHours?.start
      ),
      end: halfRound(
        "ceil",
        slotsMinMaxHours?.end !== undefined ? slotsMinMaxHours?.end : projectMinMaxHours?.end
      ),
    };
  } else {
    // if there are none, fail safe
    displayHours = {start: MIN_START_DISPLAY_HOUR, end: MIN_END_DISPLAY_HOUR};
  }

  // Display one hour before display start, but do not go below 0 hours
  displayHours.start = Math.max(displayHours.start - SAFE_HOUR_DISTANCE_FROM_SESSIONS_TOP, 0);
  // Display 2 hours after display end, but do not go after 24 hours
  displayHours.end = Math.min(displayHours.end + SAFE_HOUR_DISTANCE_FROM_SESSIONS_BOTTOM, 24);

  // We should at least display from MIN to MAX default display hour
  displayHours.start = Math.min(MIN_START_DISPLAY_HOUR, displayHours.start);
  displayHours.end = Math.max(MIN_END_DISPLAY_HOUR, displayHours.end);

  return displayHours;
};
