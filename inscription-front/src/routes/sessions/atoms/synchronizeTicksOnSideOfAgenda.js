import {trySeveralTimes} from "@shared/utils/trySeveralTimes";

// Synchronize the height of the little ticks on the side of the calendar
export const synchronizeTicksOnSideOfAgenda = (className, cellDisplayHeight) => {
  // The trick is to spot the timescale labels but there is no eaasy hook (class names change all the time)
  // We created a custom class on which we can get a hook, named "timescale-cell".
  trySeveralTimes(() => {
    const elements = document.querySelector(`.${className}`)?.parentNode?.parentNode?.parentNode
      ?.parentNode?.parentNode?.lastChild?.firstChild.childNodes; // Go to the TicksLayout-table-XXXX and iterate
    if (elements?.length > 0) {
      elements.forEach((el) => (el.firstChild.style.height = `${cellDisplayHeight}px`));
      return true;
    }
  });
};
