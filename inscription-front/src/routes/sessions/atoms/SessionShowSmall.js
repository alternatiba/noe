import {Card, Space, Tag, Tooltip} from "antd";
import React from "react";
import Paragraph from "antd/es/typography/Paragraph";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {WarningOutlined} from "@ant-design/icons";
import {getFullSessionName, getVolunteeringCoefficient} from "@shared/utils/sessionsUtilities";
import {registrationsSelectors} from "@features/registrations";
import {t} from "i18next";
import {getSessionSubscription} from "@utils/registrationsUtilities";
import {getSessionInfoAlert} from "./getSessionInfoAlert";
import {SubscribeUnsubscribeButton} from "./SubscribeUnsubscribeButton";
import {CategoryTagWithVolunteeringMajoration} from "./CategoryTagWithVolunteeringMajoration";
import {SessionFilling} from "./SessionFilling";
import {getSessionMetadata} from "@shared/utils/getSessionMetadata";
import {dateFormatter} from "@shared/utils/formatters";
import {viewSelectors} from "@features/view";
import {useGetCustomFieldsForEndpointAndMode} from "@shared/hooks/useGetCustomFieldsForEndpointAndMode";
import {getCustomFieldForSession} from "./getCustomFieldForSession";
import {AddSessionToCalendarButton} from "@shared/components/buttons/AddSessionToCalendarButton";

function SessionShowSmall({
  session,
  onSubscribe,
  onUnsubscribe,
  hoverable = false,
  style,
  onClick,
  withMargins,
}) {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const volunteeringCoefficient = getVolunteeringCoefficient(session);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const sessionSubscription = getSessionSubscription(currentRegistration, session);
  const darkMode = useSelector(viewSelectors.selectDarkMode);

  const {
    isSteward,
    sessionIsFull,
    participantIsNotAvailable,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    registrationIncomplete,
    volunteeringBeginsSoon,
    shouldBeDimmed,
    inConflictWithRegistrationDates,
    shouldBeAddedAutomagicallyInUsersPlanning,
  } = getSessionMetadata(session, currentRegistration);

  const infoAlert = getSessionInfoAlert(
    sessionSubscription,
    isSteward,
    currentProject.blockSubscriptions,
    registrationIncomplete,
    participantIsNotAvailable,
    sessionIsFull,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    inConflictWithRegistrationDates
  );

  const customFieldsToDisplay = useGetCustomFieldsForEndpointAndMode({
    mode: "inSessionsCards",
  });

  const cardOpacity = shouldBeDimmed ? 0.5 : 1;

  return (
    <div className="containerV" style={{height: "100%", padding: withMargins && "15px 15px"}}>
      <div style={{position: "relative", zIndex: 1}}>
        <CategoryTagWithVolunteeringMajoration
          volunteeringCoefficient={volunteeringCoefficient}
          category={session.activity?.category}
          invertedColors
          style={{margin: 0, position: "absolute", top: 4, right: 4, opacity: cardOpacity}}
        />

        {inConflictWithRegistrationDates && (
          <div
            style={{
              position: "absolute",
              background: "white",
              borderRadius: 50,
              height: 26,
              width: 26,
              paddingLeft: 4,
              paddingTop: 1.5,
              top: 4,
              left: 4,
            }}>
            <WarningOutlined style={{fontSize: 18, color: "red"}} />
          </div>
        )}
      </div>
      <Tooltip title={infoAlert?.message} mouseEnterDelay={0.6}>
        <Card
          hoverable={hoverable}
          title={
            <div style={{textAlign: "center", marginTop: 12, marginBottom: 12}}>
              <div
                style={{
                  // Allow activity name to go on 3 lines not more
                  whiteSpace: "normal",
                  fontWeight: "bold",
                  overflow: "hidden",
                  display: "-webkit-box",
                  WebkitLineClamp: "3",
                  WebkitBoxOrient: "vertical",
                }}>
                {getFullSessionName(session, currentProject.sessionNameTemplate)}
              </div>
              <CategoryTagWithVolunteeringMajoration
                category={session.activity?.category}
                style={{
                  textOverflow: "ellipsis",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  maxWidth: 300,
                  margin: "-5px 0 -10px 0",
                  opacity: 0.85,
                }}>
                {session.activity?.category.name}
              </CategoryTagWithVolunteeringMajoration>
            </div>
          }
          style={{
            opacity: cardOpacity,
            transition: "opacity 0.3s ease-in-out, box-shadow .3s, border-color .3s", // Also get the transition for bow-shadow cause otherwie it will be overriden
            border:
              volunteeringCoefficient > 0
                ? `2px solid ${session.activity.category.color}`
                : isSteward && `1px dashed ${"rgba(100, 100, 100)"}`,
            outline: inConflictWithRegistrationDates ? "3px solid #FF9999" : undefined,
            outlineOffset: inConflictWithRegistrationDates ? 4 : undefined,
            background:
              isSteward && (darkMode ? "rgba(100, 100, 255, 0.06)" : "rgba(0, 0, 255, 0.08)"),
            height: "100%",
            overflow: "hidden",
            ...style,
          }}
          headStyle={{
            background: session.activity?.category.color,
            color: "white",
            borderRadius: 0,
          }}
          onClick={onClick}>
          <div style={{textAlign: "center", marginBottom: 15}}>
            {dateFormatter.longDateTimeRange(session.start, session.end)}
          </div>

          {session.activity?.summary && (
            <Paragraph ellipsis={{rows: 3}} style={{color: "grey"}}>
              {session.activity.summary}
            </Paragraph>
          )}

          {customFieldsToDisplay.map((customField) => {
            const customFieldValue = getCustomFieldForSession(customField, session);
            return customFieldValue ? (
              <div className="containerH buttons-container">
                <strong style={{flexGrow: 0, marginBottom: 15}}>{customField.label}:</strong>
                {customFieldValue}
              </div>
            ) : null;
          })}

          <div className="containerH buttons-container">
            {!session.everybodyIsSubscribed && (
              <>
                <strong style={{flexGrow: 0, marginBottom: 15}}>
                  {t("sessions:filling.label")}:
                </strong>
                <SessionFilling
                  showLabel={false}
                  computedMaxNumberOfParticipants={session.computedMaxNumberOfParticipants}
                  numberOfParticipants={session.numberParticipants}
                />
              </>
            )}
          </div>

          <div
            className="containerH"
            style={{
              alignItems: "baseline",
              flexWrap: "wrap",
              margin: 5,
              paddingBottom: 7,
              rowGap: 8,
              justifyContent: "center",
            }}>
            {session.activity?.secondaryCategories?.map((tagName, index) => (
              <Tag key={index}>{tagName}</Tag>
            ))}
          </div>
        </Card>
      </Tooltip>
      <div
        style={{
          position: "relative",
          bottom: 16,
          minHeight: 32,
          display: "flex",
          justifyContent: "center",
        }}>
        <div
          style={{
            width: "60%",
            minWidth: "fit-content",
            background: "var(--colorBgContainer)",
            borderRadius: 10,
          }}>
          <Space.Compact block>
            <SubscribeUnsubscribeButton
              onUnsubscribe={onUnsubscribe}
              onSubscribe={onSubscribe}
              sessionIsFull={sessionIsFull}
              isSteward={isSteward}
              sessionSubscription={sessionSubscription}
              tooltipMessage={infoAlert?.message}
              shouldBeAddedAutomagicallyInUsersPlanning={shouldBeAddedAutomagicallyInUsersPlanning}
              registrationIncomplete={registrationIncomplete}
              alreadySubscribedToOtherSession={alreadySubscribedToOtherSession}
              alreadyStewardOnOtherSession={alreadyStewardOnOtherSession}
              participantIsNotAvailable={participantIsNotAvailable}
              volunteeringBeginsSoon={volunteeringBeginsSoon}
              block
            />
            <AddSessionToCalendarButton session={session} />
          </Space.Compact>
        </div>
      </div>
    </div>
  );
}

export default SessionShowSmall;
