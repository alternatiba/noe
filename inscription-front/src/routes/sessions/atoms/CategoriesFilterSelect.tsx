import {CategoriesSelectComponent} from "@shared/components/CategoriesSelectComponent";
import {useSessionsFilter} from "./useSessionsFilter";

export const CategoriesFilterSelect = () => {
  const [{categoriesFilter}, setSessionsFilter] = useSessionsFilter();

  return (
    <CategoriesSelectComponent
      style={{minWidth: 200, flexGrow: 4, flexBasis: 5}}
      value={categoriesFilter}
      onChange={(value) => setSessionsFilter({categoriesFilter: value})}
    />
  );
};
