import {ColorDot} from "@shared/components/ColorDot";
import React from "react";

export const categoriesOptionsWithColorDot = (categories) =>
  categories.map((category) => ({
    value: category._id,
    label: (
      <div className="containerH" style={{alignItems: "center"}}>
        <ColorDot color={category.color} size={15} style={{marginBottom: 1, marginRight: 8}} />
        {category.name}
      </div>
    ),
  }));
