import React, {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {Button, Card, List, Result} from "antd";
import SessionShowSmall from "./atoms/SessionShowSmall";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {ReactComponent as SvgExplore} from "@images/undraw/undraw_explore.svg";

import dayjs from "@shared/services/dayjs";
import {currentProjectSelectors} from "@features/currentProject";
import {CarryOutOutlined, ScheduleOutlined} from "@ant-design/icons";
import {paginationPageSizes} from "@features/view";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {Trans, useTranslation} from "react-i18next";
import {PageHeading} from "@shared/components/PageHeading";
import {GetPdfPlanningButton} from "@shared/components/buttons/GetPdfPlanningButton";
import {useStickyShadow} from "@shared/hooks/useStickyShadow";
import {useSessionsViewUrl} from "./atoms/useSessionsViewUrl";
import {ConnectedAsAlert} from "../participants/atoms/ConnectedAsAlert";
import {useLoadSessionsView} from "./atoms/useLoadSessionsView";
import {ChangeSessionViewButtons} from "./atoms/ChangeSessionViewButtons";
import {SessionFilterControls} from "./atoms/SessionFilterControls";
import {sessionShouldBeShown} from "./atoms/sessionShouldBeShown";
import {useSavedPagination} from "@shared/hooks/useSavedPagination";
import {useNavigate} from "react-router-dom";
import {SvgImageContainer} from "@shared/components/SvgImageContainer";
import {H3Title} from "@shared/layout/typography";
import {dateFormatter} from "@shared/utils/formatters";
import {SavedWindowScroll} from "@shared/components/SavedWindowScroll";

const LoadPastSessionsFakeCard = () => {
  const dispatch = useDispatch();
  return (
    <div
      className="containerV"
      style={{height: "100%", minHeight: 250, padding: "15px 15px 50px 15px"}}>
      <Card
        className="containerV"
        style={{
          background: "rgba(0, 0, 0, 0.03)",
          height: "100%",
          color: "gray",
          fontWeight: "bold",
          textAlign: "center",
          justifyContent: "center",
        }}>
        <Trans
          i18nKey="list.toSeePastSessionsClickOnButton"
          ns="sessions"
          components={{
            button: (
              <Button
                style={{marginTop: 10}}
                onClick={() => {
                  dispatch(sessionsActions.updateFilteredList({showPastSessionsFilter: true}));
                }}
              />
            ),
          }}
        />
      </Card>
    </div>
  );
};

const DateBookmarkFakeCard = ({dateString}) => {
  return (
    <div
      className="containerV"
      style={{height: "100%", minHeight: 250, padding: "15px 15px 50px 15px"}}>
      <Card
        className="containerV bg-noe-gradient"
        style={{
          height: "100%",
          fontWeight: "bold",
          textAlign: "center",
          justifyContent: "center",
        }}>
        <H3Title style={{margin: 0, color: "white"}}>{dateString}</H3Title>
      </Card>
    </div>
  );
};

const SessionList = () => {
  const navigate = useNavigate();
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const viewUrl = useSessionsViewUrl();
  const {isMobileView} = useWindowDimensions();

  useStickyShadow();

  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const sessions = useSelector(sessionsSelectors.selectListFiltered);

  const sessionFilter = useSelector(sessionsSelectors.selectListFilter);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const sessionsShown = sessions.filter((session) =>
    sessionShouldBeShown(viewUrl, session, currentRegistration)
  );

  const sessionsShownWithDayBookmarks = Object.values(
    sessionsShown.reduce((groupByDayAcc, session) => {
      const formattedDate = dateFormatter.longDate(session.start, {short: true});
      return {
        ...groupByDayAcc,
        [formattedDate]: [
          ...(groupByDayAcc[formattedDate] || [
            {
              isDateBookmark: true,
              dateString: dateFormatter.longDate(session.start),
            },
          ]),
          session,
        ],
      };
    }, {})
  ).flat();

  const shouldDisplayPastSessionsCard =
    !sessionFilter.showPastSessionsFilter && dayjs(currentProject.start).isBefore(dayjs());

  const shouldDisplayWelcomeMessage =
    sessionsShown?.length === 0 &&
    (!currentRegistration.sessionsSubscriptions ||
      currentRegistration.sessionsSubscriptions.length === 0);

  const subtitle =
    viewUrl === "subscribed" ? (
      <Trans i18nKey="list.subtitleSubscribed" ns="sessions" />
    ) : (
      <Trans i18nKey="list.subtitleAll" ns="sessions" />
    );

  const [pagination, setPagination] = useSavedPagination();

  useLoadSessionsView(viewUrl, "list");

  const onSubscribe = (id) => dispatch(sessionsActions.subscribe(id));
  const onUnsubscribe = (id) => dispatch(sessionsActions.unsubscribe(id));

  // Memoizing this component avoids triple rendering of the whole page when subscribing/unsubscribing to a session
  const MemoizedSessionShowSmall = React.memo(({session}) =>
    session.isLoadPastSessionsFakeCard ? (
      <LoadPastSessionsFakeCard />
    ) : session.isDateBookmark ? (
      <DateBookmarkFakeCard dateString={session.dateString} />
    ) : (
      <SessionShowSmall
        hoverable
        session={session}
        onClick={() => navigate(`./../${session._id}`)}
        onUnsubscribe={() => onUnsubscribe(session._id)}
        onSubscribe={() => onSubscribe(session._id)}
        withMargins
      />
    )
  );

  const getMemoizedSessionSmall = useCallback(
    (session) => <MemoizedSessionShowSmall session={session} />,
    []
  );

  return (
    <div className="page-container">
      <SavedWindowScroll />
      <div style={{paddingBottom: 10}}>
        <PageHeading
          icon={viewUrl === "subscribed" ? <CarryOutOutlined /> : <ScheduleOutlined />}
          title={viewUrl === "subscribed" ? t("sessions:labelSubscribed") : t("sessions:labelAll")}
          customButtons={
            <GetPdfPlanningButton
              elementsActions={registrationsActions}
              id={currentRegistration._id}
              tooltip={t("sessions:searchControls.pdfPlanningButton.tooltip")}
              disabled={!currentRegistration?.inDatabase.everythingIsOk}
              type="primary"
              readOnly
              noText={isMobileView}
            />
          }
        />
      </div>
      <div style={{paddingBottom: 16}}>{subtitle}</div>
      <ConnectedAsAlert />

      <div className={`full-width-content with-margins ${!isMobileView ? "sticky-element" : ""}`}>
        <SessionFilterControls isMobileView={isMobileView} />
      </div>
      {viewUrl === "subscribed" && shouldDisplayWelcomeMessage ? (
        <Result
          className={"fade-in"}
          style={{marginTop: 30}}
          icon={<SvgImageContainer svg={SvgExplore} width={"min(85%, 500px)"} />}
          title={t("sessions:list.subscribedSplashScreen.title")}
          subTitle={<Trans ns="sessions" i18nKey="list.subscribedSplashScreen.subtitle" />}
          extra={
            <Button size={"large"} type="primary" onClick={() => navigate("../sessions/all")}>
              {t("sessions:list.subscribedSplashScreen.subscribeToSessionsButton")}
            </Button>
          }
        />
      ) : (
        <List
          style={{marginTop: "10px", marginBottom: "40px"}}
          grid={{gutter: 16, xs: 1, sm: 1, md: 2, lg: 2, xl: 3, xxl: 4}}
          dataSource={
            shouldDisplayPastSessionsCard
              ? [
                  {
                    isLoadPastSessionsFakeCard: true,
                    start: dayjs(sessions[0]?.start).startOf("day"),
                  },
                  ...sessionsShownWithDayBookmarks,
                ]
              : sessionsShownWithDayBookmarks
          }
          renderItem={getMemoizedSessionSmall}
          pagination={{
            position: "both",
            style: {textAlign: "center"},
            size: isMobileView && "small",
            showSizeChanger: true,
            onChange: (current, pageSize) => {
              setPagination(current, pageSize);
              window.scrollTo({top: 0, behavior: "smooth"});
            },
            current: pagination.current,
            pageSize: pagination.pageSize,
            pageSizeOptions: paginationPageSizes,
            total: sessionsShown.length,
          }}
        />
      )}
      <ChangeSessionViewButtons viewMode="list" />
    </div>
  );
};

export default SessionList;
