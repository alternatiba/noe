import {currentProjectSelectors} from "@features/currentProject";
import {viewActions, viewSelectors} from "@features/view";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {getNumberOfDaysToDisplay} from "@utils/agendaUtilities";
import {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";

/**
 * Agenda display parameter options. (lighter version than in the orga-front frontend)
 */
export type AgendaParamsProps = {
  /**
   * Height of agenda cells.
   */
  cellDisplayHeight: number;

  /**
   * Start date to display agenda from. Previously in UNIX format, now as a ISO String.
   */
  currentAgendaDate: string | number;

  /**
   * Whether to display appointments overlapping on each other or the one next to each other.
   */
  slotsOnEachOther: boolean;

  /**
   * The number of days to display in the agenda
   */
  numberOfDaysDisplayed: number;

  /**
   * Categories to filter appointments by.
   */
  categoriesFilter: string[];

  /**
   * If you want to force the start hours in the agenda display
   */
  forcedDisplayHours: {
    start: number;
    end: number;
  };
};

export const useAgendaParams = () => {
  const dispatch = useDispatch();
  const agendaParams: AgendaParamsProps = useSelector(viewSelectors.selectAgendaParams);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const {windowWidth} = useWindowDimensions();

  const setAgendaParams = useCallback(
    (params: Partial<AgendaParamsProps>) => {
      dispatch(viewActions.changeAgendaParams({projectId: currentProject._id, params}));
    },
    [currentProject._id, dispatch]
  );

  // Number of days to display. Will be set asynchronously in useEffect depending on the number of places and other params
  const numberOfDaysDisplayed =
    agendaParams?.numberOfDaysDisplayed || getNumberOfDaysToDisplay(windowWidth, currentProject);

  return [{...agendaParams, numberOfDaysDisplayed}, setAgendaParams] as const;
};
