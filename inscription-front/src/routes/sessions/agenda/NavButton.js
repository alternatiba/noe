import {Button} from "antd";
import {ArrowLeftOutlined, ArrowRightOutlined} from "@ant-design/icons";
import React from "react";

// Left and right arrows to navigate in the agenda days
export const NavButton = ({type, onClick}) => (
  <Button
    type="link"
    size="large"
    icon={type === "forward" ? <ArrowRightOutlined /> : <ArrowLeftOutlined />}
    onClick={onClick}
  />
);
