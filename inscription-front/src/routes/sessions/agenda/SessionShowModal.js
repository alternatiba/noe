import {Modal} from "antd";
import SessionShow from "../SessionShow";
import {useDispatch} from "react-redux";
import {useCallback, useEffect, useState} from "react";
import {setNewSearchParamsWithoutRefresh} from "@utils/setNewSearchParamsWithoutRefresh";
import {sessionsActions} from "@features/sessions";

export const useSessionShowModalOpenCloseManagement = () => {
  const dispatch = useDispatch();
  const [sessionShowModalOpen, setSessionShowModalOpen] = useState(false);

  const openSessionShowModal = (sessionId) => {
    // save the session in the "session" query param of the URL
    const newSearchParams = new URLSearchParams(window.location.search);
    newSearchParams.set("session", sessionId);
    setNewSearchParamsWithoutRefresh(newSearchParams);
    // Load editing state before displaying SessionShow component
    dispatch(sessionsActions.loadEditing(sessionId)).then(() => setSessionShowModalOpen(true));
  };

  const closeSessionModal = useCallback(() => {
    setSessionShowModalOpen(false);

    // Briefly wait for the modal to close before removing data
    setTimeout(() => dispatch(sessionsActions.setEditing({})), 200);

    // Remove the URL query param without re-rendering
    const newSearchParams = new URLSearchParams(window.location.search);
    newSearchParams.delete("session");
    setNewSearchParamsWithoutRefresh(newSearchParams);
  }, [setSessionShowModalOpen, dispatch]);

  // On first of open of the page, if there is a ?session= URL param, open the session linked to that id
  useEffect(() => {
    const sessionId = new URLSearchParams(window.location.search).get("session");
    if (sessionId) openSessionShowModal(sessionId);
  }, []);

  // If the user hits the BACK button and the modal is open, don't navigate : close the modal instead.
  useEffect(() => {
    if (sessionShowModalOpen) {
      window.addEventListener("popstate", closeSessionModal);
      return () => window.removeEventListener("popstate", closeSessionModal);
    }
  }, [closeSessionModal, sessionShowModalOpen]);

  return {openSessionShowModal, closeSessionModal, sessionShowModalOpen};
};

export const SessionShowModal = ({onClose, open}) => {
  return (
    <Modal
      open={open}
      onCancel={onClose}
      width={"min(100vw, 1400px)"}
      footer={false}
      style={{marginTop: 20, marginBottom: 20, padding: 0}}
      centered>
      <div className="modal-page-content">
        <SessionShow asModal navigatePathRoot={"./../../"} />
      </div>
    </Modal>
  );
};
