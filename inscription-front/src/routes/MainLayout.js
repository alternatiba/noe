import {useEffect, useLayoutEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {CalendarFilled, CalendarOutlined, PlaySquareOutlined} from "@ant-design/icons";
import {currentUserActions, currentUserSelectors} from "@features/currentUser";
import {LayoutStructure} from "@shared/layout/LayoutStructure";
import {useRedirectToProjectIfOffline} from "@shared/utils/offlineModeUtilities";
import {instanceName, URLS} from "@app/configuration";
import {useTranslation} from "react-i18next";
import {MenuLayout} from "@shared/components/Menu";
import {useBrowserTabTitle} from "@shared/hooks/useBrowserTabTitle";
import {Outlet} from "react-router";
import {setAppTheme} from "@shared/layout/DynamicProjectThemeProvider";
import {useNOEInstanceName} from "@shared/hooks/useNOEInstanceName";

export default function MainLayout({page}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);

  useRedirectToProjectIfOffline();

  // ****** TAB TITLE ******

  useBrowserTabTitle(
    page,
    {
      projects: t("projects:labelMyProjects"),
      "public-projects": t("projects:labelPublicProjects"),
    },
    {
      suffix: useNOEInstanceName("inscriptionFront"),
    }
  );

  // ***** RESET STUFF ******

  // On Main layout, reset app theme
  useLayoutEffect(() => {
    setAppTheme(undefined);
  }, []);

  // If we are on the main page, reset the connection as another user automatically
  useEffect(() => {
    if (authenticatedUser._id) dispatch(currentUserActions.changeConnectedAsUser(undefined));
  }, [authenticatedUser, dispatch]);

  // ****** SIDE MENU ******

  const menu = {
    top: (
      <MenuLayout
        rootNavUrl={"/"}
        className={"userTourProjectsLists"}
        selectedItem={page}
        items={[
          // User events
          authenticatedUser._id && {
            label: t("projects:labelMyProjects"),
            key: "projects",
            icon: <CalendarOutlined />,
          },

          // Public events
          {
            label: t("projects:labelPublicProjects"),
            key: "public-projects",
            icon: <CalendarFilled />,
          },
        ]}
      />
    ),
    footer: (
      <MenuLayout
        selectedItem={page}
        items={[
          // Orga front
          authenticatedUser.superAdmin && {
            label: t("common:pagesNavigation.orgaFront"),
            url: `${URLS.ORGA_FRONT}/projects?no-redir`,
            icon: <PlaySquareOutlined />,
          },
        ]}
      />
    ),
  };

  return (
    <LayoutStructure title={instanceName} menu={menu} profileUser={authenticatedUser}>
      <Outlet />
    </LayoutStructure>
  );
}
