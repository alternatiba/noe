import {Button, Modal, Result} from "antd";
import {useNavigate} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {useDispatch, useSelector} from "react-redux";
import {SvgImageContainer} from "@shared/components/SvgImageContainer";
import {ReactComponent as SvgPartying} from "@images/undraw/undraw_partying.svg";

import {useProjectOpeningStateAccessRights} from "@utils/useProjectOpeningStateAccessRights";

export default function RegistrationSuccessfulModal() {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const registrationSuccessfulModalOpen = useSelector(
    registrationsSelectors.selectRegistrationSuccessfulModalOpen
  );

  const closeModal = () => dispatch(registrationsActions.setRegistrationSuccessfulModalOpen(false));

  const {giveAccessToSessionsPage} = useProjectOpeningStateAccessRights();

  return (
    <Modal open={registrationSuccessfulModalOpen} footer={null} onCancel={closeModal} maskClosable>
      <Result
        status="success"
        icon={<SvgImageContainer svg={SvgPartying} width={"90%"} />}
        title={t("registrations:messages.bravoYouAreRegistered.title")}
        subTitle={
          <div className={"containerV"} style={{marginTop: 26, gap: 20, alignItems: "center"}}>
            {giveAccessToSessionsPage ? (
              <>
                {t("registrations:messages.bravoYouAreRegistered.subTitle")}
                <Button
                  type={"primary"}
                  onClick={() => {
                    navigate("./../sessions/all");
                    closeModal();
                  }}>
                  {t("sessions:list.subscribedSplashScreen.subscribeToSessionsButton")}
                </Button>
              </>
            ) : (
              <>
                {t("registrations:messages.bravoYouAreRegistered.comeBackWhenSubscriptionsAreOpen")}
                <Button type={"primary"} onClick={closeModal}>
                  {t("common:ok")}
                </Button>
              </>
            )}
          </div>
        }
      />
    </Modal>
  );
}
