import {SwapOutlined} from "@ant-design/icons";
import {currentProjectSelectors} from "@features/currentProject";
import {currentUserSelectors} from "@features/currentUser";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {FormElement} from "@shared/inputs/FormElement";
import {TextInputEmail} from "@shared/inputs/TextInput";
import {Alert, Button, Form, Modal, Tooltip} from "antd";
import React, {useState} from "react";
import {Trans, useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";

type TransferTicketButtonProps = {ticketId: string};

type TransferEmailFormValues = {toEmail: string};

export const TransferTicketButton = ({ticketId}: TransferTicketButtonProps) => {
  const [transferModalOpen, setTransferModalOpen] = useState(false);
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const ticketingMode = useSelector((s) => currentProjectSelectors.selectProject(s).ticketingMode);
  const currentUserEmail = useSelector((s) => currentUserSelectors.selectUser(s).email);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const currentRegistrationTickets = currentRegistration[`${ticketingMode}Tickets`];
  const [transferEmailForm] = Form.useForm<TransferEmailFormValues>();

  const onlyOneTicketLeft = currentRegistrationTickets?.length === 1;

  const transferTicket = ({toEmail}: TransferEmailFormValues) => {
    dispatch(registrationsActions.transferTicket({ticketId, toEmail}));
    closeModal();
  };
  const closeModal = () => setTransferModalOpen(false);

  return (
    <>
      <Tooltip title={t("registrations:ticketing.transfer.button.tooltip")}>
        <Button type={"link"} icon={<SwapOutlined />} onClick={() => setTransferModalOpen(true)}>
          {t("registrations:ticketing.transfer.button.label")}
        </Button>
      </Tooltip>

      <Modal
        open={transferModalOpen}
        title={t("registrations:ticketing.transfer.modalTitle")}
        onCancel={closeModal}
        okButtonProps={{danger: onlyOneTicketLeft}}
        okText={
          onlyOneTicketLeft
            ? t("registrations:ticketing.transfer.transferAndUnregister")
            : t("registrations:ticketing.transfer.transfer")
        }
        onOk={transferEmailForm.submit}>
        <div className={"container-grid"}>
          <Alert
            message={<Trans ns="registrations" i18nKey="ticketing.transfer.infoAlert" />}
            style={{marginBottom: 26}}
          />
          {onlyOneTicketLeft && (
            <Alert
              type={"warning"}
              showIcon
              style={{marginBottom: 26}}
              message={t("registrations:ticketing.transfer.warningOnlyOneTicketAlert.message")}
              description={
                <Trans
                  ns="registrations"
                  i18nKey="ticketing.transfer.warningOnlyOneTicketAlert.description"
                />
              }
            />
          )}

          <FormElement form={transferEmailForm} onFinish={transferTicket}>
            <TextInputEmail
              name={"toEmail"}
              label={t("registrations:ticketing.transfer.toEmail.label")}
              placeholder={t("fieldsBuilder:defaultPlaceholders.email")}
              required
              rules={[
                // Prevent user from submitting their own email
                {
                  validator: async (_, value) =>
                    value === currentUserEmail
                      ? Promise.reject(
                          t("registrations:ticketing.transfer.toEmail.errorCantBeSameEmailAsUser")
                        )
                      : Promise.resolve(),
                },
              ]}
            />
          </FormElement>

          <Alert type={"warning"} message={t("registrations:ticketing.transfer.warningAlert")} />
        </div>
      </Modal>
    </>
  );
};
