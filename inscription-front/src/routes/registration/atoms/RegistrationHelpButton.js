import {Button, ButtonProps, Card, Modal} from "antd";
import {t} from "i18next";
import React from "react";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {URLS} from "@app/configuration";

export default function RegistrationHelpButton(props: ButtonProps) {
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  const mailToHref = (recipient, subject) =>
    `mailto:${recipient}?subject=[${currentProject.name}] ${subject} - (url: ${window.location.pathname})`;

  const HelpCard = ({title, subtitle, description, href, style, inNewTab = false}) => (
    <a href={href} target={inNewTab && "_blank"} rel="noreferrer">
      <Card style={{color: "inherit", height: "100%", ...style}}>
        <p>
          <strong>{title}</strong>
          <br />
          {subtitle}
        </p>
        <i style={{color: "gray"}}>{description}</i>
      </Card>
    </a>
  );

  // TODO à traduire !
  return (
    <Button
      onClick={() =>
        Modal.info({
          footer: false,
          maskClosable: true,
          width: "min(90vw, 500px)",
          title: t("registrations:aProblemToRegister"),
          content: (
            <div className={"container-grid"}>
              <HelpCard
                href={mailToHref(
                  currentProject.contactEmail || process.env.REACT_APP_CONTACT_US_EMAIL,
                  t("registrations:registrationHelpButton.contactEventTeam.emailSubject")
                )}
                style={{
                  borderColor: "var(--noe-primary)",
                  background: "color-mix(in srgb, var(--noe-primary) 8%, white)",
                }}
                title={t("registrations:registrationHelpButton.contactEventTeam.title")}
                subtitle={t("registrations:registrationHelpButton.contactEventTeam.subtitle")}
                description={t("registrations:registrationHelpButton.contactEventTeam.description")}
              />
              <HelpCard
                href={`${URLS.WEBSITE}/community/help-center/participant`}
                inNewTab
                title={t("registrations:registrationHelpButton.consultHelpCenter.title")}
                subtitle={t("registrations:registrationHelpButton.consultHelpCenter.subtitle")}
                description={t(
                  "registrations:registrationHelpButton.consultHelpCenter.description"
                )}
              />
              <HelpCard
                href={mailToHref(
                  process.env.REACT_APP_CONTACT_US_EMAIL,
                  t("registrations:registrationHelpButton.contactNOETeam.emailSubject")
                )}
                title={t("registrations:registrationHelpButton.contactNOETeam.title")}
                subtitle={t("registrations:registrationHelpButton.contactNOETeam.subtitle")}
                description={t("registrations:registrationHelpButton.contactNOETeam.description")}
              />
            </div>
          ),
        })
      }
      {...props}
    />
  );
}
