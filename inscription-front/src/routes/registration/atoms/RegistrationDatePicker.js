import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import dayjs, {dayjsBase} from "@shared/services/dayjs";
import {Divider, Select} from "antd";
import {ClockCircleOutlined} from "@ant-design/icons";
import React from "react";
import {DatePickerComponent} from "@shared/inputs/DateTimeInput";

export const RegistrationDatePicker = ({
  onChange,
  onChangeCallback,
  value,
  introText,
  defaultPickerValue,
  nextComponentToActivate,
  ...props
}: {
  defaultPickerValue: string,
  value: string,
}) => {
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const breakfastHour = dayjs(currentProject.breakfastTime).hour();
  const lunchHour = dayjs(currentProject.lunchTime).hour();
  const dinnerHour = dayjs(currentProject.dinnerTime).hour();
  const defaultHour = breakfastHour + 1;

  console.log("DATE PICKER", value, defaultPickerValue);

  const timeStampObject = (label: string, hour: number) => ({
    key: hour,
    value: hour,
    label: t("registrations:availabilities.datePicker.timestampTemplate", {label, hour}),
  });

  const timeStamps = [
    timeStampObject(
      t("registrations:availabilities.timestamps.earlyInTheMorning"),
      breakfastHour - 1
    ),
    timeStampObject(t("registrations:availabilities.timestamps.inTheMorning"), defaultHour),
    timeStampObject(t("registrations:availabilities.timestamps.afterLunch"), lunchHour + 1),
    timeStampObject(
      t("registrations:availabilities.timestamps.inTheLateAfternoon"),
      dinnerHour - 2
    ),
    timeStampObject(t("registrations:availabilities.timestamps.afterDinner"), dinnerHour + 1),
  ];

  const mergeDate = (isoDate: string, hours?: number): string =>
    hours ? dayjs(isoDate)?.set("hour", hours).toISOString() : isoDate;

  return (
    <div className="containerH" style={{alignItems: "baseline", flexWrap: "wrap"}}>
      {introText}
      <DatePickerComponent
        bordered={!value}
        style={{minWidth: 170, maxWidth: "fit-content", marginLeft: value ? 4 : 10}}
        value={value}
        placeholder={t("registrations:availabilities.datePicker.datePlaceholder")}
        showTime={false}
        showToday={false}
        useMinutes={false}
        format={`ddd ${dayjsBase.localeData().longDateFormat("LL")}`}
        disableDatesIfOutOfProject
        defaultPickerValue={
          defaultPickerValue
            ? dayjs(defaultPickerValue).set("hour", dayjs(value)?.hour() || defaultHour)
            : undefined
        }
        disableDatesBeforeNow={false}
        onChange={(value) => {
          console.log("DATE PICKER COMP ONCHANGE", value);

          onChange(value);
          onChangeCallback?.();
        }}
        {...props}
      />
      {value && (
        <Select
          style={{minWidth: 150, maxWidth: "fit-content"}}
          className={"fade-in"}
          readOnly={!value}
          value={dayjs(value)?.hour()}
          placeholder={t("registrations:availabilities.datePicker.hourPlaceholder")}
          dropdownMatchSelectWidth={false}
          options={[
            {label: t("registrations:availabilities.datePicker.shortcuts"), options: timeStamps},
            {
              label: <Divider style={{margin: "10px 0 5px 0"}} />,
              options: [...Array(24).keys()]
                .filter((hour) => !timeStamps.find((option) => option.key === hour))
                .map((hour) => ({
                  key: hour,
                  value: hour,
                  label: t("registrations:availabilities.datePicker.aroundXh", {hour}),
                })),
            },
          ]}
          suffixIcon={<ClockCircleOutlined style={{color: "#bfc2cb"}} />}
          onChange={(hour) => {
            console.log("TIME PICKER COMP ONCHANGE", hour, mergeDate(value, hour));
            onChange(mergeDate(value, hour));
          }}
          bordered={false}
        />
      )}
    </div>
  );
};
