import {Trans} from "react-i18next";
import {textLogo} from "@app/configuration";
import {DiscordLink, ContactNOELink} from "@shared/components/NOESocialIcons";
import {useTranslation} from "react-i18next";

export const NoeOpenSourceParagraph = () => {
  const {t} = useTranslation();

  const MailToLink = () => <ContactNOELink showTitle>{t("common:email")}</ContactNOELink>;

  return (
    <div style={{marginTop: 14, opacity: 0.6}}>
      <Trans
        ns="common"
        i18nKey="noeOpenSourceParagraph"
        components={{
          noeTextLogo: (
            <img
              src={textLogo}
              alt="NOÉ"
              height="18"
              style={{display: "inline-block", marginBottom: 4}}
            />
          ),
          discordLink: <DiscordLink showTitle />,
          mailToLink: <MailToLink />,
        }}
      />
    </div>
  );
};
