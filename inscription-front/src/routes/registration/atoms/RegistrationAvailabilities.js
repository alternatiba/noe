import {Alert, Button} from "antd";
import {AvailabilitySlotsTableSimple} from "@shared/components/AvailabilitySlotsTable/AvailabilitySlotsTableSimple";
import dayjs, {dayjsBase} from "@shared/services/dayjs";
import React, {useState} from "react";
import {FormElement} from "@shared/inputs/FormElement";
import {useDispatch} from "react-redux";
import {registrationsActions} from "@features/registrations";
import {personName} from "@shared/utils/utilities";
import {Trans, useTranslation} from "react-i18next";
import {CardElement} from "@shared/components/CardElement";
import {InfoAlert} from "../Registration";
import {RegistrationDatePicker} from "./RegistrationDatePicker";
import {FormItem} from "@shared/inputs/FormItem";
import {H3Title} from "@shared/layout/typography";

export const humanizeDate = (date) =>
  dayjs(date).format(`dddd ${dayjsBase.localeData().longDateFormat("LL")}`);

export function RegistrationAvailabilities({
  currentProject,
  saveData,
  currentRegistration,
  stewardLinkedToRegistration,
  currentAvailabilities,
  datesForm,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const [availabilitySlotsInterface, setAvailabilitySlotsInterface] = useState(
    currentAvailabilities.length <= 1 ? "simple" : "advanced"
  );

  const firstSlotStart = currentAvailabilities[0]?.start;
  const firstSlotEnd = currentAvailabilities[0]?.end;

  const stewardLinkedHasDefinedAvailabilities =
    stewardLinkedToRegistration?.availabilitySlots?.length > 0;

  // ************************* //
  // ***** AVAILABILITIES **** //
  // ************************* //

  const changeAvailabilitySlots = (slots) =>
    dispatch(
      registrationsActions.setCurrentWithMetadata({
        availabilitySlots: slots,
      })
    );

  return (
    <>
      {/* SIMPLE */}
      {availabilitySlotsInterface === "simple" && ( // zero availabilitySlots given: simple interface
        <div className="containerV">
          <FormElement
            form={datesForm}
            initialValues={currentAvailabilities[0]}
            onValuesChange={(_, availabilitySlotValues) => {
              saveData();
              changeAvailabilitySlots([availabilitySlotValues]);
            }}>
            {/* Section header */}
            <div className="list-element-header">
              <H3Title>{t("registrations:availabilities.title")}</H3Title>
              <InfoAlert
                message={
                  <Trans
                    i18nKey="availabilities.infoMessage"
                    ns="registrations"
                    values={{
                      start: humanizeDate(currentProject.start),
                      end: humanizeDate(currentProject.end),
                    }}
                  />
                }
              />
            </div>

            {/* Start date picker */}
            <FormItem
              name={"start"}
              rules={[
                {required: true, message: t("registrations:availabilities.errors.addAStartDate")},
              ]}>
              <RegistrationDatePicker
                introText={t("registrations:availabilities.iArriveOn")}
                defaultPickerValue={currentProject.start}
                onChangeCallback={() =>
                  // Open the end date input if empty once the date has been selected
                  !firstSlotEnd && setTimeout(() => document.getElementById("end")?.click(), 300)
                }
              />
            </FormItem>

            {/* End date picker */}
            <FormItem
              name={"end"}
              rules={[
                {required: true, message: t("registrations:availabilities.errors.addAnEndDate")},
              ]}>
              <RegistrationDatePicker
                introText={t("registrations:availabilities.andILeaveOn")}
                defaultPickerValue={firstSlotStart || currentProject.start}
              />
            </FormItem>
          </FormElement>

          <div className="containerH" style={{alignItems: "baseline", color: "grey"}}>
            <p>
              {t("registrations:availabilities.datesAreComplicated")}
              <Button
                type="link"
                style={{paddingLeft: 8}}
                onClick={() => setAvailabilitySlotsInterface("advanced")}>
                {t("registrations:availabilities.accessAdvancedEditor")}
              </Button>
            </p>
          </div>
        </div>
      )}

      {/* ADVANCED */}
      {availabilitySlotsInterface === "advanced" && ( // one or more availability: choice between simple and advanced
        <AvailabilitySlotsTableSimple
          title={t("registrations:availabilities.title")}
          disableDatesIfOutOfProject
          useMinutes={false}
          defaultPickerValue={currentProject.start}
          availabilitySlots={currentAvailabilities}
          onChange={changeAvailabilitySlots}
        />
      )}

      {stewardLinkedToRegistration && ( // Alert when the user is linked to a steward
        <Alert
          style={{marginBottom: 20}}
          message={t("registrations:availabilities.linkedToStewardAlert.message", {
            name: personName(currentRegistration.steward),
          })}
          description={
            <>
              {t("registrations:availabilities.linkedToStewardAlert.description")}
              {stewardLinkedHasDefinedAvailabilities && (
                <CardElement
                  style={{marginTop: 20, marginBottom: 0, opacity: 0.9}}
                  size="small"
                  title={t("registrations:availabilities.linkedToStewardAlert.dates.title")}>
                  <Trans
                    ns="registrations"
                    i18nKey="availabilities.linkedToStewardAlert.dates.description"
                  />
                  <AvailabilitySlotsTableSimple
                    disabled
                    availabilitySlots={stewardLinkedToRegistration.availabilitySlots}
                  />
                </CardElement>
              )}
            </>
          }
          type="info"
        />
      )}
    </>
  );
}
