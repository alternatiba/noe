import dayjs from "@shared/services/dayjs";

//*******************************//
//******* SLOT UTILITIES ********//
//*******************************//

// DISPLAY THE SLOT NUMBER
export const slotNumberString = (slot) => {
  const numberOfSlotsInSession = slot.session.slots.length;
  return numberOfSlotsInSession > 1
    ? "(" +
        (slot.session.slots.findIndex((s) => s._id === slot._id) + 1) +
        "/" +
        numberOfSlotsInSession +
        ") "
    : "";
};

export const getSessionElementsFromListInSlot = (elementName, slot, getKey: (s: any) => string) =>
  slot.session[elementName].map(getKey);

//*****************************//
//******* AGENDA PARAMS *******//
//*****************************//

// Cell sizes and duration
export const CELL_DURATION_MINUTES = 30; // The smallest unit of time for drag and drop
export const CELLS_DISPLAY_HEIGHTS = [25, 35, 45];

// DEFAULT AGENDA PARAMS
export const DEFAULT_AGENDA_PARAMS = {
  slotsOnEachOther: false,
  cellDisplayHeight: CELLS_DISPLAY_HEIGHTS[1],
};

// Number of days to display by default
const MAX_DAYS_TO_DISPLAY_BY_DEFAULT = 4;
export const getNumberOfDaysToDisplay = (windowWidth, project) =>
  Math.min(
    Math.max(1, Math.ceil((windowWidth - 280) / 450)), // Screen adaption
    dayjs(project.end).diff(project.start, "day") + 1, // Not more than the project opening dates
    MAX_DAYS_TO_DISPLAY_BY_DEFAULT // Default limit
  );

export const getAgendaDisplayStartDate = (project) => {
  const now = dayjs(); // Now is in the time of the project
  const start = dayjs(project.start);
  const end = dayjs(project.end);

  // If we are during the event, start at the current date. Else, start at project start date
  return (now.isBefore(start) || now.isAfter(end) ? start : now).toISOString();
};
