import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {registrationsSelectors} from "@features/registrations";

/**
 * Custom hook to determine access rights based on project opening state
 */
export const useProjectOpeningStateAccessRights = () => {
  const {secretSchedule, openingState} = useSelector(currentProjectSelectors.selectProject);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const authenticatedRegistration = useSelector(registrationsSelectors.selectAuthenticated);

  // Check if user is linked to a steward
  const userIsLinkedToSteward = currentRegistration?.steward !== undefined;
  const authenticatedUserIsProjectOrga = !!authenticatedRegistration?.role;

  // Determine if opening state allows access to sessions
  const openingStateAllowsAccessToSessions =
    (userIsLinkedToSteward && openingState === "registerForStewardsOnly") ||
    openingState === "registerForAll";

  // Check if user has access to secret schedule
  const userHasAccessToSecretSchedule =
    !secretSchedule || currentRegistration?.inDatabase?.everythingIsOk;

  // Determine if user should have access to sessions page
  const giveAccessToSessionsPageForNormalUsers =
    userHasAccessToSecretSchedule && openingStateAllowsAccessToSessions;

  // Or because they are authenticated as an orga
  const giveAccessToSessionsPage =
    giveAccessToSessionsPageForNormalUsers || !!authenticatedUserIsProjectOrga;

  // Return object with access rights and user status
  return {
    userIsLinkedToSteward,
    authenticatedUserIsProjectOrga,
    giveAccessToSessionsPage,
    giveAccessToSessionsPageForNormalUsers,
    openingStateAllowsAccessToSessions,
    userHasAccessToSecretSchedule,
  };
};
