import {getSessionElementsFromListInSlot, slotNumberString} from "./agendaUtilities";
import {getFullSessionName} from "./sessionsUtilities";

describe("agendaUtilities", () => {
  describe("slotNumberString", () => {
    it("should return empty string if only 1 slot", () => {
      const slot = {
        session: {
          slots: [{}],
        },
      };
      expect(slotNumberString(slot)).toEqual("");
    });

    it("should return formatted slot number string if multiple slots", () => {
      const slot = {
        session: {
          slots: [{}, {_id: "123"}, {}],
        },
        _id: "123",
      };

      expect(slotNumberString(slot)).toEqual("(2/3) ");
    });
  });

  describe("getSessionName", () => {
    const sessionWithTeam = {
      name: "Session 1",
      activity: {name: "Activity 1"},
      team: {name: "Team 1"},
    };
    const lightSessionWithTeam = {
      name: "Session 1",
      activity: {name: "Activity 1"},
      team: "myTeamId",
    };
    const sessionWithoutTeam = {
      name: "Session 2",
      activity: {name: "Activity 2"},
    };
    const teams = [{_id: "myTeamId", name: "Team 1"}];
    const defaultSessionNameTemplate = "{{sessionName}} - {{activityName}}";

    it("should format session name with team if teams are given", () => {
      expect(getFullSessionName(sessionWithTeam, defaultSessionNameTemplate, teams)).toEqual(
        "Session 1 - Activity 1 (Team 1)"
      );
    });

    it("should format session name without team if session has no team", () => {
      expect(getFullSessionName(sessionWithoutTeam, defaultSessionNameTemplate, teams)).toEqual(
        "Session 2 - Activity 2"
      );
    });

    it("should find team name inside the session if given, or in teams array if the id only is given", () => {
      // ID + non teams array : can't find. No team
      expect(getFullSessionName(lightSessionWithTeam, defaultSessionNameTemplate)).toEqual(
        "Session 1 - Activity 1"
      );

      // ID + array: find in array by id
      expect(getFullSessionName(lightSessionWithTeam, defaultSessionNameTemplate, teams)).toEqual(
        "Session 1 - Activity 1 (Team 1)"
      );
      // Name inside session + no array : find in session
      expect(getFullSessionName(sessionWithTeam, defaultSessionNameTemplate)).toEqual(
        "Session 1 - Activity 1 (Team 1)"
      );
    });

    it("should format session name according to the sessionNameTemplate", () => {
      const sessionNameTemplate2 = "{{activityName}} > {{sessionName}}";
      expect(getFullSessionName(sessionWithoutTeam, sessionNameTemplate2, teams)).toEqual(
        "Activity 2 > Session 2"
      );
    });
  });
});
