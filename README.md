![NOÉ](shared/images/readme-banner.jpg)

### 👉 [get.noe-app.io](https://get.noe-app.io) 👈

> [🎥 **Découvrir NOÉ** en vidéo](https://www.youtube.com/playlist?list=PLxTDjZn9usXIewHmWius7tiWdcm6zdcAY)
>
> [❤ **Soutenir** le projet !](https://opencollective.com/noeappio)
>
> [🤓 **Tester** la plateforme](https://get.noe-app.io/docs)
>
> [🖥️ **Contribuer** au projet / documentation technique](CONTRIBUTING.md)
>
> [📃 **Changelog**](https://bit.ly/changelog-noeappio-gitlab)
>
> [🚀 **Utiliser NOÉ** pour mon
événement](mailto:hello@noe-app.io?subject=J'aimerais&#32;utiliser&#32;NOÉ&#32;pour&#32;mon&#32;événement)
<br/>

> _Nos réseaux :_
>
> [🥳 Rejoignez la communauté NOÉ sur **Discord** !](https://discord.gg/hdrJnFqmXJ)
>
> [🔔 Ne loupez rien des nouveautés, abonnez-vous au canal **Telegram**](https://t.me/noeappio)

# NOÉ - Planification d'événements

Vous retrouverez toutes ces informations sur [notre site internet](https://get.noe-app.io).

## Présentation

NOÉ est le diminutif de "Nouvelle Organisation d'Événements". C'est un outil **open-source**, destiné aux
organisateur⋅ices d'événements qui souhaitent organiser des événements plus horizontaux, auto-organisés et
participatifs, grâce à un ensemble d'outils qui les aident à toutes les étapes de l'organisation :

- **Déclarez votre programme artistique, musical ou éducatif** directement dans NOÉ. L'application devient votre
  principale source d'information, où vous pouvez stocker le "quoi, où, quand et avec qui" de votre événement.

- **Invitez les participants à votre événement**. NOÉ permet à vos participant⋅es de s'inscrire aux activités, ateliers
  et sessions de volontariat qu'iels souhaitent, de manière totalement libre, même pour les très grands événements.
  Donnez les informations essentielles sur votre page d'accueil, entièrement modifiable avec un éditeur intégré dans
  NOÉ.

- **Intégrez le volontariat, de manière native**. Pour que votre événement se déroule bien, il se peut que vous ayez
  besoin que chaque participant⋅e passe un certain temps à aider en tant que bénévole. Définissez votre fourchette de
  temps de bénévolat acceptable par jour, afin que les participant⋅es soient encouragé⋅es à remplir leur jauge de
  bénévolat. Encouragez les personnes à se porter volontaires pour les bénévolats plus fatiguants, en fixant une
  majoration du temps de volontariat sur les postes qui ont besoin d'un coup de pouce.

- **Surveillez les inscriptions de vos participants et obtenez des informations clés pour votre événement**. Construisez
  votre formulaire d'inscription personnalisé directement dans NOÉ, demandez ce que vous avez besoin de savoir à vos
  participant⋅es. Compilez les résultats et prenez de meilleures décisions pour économiser du temps, de l'énergie et de
  l'argent. Exemples : "combien de personnes mangent sur place pour le déjeuner de demain, quelle quantité de nourriture
  dois-je préparer ?" / "Suis-je sûr d'avoir au moins 2 personnes formées aux premiers secours sur le site, à tout
  moment de l'événement ?" / "Combien de personnes doivent encore se présenter au point d'accueil ce soir ?" / "Combien
  de personnes participent à la formation XYZ à 18h ? Faut-il ajouter des bancs dans la salle ?"

- **Gardez le contact avec vos participant⋅es**. Vous êtes en mesure d'extraire les numéros de téléphone, les emails ou
  toute autre information utile très rapidement, ce qui vous permet de dialoguer facilement avec vos participants,
  d'envoyer des emails ciblés ou des SMS de groupe aux personnes qui en ont besoin. Exemple : envoyer des SMS à toutes
  les personnes qui se sont inscrites à un atelier particulier et dont l'horaire a changé.

Le projet NOÉ a été lancé par Alternatiba en 2019. Alternatiba est une association militant pour la justice sociale et
climatique. À l'origine, NOÉ a été créé pour aider à organiser les Camps Climat (qui sont des rassemblements de
militants dans toute la France pour s'informer sur les questions de climat et de justice sociale, se rencontrer,
partager et fédérer le mouvement climat).

Puis nous avons vu que NOÉ attirait également d'autres acteurs et associations, qui avaient également besoin d'un outil
nativement horizontal pour l'organisation de leurs événements... C'est ainsi que NOÉ a commencé à être utilisé dans des
festivals, et autres séminaires.

> _English version_
>
> NOÉ is the short name for "Nouvelle Organization d'Événements" (which could translate to "New Ways to Organize
> Events"). It is an **open-source** tool for event organizators that wish to organize more horizontal, auto-organized and
> participative events, through a set of tools that helps during all the steps of the organization:
>
> - **Declare your artistic, musical or educational schedule** directly in NOÉ. It becomes your main source of
    information, where you can store the "what, where, when and with who" information of your event.
>
> - **Invite participants to your event**. NOÉ provides a way for your participants to register to activities, workshops
    and volunteering sessions that they want, totally freely, even in very big events. Give essential information in
    your welcome page, fully editable with a built-in editor, inside NOÉ.
>
> - **Integrate volunteering, natively**. For your event to run perfectly, you may need each participant to spend some
    time helping as a volunteer. Set your desired range of acceptable volunteering time per day, so participants get
    encouraged to fill their volunteering gauge. Encourage people to volunteer in more exhausting volunteerings by
    setting a volunteering majoration on the shifts that need a boost.
>
> - **Monitor your participants' registrations and get key insights from them**. Build your custom registration form
    directly inside NOÉ, ask what you need to know to your participants. Compile results and make better decisions
    overall to spare time, effort and money. Examples: "how many people are eating on site for tomorrow's lunch, how
    much food should I prepare ?" / "Am I sure that I have at least 2 people with first aid training on site, anytime in
    the event ?" / "How many people are still to come at the welcome point this evening ?" / “How many people are
    attending the formation XYZ at 6pm ? Should we add more benches ?”
>
> - **Keep in touch with your participants**. You’re able to extract phone numbers, emails or any other useful
    information really quickly, so you can talk easily with your participants, send targeted group emails or group text
    messages to people who need it. Ex: send text messages to all people who registered a particular workshop whose
    schedule has changed.
>
> The NOÉ project was launched by Alternatiba in 2019. Alternatiba is a French association campaigning for social and
> climate justice. Originally, NOÉ was created to help organize Climate Camps (which are gatherings of activists all over
> France to learn about climate and social justice issues, meet, share, and celebrate all together).
>
> Then we saw that NOÉ was also attracting other actors and associations, who also needed a natively horizontal tool for
> the organization of their events. That’s how NOÉ began to be used in festivals, and other seminaries.


## Par où commencer ?

### Comprendre NOÉ en vidéo

[Je découvre NOÉ en 20 minutes !](https://peertube.virtual-assembly.org/videos/watch/37c55e47-428c-497f-b454-28a16de73041?start=4m28s)

### Installer un serveur NOÉ

Toutes les infos sont disponibles dans le [guide de contribution](CONTRIBUTING.md).

## Envie de sauter le pas avec votre événement ?

Envoyez un email
à [hello@noe-app.io](mailto:hello@noe-app.io?subject=J'aimerais&#32;utiliser&#32;NOÉ&#32;pour&#32;mon&#32;événement), on
se fera un plaisir de voir avec vous ce que l'on peut faire.