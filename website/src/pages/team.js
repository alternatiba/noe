import Translate, {translate} from "@docusaurus/Translate";
import {
  SectionPrimary,
  SectionPrimaryInverse,
  SectionSecondary,
  SectionTertiary,
} from "@site/src/components/design-system/Section";
import {Stack} from "@mui/joy";
import {ContactUsBanner} from "@site/src/components/ContactUsBanner";
import {MemberPresentation} from "@site/src/components/MemberPresentation";
import Grid from "../components/design-system/Grid";
import Layout from "@theme/Layout";
import React from "react";
import {LinkedinOutlined, LinkOutlined} from "@ant-design/icons";
import Link from "@docusaurus/Link";

const TEAM_MEMBERS = [
  {
    name: "Jules",
    bio: translate({id: "team.members.jules.bio"}),
    picture: "jules.jpg",
    links: [
      {icon: <LinkedinOutlined />, href: "https://www.linkedin.com/in/juleslcrr/"},
      {icon: <LinkOutlined />, href: "https://juleslecuir.com"},
    ],
  },
  {
    name: "Marie",
    bio: translate({id: "team.members.marie.bio"}),
    picture: "marie.jpg",
    links: [{icon: <LinkedinOutlined />, href: "https://www.linkedin.com/in/m-bernard/"}],
  },
  {
    name: "Ancelin",
    bio: translate({id: "team.members.ancelin.bio"}),
    picture: "ancelin.jpeg",
    links: [
      {
        icon: <LinkedinOutlined />,
        href: "https://www.linkedin.com/in/ancelin-moulherat-bb7889172/",
      },
    ],
  },
  {
    name: "Pierre",
    bio: translate({id: "team.members.pierre.bio"}),
    picture: "pierre.jpg",
    links: [
      {
        icon: <LinkedinOutlined />,
        href: "https://www.linkedin.com/in/brenot/",
      },
    ],
  },
];

const PARTNERS = [
  {name: "Data Players", href: "https://data-players.com/"},
  {name: "Assemblée Virtuelle", href: "http://www.virtual-assembly.org/"},
  {name: "TiBillet", href: "https://tibillet.org/"},
  {name: "Alternatiba", href: "https://alternatiba.eu/"},
  {name: "Nua.ge", href: "https://nua.ge/"},
  {name: "Eleven Labs", href: "https://eleven-labs.com/"},
];

const EVENTS = [
  {name: "Décroissance, le festival", href: "https://decroissancelefestival.org "},
  {name: "Camps Climat d'Alternatiba", href: "https://campclimat.eu/"},
  {name: "Festival Oasis", href: "https://cooperative-oasis.org/articles/festival-oasis-2023/"},
  {name: "Ethereal Decibel Festival", href: "https://etherealdecibel.com"},
  {name: "Crème Brûlée", href: "https://sites.google.com/view/cremebruleee/homepage"},
  {name: "Opal Festival", href: "https://www.opal-festival.com/"},
];

export default function Team() {
  return (
    <Layout title={translate({id: "team.tagline"})} description={translate({id: "team.summary"})}>
      {/* OUR TEAM HERO HEADER */}
      {/* -10 / +10 so the white above doesn't show when scrolling*/}
      <SectionPrimaryInverse style={{paddingTop: "10rem", marginTop: "-10rem"}}>
        <Stack my={8} className={"fade-in from-bottom"} alignItems={"center"}>
          {/* Tagline */}
          <h1 className={"text-gradient text-h1"} style={{filter: "brightness(1.6)"}}>
            <Translate id={"team.tagline"} />
          </h1>
        </Stack>
      </SectionPrimaryInverse>

      {/* SUMMARY */}
      <SectionSecondary maxWidth={"45rem"}>
        <Stack alignItems={"center"} textAlign={"center"} className={"displayText"}>
          <p style={{marginBottom: 0}}>
            <Translate id={"team.summary"} />
          </p>
        </Stack>
      </SectionSecondary>

      {/* TEAM MEMBERS */}
      <SectionTertiary py={10}>
        <Grid container>
          {TEAM_MEMBERS.map((member) => (
            <Grid xs={12} md={6} lg={3} key={member.name}>
              <MemberPresentation {...member} />
            </Grid>
          ))}
        </Grid>
      </SectionTertiary>

      {/* Team members */}
      <SectionPrimary style={{color: "initial"}}>
        <Stack
          alignItems={"center"}
          textAlign={"center"}
          sx={{
            background: "white",
            borderRadius: 12,
            padding: {xs: "3rem 1.5rem", md: "3rem"},
          }}>
          <Grid container maxWidth={"60rem"}>
            <Grid xs={12}>
              <h2 className={"baseFont"}>
                <Translate id={"team.ourEcosystemIsWonderful"} />
              </h2>
            </Grid>
            <Grid xs={12} md={6}>
              <h3 className="baseFont" style={{marginBottom: "2rem"}}>
                <Translate id={"team.partners"} />
              </h3>
              {PARTNERS.map((partner) => (
                <p>
                  <Link href={partner.href}>{partner.name}</Link>
                </p>
              ))}
            </Grid>
            <Grid xs={12} md={6}>
              <h3 className="baseFont" style={{marginBottom: "2rem"}}>
                <Translate id={"team.users"} />
              </h3>
              {EVENTS.map((partner) => (
                <p>
                  <Link href={partner.href}>{partner.name}</Link>
                </p>
              ))}
            </Grid>
            <Grid xs={12}>
              <p>
                <Translate id={"team.andMore"} />
              </p>
            </Grid>
          </Grid>
        </Stack>
      </SectionPrimary>

      <ContactUsBanner />
    </Layout>
  );
}
