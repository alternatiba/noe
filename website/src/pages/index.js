import Layout from "@theme/Layout";
import {translate} from "@docusaurus/Translate";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import FeaturesGrid from "./_index/FeaturesGrid";
import {ContactUsBanner} from "../components/ContactUsBanner";
import {SummaryAndLinks} from "./_index/SummaryAndLinks";
import {YourFirstPlanningIn4Minutes} from "./_index/YourFirstPlanningIn4Minutes";
import {NoeHeroHeader} from "./_index/NoeHeroHeader";
import {TheyTrustUs} from "./_index/TheyTrustUs";
import {useEffect} from "react";

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  const announcementMessage = siteConfig.customFields.ANNOUNCEMENT_MESSAGE;

  useEffect(() => {
    if (announcementMessage) {
      // Set the top CSS attribute of navbar to var(--announcement-height)
      const navbar = document.querySelector("nav.navbar");
      const announcementBar = document.querySelector(
        "[class*=announcementBarContent_node_modules]"
      );
      if (navbar && announcementBar) {
        navbar.style.top = "var(--docusaurus-announcement-bar-height)";
        announcementBar.classList.add("on-main-page");
      }
    }
  });

  return (
    <Layout
      title={translate({id: "homepage.tagline"})}
      description={translate({id: "homepage.summary"})}>
      <NoeHeroHeader />
      <SummaryAndLinks />
      <FeaturesGrid />
      <YourFirstPlanningIn4Minutes />
      <TheyTrustUs />
      <ContactUsBanner />
    </Layout>
  );
}
