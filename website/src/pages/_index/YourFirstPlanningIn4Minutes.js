import {SectionPrimary} from "../../components/design-system/Section";
import Grid from "../../components/design-system/Grid";
import {Stack} from "@mui/joy";
import {LesTutosNOEPlaylistIframe} from "@site/src/components/LesTutosNOEPlaylistIframe";
import Translate from "@docusaurus/Translate";
import Link from "@docusaurus/Link";

export const YourFirstPlanningIn4Minutes = () => (
  <SectionPrimary>
    <Grid container rowSpacing={8}>
      {/* "As simple as that" tagline and action button */}
      <Grid xs={12} md={6} mb={8}>
        <Stack
          gap={4}
          alignItems={{xs: "center", md: "flex-start"}}
          justifyContent={"center"}
          textAlign={{xs: "center", md: "left"}}
          height={"100%"}>
          <h2 className={"text-h1"} style={{margin: "unset"}}>
            <Translate id="asSimpleAsThat" />
          </h2>
          <p style={{margin: 0}} className={"displayText"}>
            <Translate id="createPlanningIn5Minutes" />
          </p>
          <Link className="button button--secondary" href="/docs">
            <Translate id="tryNOE" />
          </Link>
        </Stack>
      </Grid>

      {/* YouTube LesTutosNOE Playlist */}
      <Grid xs={12} md={6}>
        <Stack alignItems={"center"} gap={2}>
          <LesTutosNOEPlaylistIframe />
          <Link
            className="button"
            href="https://youtube.com/playlist?list=PLxTDjZn9usXIewHmWius7tiWdcm6zdcAY&si=xSErwEPBaL-FQ76N">
            <Translate id="watchPlaylistOnYouTube" />
          </Link>
        </Stack>
      </Grid>
    </Grid>
  </SectionPrimary>
);
