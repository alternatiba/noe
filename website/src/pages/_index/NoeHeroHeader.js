import {SectionPrimary} from "../../components/design-system/Section";
import Grid from "../../components/design-system/Grid";
import clsx from "clsx";
import styles from "../index.module.less";
import {Stack} from "@mui/joy";
import Translate from "@docusaurus/Translate";
import Link from "@docusaurus/Link";
import NoeLogo from "@site/static/img/shared/logo-white-with-white-text.svg";
import NoeOverviewVideo from "@site/static/video/noe-overview.mp4";
import NoeOverviewThumbnail from "@site/static/video/noe-overview-thumbnail.png";
import {useEffect} from "react";

const EmbeddedVideo = ({
  id,
  src,
  type,
  style,
  thumbnail,
  delayBeforeControlsVisible = 10,
  ...videoControls
}: {
  id: string,
  src: string,
  type: string,
  delayBeforeControlsVisible?: number,
} & Partial<HTMLVideoElement>) => {
  // Hide video controls for the X first seconds, then allow them
  useEffect(() => {
    setTimeout(() => {
      const video = document.getElementById(id);
      video?.setAttribute("controls", "controls");
    }, delayBeforeControlsVisible * 1000);
  }, []);

  return (
    <video
      muted
      autoPlay
      loop
      id={id}
      poster={NoeOverviewThumbnail}
      {...videoControls}
      style={{
        width: "100%",
        borderRadius: 12,
        aspectRatio: "16 / 9",
        background: "white",
        ...style,
      }}>
      <source src={src} type={type} />
    </video>
  );
};

export function NoeHeroHeader() {
  return (
    // Keep pt = 30 so we don't have white glitches on scroll between header and hero banner
    <SectionPrimary sx={{pt: "2rem"}}>
      <Grid container direction={{xs: "column", md: "row"}} my={{xs: 0, md: 8}} mx={0} rowGap={10}>
        {/* Tagline */}
        <Grid xs={12} md={5}>
          {/* TEXT AND CALL TO ACTION BUTTON */}
          <Stack
            gap={10}
            textAlign={{xs: "center", md: "left"}}
            alignItems={{xs: "center", md: "start"}}
            justifyContent={"center"}
            height={"100%"}>
            {/* Logo */}
            <NoeLogo className={clsx(styles.noeLogo, "fade-in from-bottom")} />

            {/* Tagline */}
            <h1 className={"fade-in from-bottom homepageTagline"} style={{animationDelay: "0.3s"}}>
              <Translate id={"homepage.taglineWithNewLines"} />
            </h1>

            {/* Button discover NOÉ */}
            <Link
              className={clsx(
                styles.heroButton,
                " button button--secondary button--outline button--lg fade-in from-bottom"
              )}
              style={{borderWidth: 3, color: "white", animationDelay: "0.6s"}}
              to="/docs">
              <Translate id="homepage.heroButton" />
            </Link>
          </Stack>
        </Grid>

        {/* OVERVIEW DEMO VIDEO */}
        <Grid xs={12} md={7}>
          <EmbeddedVideo
            className={"fade-in from-bottom shadow"}
            id={"noe-overview-video"}
            style={{width: "100%", borderRadius: 12, animationDelay: "0.9s"}}
            src={NoeOverviewVideo}
            type="video/mp4"
          />
        </Grid>
      </Grid>
    </SectionPrimary>
  );
}
