export function Signature({title}) {
  return (
    <div
      style={{
        marginTop: 10,
        textAlign: "right",
        opacity: 0.7,
        fontStyle: "italic",
      }}>
      ~ {title}
    </div>
  );
}
