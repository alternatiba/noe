import {Box, Stack} from "@mui/joy";
import React from "react";
import Link from "@docusaurus/Link";
import useBaseUrl from "@docusaurus/core/lib/client/exports/useBaseUrl";

export const MemberPresentation = ({name, bio, picture, links}) => {
  return (
    <Stack alignItems={"center"} gap={6}>
      <img
        width={"90%"}
        height={"auto"}
        src={useBaseUrl("/img/team/" + picture)}
        alt={`A picture of ${name}`}
        style={{borderRadius: "50%", aspectRatio: "1 / 1"}}
      />
      <Box textAlign={"center"}>
        <h2>{name}</h2>
        <p>{bio}</p>
        <Stack justifyContent={"center"} direction={"row"} gap={3}>
          {links.map(({icon, href}) => (
            <Link
              className="button button--secondary button--lg"
              href={href}
              style={{padding: 7, minWidth: 44}}>
              {icon}
            </Link>
          ))}
        </Stack>
      </Box>
    </Stack>
  );
};
