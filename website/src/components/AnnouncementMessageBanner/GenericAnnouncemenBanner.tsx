import {CSSProperties, ReactNode} from "react";

// This component is shared with orga-front and inscription-front too !
export const GenericAnnouncementBanner = ({
  htmlString,
  button,
  styles = {},
}: {
  htmlString: string;
  button?: ReactNode;
  styles?: {container?: CSSProperties; content?: CSSProperties};
}) => {
  return (
    <div
      // Main container with gradient
      className="announcement-message-banner"
      style={{
        background: "var(--noe-gradient)",
        fontSize: 12,
        color: "var(--text)",
        fontWeight: 600,
        ...styles.container,
      }}>
      <div
        // Overlay container with white background. Contains the message content and a button on the far right
        className={"containerH flex-space"}
        style={{
          background: "var(--colorBgLayout)",
          opacity: 0.7,
          justifyContent: "space-between",
          ...styles.content,
        }}>
        <div
          // Message content, centered
          className={"containerH"}
          style={{
            justifyContent: "center",
            fontWeight: 600,
          }}>
          <div
            style={{paddingTop: 2, textAlign: "center"}}
            dangerouslySetInnerHTML={{__html: htmlString}}
          />
        </div>

        {button}
      </div>
    </div>
  );
};
