import {useLayoutEffect} from "react";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import {GenericAnnouncementBanner} from "./GenericAnnouncemenBanner";

export const AnnouncementMessageBanner = () => {
  const {
    siteConfig,
    i18n: {currentLocale, defaultLocale},
  } = useDocusaurusContext();

  const announcementMessage =
    siteConfig.customFields[`ANNOUNCEMENT_MESSAGE_${currentLocale.toUpperCase()}`] ||
    siteConfig.customFields[`ANNOUNCEMENT_MESSAGE_${defaultLocale.toUpperCase()}`];

  const updateAnnouncementBarHeight = () => {
    const announcementBar = document.querySelector(".announcement-message-banner");
    if (announcementBar) {
      // Get the height of the announcement bar
      const announcementBarHeight = announcementBar.offsetHeight;
      document.documentElement.style.setProperty(
        "--announcement-bar-height",
        `${announcementBarHeight}px`
      );
    } else {
      document.documentElement.style.setProperty("--announcement-bar-height", `0px`);
    }
  };

  useLayoutEffect(() => {
    updateAnnouncementBarHeight();
    window.addEventListener("resize", updateAnnouncementBarHeight);

    return () => {
      window.removeEventListener("resize", updateAnnouncementBarHeight);
    };
  }, []);

  return announcementMessage ? (
    <GenericAnnouncementBanner
      htmlString={announcementMessage}
      styles={{content: {padding: "4px 12px", alignItems: "center"}}}
    />
  ) : null;
};
