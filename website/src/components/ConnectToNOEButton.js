import React from "react";
import Translate from "@docusaurus/Translate";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";

export const ConnectToNOEButton = () => {
  const {siteConfig} = useDocusaurusContext();
  const INSCRIPTION_FRONT_URL = siteConfig.customFields.INSCRIPTION_FRONT_URL;
  const ORGA_FRONT_URL = siteConfig.customFields.ORGA_FRONT_URL;

  return (
    <div className="dropdown dropdown--hoverable dropdown--right connect-to-noe-button">
      <button className="button button--secondary" style={{marginRight: 12}}>
        <Translate id={"navbar.logIn"} />
      </button>
      <ul className="dropdown__menu">
        <li>
          <a className="dropdown__link" href={INSCRIPTION_FRONT_URL}>
            <Translate id={"inscriptionFront"} />
          </a>
        </li>
        <li>
          <a className="dropdown__link" href={ORGA_FRONT_URL}>
            <Translate id={"orgaFront"} />
          </a>
        </li>
      </ul>
    </div>
  );
};
