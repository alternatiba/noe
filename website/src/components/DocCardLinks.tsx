import {ReactNode} from "react";

type CardLinkItem = {
  href: string;
  title: string;
  description: string;
  icon: ReactNode;
};

type DocCardLinksProps = {items: Array<CardLinkItem>};

const DocCardLinks = ({items}: DocCardLinksProps) => {
  // This copies the html code of the <DocCardsList /> component from Docusaurus, but make it go in a
  return (
    <section className="row" style={{paddingTop: "0.3rem"}}>
      {items.map(({href, title, description, icon}) => (
        <article className="col col--12 margin-bottom--lg">
          <a
            className="card padding--lg"
            style={{
              // @ts-ignore
              "--ifm-link-color": "var(--ifm-color-emphasis-800)",
              "--ifm-link-hover-color": "var(--ifm-color-emphasis-700)",
              "--ifm-link-hover-decoration": "none",
              border: "1px solid var(--ifm-color-emphasis-200)",
              boxShadow: "0 1.5px 3px 0 #00000026",
              transition: "all var(--ifm-transition-fast) ease",
              transitionProperty: "border,box-shadow",
            }}
            href={href}>
            <h3
              className="text--truncate"
              style={{
                fontSize: "1.2rem",
                marginBottom: 0,
              }}
              title={title}>
              {icon} {title}
            </h3>
            {description && (
              <p
                style={{
                  color: "var(--ifm-color-emphasis-600)",
                  marginBottom: 0,
                  marginTop: "0.5rem",
                }}>
                {description}
              </p>
            )}
          </a>
        </article>
      ))}
    </section>
  );
};

export default DocCardLinks;
