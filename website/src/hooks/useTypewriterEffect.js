// This code is not used anymore but we keep it just in case
import {useLayoutEffect} from "react";

export const useTypewriterEffect = (
  elementId,
  textToWrite,
  typingSpeed = 40 // time delay of print out
) => {
  function typewriter(fullText, element, charPosition = 0) {
    const caret = "<strong>|</strong>";
    const shouldDisplayCaret = charPosition + 1 < fullText.length;
    const displayText = fullText.slice(0, charPosition) + (shouldDisplayCaret ? caret : "");
    element.innerHTML = displayText.replace(/\n/g, "<br/>");
    charPosition++;
    if (charPosition <= fullText.length) {
      setTimeout(() => typewriter(fullText, element, charPosition), typingSpeed);
    }
  }

  // Start typewriting ASAP (useLayoutEffect)
  useLayoutEffect(() => {
    const canTypewrite = window.screen.width >= 1443;
    const typeWritedElement = window.document.getElementById(elementId);

    if (typeWritedElement && canTypewrite) {
      typewriter(textToWrite, typeWritedElement);
    } else {
      typeWritedElement.innerHTML = textToWrite;
    }
  }, []);
};
