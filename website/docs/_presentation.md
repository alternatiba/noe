---
sidebar_position: 2
---

<!-- TODO: Est ce que cette page est encore d'actualité ? -->

# Présentation

**Découvrons NOÉ en 5 minutes ! 🥳**

## C'est quoi, NOÉ ?

NOÉ (Nouvelle Organisation d'Événements) est un outil **open-source** conçu pour les organisateur⋅ices d'événements. Il facilite la création d'événements plus horizontaux, auto-organisés et participatifs grâce à un ensemble de fonctionnalités couvrant toutes les étapes de l'organisation :

1. **Gestion du programme** : Créez et gérez votre programme artistique, musical ou éducatif directement dans NOÉ. L'application centralise toutes les informations essentielles de votre événement (quoi, où, quand, avec qui).

2. **Inscription des participant·es** : Invitez les participant⋅es à s'inscrire librement aux activités, ateliers et créneaux de volontariat, même pour les grands événements. Gérez leur degré d'autonomie avec les nombreuses options. Personnalisez votre page d'accueil avec un éditeur intégré pour communiquer les informations clés.

3. **Gestion du volontariat** : Intégrez naturellement le volontariat à votre événement. Définissez une fourchette de temps de bénévolat acceptable par jour et encouragez la participation avec un système de majoration pour les tâches plus exigeantes.

4. **Suivi des inscriptions** : Créez votre formulaire d'inscription personnalisé, collectez les informations nécessaires et analysez les données pour prendre des décisions éclairées, optimisant ainsi temps, énergie et ressources.

5. **Communication avec les participants** : Facilitez le contact avec vos participant⋅es en extrayant rapidement les coordonnées (numéros de téléphone, emails) pour envoyer des communications ciblées ou des SMS groupés.

6. **La gare centrale pour tout votre événement** : les champs personnalisés permettent d'ajouter n'importe quelle information pertinente dans NOÉ, pour que vous puissiez avoir tout au même endroit.

NOÉ s'adapte à diverses typologies d'événements (festivals, résidences, universités d'été, etc.) et offre une solution complète pour une organisation efficace et participative.

### Quelles fonctionnalités principales ? (liste non-exhaustive !)

:::info

NOÉ est conçu dans une logique modulaire et s'adapte à de nombreuses typologies d'événements : festivals,
résidences, universités d'été, etc. Pourquoi pas le vôtre ?

:::

Les organisateur⋅ices peuvent :

- Saisir tous les éléments du programme de l'événement (lieux, activités, intervenant⋅es) ainsi que toutes les contraintes associées (disponibilité des personnes, des salles, etc.),
- Réagencer facilement les sessions grâce à une vue agenda qui permet de déplacer, redimensionner, ou cloner les sessions facilement
- Consulter et modifier en deux clics les données d'inscription des participant⋅es
- Créer des équipes de participant⋅es
- Inscrire, ou désinscrire des participant⋅es à des sessions, ou des équipes.
- Et plein d'autres choses...

Les participant⋅es peuvent :

- S'inscrire à l'événement,
- Choisir les activités auxquelles iels souhaitent prendre part, en recherchant, filtrant, les sessions disponibles,
- Consulter leur emploi du temps facilement, sur PC ou sur mobile.
- Exporter leur planning en PDF
- Et d'autres choses encore...
