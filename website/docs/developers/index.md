---
sidebar_position: 4
---

# Pour les développeur·euses

Tout ce qui pourrait intéresser les gens qui codent est ici !

Si vous voulez contribuer à NOÉ ou comprendre comment le déployer sur votre serveur, vous êtes au bon endroit.

```mdx-code-block
import DocCardList from '@theme/DocCardList';

<DocCardList />
```