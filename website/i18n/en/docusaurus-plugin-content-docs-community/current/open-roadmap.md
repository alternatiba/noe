---
hide_table_of_contents: true
sidebar_position: 3
---

# Open-Roadmap

We are developing NOÉ continuously, in *open* mode. Here are the developments we plan to make in the future and their priorities.

```mdx-code-block
import {NotionEmbedding} from "@site/src/components/NotionEmbedding";

<NotionEmbedding url={"https://v2-embednotion.com/d3879877b3cd4bcc909ef978fee05320?v=22f99327c7de4fd68bc1bb60f6759152&pvs=4"}
style={{marginTop: -50}} />
```