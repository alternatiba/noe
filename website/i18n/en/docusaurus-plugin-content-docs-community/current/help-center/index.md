---
sidebar_position: 1
---

# Help center

:::info Welcome to the NOÉ Help center!

Here you can look for answers to all your questions.

:::

<br/>

```mdx-code-block
import DocCardList from '@theme/DocCardList';

<DocCardList />
```

---

Haven't found the answer to your question?

```mdx-code-block
import DocCardLinks from '@site/src/components/DocCardLinks';

<DocCardLinks items={[
  {href: "/community/contact", title: "Contact us", icon: "💬"},
]} />
```
