---
sidebar_position: 2
---

<!-- TODO: Est ce que cette page est encore d'actualité ? -->

# Presentation

**Let's discover NOÉ in 5 minutes! 🥳**

## What is NOÉ ?

NOÉ (New Event Organization) is an **open-source** tool designed for event organizers. It facilitates the creation of more horizontal, self-organized, and participatory events through a set of features covering all stages of organization:

1. **Program Management**: Create and manage your artistic, musical, or educational program directly in NOÉ. The application centralizes all essential information about your event (what, where, when, with whom).

2. **Participant Registration**: Invite participants to freely register for activities, workshops, and volunteer shifts, even for large events. Manage their level of autonomy with numerous options. Customize your homepage with an integrated editor to communicate key information.

3. **Volunteer Management**: Naturally integrate volunteering into your event. Define an acceptable range of volunteer time per day and encourage participation with a bonus system for more demanding tasks.

4. **Registration Tracking**: Create your personalized registration form, collect the necessary information, and analyze the data to make informed decisions, thus optimizing time, energy, and resources.

5. **Communication with Participants**: Facilitate contact with your participants by quickly extracting contact details (phone numbers, emails) to send targeted communications or group SMS.

6. **A Central Hub for Your Entire Event**: Custom fields allow you to add any relevant information in NOÉ, so you can have everything in one place.

NOÉ adapts to various types of events (festivals, residencies, summer universities, etc.) and offers a complete solution for efficient and participatory organization.

### What are the main features? (a non-exhaustive list!)

:::info

NOÉ is designed in a modular way and can be adapted to many types of events: festivals
residencies, summer universities, etc. Why not yours?

:::

Organizers can :

- Enter all elements of the event program (venues, activities, speakers) as well as all associated constraints (availability of people, rooms, etc.),
- Easily rearrange sessions thanks to an agenda view that allows you to move, resize, or clone sessions easily
- Consult and modify in two clicks the registration data of participants
- Create teams of participants
- Register, or unregister participants to sessions, or teams.
- And many other things...

Participants can:

- Register for the event,
- Choose the activities they want to take part in, by searching, filtering, the available sessions,
- Consult their schedule easily, on PC or mobile.
- Export their schedule in PDF
- And much more...
