---
sidebar_position: 3
---

# Nested fields

Nested fields are fields that contain other fields. For example, some NOÉ objects have a
`customFields` field, which stores all the object's custom field values&nbsp;:

```json
{
  "id": "1234567890",
  "name": "My object",
  ...
  "customFields": {
    "field1": "customValue1",
    "field2": "customValue2"
  }
}
```

## Partial modification

Sometimes, you only want to modify one value of a nested field. For example, you may only want to modify
the `field1` value of `customFields`, and leave the rest of the fields unchanged. To do this, you can chain the nested fields together with a dot `.`&nbsp;:

#### Modification request
```json
{
  "id": "1234567890",
  "name": "My modified object",
  ...
  "customFields.field1": "customValue1Modified",
  "customFields.field3": "customValue3New"
}
```

#### Result
```json
{
  "id": "1234567890",
  "name": "My modified object",
  ...
  "customFields": {
    "field1": "customValue1Modified",
    "field2": "customValue2",
    "field3": "customValue3New"
  }
}
```

:::info Delete a field

To delete a field, simply submit the value `null` :

```json
{
  "id": "1234567890",
  "name": "My modified object",
  ...
  "customFields.field1": null
}
```
:::

## Total modification

If, on the other hand, you want to completely replace the `customFields` field, you can do so by submitting an object directly:

#### Modification request
```json
{
  "id": "1234567890",
  "name": "My modified object",
  ...
  "customFields": {
    "field3": "customValue3"
  }
}
```

#### Result
```json
{
  "id": "1234567890",
  "name": "My modified object",
  ...
  "customFields": {
    "field3": "customValue3"
  }
}
```