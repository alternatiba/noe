---
sidebar_position: 2
---

# Deploying the application

We invite you to refer to the [file `CONTRIBUTING.md`](https://gitlab.com/noe-app/noe/-/blob/master/CONTRIBUTING.md#d%C3%A9ployer-lapplication) available in our GitLab repository.

More information will be available on this website soon.
