---
hide_table_of_contents: true
sidebar_label: Je suis encadrant·e
title: ""
---

```mdx-code-block
import {NotionEmbedding} from "@site/src/components/NotionEmbedding";

<NotionEmbedding url={"https://v2-embednotion.com/Je-suis-encadrant-e-3eaae1e27128486aa727cc16317c5f1b?pvs=4"}/>
```