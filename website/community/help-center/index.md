---
sidebar_position: 1
---

# Centre d'aide

:::info Bienvenue sur le centre d’aide de NOÉ !

Ici tu peux chercher les réponses à toutes tes questions.

:::

<br/>

```mdx-code-block
import DocCardList from '@theme/DocCardList';

<DocCardList />
```

---

Tu ne trouves pas la réponse à ta question ?

```mdx-code-block
import DocCardLinks from '@site/src/components/DocCardLinks';

<DocCardLinks items={[
  {href: "/community/contact", title: "Contacte-nous", icon: "💬"},
]} />
```
