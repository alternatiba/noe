---
sidebar_position: 2
---

# Contactez-nous

```mdx-code-block
import DocCardLinks from '@site/src/components/DocCardLinks';
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
```

Merci pour l'intérêt que tu portes à NOÉ ! Qu'est-ce qui t'amène ici ?

```mdx-code-block
<DocCardLinks items={[
  {href: "https://tally.so/r/mR0eap", title: "Utiliser NOÉ pour mon événement", icon: "🆕"},
  {href: "https://tally.so/r/npyoaq", title: "Signaler un bug", icon: "🐛"},
  {href: "https://tally.so/r/3Edl9X", title: "Proposer une nouvelle fonctionnalité", icon: "💡"},
]} />
```

---

Pas trouvé ce que tu cherches ?

```mdx-code-block
<DocCardLinks items={[
 {
    href: `mailto:${useDocusaurusContext().siteConfig.customFields.CONTACT_US_EMAIL}?subject=Contact depuis le site internet de NOÉ`,
    title: `Envoyer un email à ${useDocusaurusContext().siteConfig.customFields.CONTACT_US_EMAIL}`,
    description: "N'envoie un email que pour d'autres demandes non couvertes par les autres liens. On ne peut pas gérer tous les emails de la communauté sinon. Merciiiii !",
    icon: "✉️"
  },
]} />
```
